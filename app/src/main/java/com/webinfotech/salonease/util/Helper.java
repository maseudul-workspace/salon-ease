package com.webinfotech.salonease.util;

import android.content.Context;
import android.util.Log;

import com.google.android.play.core.appupdate.AppUpdateInfo;
import com.google.android.play.core.appupdate.AppUpdateManager;
import com.google.android.play.core.appupdate.AppUpdateManagerFactory;
import com.google.android.play.core.install.model.UpdateAvailability;
import com.google.android.play.core.tasks.Task;
import com.webinfotech.salonease.presentation.ui.bottomsheets.LoginBottomSheet;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import androidx.fragment.app.FragmentManager;

public class Helper {

    public static void showLoginBottomSheet(FragmentManager fragmentManager) {
        LoginBottomSheet loginBottomSheet = new LoginBottomSheet();
        loginBottomSheet.show(fragmentManager, "");
    }

    public static double getHoursDifference(String bookingDateStr) {
        Date bookingDate = null;
        try {
            bookingDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(bookingDateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Date currentDate = new Date();
        long diff = bookingDate.getTime() - currentDate.getTime();
        double seconds = diff / 1000;
        double minutes = seconds / 60;
        double hours = minutes / 60;
        return hours;
    }

    public static double getMinuteDifference(String bookingDateStr) {
        Date bookingDate = null;
        try {
            bookingDate = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(bookingDateStr);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        Date currentDate = new Date();
        long diff = bookingDate.getTime() - currentDate.getTime();
        double seconds = diff / 1000;
        double minutes = seconds / 60;
        return minutes;
    }

}
