package com.webinfotech.salonease.domain.interactors.impl;

import com.webinfotech.salonease.domain.executors.Executor;
import com.webinfotech.salonease.domain.executors.MainThread;
import com.webinfotech.salonease.domain.interactors.FetchWishlistInteractor;
import com.webinfotech.salonease.domain.interactors.base.AbstractInteractor;
import com.webinfotech.salonease.domain.models.WalletResponse;
import com.webinfotech.salonease.domain.models.Wishlist;
import com.webinfotech.salonease.domain.models.WishlistWrapper;
import com.webinfotech.salonease.repository.AppRepositoryImpl;

public class FetchWishlistInteractorImpl extends AbstractInteractor implements FetchWishlistInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String apiToken;

    public FetchWishlistInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String apiToken) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.apiToken = apiToken;
    }

    private void notifyError(String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onFetchWishlistFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(Wishlist[] wishlists){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onFetchWishlistSuccess(wishlists);
            }
        });
    }

    @Override
    public void run() {
        final WishlistWrapper wishlistWrapper = mRepository.fetchWishlist(apiToken);
        if (wishlistWrapper == null) {
            notifyError("Please Check Your Internet Connection", 0);
        } else if (!wishlistWrapper.status) {
            notifyError(wishlistWrapper.message, 0);
        } else {
            postMessage(wishlistWrapper.wishlists);
        }
    }
}
