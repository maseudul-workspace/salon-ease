package com.webinfotech.salonease.domain.interactors;

public interface UpdateFirebaseInteractor {
    interface Callback {
        void onFirebaseTokenUpdateSuccess();
        void onFirebaseTokenUpdateFail();
    }
}
