package com.webinfotech.salonease.domain.interactors;

import com.webinfotech.salonease.domain.models.ThirdCategory;

public interface FetchThirdCategoryInteractor {
    interface Callback {
        void onGettingThirdCategorySuccess(ThirdCategory[] thirdCategories);
        void ongGettingThirdCategoryFail(String errorMsg);
    }
}
