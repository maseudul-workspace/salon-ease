package com.webinfotech.salonease.domain.interactors.impl;

import com.webinfotech.salonease.domain.executors.Executor;
import com.webinfotech.salonease.domain.executors.MainThread;
import com.webinfotech.salonease.domain.interactors.FetchThirdCategoryInteractor;
import com.webinfotech.salonease.domain.interactors.base.AbstractInteractor;
import com.webinfotech.salonease.domain.models.ThirdCategory;
import com.webinfotech.salonease.domain.models.ThirdCategoryWrapper;
import com.webinfotech.salonease.repository.AppRepositoryImpl;

public class FetchThirdCategoryInteractorImpl extends AbstractInteractor implements FetchThirdCategoryInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    int subcategoryId;

    public FetchThirdCategoryInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, int subcategoryId) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.subcategoryId = subcategoryId;
    }

    private void notifyError(String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.ongGettingThirdCategoryFail(errorMsg);
            }
        });
    }

    private void postMessage(ThirdCategory[] thirdCategories){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingThirdCategorySuccess(thirdCategories);
            }
        });
    }

    @Override
    public void run() {
        ThirdCategoryWrapper thirdCategoryWrapper = mRepository.fetchThirdCategoryy(subcategoryId);
        if (thirdCategoryWrapper == null) {
            notifyError("");
        } else if (!thirdCategoryWrapper.status) {
            notifyError(thirdCategoryWrapper.message);
        } else {
            postMessage(thirdCategoryWrapper.thirdCategories);
        }
    }

}
