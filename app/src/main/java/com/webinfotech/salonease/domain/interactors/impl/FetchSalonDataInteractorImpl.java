package com.webinfotech.salonease.domain.interactors.impl;

import com.webinfotech.salonease.domain.executors.Executor;
import com.webinfotech.salonease.domain.executors.MainThread;
import com.webinfotech.salonease.domain.interactors.FetchSalonDataInteractor;
import com.webinfotech.salonease.domain.interactors.base.AbstractInteractor;
import com.webinfotech.salonease.domain.models.SalonData;
import com.webinfotech.salonease.domain.models.SalonDataWrapper;
import com.webinfotech.salonease.repository.AppRepositoryImpl;

public class FetchSalonDataInteractorImpl extends AbstractInteractor implements FetchSalonDataInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    int serviceCityId;
    int categoryId;
    int page;
    int type;
    double latitude;
    double longitude;
    int clientType;
    String priceFrom;
    String priceTo;
    int ac;
    int parking;
    int wifi;
    int music;
    int sortBy;

    public FetchSalonDataInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, int serviceCityId, int categoryId, int page, int type, double latitude, double longitude, int clientType, String priceFrom, String priceTo, int ac, int parking, int wifi, int music, int sortBy) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.serviceCityId = serviceCityId;
        this.categoryId = categoryId;
        this.page = page;
        this.type = type;
        this.latitude = latitude;
        this.longitude = longitude;
        this.clientType = clientType;
        this.priceFrom = priceFrom;
        this.priceTo = priceTo;
        this.ac = ac;
        this.parking = parking;
        this.wifi = wifi;
        this.music = music;
        this.sortBy = sortBy;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingSalonDataFail(errorMsg);
            }
        });
    }

    private void postMessage(SalonData[] salonData, int totalPage){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingSalonDataSuccess(salonData, totalPage);
            }
        });
    }

    @Override
    public void run() {
        final SalonDataWrapper salonDataWrapper = mRepository.fetchSalonData(serviceCityId, categoryId, page, type, latitude, longitude, clientType, priceFrom, priceTo, ac, parking, wifi, music, sortBy);
        if (salonDataWrapper == null) {
            notifyError("Please Check Your Internet Connection");
        } else if (!salonDataWrapper.status) {
            notifyError(salonDataWrapper.message);
        } else {
            postMessage(salonDataWrapper.salonData, salonDataWrapper.totalPage);
        }
    }
}
