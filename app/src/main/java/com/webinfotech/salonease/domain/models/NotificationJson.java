package com.webinfotech.salonease.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NotificationJson {

    @SerializedName("sound")
    @Expose
    public boolean sound;

    @SerializedName("title")
    @Expose
    public String message;

}
