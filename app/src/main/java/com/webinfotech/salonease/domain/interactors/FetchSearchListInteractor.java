package com.webinfotech.salonease.domain.interactors;

import com.webinfotech.salonease.domain.models.SalonUser;

public interface FetchSearchListInteractor {
    interface Callback {
        void onGettingSearchListSuccess(SalonUser[] salonUsers, int totalPage, String searchKey);
        void onGettingSearchListFail(String errorMsg);
    }
}
