package com.webinfotech.salonease.domain.interactors;

import com.webinfotech.salonease.domain.models.SalonUser;

public interface FetchTopFreelancerInteractor {
    interface Callback {
        void onFetchTopFreelancerSuccess(SalonUser[] salonUsers);
        void onFetchTopFreelancerFail(String errorMsg);
    }
}
