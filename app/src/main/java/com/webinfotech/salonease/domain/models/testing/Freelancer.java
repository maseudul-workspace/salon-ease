package com.webinfotech.salonease.domain.models.testing;

public class Freelancer {

    public int id;
    public String name;
    public String image;

    public Freelancer(int id, String name, String image) {
        this.id = id;
        this.name = name;
        this.image = image;
    }

}
