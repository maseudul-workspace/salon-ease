package com.webinfotech.salonease.domain.interactors;

import com.webinfotech.salonease.domain.models.Orders;

public interface FetchOrderHistoryInteractor {
    interface Callback {
        void onGettingOrderHistorySuccess(Orders[] orders);
        void onGettingOrderHistoryFail(String errorMsg, int loginError);
    }
}
