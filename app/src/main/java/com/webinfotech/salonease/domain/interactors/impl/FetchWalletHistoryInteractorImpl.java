package com.webinfotech.salonease.domain.interactors.impl;

import com.webinfotech.salonease.domain.executors.Executor;
import com.webinfotech.salonease.domain.executors.MainThread;
import com.webinfotech.salonease.domain.interactors.FetchWalletHistoryInteractor;
import com.webinfotech.salonease.domain.interactors.base.AbstractInteractor;
import com.webinfotech.salonease.domain.models.WalletResponse;
import com.webinfotech.salonease.domain.models.WalletResponseData;
import com.webinfotech.salonease.repository.AppRepositoryImpl;

public class FetchWalletHistoryInteractorImpl extends AbstractInteractor implements FetchWalletHistoryInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String apiToken;
    int page;

    public FetchWalletHistoryInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String apiToken, int page) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.apiToken = apiToken;
        this.page = page;
    }

    private void notifyError(String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onWalletHistoryFetchFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(WalletResponse walletResponse, int totalPage){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onWalletHistoryFetchSuccess(walletResponse, totalPage);
            }
        });
    }

    @Override
    public void run() {
        final WalletResponse walletResponse = mRepository.fetchWalletHistory(apiToken, page);
        if (walletResponse == null) {
            notifyError("Please Check Your Internet Connection", 0);
        } else if (!walletResponse.status) {
            notifyError(walletResponse.message, walletResponse.login_error);
        } else {
            postMessage(walletResponse, walletResponse.total_pages);
        }
    }
}
