package com.webinfotech.salonease.domain.interactors;

public interface AddAddressInteractor {
    interface Callback {
        void onAddShippingAddressSuccess();
        void onAddShippingAddressFail(String errorMsg, int loginError);
    }
}
