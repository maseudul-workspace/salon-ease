package com.webinfotech.salonease.domain.interactors.impl;

import com.webinfotech.salonease.domain.executors.Executor;
import com.webinfotech.salonease.domain.executors.MainThread;
import com.webinfotech.salonease.domain.interactors.FetchSalonDetailsInteractor;
import com.webinfotech.salonease.domain.interactors.base.AbstractInteractor;
import com.webinfotech.salonease.domain.models.SalonData;
import com.webinfotech.salonease.domain.models.SalonDetailsWrapper;
import com.webinfotech.salonease.domain.models.SalonUser;
import com.webinfotech.salonease.repository.AppRepositoryImpl;

public class FetchSalonDetailsInteractorImpl extends AbstractInteractor implements FetchSalonDetailsInteractor {

    Callback mCallback;
    AppRepositoryImpl mRepository;
    int serviceId;

    public FetchSalonDetailsInteractorImpl(Executor threadExecutor, MainThread mainThread, Callback mCallback, AppRepositoryImpl mRepository, int serviceId) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mRepository = mRepository;
        this.serviceId = serviceId;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingSalonDetailsFail(errorMsg);
            }
        });
    }

    private void postMessage(SalonUser salonUser){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingSalonDetailsSuccess(salonUser);
            }
        });
    }

    @Override
    public void run() {
        final SalonDetailsWrapper salonDetailsWrapper = mRepository.fetchSalonDetails(serviceId);
        if (salonDetailsWrapper == null) {
            notifyError("Please Check Your Internet Connection");
        } else if (!salonDetailsWrapper.status) {
            notifyError(salonDetailsWrapper.message);
        } else {
            postMessage(salonDetailsWrapper.salonUser);
        }
    }
}
