package com.webinfotech.salonease.domain.models;

public class TimeSlots {

    public int id;
    public String timeSlot;
    public boolean isTimeSelected;

    public TimeSlots(int id, String timeSlot, boolean isTimeSelected) {
        this.id = id;
        this.timeSlot = timeSlot;
        this.isTimeSelected = isTimeSelected;
    }

}
