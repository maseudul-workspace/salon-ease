package com.webinfotech.salonease.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AppLoadData {

    @SerializedName("sliders")
    @Expose
    public Slider[] sliders;

    @SerializedName("category")
    @Expose
    public Category[] categories;

    @SerializedName("top_saloon")
    @Expose
    public SalonUser[] topSaloons;

    @SerializedName("top_free_launcer")
    @Expose
    public SalonUser[] topFreeLaunchers;

    @SerializedName("deal_of_the_day")
    @Expose
    public SalonUser[] dealOfTheDays;

    @SerializedName("combo_products")
    @Expose
    public ComboServiceData[] comboServiceData;

}
