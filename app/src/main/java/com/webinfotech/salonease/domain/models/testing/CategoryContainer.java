package com.webinfotech.salonease.domain.models.testing;

import java.util.ArrayList;

public class CategoryContainer {
    public ArrayList<Category> categories;

    public CategoryContainer(ArrayList<Category> categories) {
        this.categories = categories;
    }
}
