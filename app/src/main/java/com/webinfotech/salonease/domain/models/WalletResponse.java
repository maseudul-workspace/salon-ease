package com.webinfotech.salonease.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WalletResponse {

    @SerializedName("status")
    @Expose
    public boolean status;

    @SerializedName("message")
    @Expose
    public String message;

    @SerializedName("error_code")
    @Expose
    public boolean error = false;

    @SerializedName("error_message")
    @Expose
    public ErrorMesage errorMesage;

    @SerializedName("login_error")
    @Expose
    public int login_error = 0;


    @SerializedName("current_page")
    @Expose
    public int current_page;

    @SerializedName("total_pages")
    @Expose
    public int total_pages;

    @SerializedName("has_more_pages")
    @Expose
    public boolean has_more_pages;

    @SerializedName("total_data")
    @Expose
    public int total_data;

    @SerializedName("wallet_amount")
    @Expose
    public double walletAmount;
    @SerializedName("wallet_history")
    @Expose
    public WalletResponseData[] walletResponseData;


}
