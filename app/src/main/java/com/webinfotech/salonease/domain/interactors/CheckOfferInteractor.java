package com.webinfotech.salonease.domain.interactors;

public interface CheckOfferInteractor {
    interface Callback {
        void onCheckOfferSuccess();
        void onCheckOfferFail(String errorMsg, int loginError);
    }
}
