package com.webinfotech.salonease.domain.models;

public class Quantity {
    public int qty;
    public boolean isSelected = false;

    public Quantity(int qty, boolean isSelected) {
        this.qty = qty;
        this.isSelected = isSelected;
    }
}
