package com.webinfotech.salonease.domain.interactors.impl;

import com.webinfotech.salonease.domain.executors.Executor;
import com.webinfotech.salonease.domain.executors.MainThread;
import com.webinfotech.salonease.domain.interactors.FetchCombosInteractor;
import com.webinfotech.salonease.domain.interactors.base.AbstractInteractor;
import com.webinfotech.salonease.domain.models.ComboServiceData;
import com.webinfotech.salonease.domain.models.CombosWrapper;
import com.webinfotech.salonease.domain.models.SalonUser;
import com.webinfotech.salonease.repository.AppRepositoryImpl;

public class FetchCombosInteractorImpl extends AbstractInteractor implements FetchCombosInteractor {

    Callback mCallback;
    AppRepositoryImpl mRepository;
    double latitude;
    double longitude;

    public FetchCombosInteractorImpl(Executor threadExecutor, MainThread mainThread, Callback mCallback, AppRepositoryImpl mRepository, double latitude, double longitude) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mRepository = mRepository;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingComboListFail(errorMsg);
            }
        });
    }

    private void postMessage(ComboServiceData[] comboServiceData){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingComboListSuccess(comboServiceData);
            }
        });
    }

    @Override
    public void run() {
        final CombosWrapper combosWrapper = mRepository.fetchCombos(latitude, longitude);
        if (combosWrapper == null) {
            notifyError("Please Check Your Internet Connection");
        } else if (!combosWrapper.status) {
            notifyError(combosWrapper.message);
        } else {
            postMessage(combosWrapper.comboServiceData);
        }
    }
}
