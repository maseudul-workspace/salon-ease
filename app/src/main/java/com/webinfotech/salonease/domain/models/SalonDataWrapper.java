package com.webinfotech.salonease.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SalonDataWrapper {

    @SerializedName("status")
    @Expose
    public boolean status;

    @SerializedName("message")
    @Expose
    public String message;

    @SerializedName("tatal_page")
    @Expose
    public int totalPage;

    @SerializedName("data")
    @Expose
    public SalonData[] salonData;

}
