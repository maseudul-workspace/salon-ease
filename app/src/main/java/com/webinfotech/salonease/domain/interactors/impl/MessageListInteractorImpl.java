package com.webinfotech.salonease.domain.interactors.impl;

import com.webinfotech.salonease.domain.executors.Executor;
import com.webinfotech.salonease.domain.executors.MainThread;
import com.webinfotech.salonease.domain.interactors.MessageListInteractor;
import com.webinfotech.salonease.domain.interactors.base.AbstractInteractor;
import com.webinfotech.salonease.domain.models.MessageData;
import com.webinfotech.salonease.domain.models.MessageWrapper;
import com.webinfotech.salonease.repository.AppRepositoryImpl;

public class MessageListInteractorImpl extends AbstractInteractor implements MessageListInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String apiToken;
    int page;

    public MessageListInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String apiToken, int page) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.apiToken = apiToken;
        this.page = page;
    }

    private void notifyError(String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onMessageListFetchFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(MessageData[] messageData, int totalPage){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onMessageListFetchSuccess(messageData, totalPage);
            }
        });
    }

    @Override
    public void run() {
        final MessageWrapper messageWrapper = mRepository.fetchMessagelist(apiToken, page);
        if (messageWrapper == null) {
            notifyError("Please Check Your Internet Connection", 0);
        } else if (!messageWrapper.status) {
            notifyError(messageWrapper.message, messageWrapper.login_error);
        } else {
            postMessage(messageWrapper.messageData, messageWrapper.total_page);
        }
    }
}
