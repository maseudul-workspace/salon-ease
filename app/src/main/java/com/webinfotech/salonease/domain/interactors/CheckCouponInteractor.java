package com.webinfotech.salonease.domain.interactors;

public interface CheckCouponInteractor {
    interface Callback {
        void onCouponCheckSuccess();
        void onCouponCheckFail(String errorMsg, int loginError);
    }
}
