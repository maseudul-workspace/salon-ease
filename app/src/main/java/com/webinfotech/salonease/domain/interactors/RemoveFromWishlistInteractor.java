package com.webinfotech.salonease.domain.interactors;

public interface RemoveFromWishlistInteractor {
    interface Callback {
        void onRemoveFromWishlistSuccess();
        void onRemoveFromWishlistFail(String errorMsg, int loginError);
    }
}
