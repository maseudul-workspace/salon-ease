package com.webinfotech.salonease.domain.interactors;

import com.webinfotech.salonease.domain.models.SalonData;

public interface FetchSalonDataInteractor {
    interface Callback {
        void onGettingSalonDataSuccess(SalonData[] salonData, int totalPage);
        void onGettingSalonDataFail(String errorMsg);
    }
}
