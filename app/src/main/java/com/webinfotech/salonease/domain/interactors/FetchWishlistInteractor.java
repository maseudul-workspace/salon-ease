package com.webinfotech.salonease.domain.interactors;

import com.webinfotech.salonease.domain.models.Wishlist;

public interface FetchWishlistInteractor {
    interface Callback {
        void onFetchWishlistSuccess(Wishlist[] wishlists);
        void onFetchWishlistFail(String errorMsg, int loginError);
    }
}
