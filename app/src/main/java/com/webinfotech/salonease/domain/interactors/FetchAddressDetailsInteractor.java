package com.webinfotech.salonease.domain.interactors;

import com.webinfotech.salonease.domain.models.ShippingAddress;

public interface FetchAddressDetailsInteractor {
    interface Callback {
        void onGettingAddressDetailsSuccess(ShippingAddress shippingAddress);
        void onGettingAddressDetailsFail(String errorMsg, int loginError);
    }
}
