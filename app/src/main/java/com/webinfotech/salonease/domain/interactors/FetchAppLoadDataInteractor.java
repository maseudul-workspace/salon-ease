package com.webinfotech.salonease.domain.interactors;

import com.webinfotech.salonease.domain.models.AppLoadData;

public interface FetchAppLoadDataInteractor {
    interface Callback {
        void onAppLoadDataFetchSuccess(AppLoadData appLoadData);
        void onAppLoadDataFetchFail(String errorMsg);
    }
}
