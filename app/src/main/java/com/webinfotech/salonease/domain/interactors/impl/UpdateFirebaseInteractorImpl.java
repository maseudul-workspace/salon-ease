package com.webinfotech.salonease.domain.interactors.impl;

import com.webinfotech.salonease.domain.executors.Executor;
import com.webinfotech.salonease.domain.executors.MainThread;
import com.webinfotech.salonease.domain.interactors.UpdateFirebaseInteractor;
import com.webinfotech.salonease.domain.interactors.base.AbstractInteractor;
import com.webinfotech.salonease.domain.models.CommonResponse;
import com.webinfotech.salonease.repository.AppRepositoryImpl;

public class UpdateFirebaseInteractorImpl extends AbstractInteractor implements UpdateFirebaseInteractor {

    Callback mCallback;
    AppRepositoryImpl mRepository;
    String apiToken;
    int userId;
    String token;

    public UpdateFirebaseInteractorImpl(Executor threadExecutor, MainThread mainThread, Callback mCalback, AppRepositoryImpl mRepository, String apiToken, int userId, String token) {
        super(threadExecutor, mainThread);
        this.mCallback = mCalback;
        this.mRepository = mRepository;
        this.apiToken = apiToken;
        this.userId = userId;
        this.token = token;
    }

    private void notifyError() {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onFirebaseTokenUpdateFail();
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onFirebaseTokenUpdateSuccess();
            }
        });
    }

    @Override
    public void run() {
        final CommonResponse commonResponse = mRepository.updateFirebaseToken(apiToken, userId, token);
        if (commonResponse == null) {
            notifyError();
        } else if (!commonResponse.status) {
            notifyError();
        } else {
            postMessage();
        }
    }
}
