package com.webinfotech.salonease.domain.interactors.impl;

import com.webinfotech.salonease.domain.executors.Executor;
import com.webinfotech.salonease.domain.executors.MainThread;
import com.webinfotech.salonease.domain.interactors.FetchAppLoadDataInteractor;
import com.webinfotech.salonease.domain.interactors.base.AbstractInteractor;
import com.webinfotech.salonease.domain.models.AppLoadData;
import com.webinfotech.salonease.domain.models.AppLoadDataWrapper;
import com.webinfotech.salonease.repository.AppRepositoryImpl;

public class FetchAppLoadDataInteractorImpl extends AbstractInteractor implements FetchAppLoadDataInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    double latitude;
    double longitude;

    public FetchAppLoadDataInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, double latitude, double longitude) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    private void notifyError(String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onAppLoadDataFetchFail(errorMsg);
            }
        });
    }

    private void postMessage(AppLoadData appLoadData){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onAppLoadDataFetchSuccess(appLoadData);
            }
        });
    }

    @Override
    public void run() {
        final AppLoadDataWrapper appLoadDataWrapper = mRepository.fetchAppLoadData(latitude, longitude);
        if (appLoadDataWrapper == null) {
            notifyError("Please Check Your Internet Connection");
        } else if (!appLoadDataWrapper.status) {
            notifyError(appLoadDataWrapper.message);
        } else {
            postMessage(appLoadDataWrapper.appLoadData);
        }
    }
}
