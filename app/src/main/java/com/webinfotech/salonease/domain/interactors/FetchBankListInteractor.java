package com.webinfotech.salonease.domain.interactors;

import com.webinfotech.salonease.domain.models.BankList;

public interface FetchBankListInteractor {
    interface Callback {
        void onGettingBankListSuccess(BankList[] bankLists);
        void onGettingBankListFail(String errorMsg, int loginError);
    }
}
