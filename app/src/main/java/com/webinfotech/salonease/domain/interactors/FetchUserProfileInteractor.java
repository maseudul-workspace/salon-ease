package com.webinfotech.salonease.domain.interactors;

import com.webinfotech.salonease.domain.models.UserInfo;

public interface FetchUserProfileInteractor {
    interface Callback {
        void onFetchUserProfileSuccess(UserInfo userInfo);
        void onFetchUserProfileFail(String errorMsg, int loginError);
    }
}
