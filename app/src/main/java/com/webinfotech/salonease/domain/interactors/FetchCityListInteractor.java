package com.webinfotech.salonease.domain.interactors;

import com.webinfotech.salonease.domain.models.City;

public interface FetchCityListInteractor {
    interface Callback {
        void onGettingCityListSuccess(City[] cities);
        void onGettingCityListFail(String errorMsg);
    }
}
