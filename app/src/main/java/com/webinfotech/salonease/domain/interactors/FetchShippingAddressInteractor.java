package com.webinfotech.salonease.domain.interactors;


import com.webinfotech.salonease.domain.models.ShippingAddress;

public interface FetchShippingAddressInteractor {
    interface Callback {
        void onGettingAddressListSucces(ShippingAddress[] shippingAddresses);
        void onGettingAddressListFail(String errorMsg, int loginError);
    }
}
