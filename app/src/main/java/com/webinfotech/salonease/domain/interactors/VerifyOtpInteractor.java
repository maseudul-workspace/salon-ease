package com.webinfotech.salonease.domain.interactors;

import com.webinfotech.salonease.domain.models.UserInfo;

public interface VerifyOtpInteractor {
    interface Callback {
        void onVerifyOtpSuccess(UserInfo userInfo);
        void onVerifyOtpFail(String errorMsg);
    }
}
