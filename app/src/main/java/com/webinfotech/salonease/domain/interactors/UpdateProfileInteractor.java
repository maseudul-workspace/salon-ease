package com.webinfotech.salonease.domain.interactors;

public interface UpdateProfileInteractor {
    interface Callback {
        void onProfileUpdateSuccess();
        void onProfileUpdateFail(String errorMsg, int loginError);
    }
}
