package com.webinfotech.salonease.domain.interactors.impl;

import com.webinfotech.salonease.domain.executors.Executor;
import com.webinfotech.salonease.domain.executors.MainThread;
import com.webinfotech.salonease.domain.interactors.FetchOffersInteractor;
import com.webinfotech.salonease.domain.interactors.base.AbstractInteractor;
import com.webinfotech.salonease.domain.models.City;
import com.webinfotech.salonease.domain.models.OffersData;
import com.webinfotech.salonease.domain.models.OffersDataWrapper;
import com.webinfotech.salonease.repository.AppRepositoryImpl;

public class FetchOffersInteractorImpl extends AbstractInteractor implements FetchOffersInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String apiToken;

    public FetchOffersInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String apiToken) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.apiToken = apiToken;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onOffersFetchFail(errorMsg);
            }
        });
    }

    private void postMessage(OffersData offersData){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onOffersFetchSuccess(offersData);
            }
        });
    }

    @Override
    public void run() {
        final OffersDataWrapper offersDataWrapper = mRepository.fetchOffers(apiToken);
        if (offersDataWrapper == null) {
            notifyError("");
        } else if (!offersDataWrapper.status) {
            notifyError("");
        } else {
            postMessage(offersDataWrapper.offersData);
        }
    }
}
