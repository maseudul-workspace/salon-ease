package com.webinfotech.salonease.domain.interactors;

import com.webinfotech.salonease.domain.models.SalonData;
import com.webinfotech.salonease.domain.models.SalonUser;

public interface FetchSalonDetailsInteractor {
    interface Callback {
        void onGettingSalonDetailsSuccess(SalonUser salonUser);
        void onGettingSalonDetailsFail(String errorMsg);
    }
}
