package com.webinfotech.salonease.domain.interactors.impl;

import com.webinfotech.salonease.domain.executors.Executor;
import com.webinfotech.salonease.domain.executors.MainThread;
import com.webinfotech.salonease.domain.interactors.CheckLoginInteractor;
import com.webinfotech.salonease.domain.interactors.base.AbstractInteractor;
import com.webinfotech.salonease.domain.models.UserInfo;
import com.webinfotech.salonease.domain.models.UserInfoWrapper;
import com.webinfotech.salonease.repository.AppRepositoryImpl;

public class CheckLoginInteractorImpl extends AbstractInteractor implements CheckLoginInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String email;
    String password;

    public CheckLoginInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String email, String password) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.email = email;
        this.password = password;
    }

    private void notifyError(String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onLoginFail(errorMsg);
            }
        });
    }

    private void postMessage(UserInfo userInfo){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onLoginSuccess(userInfo);
            }
        });
    }

    @Override
    public void run() {
        UserInfoWrapper userInfoWrapper = mRepository.checkLogin(email, password);
        if (userInfoWrapper == null) {
            notifyError("Something went wrong");
        } else if (!userInfoWrapper.status) {
            if (userInfoWrapper.error) {
                if (userInfoWrapper.errorMesage.email != null) {
                    notifyError(userInfoWrapper.errorMesage.email[0]);
                } else if (userInfoWrapper.errorMesage.mobile != null) {
                    notifyError(userInfoWrapper.errorMesage.mobile[0]);
                } else {
                    notifyError(userInfoWrapper.message);
                }
            } else {
                notifyError(userInfoWrapper.message);
            }
        } else {
            postMessage(userInfoWrapper.userInfo);
        }
    }
}
