package com.webinfotech.salonease.domain.interactors.impl;

import com.webinfotech.salonease.domain.executors.Executor;
import com.webinfotech.salonease.domain.executors.MainThread;
import com.webinfotech.salonease.domain.interactors.SendOtpInteractor;
import com.webinfotech.salonease.domain.interactors.base.AbstractInteractor;
import com.webinfotech.salonease.domain.models.OtpResponse;
import com.webinfotech.salonease.repository.AppRepositoryImpl;

public class SendOtpInteractorImpl extends AbstractInteractor implements SendOtpInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String mobile;

    public SendOtpInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String mobile) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.mobile = mobile;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onOtpSendFail(errorMsg);
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onOtpSendSuccess();
            }
        });
    }

    @Override
    public void run() {
        OtpResponse otpResponse = mRepository.sendOtp(mobile);
        if (otpResponse == null) {
            notifyError("Please Check Your Internet Connection");
        } else if (!otpResponse.status) {
            notifyError(otpResponse.message);
        } else {
            postMessage();
        }
    }
}
