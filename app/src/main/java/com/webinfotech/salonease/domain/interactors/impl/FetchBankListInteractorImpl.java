package com.webinfotech.salonease.domain.interactors.impl;

import com.webinfotech.salonease.domain.executors.Executor;
import com.webinfotech.salonease.domain.executors.MainThread;
import com.webinfotech.salonease.domain.interactors.FetchBankListInteractor;
import com.webinfotech.salonease.domain.interactors.base.AbstractInteractor;
import com.webinfotech.salonease.domain.models.BankList;
import com.webinfotech.salonease.domain.models.BankListWrapper;
import com.webinfotech.salonease.repository.AppRepositoryImpl;

public class FetchBankListInteractorImpl extends AbstractInteractor implements FetchBankListInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    int userId;
    String apiToken;

    public FetchBankListInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, int userId, String apiToken) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.userId = userId;
        this.apiToken = apiToken;
    }

    private void notifyError(String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingBankListFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(BankList[] bankLists){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingBankListSuccess(bankLists);
            }
        });
    }

    @Override
    public void run() {
        final BankListWrapper bankListWrapper = mRepository.fetchBankList(apiToken, userId);
        if (bankListWrapper == null) {
            notifyError("PLease Check Your Internet Connection", bankListWrapper.login_error);
        } else if (!bankListWrapper.status) {
            notifyError(bankListWrapper.message, bankListWrapper.login_error);
        } else {
            postMessage(bankListWrapper.bankLists);
        }
    }
}
