package com.webinfotech.salonease.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Reviews {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("customer_id")
    @Expose
    public int customerId;

    @SerializedName("client_id")
    @Expose
    public int clientId;

    @SerializedName("comment")
    @Expose
    public String comment;

    @SerializedName("customer_name")
    @Expose
    public String customerName;

    @SerializedName("gender")
    @Expose
    public String gender;

    @SerializedName("rating")
    @Expose
    public int rating;

}
