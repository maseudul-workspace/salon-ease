package com.webinfotech.salonease.domain.interactors;

import com.webinfotech.salonease.domain.models.UserInfo;

public interface RegisterUserInteractor {
    interface Callback {
        void onRegisterUserSuccess(UserInfo userInfo);
        void onRegisterUserFail(String errorMsg);
    }
}
