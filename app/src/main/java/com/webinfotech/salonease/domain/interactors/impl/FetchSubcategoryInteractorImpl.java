package com.webinfotech.salonease.domain.interactors.impl;

import com.webinfotech.salonease.domain.executors.Executor;
import com.webinfotech.salonease.domain.executors.MainThread;
import com.webinfotech.salonease.domain.interactors.FetchSubcategoryInteractor;
import com.webinfotech.salonease.domain.interactors.base.AbstractInteractor;
import com.webinfotech.salonease.domain.models.Subcategory;
import com.webinfotech.salonease.domain.models.SubcategoryWrapper;
import com.webinfotech.salonease.repository.AppRepositoryImpl;

public class FetchSubcategoryInteractorImpl extends AbstractInteractor implements FetchSubcategoryInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    int categoryId;

    public FetchSubcategoryInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, int categoryId) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.categoryId = categoryId;
    }

    private void notifyError(String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.ongGettingSubcategoryFail(errorMsg);
            }
        });
    }

    private void postMessage(Subcategory[] subcategories){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingSubcategorySuccess(subcategories);
            }
        });
    }

    @Override
    public void run() {
        SubcategoryWrapper subcategoryWrapper = mRepository.fetchSubcategory(categoryId);
        if (subcategoryWrapper == null) {
            notifyError("");
        } else if (!subcategoryWrapper.status) {
            notifyError(subcategoryWrapper.message);
        } else {
            postMessage(subcategoryWrapper.subcategories);
        }
    }
}
