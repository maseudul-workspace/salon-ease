package com.webinfotech.salonease.domain.interactors.impl;

import com.webinfotech.salonease.domain.executors.Executor;
import com.webinfotech.salonease.domain.executors.MainThread;
import com.webinfotech.salonease.domain.interactors.UpdateProfileInteractor;
import com.webinfotech.salonease.domain.interactors.base.AbstractInteractor;
import com.webinfotech.salonease.domain.models.CommonResponse;
import com.webinfotech.salonease.repository.AppRepositoryImpl;

public class UpdateProfileInteractorImpl extends AbstractInteractor implements UpdateProfileInteractor {

    Callback mCallback;
    AppRepositoryImpl mRepository;
    String authorization;
    int userId;
    String name;
    String mobile;
    String state;
    String city;
    String gender;
    String address;
    String pin;
    String email;

    public UpdateProfileInteractorImpl(Executor threadExecutor, MainThread mainThread, Callback mCallback, AppRepositoryImpl mRepository, String authorization, int userId, String name, String mobile, String state, String city, String gender, String address, String pin, String email) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mRepository = mRepository;
        this.authorization = authorization;
        this.userId = userId;
        this.name = name;
        this.mobile = mobile;
        this.state = state;
        this.city = city;
        this.gender = gender;
        this.address = address;
        this.pin = pin;
        this.email = email;
    }

    private void notifyError(String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onProfileUpdateFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onProfileUpdateSuccess();
            }
        });
    }

    @Override
    public void run() {
        CommonResponse commonResponse = mRepository.updateProfile(authorization, userId, name, mobile, state, city, gender, address, pin, email);
        if (commonResponse == null) {
            notifyError("Please check your internet connection", 0);
        } else if (!commonResponse.status) {
            notifyError(commonResponse.message, commonResponse.login_error);
        } else {
            postMessage();
        }
    }
}
