package com.webinfotech.salonease.domain.models;

import java.util.ArrayList;

public class SalonUserContainer {

    public ArrayList<SalonUser> salonUsers;

    public SalonUserContainer(ArrayList<SalonUser> salonUsers) {
        this.salonUsers = salonUsers;
    }
}
