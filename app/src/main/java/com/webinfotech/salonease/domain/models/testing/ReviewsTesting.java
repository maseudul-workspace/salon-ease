package com.webinfotech.salonease.domain.models.testing;

public class ReviewsTesting {

    public String userImage;
    public String userName;
    public String userReview;
    public int rating;

    public ReviewsTesting(String userImage, String userName, String userReview, int rating) {
        this.userImage = userImage;
        this.userName = userName;
        this.userReview = userReview;
        this.rating = rating;
    }
}
