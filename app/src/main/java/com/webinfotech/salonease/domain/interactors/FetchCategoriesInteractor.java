package com.webinfotech.salonease.domain.interactors;

import com.webinfotech.salonease.domain.models.Category;

public interface FetchCategoriesInteractor {
    interface Callback {
        void onGettingCategoriesSuccess(Category[] categories);
        void onGettingCategoriesFail();
    }
}
