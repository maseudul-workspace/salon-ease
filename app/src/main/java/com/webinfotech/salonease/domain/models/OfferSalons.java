package com.webinfotech.salonease.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OfferSalons {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("offer_id")
    @Expose
    public int offerId;

    @SerializedName("client_id")
    @Expose
    public int client_id;

}
