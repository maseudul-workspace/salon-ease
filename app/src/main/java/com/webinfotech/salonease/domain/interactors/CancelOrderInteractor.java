package com.webinfotech.salonease.domain.interactors;

public interface CancelOrderInteractor {
    interface Callback {
        void onOrderCancelSuccess();
        void onCancelOrderFailed(String errorMsg, int loginError);
    }
}
