package com.webinfotech.salonease.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SalonUser {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("email")
    @Expose
    public String email;

    @SerializedName("work_experience")
    @Expose
    public String workExperience;

    @SerializedName("state")
    @Expose
    public String state;

    @SerializedName("city")
    @Expose
    public String city;

    @SerializedName("address")
    @Expose
    public String address;

    @SerializedName("image")
    @Expose
    public String image;

    @SerializedName("opening_time")
    @Expose
    public String openingTime;

    @SerializedName("closing_time")
    @Expose
    public String closing_time;

    @SerializedName("latitude")
    @Expose
    public double latitude;

    @SerializedName("longitude")
    @Expose
    public double longitude;

    @SerializedName("client_type")
    @Expose
    public int clientType;

    @SerializedName("avarage_rating")
    @Expose
    public double rating;

    @SerializedName("description")
    @Expose
    public String description;

    @SerializedName("distance")
    @Expose
    public double distance;

    @SerializedName("ac")
    @Expose
    public int ac;

    @SerializedName("parking")
    @Expose
    public int parking;

    @SerializedName("wifi")
    @Expose
    public int wifi;

    @SerializedName("music")
    @Expose
    public int music;

    @SerializedName("images")
    @Expose
    public SalonImages[] images;

    @SerializedName("reviews")
    @Expose
    public Reviews[] reviews;

    @SerializedName("services")
    @Expose
    public Service[] services;

    @SerializedName("deals")
    @Expose
    public Service[] deals;

    @SerializedName("comboJobs")
    @Expose
    public ComboServiceData[] comboServiceData;

}