package com.webinfotech.salonease.domain.interactors;

import com.webinfotech.salonease.domain.models.PaymentDataMain;

public interface PlaceOrderInteractor {
    interface Callback {
        void onPlaceOrderSuccess(PaymentDataMain paymentDataMain);
        void onPlaceOrderFail(String errorMsg, int loginError);
    }
}
