package com.webinfotech.salonease.domain.interactors;

public interface AddToWishlistInteractor {
    interface Callback {
        void onAddToWishlistSuccess();
        void onAddToWishlistFail(String errorMsg, int loginError);
    }
}
