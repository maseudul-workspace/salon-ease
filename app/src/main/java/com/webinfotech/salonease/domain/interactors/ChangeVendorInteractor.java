package com.webinfotech.salonease.domain.interactors;

public interface ChangeVendorInteractor {
    interface Callback {
        void onVendorChangeSuccess();
        void onVendorChangeFail(String errorMsg, int loginError);
    }
}
