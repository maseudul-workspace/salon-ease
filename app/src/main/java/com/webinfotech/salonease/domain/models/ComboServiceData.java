package com.webinfotech.salonease.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ComboServiceData {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("client_id")
    @Expose
    public int clientId;

    @SerializedName("category_id")
    @Expose
    public int categoryId;

    @SerializedName("category_name")
    @Expose
    public String categoryName;

    @SerializedName("combo_name")
    @Expose
    public String comboName;

    @SerializedName("price")
    @Expose
    public double price;

    @SerializedName("mrp")
    @Expose
    public double mrp;

    @SerializedName("status")
    @Expose
    public int status;

    @SerializedName("client_name")
    @Expose
    public String clientName;

    @SerializedName("distance")
    @Expose
    public String distance;

    @SerializedName("client_address")
    @Expose
    public String clientAddress;

    @SerializedName("client_mobile")
    @Expose
    public String clientMobile;

    @SerializedName("client_image")
    @Expose
    public String client_image;

    @SerializedName("client_type")
    @Expose
    public int clientType;

    @SerializedName("combo_services")
    @Expose
    public ComboService[] comboServices;

    public boolean isComboSelected = false;

}
