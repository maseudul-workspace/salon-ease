package com.webinfotech.salonease.domain.interactors;

public interface SubmitReviewInteractor {
    interface Callback {
        void onReviewSubmitSuccess();
        void onReviewSubmitFail(String errorMsg, int loginError);
    }
}
