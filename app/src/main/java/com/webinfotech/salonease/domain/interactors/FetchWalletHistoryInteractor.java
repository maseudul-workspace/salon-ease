package com.webinfotech.salonease.domain.interactors;


import com.webinfotech.salonease.domain.models.WalletResponse;

public interface FetchWalletHistoryInteractor {
    interface Callback {
        void onWalletHistoryFetchSuccess(WalletResponse walletResponse, int totalPage);
        void onWalletHistoryFetchFail(String errorMsg, int loginError);
    }
}
