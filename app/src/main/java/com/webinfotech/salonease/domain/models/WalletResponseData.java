package com.webinfotech.salonease.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class WalletResponseData {
    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("wallet_id")
    @Expose
    public int wallet_id;

    @SerializedName("transaction_type")
    @Expose
    public String transaction_type;

    @SerializedName("amount")
    @Expose
    public String amount;

    @SerializedName("total_amount")
    @Expose
    public String payment_status;

    @SerializedName("comment")
    @Expose
    public String comment;

    @SerializedName("created_at")
    @Expose
    public String created_at;

    @SerializedName("updated_at")
    @Expose
    public String updated_at;
}
