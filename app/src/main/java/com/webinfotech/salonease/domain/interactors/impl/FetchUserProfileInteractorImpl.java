package com.webinfotech.salonease.domain.interactors.impl;

import com.webinfotech.salonease.domain.executors.Executor;
import com.webinfotech.salonease.domain.executors.MainThread;
import com.webinfotech.salonease.domain.interactors.FetchUserProfileInteractor;
import com.webinfotech.salonease.domain.interactors.base.AbstractInteractor;
import com.webinfotech.salonease.domain.models.UserInfo;
import com.webinfotech.salonease.domain.models.UserInfoWrapper;
import com.webinfotech.salonease.repository.AppRepositoryImpl;

public class FetchUserProfileInteractorImpl extends AbstractInteractor implements FetchUserProfileInteractor {

    Callback mCallback;
    AppRepositoryImpl mRepository;
    String apiToken;
    int userId;

    public FetchUserProfileInteractorImpl(Executor threadExecutor, MainThread mainThread, Callback mCallback, AppRepositoryImpl mReposjtory, String apiToken, int userId) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mRepository = mReposjtory;
        this.apiToken = apiToken;
        this.userId = userId;
    }

    private void notifyError(String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onFetchUserProfileFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(UserInfo userInfo){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onFetchUserProfileSuccess(userInfo);
            }
        });
    }

    @Override
    public void run() {
        final UserInfoWrapper userInfoWrapper = mRepository.fetchUserProfile(apiToken, userId);
        if (userInfoWrapper == null) {
            notifyError("Please Check Your Internet Connection", 0);
        } else if (!userInfoWrapper.status) {
            notifyError(userInfoWrapper.message, userInfoWrapper.login_error);
        } else {
            postMessage(userInfoWrapper.userInfo);
        }
    }
}
