package com.webinfotech.salonease.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AdminCommission {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("from_amount")
    @Expose
    public double fromAmount;

    @SerializedName("to_amount")
    @Expose
    public double toAmount;

    @SerializedName("charge_amount")
    @Expose
    public double chargeAmount;

}
