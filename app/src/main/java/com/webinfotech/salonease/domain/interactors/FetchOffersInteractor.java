package com.webinfotech.salonease.domain.interactors;

import com.webinfotech.salonease.domain.models.OffersData;

public interface FetchOffersInteractor {
    interface Callback {
        void onOffersFetchSuccess(OffersData offersData);
        void onOffersFetchFail(String errorMsg);
    }
}
