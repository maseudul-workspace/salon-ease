package com.webinfotech.salonease.domain.interactors.impl;

import com.webinfotech.salonease.domain.executors.Executor;
import com.webinfotech.salonease.domain.executors.MainThread;
import com.webinfotech.salonease.domain.interactors.PlaceOrderInteractor;
import com.webinfotech.salonease.domain.interactors.base.AbstractInteractor;
import com.webinfotech.salonease.domain.models.OrderPlaceResponse;
import com.webinfotech.salonease.domain.models.PaymentDataMain;
import com.webinfotech.salonease.repository.AppRepositoryImpl;

import java.util.ArrayList;

public class PlaceOrderInteractorImpl extends AbstractInteractor implements PlaceOrderInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String authorization;
    int vendorId;
    ArrayList<Integer> serviceIds;
    ArrayList<Integer> quantities;
    String serviceTime;
    String addressId;
    String offerId;
    String offerJobId;
    String couponId;
    int isWallet;

    public PlaceOrderInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String authorization, int vendorId, ArrayList<Integer> serviceIds, ArrayList<Integer> quantities, String serviceTime, String addressId, String offerId, String offerJobId, String couponId, int isWallet) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.authorization = authorization;
        this.vendorId = vendorId;
        this.serviceIds = serviceIds;
        this.quantities = quantities;
        this.serviceTime = serviceTime;
        this.addressId = addressId;
        this.offerId = offerId;
        this.offerJobId = offerJobId;
        this.couponId = couponId;
        this.isWallet = isWallet;
    }

    private void notifyError(String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onPlaceOrderFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(PaymentDataMain paymentDataMain){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onPlaceOrderSuccess(paymentDataMain);
            }
        });
    }

    @Override
    public void run() {
        final OrderPlaceResponse orderPlaceResponse = mRepository.placeOrder(authorization, vendorId, serviceIds, quantities, serviceTime, addressId, offerId, offerJobId, couponId, isWallet);
        if (orderPlaceResponse == null) {
            notifyError("Please Internet Connection", 0);
        } else if (!orderPlaceResponse.status) {
            notifyError(orderPlaceResponse.message, orderPlaceResponse.login_error);
        } else {
            postMessage(orderPlaceResponse.paymentDataMain);
        }
    }
}
