package com.webinfotech.salonease.domain.interactors;

import com.webinfotech.salonease.domain.models.ComboServiceData;

public interface FetchCombosInteractor {
    interface Callback {
        void onGettingComboListSuccess(ComboServiceData[] comboServiceData);
        void onGettingComboListFail(String errorMsg);
    }
}
