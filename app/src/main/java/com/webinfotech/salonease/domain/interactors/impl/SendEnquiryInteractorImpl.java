package com.webinfotech.salonease.domain.interactors.impl;

import com.webinfotech.salonease.domain.executors.Executor;
import com.webinfotech.salonease.domain.executors.MainThread;
import com.webinfotech.salonease.domain.interactors.SendEnquiryInteractor;
import com.webinfotech.salonease.domain.interactors.base.AbstractInteractor;
import com.webinfotech.salonease.domain.models.CommonResponse;
import com.webinfotech.salonease.repository.AppRepositoryImpl;

public class SendEnquiryInteractorImpl extends AbstractInteractor implements SendEnquiryInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String apiToken;
    int categoryId;
    int subcategoryId;
    int thirdcategoryId;
    String name;
    String mobile;
    String message;
    String booking_date;

    public SendEnquiryInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String apiToken, int categoryId, int subcategoryId, int thirdcategoryId, String name, String mobile, String message, String booking_date) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.apiToken = apiToken;
        this.categoryId = categoryId;
        this.subcategoryId = subcategoryId;
        this.thirdcategoryId = thirdcategoryId;
        this.name = name;
        this.mobile = mobile;
        this.message = message;
        this.booking_date = booking_date;
    }

    private void notifyError(String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onSendFeedbackFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onSendFeedbackSuccess();
            }
        });
    }

    @Override
    public void run() {
        final CommonResponse commonResponse = mRepository.sendFeedback(apiToken, categoryId, subcategoryId, thirdcategoryId, name, mobile, message, booking_date);
        if (commonResponse == null) {
            notifyError("Slow Internet Connection", commonResponse.login_error);
        } else if (!commonResponse.status) {
            notifyError(commonResponse.message, commonResponse.login_error);
        } else {
            postMessage();
        }
    }
}
