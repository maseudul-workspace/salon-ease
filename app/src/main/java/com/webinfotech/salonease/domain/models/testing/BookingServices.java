package com.webinfotech.salonease.domain.models.testing;

public class BookingServices {

    public int serviceId;
    public int serviceFor;
    public int price;
    public String name;

    public BookingServices(int serviceId, int serviceFor, int price, String name) {
        this.serviceId = serviceId;
        this.serviceFor = serviceFor;
        this.price = price;
        this.name = name;
    }
}
