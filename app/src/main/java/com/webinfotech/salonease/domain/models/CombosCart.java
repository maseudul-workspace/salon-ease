package com.webinfotech.salonease.domain.models;

public class CombosCart {

    public int comboId;
    public String comboName;
    public double price;
    public int qty;
    public ComboService[] comboServices;

    public CombosCart(int comboId, String comboName, double price, int qty, ComboService[] comboServices) {
        this.comboId = comboId;
        this.comboName = comboName;
        this.price = price;
        this.qty = qty;
        this.comboServices = comboServices;
    }
}
