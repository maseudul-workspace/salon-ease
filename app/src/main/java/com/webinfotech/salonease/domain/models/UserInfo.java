package com.webinfotech.salonease.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Raj on 12-02-2019.
 */

public class UserInfo {

    @SerializedName("id")
    @Expose
    public int userId;

    @SerializedName("api_token")
    @Expose
    public String apiToken;

    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("email")
    @Expose
    public String email;

    @SerializedName("mobile")
    @Expose
    public String mobile;

    @SerializedName("state")
    @Expose
    public String state;

    @SerializedName("dob")
    @Expose
    public String dob;

    @SerializedName("gender")
    @Expose
    public String gender;

    @SerializedName("city")
    @Expose
    public String city;

    @SerializedName("address")
    @Expose
    public String address;

    @SerializedName("pin")
    @Expose
    public String pin;

    @SerializedName("latitude")
    @Expose
    public double latitude;

    @SerializedName("longitude")
    @Expose
    public double longitude;

    @SerializedName("is_registered")
    @Expose
    public int isRegistered;

}
