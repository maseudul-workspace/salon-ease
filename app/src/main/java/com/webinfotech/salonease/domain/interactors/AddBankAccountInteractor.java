package com.webinfotech.salonease.domain.interactors;

public interface AddBankAccountInteractor {
    interface Callback {
        void onAddAccountSuccess();
        void onAddAccountFail(String errorMsg, int loginError);
    }
}
