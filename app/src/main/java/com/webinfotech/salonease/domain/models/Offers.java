package com.webinfotech.salonease.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Offers {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("category_id")
    @Expose
    public int categoryId;

    @SerializedName("category_name")
    @Expose
    public String categoryName;

    @SerializedName("sub_category_id")
    @Expose
    public int subCategoryId;

    @SerializedName("sub_category_name")
    @Expose
    public String subCategoryName;

    @SerializedName("third_category_id")
    @Expose
    public int thirdCategoryId;

    @SerializedName("third_category_name")
    @Expose
    public String thirdCategoryName;

    @SerializedName("range_type")
    @Expose
    public int rangeType;

    @SerializedName("from_date")
    @Expose
    public String fromDate;

    @SerializedName("to_date")
    @Expose
    public String toDate;

    @SerializedName("total_user")
    @Expose
    public int totalUser;

    @SerializedName("price")
    @Expose
    public double price;

    @SerializedName("offer_received_user")
    @Expose
    public int offer_received_user;

    @SerializedName("description")
    @Expose
    public String description;

    @SerializedName("image")
    @Expose
    public String image;

    @SerializedName("status")
    @Expose
    public int status;

    @SerializedName("offer_salons")
    @Expose
    public OfferSalons[] offerSalons;

}
