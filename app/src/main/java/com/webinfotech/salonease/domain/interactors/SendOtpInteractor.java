package com.webinfotech.salonease.domain.interactors;

public interface SendOtpInteractor {
    interface Callback {
        void onOtpSendSuccess();
        void onOtpSendFail(String errorMsg);
    }
}
