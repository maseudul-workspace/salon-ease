package com.webinfotech.salonease.domain.interactors.impl;

import com.webinfotech.salonease.domain.executors.Executor;
import com.webinfotech.salonease.domain.executors.MainThread;
import com.webinfotech.salonease.domain.interactors.FetchTopSalonsInteractor;
import com.webinfotech.salonease.domain.interactors.base.AbstractInteractor;
import com.webinfotech.salonease.domain.models.SalonListWrapper;
import com.webinfotech.salonease.domain.models.SalonUser;
import com.webinfotech.salonease.domain.models.SalonUserContainer;
import com.webinfotech.salonease.repository.AppRepositoryImpl;

public class FetchTopSalonsInteractorImpl extends AbstractInteractor implements FetchTopSalonsInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    double latitude;
    double longitude;

    public FetchTopSalonsInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, double latitude, double longitude) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onFetchTopSalonsFail(errorMsg);
            }
        });
    }

    private void postMessage(SalonUser[] salonUsers){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onFetchTopSalonsSuccess(salonUsers);
            }
        });
    }

    @Override
    public void run() {
        final SalonListWrapper salonListWrapper = mRepository.fetchTopSalons(latitude, longitude);
        if (salonListWrapper == null) {
            notifyError("Please Check Your Internet Connection");
        } else if (!salonListWrapper.status) {
            notifyError(salonListWrapper.message);
        } else {
            postMessage(salonListWrapper.salonUsers);
        }
    }
}
