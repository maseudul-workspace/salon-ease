package com.webinfotech.salonease.domain.interactors.impl;

import android.util.Log;

import com.webinfotech.salonease.domain.executors.Executor;
import com.webinfotech.salonease.domain.executors.MainThread;
import com.webinfotech.salonease.domain.interactors.RegisterUserInteractor;
import com.webinfotech.salonease.domain.interactors.base.AbstractInteractor;
import com.webinfotech.salonease.domain.models.CommonResponse;
import com.webinfotech.salonease.domain.models.RegistrationResponse;
import com.webinfotech.salonease.domain.models.UserInfo;
import com.webinfotech.salonease.domain.models.UserInfoWrapper;
import com.webinfotech.salonease.repository.AppRepositoryImpl;

public class RegisterUserInteractorImpl extends AbstractInteractor implements RegisterUserInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    int userId;
    String name;
    String email;
    String gender;
    String referalId;

    public RegisterUserInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, int userId, String name, String email, String gender, String referalId) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.userId = userId;
        this.name = name;
        this.email = email;
        this.gender = gender;
        this.referalId = referalId;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onRegisterUserFail(errorMsg);
            }
        });
    }

    private void postMessage(UserInfo userInfo){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onRegisterUserSuccess(userInfo);
            }
        });
    }

    @Override
    public void run() {
        final UserInfoWrapper userInfoWrapper = mRepository.registerUser(userId, name, email, gender, referalId);
        if (userInfoWrapper == null) {
            notifyError("Please Check Your Internet Connection");
        } else if (!userInfoWrapper.status) {
            if (userInfoWrapper.error) {
                if (userInfoWrapper.errorMesage.email != null) {
                    notifyError(userInfoWrapper.errorMesage.email[0]);
                }  else {
                    notifyError(userInfoWrapper.message);
                }
            } else {
                notifyError(userInfoWrapper.message);
            }
        } else {
            postMessage(userInfoWrapper.userInfo);
        }
    }
}
