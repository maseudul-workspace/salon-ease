package com.webinfotech.salonease.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OffersData {

    @SerializedName("admin_commissions")
    @Expose
    public AdminCommission[] adminCommissions;

    @SerializedName("coupons")
    @Expose
    public Coupon coupon;

    @SerializedName("offers")
    @Expose
    public Offers[] offers;

}
