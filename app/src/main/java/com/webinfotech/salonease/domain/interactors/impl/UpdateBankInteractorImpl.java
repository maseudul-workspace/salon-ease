package com.webinfotech.salonease.domain.interactors.impl;

import com.webinfotech.salonease.domain.executors.Executor;
import com.webinfotech.salonease.domain.executors.MainThread;
import com.webinfotech.salonease.domain.interactors.UpdateBankInteractor;
import com.webinfotech.salonease.domain.interactors.base.AbstractInteractor;
import com.webinfotech.salonease.domain.models.CommonResponse;
import com.webinfotech.salonease.repository.AppRepositoryImpl;

public class UpdateBankInteractorImpl extends AbstractInteractor implements UpdateBankInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String apiToken;
    int bankAccountId;
    String name;
    String bankName;
    String accountNo;
    String ifsc;
    String branchName;

    public UpdateBankInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String apiToken, int bankAccountId, String name, String bankName, String accountNo, String ifsc, String branchName) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.apiToken = apiToken;
        this.bankAccountId = bankAccountId;
        this.name = name;
        this.bankName = bankName;
        this.accountNo = accountNo;
        this.ifsc = ifsc;
        this.branchName = branchName;
    }

    private void notifyError(String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onBankUpdateFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onBankUpdateSuccess();
            }
        });
    }

    @Override
    public void run() {
        final CommonResponse commonResponse = mRepository.updateBankAccount(apiToken, bankAccountId, name, bankName, accountNo, ifsc, branchName);
        if (commonResponse == null) {
            notifyError("Slow Internet Connection", commonResponse.login_error);
        } else if (!commonResponse.status) {
            notifyError(commonResponse.message, commonResponse.login_error);
        } else {
            postMessage();
        }
    }
}
