package com.webinfotech.salonease.domain.interactors;

public interface UpdateBankInteractor {
    interface Callback {
        void onBankUpdateSuccess();
        void onBankUpdateFail(String errorMsg, int loginError);
    }
}
