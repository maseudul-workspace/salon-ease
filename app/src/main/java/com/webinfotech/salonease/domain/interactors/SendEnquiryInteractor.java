package com.webinfotech.salonease.domain.interactors;

public interface SendEnquiryInteractor {
    interface Callback {
        void onSendFeedbackSuccess();
        void onSendFeedbackFail(String errorMsg, int loginError);
    }
}
