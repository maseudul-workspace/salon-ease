package com.webinfotech.salonease.domain.interactors;

import com.webinfotech.salonease.domain.models.Subcategory;

public interface FetchSubcategoryInteractor {
    interface Callback {
        void onGettingSubcategorySuccess(Subcategory[] subcategories);
        void ongGettingSubcategoryFail(String errorMsg);
    }
}
