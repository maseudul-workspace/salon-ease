package com.webinfotech.salonease.domain.interactors.impl;

import com.webinfotech.salonease.domain.executors.Executor;
import com.webinfotech.salonease.domain.executors.MainThread;
import com.webinfotech.salonease.domain.interactors.FetchSearchListInteractor;
import com.webinfotech.salonease.domain.interactors.base.AbstractInteractor;
import com.webinfotech.salonease.domain.models.SalonUser;
import com.webinfotech.salonease.domain.models.SalonListWrapper;
import com.webinfotech.salonease.repository.AppRepositoryImpl;

public class FetchSearchListInteractorImpl extends AbstractInteractor implements FetchSearchListInteractor {

    Callback mCallback;
    AppRepositoryImpl mRepository;
    String searchKey;
    double latitude;
    double longitude;
    int page;

    public FetchSearchListInteractorImpl(Executor threadExecutor, MainThread mainThread, Callback mCallback, AppRepositoryImpl mRepository, String searchKey, double latitude, double longitude, int page) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mRepository = mRepository;
        this.searchKey = searchKey;
        this.latitude = latitude;
        this.longitude = longitude;
        this.page = page;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingSearchListFail(errorMsg);
            }
        });
    }

    private void postMessage(SalonUser[] salonUsers, int totalPage, String searchKey){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingSearchListSuccess(salonUsers, totalPage, searchKey);
            }
        });
    }

    @Override
    public void run() {
        final SalonListWrapper salonListWrapper = mRepository.fetchSearchList(searchKey, latitude, longitude, page);
        if (salonListWrapper == null) {
            notifyError("Please Check Your Internet Connection");
        } else if (!salonListWrapper.status) {
            notifyError(salonListWrapper.message);
        } else {
            postMessage(salonListWrapper.salonUsers, salonListWrapper.totalPage, salonListWrapper.searchKey);
        }
    }
}
