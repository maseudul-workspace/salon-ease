package com.webinfotech.salonease.domain.interactors;

import com.webinfotech.salonease.domain.models.SalonUser;

public interface FetchDealsInteractor {
    interface Callback {
        void onGettingDealsSuccess(SalonUser[] salonUsers);
        void onGettingDealsFail(String errorMsg);
    }
}
