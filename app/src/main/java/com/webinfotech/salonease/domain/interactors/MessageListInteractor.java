package com.webinfotech.salonease.domain.interactors;

import com.webinfotech.salonease.domain.models.MessageData;
import com.webinfotech.salonease.domain.models.MessageWrapper;

public interface MessageListInteractor {
    interface Callback {
        void onMessageListFetchSuccess(MessageData[] messageData, int totalPage);
        void onMessageListFetchFail(String errorMsg, int loginError);
    }
}
