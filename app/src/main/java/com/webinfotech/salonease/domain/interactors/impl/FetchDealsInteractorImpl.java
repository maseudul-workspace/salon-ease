package com.webinfotech.salonease.domain.interactors.impl;

import com.webinfotech.salonease.domain.executors.Executor;
import com.webinfotech.salonease.domain.executors.MainThread;
import com.webinfotech.salonease.domain.interactors.FetchCombosInteractor;
import com.webinfotech.salonease.domain.interactors.FetchDealsInteractor;
import com.webinfotech.salonease.domain.interactors.base.AbstractInteractor;
import com.webinfotech.salonease.domain.models.SalonListWrapper;
import com.webinfotech.salonease.domain.models.SalonUser;
import com.webinfotech.salonease.repository.AppRepositoryImpl;

public class FetchDealsInteractorImpl extends AbstractInteractor implements FetchDealsInteractor {

    Callback mCallback;
    AppRepositoryImpl mRepository;
    double latitude;
    double longitude;

    public FetchDealsInteractorImpl(Executor threadExecutor, MainThread mainThread, Callback mCallback, AppRepositoryImpl mRepository, double latitude, double longitude) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mRepository = mRepository;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingDealsFail(errorMsg);
            }
        });
    }

    private void postMessage(SalonUser[] salonUsers){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingDealsSuccess(salonUsers);
            }
        });
    }

    @Override
    public void run() {
        final SalonListWrapper salonListWrapper = mRepository.fetchDeals(latitude, longitude);
        if (salonListWrapper == null) {
            notifyError("Please Check Your Internet Connection");
        } else if (!salonListWrapper.status) {
            notifyError(salonListWrapper.message);
        } else {
            postMessage(salonListWrapper.salonUsers);
        }
    }
}
