package com.webinfotech.salonease.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ServiceCart {

    public int serviceId;
    public String serviceName;
    public double price;
    public String image;
    public int qty;
    public int offerId;
    public double offerPrice;
    public boolean offerApplied;
    public String isDeal;

    public ServiceCart(int serviceId, String serviceName, double price, String image, int qty, int offerId, double offerPrice, boolean offerApplied, String isDeal) {
        this.serviceId = serviceId;
        this.serviceName = serviceName;
        this.price = price;
        this.image = image;
        this.qty = qty;
        this.offerId = offerId;
        this.offerPrice = offerPrice;
        this.offerApplied = offerApplied;
        this.isDeal = isDeal;
    }
}
