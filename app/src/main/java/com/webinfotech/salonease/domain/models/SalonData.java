package com.webinfotech.salonease.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SalonData {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("category_id")
    @Expose
    public int categoryId;

    @SerializedName("category_name")
    @Expose
    public String categoryName;

    @SerializedName("sub_category_name")
    @Expose
    public String subCategoryName;

    @SerializedName("last_category_name")
    @Expose
    public String lastCategoryName;

    @SerializedName("description")
    @Expose
    public String description;

    @SerializedName("mrp")
    @Expose
    public String mrp;

    @SerializedName("price")
    @Expose
    public String price;

    @SerializedName("distance")
    @Expose
    public double distance;

    @SerializedName("user_data")
    @Expose
    public SalonUser salonUser;

}
