package com.webinfotech.salonease.domain.interactors;

public interface ResetPasswordInteractor {
    interface Callback {
        void onResetPasswordSuccess();
        void onResetPasswordFail(String errorMsg);
    }
}
