package com.webinfotech.salonease.domain.interactors.impl;

import com.webinfotech.salonease.domain.executors.Executor;
import com.webinfotech.salonease.domain.executors.MainThread;
import com.webinfotech.salonease.domain.interactors.FetchTopFreelancerInteractor;
import com.webinfotech.salonease.domain.interactors.FetchTopSalonsInteractor;
import com.webinfotech.salonease.domain.interactors.base.AbstractInteractor;
import com.webinfotech.salonease.domain.models.SalonListWrapper;
import com.webinfotech.salonease.domain.models.SalonUser;
import com.webinfotech.salonease.repository.AppRepositoryImpl;

public class FetchTopFreelancerInteractorImpl extends AbstractInteractor implements FetchTopFreelancerInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    double latitude;
    double longitude;

    public FetchTopFreelancerInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, double latitude, double longitude) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onFetchTopFreelancerFail(errorMsg);
            }
        });
    }

    private void postMessage(SalonUser[] salonUsers){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onFetchTopFreelancerSuccess(salonUsers);
            }
        });
    }

    @Override
    public void run() {
        final SalonListWrapper salonListWrapper = mRepository.fetchTopFreeLancers(latitude, longitude);
        if (salonListWrapper == null) {
            notifyError("Please Check Your Internet Connection");
        } else if (!salonListWrapper.status) {
            notifyError(salonListWrapper.message);
        } else {
            postMessage(salonListWrapper.salonUsers);
        }
    }
}
