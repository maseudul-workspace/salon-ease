package com.webinfotech.salonease.domain.interactors.impl;

import com.webinfotech.salonease.domain.executors.Executor;
import com.webinfotech.salonease.domain.executors.MainThread;
import com.webinfotech.salonease.domain.interactors.FetchCategoriesInteractor;
import com.webinfotech.salonease.domain.interactors.base.AbstractInteractor;
import com.webinfotech.salonease.domain.models.Category;
import com.webinfotech.salonease.domain.models.CategoryWrapper;
import com.webinfotech.salonease.repository.AppRepositoryImpl;

public class FetchCategoriesInteractorImpl extends AbstractInteractor implements FetchCategoriesInteractor {

    Callback mCallback;
    AppRepositoryImpl mRepository;

    public FetchCategoriesInteractorImpl(Executor threadExecutor, MainThread mainThread, Callback mCallback, AppRepositoryImpl mRepository) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mRepository = mRepository;
    }

    private void notifyError() {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingCategoriesFail();
            }
        });
    }

    private void postMessage(Category[] categories){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingCategoriesSuccess(categories);
            }
        });
    }

    @Override
    public void run() {
        final CategoryWrapper categoryWrapper = mRepository.fetchCategories();
        if (categoryWrapper == null) {
            notifyError();
        } else if (!categoryWrapper.status) {
            notifyError();
        } else {
            postMessage(categoryWrapper.categories);
        }
    }
}
