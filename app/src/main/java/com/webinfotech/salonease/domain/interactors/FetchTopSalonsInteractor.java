package com.webinfotech.salonease.domain.interactors;

import com.webinfotech.salonease.domain.models.SalonUser;

public interface FetchTopSalonsInteractor {
    interface Callback {
        void onFetchTopSalonsSuccess(SalonUser[] salonUsers);
        void onFetchTopSalonsFail(String errorMsg);
    }
}
