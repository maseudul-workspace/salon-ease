package com.webinfotech.salonease.domain.interactors;

public interface RequestPasswordChangeInteractor {
    interface Callback {
        void onRequestPasswordChangedSuccess();
        void onRequestPasswordChangedFail(String errorMsg);
    }
}
