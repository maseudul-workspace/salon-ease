package com.webinfotech.salonease.domain.interactors.impl;


import com.webinfotech.salonease.domain.executors.Executor;
import com.webinfotech.salonease.domain.executors.MainThread;
import com.webinfotech.salonease.domain.interactors.FetchAddressDetailsInteractor;
import com.webinfotech.salonease.domain.interactors.base.AbstractInteractor;
import com.webinfotech.salonease.domain.models.AddressDetailsWrapper;
import com.webinfotech.salonease.domain.models.ShippingAddress;
import com.webinfotech.salonease.repository.AppRepositoryImpl;

public class FetchAddressDetailsInteractorImpl extends AbstractInteractor implements FetchAddressDetailsInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String apiToken;
    int userId;
    int addressId;

    public FetchAddressDetailsInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String apiToken, int userId, int addressId) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.apiToken = apiToken;
        this.userId = userId;
        this.addressId = addressId;
    }

    private void notifyError(String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingAddressDetailsFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(ShippingAddress shippingAddresses){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingAddressDetailsSuccess(shippingAddresses);
            }
        });
    }

    @Override
    public void run() {
        AddressDetailsWrapper shippingAddressDetailsWrapper = mRepository.fetchAddressDetails(apiToken, addressId);
        if (shippingAddressDetailsWrapper == null) {
            notifyError("Slow Internet Connection", shippingAddressDetailsWrapper.login_error);
        } else if (!shippingAddressDetailsWrapper.status) {
            notifyError(shippingAddressDetailsWrapper.message, shippingAddressDetailsWrapper.login_error);
        } else {
            postMessage(shippingAddressDetailsWrapper.shippingAddresses);
        }
    }
}
