package com.webinfotech.salonease.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SalonListWrapper {

    @SerializedName("status")
    @Expose
    public Boolean status;

    @SerializedName("message")
    @Expose
    public String message;

    @SerializedName("tatal_page")
    @Expose
    public int totalPage;

    @SerializedName("total_page")
    @Expose
    public int total_page;

    @SerializedName("search_key")
    @Expose
    public String searchKey;

    @SerializedName("data")
    @Expose
    public SalonUser[] salonUsers;

}
