package com.webinfotech.salonease.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MessageWrapper {

    @SerializedName("status")
    @Expose
    public boolean status;

    @SerializedName("message")
    @Expose
    public String message;

    @SerializedName("login_error")
    @Expose
    public int login_error = 0;


    @SerializedName("current_page")
    @Expose
    public int current_page;

    @SerializedName("total_page")
    @Expose
    public int total_page;

    @SerializedName("has_more_page")
    @Expose
    public boolean has_more_page;

    @SerializedName("total_data")
    @Expose
    public int total_data;

    @SerializedName("data")
    @Expose
    public MessageData[] messageData;

}
