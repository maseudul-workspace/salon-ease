package com.webinfotech.salonease.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.goodiebag.pinview.Pinview;
import com.webinfotech.salonease.R;
import com.webinfotech.salonease.domain.executors.impl.ThreadExecutor;
import com.webinfotech.salonease.presentation.presenters.PhoneVerificationPresenter;
import com.webinfotech.salonease.presentation.presenters.impl.PhoneVerificationPresenterImpl;
import com.webinfotech.salonease.threading.MainThreadImpl;
import com.webinfotech.salonease.util.GlideHelper;

public class PhoneVerificationActivity extends AppCompatActivity implements PhoneVerificationPresenter.View {

    @BindView(R.id.img_view_otp)
    ImageView imgViewOtp;
    @BindView(R.id.edit_text_phone)
    EditText editTextPhone;
    @BindView(R.id.layout_otp)
    View layoutOtp;
    @BindView(R.id.layout_phone)
    View layoutPhone;
    @BindView(R.id.pinview)
    Pinview pinview;
    PhoneVerificationPresenterImpl mPresenter;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone_verification);
        ButterKnife.bind(this);
        GlideHelper.setImageView(this, imgViewOtp, "https://italiansbarber.com/wp-content/uploads/2016/05/mens-haircuts-brown-short-hair_1024x1024-2.jpg");
        initialisePresenter();
        setUpProgressDialog();
    }

    private void initialisePresenter() {
        mPresenter = new PhoneVerificationPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    public void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @OnClick(R.id.btn_submit) void onSubmitClicked() {
        if (editTextPhone.getText().toString().trim().isEmpty()) {
            Toast.makeText(this, "Please Enter Your 10 Digits Phone No", Toast.LENGTH_LONG).show();
        } else if (editTextPhone.getText().toString().trim().length() < 10) {
            Toast.makeText(this, "Phone No Must Be 10 Digits", Toast.LENGTH_LONG).show();
        } else {
            mPresenter.sendOtp(editTextPhone.getText().toString());
        }
    }

    @OnClick(R.id.btn_check_otp) void onOtpCheckClicked() {
        if (pinview.getValue().length() < 5) {
            Toast.makeText(this, "OTP should be 5 digit", Toast.LENGTH_SHORT).show();
        } else {
            mPresenter.verifyOtp(editTextPhone.getText().toString(), pinview.getValue().toString());
        }
    }

    @Override
    public void onOtpSendSuccess() {
        layoutPhone.setVisibility(View.GONE);
        layoutOtp.setVisibility(View.VISIBLE);
    }

    @Override
    public void goToMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void goToRegisterActivity() {
        Intent intent = new Intent(this, RegisterActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

}