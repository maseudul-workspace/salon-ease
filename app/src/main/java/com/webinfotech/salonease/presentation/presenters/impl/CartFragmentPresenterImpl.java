package com.webinfotech.salonease.presentation.presenters.impl;

import android.content.Context;
import android.util.Log;
import android.widget.TableRow;
import android.widget.Toast;

import com.webinfotech.salonease.AndroidApplication;
import com.webinfotech.salonease.domain.executors.Executor;
import com.webinfotech.salonease.domain.executors.MainThread;
import com.webinfotech.salonease.domain.interactors.CheckCouponInteractor;
import com.webinfotech.salonease.domain.interactors.CheckOfferInteractor;
import com.webinfotech.salonease.domain.interactors.FetchShippingAddressInteractor;
import com.webinfotech.salonease.domain.interactors.impl.CheckCouponInteractorImpl;
import com.webinfotech.salonease.domain.interactors.impl.CheckOfferInteractorImpl;
import com.webinfotech.salonease.domain.interactors.impl.FetchShippingAddressInteractorImpl;
import com.webinfotech.salonease.domain.models.ShippingAddress;
import com.webinfotech.salonease.domain.models.UserInfo;
import com.webinfotech.salonease.presentation.presenters.CartFragmentPresenter;
import com.webinfotech.salonease.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.salonease.repository.AppRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class CartFragmentPresenterImpl extends AbstractPresenter implements CartFragmentPresenter,
                                                                            CheckOfferInteractor.Callback,
                                                                            CheckCouponInteractor.Callback,
                                                                            FetchShippingAddressInteractor.Callback
{

    Context mContext;
    CartFragmentPresenter.View mView;

    public CartFragmentPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void checkOffer(int offerId, int jobId, int vendorId) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo != null) {
            CheckOfferInteractorImpl checkOfferInteractor = new CheckOfferInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, offerId, jobId, vendorId);
            checkOfferInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void checkCoupon(int couponId) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo != null && userInfo.isRegistered == 2) {
            CheckCouponInteractorImpl checkCouponInteractor = new CheckCouponInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, couponId);
            checkCouponInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void fetchShippingAddress() {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo != null && userInfo.isRegistered == 2) {
            FetchShippingAddressInteractorImpl fetchShippingAddressInteractor = new FetchShippingAddressInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, userInfo.userId);
            fetchShippingAddressInteractor.execute();
        }
    }

    @Override
    public void onCheckOfferSuccess() {
        mView.hideLoader();
        mView.isOfferValid(true);
    }

    @Override
    public void onCheckOfferFail(String errorMsg, int loginError) {
        mView.hideLoader();
        mView.isOfferValid(false);
    }

    @Override
    public void onCouponCheckSuccess() {
        mView.hideLoader();
        mView.isCouponValid(true);
    }

    @Override
    public void onCouponCheckFail(String errorMsg, int loginError) {
        mView.hideLoader();
        mView.isCouponValid(false);
        Toast.makeText(mContext, errorMsg, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onGettingAddressListSucces(ShippingAddress[] shippingAddresses) {
        mView.loadShippingAddress(shippingAddresses);
    }

    @Override
    public void onGettingAddressListFail(String errorMsg, int loginError) {

    }
}
