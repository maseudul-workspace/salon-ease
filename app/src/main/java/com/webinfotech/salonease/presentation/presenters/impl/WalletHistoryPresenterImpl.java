package com.webinfotech.salonease.presentation.presenters.impl;

import android.content.Context;
import android.util.Log;

import com.webinfotech.salonease.AndroidApplication;
import com.webinfotech.salonease.domain.executors.Executor;
import com.webinfotech.salonease.domain.executors.MainThread;
import com.webinfotech.salonease.domain.interactors.FetchWalletHistoryInteractor;
import com.webinfotech.salonease.domain.interactors.impl.FetchWalletHistoryInteractorImpl;
import com.webinfotech.salonease.domain.models.UserInfo;
import com.webinfotech.salonease.domain.models.WalletResponse;
import com.webinfotech.salonease.domain.models.WalletResponseData;
import com.webinfotech.salonease.presentation.presenters.WalletHistoryPresenter;
import com.webinfotech.salonease.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.salonease.presentation.ui.adapters.WalletHistoryListRecyclerViewAdapter;
import com.webinfotech.salonease.repository.AppRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class WalletHistoryPresenterImpl extends AbstractPresenter implements WalletHistoryPresenter, FetchWalletHistoryInteractor.Callback {

    Context mContext;
    WalletHistoryPresenter.View mView;
    WalletResponseData[] newWalletResponseData = null;
    WalletHistoryListRecyclerViewAdapter adapter;

    public WalletHistoryPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void onWalletHistoryFetchSuccess(WalletResponse walletResponse, int totalPage) {
        WalletResponseData[] tempWalletResponse;
        tempWalletResponse = newWalletResponseData;
        try {
            int len1 = tempWalletResponse.length;
            int len2 = walletResponse.walletResponseData.length;
            newWalletResponseData = new WalletResponseData[len1 + len2];
            System.arraycopy(tempWalletResponse, 0, newWalletResponseData, 0, len1);
            System.arraycopy(walletResponse.walletResponseData, 0, newWalletResponseData, len1, len2);
            adapter.updateData(newWalletResponseData);
        }catch (NullPointerException e){
            newWalletResponseData = walletResponse.walletResponseData;
            adapter = new WalletHistoryListRecyclerViewAdapter(mContext, walletResponse.walletResponseData);
            mView.loadAdapter(adapter, totalPage);
        }
        mView.hideLoader();
        mView.loadData(walletResponse);
        mView.hidePaginationLoader();

    }

    @Override
    public void onWalletHistoryFetchFail(String errorMsg, int loginError) {
        mView.hideLoader();
        if(loginError == 1)
        {
            AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
            androidApplication.setUserInfo(mContext,null);
            mView.onloginError();
        }
        else
        {
            Toasty.warning(mContext,errorMsg,Toasty.LENGTH_SHORT).show();
        }

    }

    @Override
    public void fetchWalletHistory(int page, String refresh) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (refresh.equals("refresh")) {
            newWalletResponseData = null;
        }
        if (userInfo != null) {
            FetchWalletHistoryInteractorImpl walletHistoryInteractorImpl = new FetchWalletHistoryInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, page);
            walletHistoryInteractorImpl.execute();
        }
        mView.showLoader();
    }
}
