package com.webinfotech.salonease.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.webinfotech.salonease.R;
import com.webinfotech.salonease.domain.models.testing.BookingServices;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class SummaryServicesAdapter extends RecyclerView.Adapter<SummaryServicesAdapter.ViewHolder> {

    Context mContext;
    ArrayList<BookingServices> bookingServices;

    public SummaryServicesAdapter(Context mContext, ArrayList<BookingServices> bookingServices) {
        this.mContext = mContext;
        this.bookingServices = bookingServices;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_order_services, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewService.setText(bookingServices.get(position).name);
        holder.txtViewPrice.setText("Rs. " + bookingServices.get(position).price);
    }

    @Override
    public int getItemCount() {
        return bookingServices.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_service)
        TextView txtViewService;
        @BindView(R.id.txt_view_price)
        TextView txtViewPrice;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
