package com.webinfotech.salonease.presentation.ui.dialogs;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.TextView;

import com.webinfotech.salonease.R;

import androidx.appcompat.app.AlertDialog;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class OfferConfirmDialog {

    public interface Callback {
        void onOfferConfirmClicked();
    }

    View dialogContainer;
    AlertDialog.Builder builder;
    AlertDialog dialog;
    Context mContext;
    Activity mActivity;
    Callback mCallback;
    @BindView(R.id.txt_view_msg)
    TextView txtViewMsg;

    public OfferConfirmDialog(Context mContext, Activity mActivity, Callback mCallback) {
        this.mContext = mContext;
        this.mActivity = mActivity;
        this.mCallback = mCallback;
    }

    public void setUpDialog() {
        dialogContainer = mActivity.getLayoutInflater().inflate(R.layout.layout_offer_confirm_dialog, null);
        builder = new AlertDialog.Builder(mContext, android.R.style.Theme_Material_Dialog_NoActionBar_MinWidth);
        builder.setView(dialogContainer);
        dialog = builder.create();
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        ButterKnife.bind(this, dialogContainer);
    }

    public void showDialog() {
        dialog.show();
    }

    public void dismissDialog() {
        dialog.dismiss();
    }

    public void setMessage(String msg) {
        txtViewMsg.setText(msg);
    }

    @OnClick(R.id.txt_view_confirm) void onConfirmClicked() {
        mCallback.onOfferConfirmClicked();
        dismissDialog();
    }

    @OnClick(R.id.txt_view_cancel) void onCancelClicked() {
        dismissDialog();
    }

}
