package com.webinfotech.salonease.presentation.ui.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;

import android.os.Parcelable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.webinfotech.salonease.AndroidApplication;
import com.webinfotech.salonease.R;
import com.webinfotech.salonease.domain.executors.impl.ThreadExecutor;
import com.webinfotech.salonease.domain.models.CombosCart;
import com.webinfotech.salonease.domain.models.Coupon;
import com.webinfotech.salonease.domain.models.Quantity;
import com.webinfotech.salonease.domain.models.ServiceCart;
import com.webinfotech.salonease.domain.models.ShippingAddress;
import com.webinfotech.salonease.presentation.presenters.CartFragmentPresenter;
import com.webinfotech.salonease.presentation.presenters.impl.CartFragmentPresenterImpl;
import com.webinfotech.salonease.presentation.ui.activities.AddAddressActivity;
import com.webinfotech.salonease.presentation.ui.adapters.CartCombosAdapter;
import com.webinfotech.salonease.presentation.ui.adapters.CartServiceAdapter;
import com.webinfotech.salonease.presentation.ui.adapters.CartShippingAddressAdapter;
import com.webinfotech.salonease.presentation.ui.adapters.ComboServiceAdapter;
import com.webinfotech.salonease.presentation.ui.adapters.QuantityAdapter;
import com.webinfotech.salonease.presentation.ui.dialogs.CouponDialogFragment;
import com.webinfotech.salonease.presentation.ui.dialogs.CustomProgressDialog;
import com.webinfotech.salonease.presentation.ui.dialogs.OfferConfirmDialog;
import com.webinfotech.salonease.threading.MainThreadImpl;

import java.io.StringReader;
import java.lang.reflect.Type;
import java.util.ArrayList;

public class CartFragment extends DialogFragment implements CartServiceAdapter.Callback,
                                                            CartCombosAdapter.Callback,
                                                            QuantityAdapter.Callback,
                                                            OfferConfirmDialog.Callback,
                                                            CartFragmentPresenter.View,
                                                            CouponDialogFragment.Callback,
                                                            BookingDateFragment.Callback,
                                                            CartShippingAddressAdapter.Callback
{

    private BottomSheetDialog addressBottomSheetDialog;
    private int couponId = 0;

    public interface Callback {
        void onBookingConfirm(ArrayList<ServiceCart> serviceCarts, ArrayList<CombosCart> combosCarts, double payableAmount, int couponId, String bookingTime, int addressId);
    }

    @BindView(R.id.recycler_view_services)
    RecyclerView recyclerViewServices;
    @BindView(R.id.recycler_view_combos)
    RecyclerView recyclerViewCombos;
    @BindView(R.id.txt_view_payable_amount)
    TextView txtViewPayableAmount;
    @BindView(R.id.btn_apply_coupon)
    Button btnApplyCoupon;
    @BindView(R.id.txt_view_coupon_title)
    TextView txtViewCouponTitle;
    @BindView(R.id.txt_view_coupon_text)
    TextView txtViewCouponText;
    @BindView(R.id.layout_coupon)
    View layoutCoupon;
    @BindView(R.id.layout_coupon_discount)
    View viewLayoutCouponDiscount;
    @BindView(R.id.txt_view_final_payable_amount)
    TextView txtViewFinalPayableAmount;
    @BindView(R.id.txt_view_coupon_discount)
    TextView txtViewCouponDiscount;
    @BindView(R.id.txt_view_net_amount)
    TextView txtViewNetAmount;
    @BindView(R.id.view_net_amount)
    View viewNetAmount;
    @BindView(R.id.txt_view_service_count)
    TextView txtViewServiceCount;
    ArrayList<ServiceCart> serviceCarts;
    ArrayList<CombosCart> combosCarts;
    CartServiceAdapter cartServiceAdapter;
    CartCombosAdapter comboServiceAdapter;
    private BottomSheetDialog serviceQtyFilterBottomSheetDialog;
    private RecyclerView serviceQtyFilterRecyclerview;
    private BottomSheetDialog comboQtyFilterBottomSheetDialog;
    private RecyclerView comboQtyFilterRecyclerview;
    private QuantityAdapter quantityAdapter;
    ArrayList<Quantity> quantities;
    int position;
    OfferConfirmDialog offerConfirmDialog;
    int vendorId;
    CartFragmentPresenterImpl mPresenter;
    CustomProgressDialog customProgressDialog;
    CouponDialogFragment couponDialogFragment = new CouponDialogFragment();
    BookingDateFragment bookingDateFragment = new BookingDateFragment();
    double payableAmount = 0;
    boolean isCouponApplied = false;
    String openingTime;
    String closingTime;
    Callback mCallback;
    TextView txtViewAddAddress;
    ImageView imgViewAddAddress;
    RecyclerView recyclerViewShippingAddress;
    int userType;
    ShippingAddress[] shippingAddresses;
    CartShippingAddressAdapter cartShippingAddressAdapter;
    int addressId = 0;
    String dateTime;

    public CartFragment() {
        // Required empty public constructor
    }

    public static CartFragment newInstance(String param1, String param2) {
        CartFragment fragment = new CartFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Material_NoActionBar_Fullscreen);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_cart, container, false);
        ButterKnife.bind(this, view);
        if (savedInstanceState != null) {
            Type serviceType = new TypeToken<ArrayList<ServiceCart>>() {}.getType();
            Type comboType = new TypeToken<ArrayList<CombosCart>>() {}.getType();
            serviceCarts = new Gson().fromJson(savedInstanceState.getString("services"), serviceType);
            combosCarts = new Gson().fromJson(savedInstanceState.getString("combos"), comboType);
            vendorId = savedInstanceState.getInt("vendorId");
            userType = savedInstanceState.getInt("userType");
        }
        setComboSizeFilterBottomSheetDialog();
        setServiceSizeFilterBottomSheetDialog();
        cartServiceAdapter = new CartServiceAdapter(getContext(), serviceCarts, this);
        comboServiceAdapter = new CartCombosAdapter(getContext(), combosCarts, this);
        recyclerViewServices.setAdapter(cartServiceAdapter);
        recyclerViewServices.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerViewCombos.setAdapter(comboServiceAdapter);
        recyclerViewCombos.setLayoutManager(new LinearLayoutManager(getContext()));
        setOfferConfirmDialog();
        initialisePresenter();
        setCustomProgressDialog();
        setPayableAmount();
        setAddressBottomSheetDialog();
        mPresenter.fetchShippingAddress();
        return view;
    }

    private void initialisePresenter() {
        mPresenter = new CartFragmentPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), getContext(), this);
    }

    private void setCustomProgressDialog() {
        customProgressDialog = new CustomProgressDialog(getContext(), getActivity());
        customProgressDialog.setUpDialog();
    }

    public void setData(ArrayList<ServiceCart> serviceCarts, ArrayList<CombosCart> combosCarts, int vendorId, String openingTime, String closingTime, int userType) {
        this.serviceCarts = serviceCarts;
        this.combosCarts = combosCarts;
        this.vendorId = vendorId;
        this.openingTime = openingTime;
        this.closingTime = closingTime;
        this.userType = userType;
    }

    private void setOfferConfirmDialog() {
        offerConfirmDialog = new OfferConfirmDialog(getContext(), getActivity(), this);
        offerConfirmDialog.setUpDialog();
    }

    @Override
    public void onComboQtyClicked(int position) {
        this.position = position;
        quantities = new ArrayList<>();
        for (int i = 1; i <= 10; i++) {
            if (combosCarts.get(position).qty == i) {
                quantities.add(new Quantity(i, true));
            } else {
                quantities.add(new Quantity(i, false));
            }
        }
        quantityAdapter = new QuantityAdapter(getContext(), this, quantities);
        comboQtyFilterRecyclerview.setAdapter(quantityAdapter);
        comboQtyFilterBottomSheetDialog.show();
    }

    @Override
    public void onServiceQtyClicked(int position) {
        this.position = position;
        quantities = new ArrayList<>();
        for (int i = 1; i <= 10; i++) {
            if (serviceCarts.get(position).qty == i) {
                quantities.add(new Quantity(i, true));
            } else {
                quantities.add(new Quantity(i, false));
            }
        }
        quantityAdapter = new QuantityAdapter(getContext(), this, quantities);
        serviceQtyFilterRecyclerview.setAdapter(quantityAdapter);
        serviceQtyFilterBottomSheetDialog.show();
    }

    @Override
    public void onOfferApplyClicked(int position) {
        this.position = position;
        double offerPrice = serviceCarts.get(position).offerPrice;
        String message = "This service will available for Rs. " + offerPrice +" only !!";
        offerConfirmDialog.setMessage(message);
        offerConfirmDialog.showDialog();
    }

    @Override
    public void onActivityCreated(Bundle arg0) {
        super.onActivityCreated(arg0);
        getDialog().getWindow()
                .getAttributes().windowAnimations = R.style.DialogAnimation;
    }

    private void setServiceSizeFilterBottomSheetDialog() {
        if (serviceQtyFilterBottomSheetDialog == null) {
            serviceQtyFilterBottomSheetDialog = new BottomSheetDialog(getContext());
            View view = LayoutInflater.from(getContext()).inflate(R.layout.bottom_sheet_quantity_layout, null);
            serviceQtyFilterRecyclerview = (RecyclerView) view.findViewById(R.id.recycler_view_quantity);
            serviceQtyFilterRecyclerview.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false));
            View btnSubmit = (View) view.findViewById(R.id.btn_qty_filter_done);
            ImageView imgViewCancel = (ImageView) view.findViewById(R.id.img_view_qty_cancel);
            imgViewCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    serviceQtyFilterBottomSheetDialog.dismiss();
                }
            });
            btnSubmit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    for (int i = 0; i < quantities.size(); i++) {
                        if (quantities.get(i).isSelected) {
                            serviceCarts.get(position).qty = quantities.get(i).qty;
                            cartServiceAdapter.setServiceCarts(serviceCarts);
                            break;
                        }
                    }
                    serviceQtyFilterBottomSheetDialog.dismiss();
                    setPayableAmount();
                }
            });
            serviceQtyFilterBottomSheetDialog.setContentView(view);
        }
    }

    private void setComboSizeFilterBottomSheetDialog() {
        if (comboQtyFilterBottomSheetDialog == null) {
            comboQtyFilterBottomSheetDialog = new BottomSheetDialog(getContext());
            View view = LayoutInflater.from(getContext()).inflate(R.layout.bottom_sheet_quantity_layout, null);
            comboQtyFilterRecyclerview = (RecyclerView) view.findViewById(R.id.recycler_view_quantity);
            comboQtyFilterRecyclerview.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false));
            View btnSubmit = (View) view.findViewById(R.id.btn_qty_filter_done);
            ImageView imgViewCancel = (ImageView) view.findViewById(R.id.img_view_qty_cancel);
            imgViewCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    comboQtyFilterBottomSheetDialog.dismiss();
                }
            });
            btnSubmit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    for (int i = 0; i < quantities.size(); i++) {
                        if (quantities.get(i).isSelected) {
                            combosCarts.get(position).qty = quantities.get(i).qty;
                            comboServiceAdapter.setCombosCarts(combosCarts);
                            break;
                        }
                    }
                    comboQtyFilterBottomSheetDialog.dismiss();
                    setPayableAmount();
                }
            });
            comboQtyFilterBottomSheetDialog.setContentView(view);
        }
    }

    @Override
    public void onQtySelected(int qty) {
        for (int i = 0; i < quantities.size(); i++) {
            if (quantities.get(i).qty == qty) {
                quantities.get(i).isSelected = true;
            } else {
                quantities.get(i).isSelected = false;
            }
        }
        quantityAdapter.updateDataSet(quantities);
    }

    @OnClick(R.id.view_cancel) void onCancelClicked(){
        dismiss();
    }

    @OnClick(R.id.btn_continue) void onBtnContinueClicked() {
        bookingDateFragment.setData(openingTime, closingTime);
        bookingDateFragment.show(getChildFragmentManager(), "Booking Date Dialog");
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof Callback)
        {
            mCallback = (Callback)context;
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("services", new Gson().toJson(serviceCarts));
        outState.putString("combos", new Gson().toJson(combosCarts));
        outState.putInt("vendorId", vendorId);
        outState.putInt("userType", userType);
    }


    @Override
    public void onOfferConfirmClicked() {
        mPresenter.checkOffer(this.serviceCarts.get(position).offerId, this.serviceCarts.get(position).serviceId, vendorId);
    }

    @Override
    public void isOfferValid(boolean isVaild) {
        if (isVaild) {
            for (int i = 0; i < serviceCarts.size(); i++) {
                if (i == position) {
                    this.serviceCarts.get(position).offerApplied = true;
                } else {
                    this.serviceCarts.get(i).offerPrice = 0;
                }
            }
            cartServiceAdapter.setServiceCarts(serviceCarts);
            layoutCoupon.setVisibility(View.GONE);
        } else {
            Toasty.normal(getContext(), "This offer is not applicable for you").show();
        }
    }

    @Override
    public void isCouponValid(boolean isValid) {
        if (isValid) {
            for (int i = 0; i < serviceCarts.size(); i++) {
                this.serviceCarts.get(i).offerPrice = 0;
            }
            cartServiceAdapter.setServiceCarts(serviceCarts);
            isCouponApplied = isValid;
            setPayableAmount();
        }
    }

    @Override
    public void loadShippingAddress(ShippingAddress[] addresses) {
        if (addresses.length > 0) {
            addresses[0].isSelected = true;
            addressId = addresses[0].id;
        }
        cartShippingAddressAdapter = new CartShippingAddressAdapter(getContext(), addresses, this);
        recyclerViewShippingAddress.setLayoutManager(new LinearLayoutManager(getContext()));
        recyclerViewShippingAddress.setAdapter(cartShippingAddressAdapter);
    }

    @Override
    public void showLoader() {
        customProgressDialog.showDialog();
    }

    @Override
    public void hideLoader() {
        customProgressDialog.dismissDialog();
    }

    @OnClick(R.id.btn_apply_coupon) void onApplyCouponClicked() {
        couponDialogFragment.show(getChildFragmentManager(), "Coupon Dialog");
    }

    @Override
    public void onCouponApplyClicked() {
        AndroidApplication androidApplication = (AndroidApplication) getContext().getApplicationContext();
        Coupon coupon = androidApplication.getOffersData().coupon;
        mPresenter.checkCoupon(coupon.id);
    }

    private void setPayableAmount() {
        payableAmount = 0;
        for (int i = 0; i < serviceCarts.size(); i++) {
            if (serviceCarts.get(i).offerApplied) {
                payableAmount = payableAmount + serviceCarts.get(i).offerPrice * serviceCarts.get(i).qty;
            } else {
                payableAmount = payableAmount + serviceCarts.get(i).price * serviceCarts.get(i).qty;
            }
        }

        for (int i = 0; i < combosCarts.size(); i++) {
            payableAmount = combosCarts.get(i).price * combosCarts.get(i).qty + payableAmount;
        }

        txtViewNetAmount.setText("Rs. " + payableAmount);

        if (isCouponApplied) {
            AndroidApplication androidApplication = (AndroidApplication) getContext().getApplicationContext();
            Coupon coupon = androidApplication.getOffersData().coupon;
            txtViewCouponTitle.setText("Coupon Applied");
            double discount = payableAmount*coupon.amount/100;
            payableAmount = payableAmount - discount;
            txtViewPayableAmount.setText("Rs. " + payableAmount);
            txtViewCouponText.setText("You saved Rs. " + discount);
            txtViewCouponText.setVisibility(View.VISIBLE);
            btnApplyCoupon.setVisibility(View.GONE);
            viewLayoutCouponDiscount.setVisibility(View.VISIBLE);
            viewNetAmount.setVisibility(View.VISIBLE);
            txtViewCouponDiscount.setText("Rs. " + discount);
        } else {
            txtViewPayableAmount.setText("Rs. " + payableAmount);
            viewLayoutCouponDiscount.setVisibility(View.GONE);
            viewNetAmount.setVisibility(View.GONE);
        }
        txtViewFinalPayableAmount.setText("Rs. " + (payableAmount*20/100));
        txtViewServiceCount.setText(Integer.toString(serviceCarts.size() + combosCarts.size()));
    }

    @Override
    public void onTimeSelected(String dateTime) {
        this.dateTime = dateTime;
        if (isCouponApplied) {
            AndroidApplication androidApplication = (AndroidApplication) getContext().getApplicationContext();
            Coupon coupon = androidApplication.getOffersData().coupon;
            couponId = coupon.id;
        }
        if (userType == 1) {
            addressBottomSheetDialog.show();
        } else {
            mCallback.onBookingConfirm(this.serviceCarts, this.combosCarts, payableAmount, couponId, dateTime, addressId);
            dismiss();
        }
    }

    private void setAddressBottomSheetDialog() {
        if (addressBottomSheetDialog == null) {
            View view = LayoutInflater.from(getContext()).inflate(R.layout.layout_address_bottom_sheet, null);
            addressBottomSheetDialog = new BottomSheetDialog(getContext());
            txtViewAddAddress = (TextView) view.findViewById(R.id.txt_view_add_address);
            imgViewAddAddress = (ImageView) view.findViewById(R.id.img_view_add_address);
            recyclerViewShippingAddress = (RecyclerView) view.findViewById(R.id.recycler_view_address);
            txtViewAddAddress.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getContext(), AddAddressActivity.class);
                    startActivity(intent);
                }
            });
            addressBottomSheetDialog.setContentView(view);
        }
    }

    @Override
    public void onAddressSelected(int id) {
        addressId = id;
        for (int i = 0; i < shippingAddresses.length; i++) {
            if (shippingAddresses[i].id == id) {
                shippingAddresses[i].isSelected = true;
            } else {
                shippingAddresses[i].isSelected = false;
            }
        }
        cartShippingAddressAdapter.updateDataSet(this.shippingAddresses);
    }

    @Override
    public void onEditClicked(int id) {

    }

    @Override
    public void onDeliverButtonClicked() {
        mCallback.onBookingConfirm(this.serviceCarts, this.combosCarts, payableAmount, couponId, dateTime, addressId);
        dismiss();
    }

}