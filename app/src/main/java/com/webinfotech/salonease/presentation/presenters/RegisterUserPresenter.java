package com.webinfotech.salonease.presentation.presenters;

public interface RegisterUserPresenter {
    void registerUser( String name,
                       String email,
                       String gender
                      );
    interface View {
        void onUserRegistrationSuccess();
        void showLoader();
        void hideLoader();
    }
}
