package com.webinfotech.salonease.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import com.webinfotech.salonease.AndroidApplication;
import com.webinfotech.salonease.domain.executors.Executor;
import com.webinfotech.salonease.domain.executors.MainThread;
import com.webinfotech.salonease.domain.interactors.SubmitReviewInteractor;
import com.webinfotech.salonease.domain.interactors.impl.SubmitReviewInteractorImpl;
import com.webinfotech.salonease.domain.models.UserInfo;
import com.webinfotech.salonease.presentation.presenters.ReviewsPresenter;
import com.webinfotech.salonease.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.salonease.repository.AppRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class ReviewsPresenterImpl extends AbstractPresenter implements ReviewsPresenter,
                                                                        SubmitReviewInteractor.Callback {

    Context mContext;
    ReviewsPresenter.View mView;

    public ReviewsPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void submitReview(int clientId, int rating, String feedback) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo != null) {
            SubmitReviewInteractorImpl submitReviewInteractor = new SubmitReviewInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, userInfo.userId, clientId, rating, feedback);
            submitReviewInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void onReviewSubmitSuccess() {
        mView.hideLoader();
        Toast.makeText(mContext, "Thank you for your feedback. Please wait for sometime to see your review", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onReviewSubmitFail(String errorMsg, int loginError) {
        mView.hideLoader();
        Toasty.warning(mContext, errorMsg).show();
    }
}
