package com.webinfotech.salonease.presentation.presenters;

import com.webinfotech.salonease.domain.models.ShippingAddress;

public interface EditAddressPresenter {
    void fetchAddressDetails(int addressId);
    void updateAddress( int addressId,
                        String name,
                        String email,
                        String mobile,
                        String city,
                        String state,
                        String pin,
                        String address,
                        double latitude,
                        double longitude
                        );
    interface View {
        void loadAddressDetails(ShippingAddress address);
        void showLoader();
        void hideLoader();
        void onAddressEditSuccess();
        void showLoginBottomSheet();
    }
}
