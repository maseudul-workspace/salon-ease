package com.webinfotech.salonease.presentation.ui.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.webinfotech.salonease.AndroidApplication;
import com.webinfotech.salonease.R;
import com.webinfotech.salonease.domain.executors.impl.ThreadExecutor;
import com.webinfotech.salonease.domain.models.Reviews;
import com.webinfotech.salonease.domain.models.Service;
import com.webinfotech.salonease.domain.models.UserInfo;
import com.webinfotech.salonease.presentation.presenters.ReviewsPresenter;
import com.webinfotech.salonease.presentation.presenters.impl.ReviewsPresenterImpl;
import com.webinfotech.salonease.presentation.ui.adapters.ReviewsAdapter;
import com.webinfotech.salonease.presentation.ui.dialogs.AddReviewDialog;
import com.webinfotech.salonease.presentation.ui.dialogs.CustomProgressDialog;
import com.webinfotech.salonease.threading.MainThreadImpl;
import com.webinfotech.salonease.util.Helper;

public class ReviewsFragment extends Fragment implements AddReviewDialog.Callback, ReviewsPresenter.View {

    @BindView(R.id.recycler_view_reviews)
    RecyclerView recyclerViewReviews;
    @BindView(R.id.txt_view_rating)
    TextView txtViewRating;
    @BindView(R.id.rating_bar_review)
    RatingBar ratingBarReview;
    Reviews[] reviews;
    double rating;
    int salonId;
    AddReviewDialog addReviewDialog;
    CustomProgressDialog customProgressDialog;
    ReviewsPresenterImpl mPresenter;

    public ReviewsFragment() {
        // Required empty public constructor
    }

    public static ReviewsFragment newInstance(String param1, String param2) {
        ReviewsFragment fragment = new ReviewsFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_reviews, container, false);
        ButterKnife.bind(this, view);
        setUpProgressDialog();
        initialisePresenter();
        if (savedInstanceState != null) {
            reviews = new Gson().fromJson(savedInstanceState.getString("reviews"), Reviews[].class);
            rating = savedInstanceState.getDouble("rating");
            salonId = savedInstanceState.getInt("salonId");
        }
        ReviewsAdapter adapter = new ReviewsAdapter(getContext(), reviews);
        recyclerViewReviews.setAdapter(adapter);
        recyclerViewReviews.setLayoutManager(new LinearLayoutManager(getContext()));
        txtViewRating.setText(Double.toString(rating));
        ratingBarReview.setRating(Math.round(rating));
        setAddReviewDialog();
        return view;
    }

    public void setRecyclerViewReviews(int salonId, Reviews[] reviews, double rating) {
        this.salonId = salonId;
        this.reviews = reviews;
        this.rating = rating;
    }

    private void initialisePresenter() {
        mPresenter = new ReviewsPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), getContext(), this);
    }

    private void setAddReviewDialog() {
        addReviewDialog = new AddReviewDialog(getContext(), getActivity(), this);
        addReviewDialog.setUpDialog();
    }

    private void setUpProgressDialog() {
        customProgressDialog = new CustomProgressDialog(getContext(), getActivity());
        customProgressDialog.setUpDialog();
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("reviews", new Gson().toJson(reviews));
        outState.putDouble("rating", rating);
        outState.putDouble("salonId", salonId);
    }

    @OnClick(R.id.btn_add_rating) void onAddRatingClicked() {
        AndroidApplication androidApplication = (AndroidApplication) getContext().getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(getContext());
        if (userInfo == null) {
            Helper.showLoginBottomSheet(getFragmentManager());
        } else {
            addReviewDialog.showDialog();
        }
    }

    @Override
    public void submitRating(int rating, String feedback) {
      mPresenter.submitReview(salonId, rating, feedback);
    }

    @Override
    public void showLoader() {
        customProgressDialog.showDialog();
    }

    @Override
    public void hideLoader() {
        customProgressDialog.dismissDialog();
    }
}