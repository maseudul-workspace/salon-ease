package com.webinfotech.salonease.presentation.presenters;

import com.webinfotech.salonease.domain.models.WalletResponse;
import com.webinfotech.salonease.presentation.ui.adapters.WalletHistoryListRecyclerViewAdapter;

public interface WalletHistoryPresenter {
    void fetchWalletHistory(int page, String refresh);
    interface View {
        void loadAdapter(WalletHistoryListRecyclerViewAdapter adapter, int totalPage);
        void loadData(WalletResponse walletResponse);
        void showLoader();
        void hideLoader();
        void hidePaginationLoader();
        void onloginError();
    }
}
