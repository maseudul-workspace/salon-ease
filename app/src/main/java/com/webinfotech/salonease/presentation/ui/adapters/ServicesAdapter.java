package com.webinfotech.salonease.presentation.ui.adapters;

import android.content.Context;
import android.graphics.Paint;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.webinfotech.salonease.R;
import com.webinfotech.salonease.domain.models.Service;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ServicesAdapter extends RecyclerView.Adapter<ServicesAdapter.ViewHolder> {

    public interface Callback {
        void selectService(int id, int serviceFor, int price, String serviceName);
    }

    Context mContext;
    Service[] services;
    Callback mCallback;

    public ServicesAdapter(Context mContext, Service[] services, Callback mCallback) {
        this.mContext = mContext;
        this.services = services;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_services, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

    }

    @Override
    public int getItemCount() {
        return services.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_man_mrp)
        TextView txtViewManMrp;
        @BindView(R.id.txt_view_woman_mrp)
        TextView txtViewWomanMrp;
        @BindView(R.id.txt_view_kid_mrp)
        TextView txtViewKidMrp;
        @BindView(R.id.txt_view_man_price)
        TextView txtViewManPrice;
        @BindView(R.id.txt_view_woman_price)
        TextView txtViewWomanPrice;
        @BindView(R.id.txt_view_kid_price)
        TextView txtViewKidPrice;
        @BindView(R.id.txt_view_description)
        TextView txtViewDescription;
        @BindView(R.id.txt_view_service)
        TextView txtViewServiceName;
        @BindView(R.id.check_box_woman_service)
        CheckBox checkBoxWomanService;
        @BindView(R.id.check_box_man_service)
        CheckBox checkBoxManService;
        @BindView(R.id.check_box_kid_service)
        CheckBox checkBoxKidService;
        @BindView(R.id.main_layout)
        View mainLayout;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void updateDataSet(Service[] services) {
        this.services = services;
        notifyDataSetChanged();
    }

}
