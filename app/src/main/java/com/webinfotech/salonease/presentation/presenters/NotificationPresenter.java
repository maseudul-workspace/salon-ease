package com.webinfotech.salonease.presentation.presenters;

import com.webinfotech.salonease.presentation.ui.adapters.NotificationListRecyclerViewAdapter;

public interface NotificationPresenter {
    void fetchMessageList(int page, String refresh);
    interface View {
        void loadAdapter(NotificationListRecyclerViewAdapter adapter, int totalPage);
        void showLoader();
        void hideLoader();
        void onLoginError();
    }
}
