package com.webinfotech.salonease.presentation.ui.dialogs;

import android.app.Activity;
import android.content.Context;
import android.view.View;

import com.webinfotech.salonease.R;

import androidx.appcompat.app.AlertDialog;
import butterknife.ButterKnife;

public class CustomProgressDialog {

    View dialogContainer;
    AlertDialog.Builder builder;
    AlertDialog dialog;
    Context mContext;
    Activity mActivity;

    public CustomProgressDialog(Context mContext, Activity mActivity) {
        this.mContext = mContext;
        this.mActivity = mActivity;
    }

    public void setUpDialog() {
        dialogContainer = mActivity.getLayoutInflater().inflate(R.layout.layout_progress_dialog, null);
        builder = new AlertDialog.Builder(mContext, android.R.style.Theme_Material_Dialog_NoActionBar_MinWidth);
        builder.setView(dialogContainer);
        dialog = builder.create();
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        dialog.setCancelable(false);
        ButterKnife.bind(this, dialogContainer);
    }

    public void showDialog() {
        dialog.show();
    }

    public void dismissDialog() {
        dialog.dismiss();
    }

}
