package com.webinfotech.salonease.presentation.ui.dialogs;

import android.app.Activity;
import android.content.Context;
import android.view.View;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.webinfotech.salonease.R;
import com.webinfotech.salonease.domain.models.Subcategory;
import com.webinfotech.salonease.domain.models.ThirdCategory;
import com.webinfotech.salonease.presentation.ui.adapters.SubcategoriesAdapter;
import com.webinfotech.salonease.presentation.ui.adapters.ThirdcategoriesAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;

public class ThirdcategoriesListDialog implements ThirdcategoriesAdapter.Callback {

    public interface Callback {
        void onThirdcategoryClicked(int position);
    }

    Context mContext;
    View dialogContainer;
    AlertDialog.Builder builder;
    AlertDialog dialog;
    Activity mActivity;
    Callback mCallback;
    @BindView(R.id.recycler_view_sub_category)
    RecyclerView recyclerViewSubCategories;

    public ThirdcategoriesListDialog(Context mContext, Activity mActivity, Callback mCallback) {
        this.mContext = mContext;
        this.mActivity = mActivity;
        this.mCallback = mCallback;
    }

    @Override
    public void onThirdCategoryClicked(int position) {
        hideDialog();
        mCallback.onThirdcategoryClicked(position);
    }

    public void setUpDialogView() {
        dialogContainer = mActivity.getLayoutInflater().inflate(R.layout.thrid_category_list_dialog, null);
        builder = new AlertDialog.Builder(mContext, android.R.style.Theme_Material_Light_Dialog_NoActionBar_MinWidth);
        builder.setView(dialogContainer);
        dialog = builder.create();
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        ButterKnife.bind(this, dialogContainer);
    }

    public void setCategories(ThirdCategory[] categories) {
        ThirdcategoriesAdapter adapter = new ThirdcategoriesAdapter(mContext, categories, this);
        recyclerViewSubCategories.setAdapter(adapter);
        recyclerViewSubCategories.setLayoutManager(new LinearLayoutManager(mContext));
        recyclerViewSubCategories.addItemDecoration(new DividerItemDecoration(recyclerViewSubCategories.getContext(), DividerItemDecoration.VERTICAL));
    }

    public void showDialog() {
        dialog.show();
    }

    public void hideDialog() {
        dialog.dismiss();
    }

}
