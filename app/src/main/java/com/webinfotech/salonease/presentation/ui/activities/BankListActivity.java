package com.webinfotech.salonease.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.webinfotech.salonease.R;
import com.webinfotech.salonease.domain.executors.impl.ThreadExecutor;
import com.webinfotech.salonease.domain.models.BankList;
import com.webinfotech.salonease.presentation.presenters.BankListPresenter;
import com.webinfotech.salonease.presentation.presenters.impl.BankListPresenterImpl;
import com.webinfotech.salonease.presentation.ui.adapters.RefundBankListAdapter;
import com.webinfotech.salonease.presentation.ui.dialogs.BankDetailsDialog;
import com.webinfotech.salonease.threading.MainThreadImpl;
import com.webinfotech.salonease.util.Helper;

public class BankListActivity extends AppCompatActivity implements BankListPresenter.View, BankDetailsDialog.Callback {

    BankDetailsDialog bankDetailsDialog;
    BankListPresenterImpl mPresenter;
    ProgressDialog progressDialog;
    @BindView(R.id.recycler_view_bank_list)
    RecyclerView recyclerViewBankList;
    @BindView(R.id.txt_view_add_account)
    TextView txtViewAddAccount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_bank_list);
        ButterKnife.bind(this);
        getSupportActionBar().setTitle("Add Bank Account");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setUpBankDetailsDialog();
        initialisePresenter();
        setUpProgressDialog();
    }

    private void initialisePresenter() {
        mPresenter = new BankListPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    private void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @Override
    public void onSubmitClicked(String name, String bankName, String ifscCode, String branchName, String accountNo) {
        mPresenter.addBankAccount(name, bankName, ifscCode, branchName, accountNo);
    }

    @Override
    public void onUpdateClicked(String name, String bankName, String ifscCode, String branchName, String accountNo) {
        mPresenter.updateBankAccount(name, bankName, ifscCode, branchName, accountNo);
    }

    private void setUpBankDetailsDialog() {
        bankDetailsDialog = new BankDetailsDialog(this, this, this);
        bankDetailsDialog.setUpDialog();
    }

    @OnClick(R.id.txt_view_add_account) void onAddAccountClicked() {
        bankDetailsDialog.showDialog();
    }

    @Override
    public void loadData(RefundBankListAdapter adapter) {
        txtViewAddAccount.setVisibility(View.GONE);
        recyclerViewBankList.setAdapter(adapter);
        recyclerViewBankList.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public void setData(BankList bankList) {
        bankDetailsDialog.setData("", bankList.bankName, bankList.ifscCode, bankList.branchName, bankList.accountNo);
    }

    @Override
    public void hideBankAddBtn() {
        txtViewAddAccount.setVisibility(View.VISIBLE);
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public void showLoginBottomSheet() {
        Helper.showLoginBottomSheet(getSupportFragmentManager());
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.fetchBankList();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}