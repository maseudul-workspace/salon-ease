package com.webinfotech.salonease.presentation.ui.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.provider.ContactsContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.webinfotech.salonease.R;
import com.webinfotech.salonease.domain.models.ComboServiceData;
import com.webinfotech.salonease.domain.models.SalonImages;
import com.webinfotech.salonease.presentation.ui.adapters.ImageGalleryAdapter;
import com.webinfotech.salonease.presentation.ui.dialogs.ImagePreviewDialog;

public class GalleryListFragment extends Fragment implements ImageGalleryAdapter.Callback{

    @BindView(R.id.recycler_view_gallery)
    RecyclerView recyclerViewGallery;
    SalonImages[] salonImages;
    ImagePreviewDialog imagePreviewDialog;

    public GalleryListFragment() {
        // Required empty public constructor
    }

    public static GalleryListFragment newInstance(String param1, String param2) {
        GalleryListFragment fragment = new GalleryListFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_gallery_list, container, false);
        ButterKnife.bind(this, view);
        if (savedInstanceState != null) {
            salonImages = new Gson().fromJson(savedInstanceState.getString("images"), SalonImages[].class);
        }
        setImagePreviewDialog();
        ImageGalleryAdapter adapter = new ImageGalleryAdapter(getContext(), salonImages, this);
        recyclerViewGallery.setAdapter(adapter);
        recyclerViewGallery.setLayoutManager(new GridLayoutManager(getContext(), 2));
        return view;
    }

    private void setImagePreviewDialog() {
        imagePreviewDialog = new ImagePreviewDialog(getContext(), getActivity());
        imagePreviewDialog.setUpDialog();
    }

    public void setRecyclerViewGallery(SalonImages[] salonImages) {
        this.salonImages = salonImages;
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("images", new Gson().toJson(salonImages));
    }

    @Override
    public void onImageClicked(String image) {
        imagePreviewDialog.setImage(image);
        imagePreviewDialog.showDialog();
    }
}