package com.webinfotech.salonease.presentation.ui.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import com.webinfotech.salonease.AndroidApplication;
import com.webinfotech.salonease.R;
import com.webinfotech.salonease.domain.executors.impl.ThreadExecutor;
import com.webinfotech.salonease.domain.models.SalonUser;
import com.webinfotech.salonease.presentation.presenters.TopSalonsPresenter;
import com.webinfotech.salonease.presentation.presenters.impl.TopSalonsPresenterImpl;
import com.webinfotech.salonease.presentation.ui.adapters.SalonListAdapter;
import com.webinfotech.salonease.presentation.ui.dialogs.CustomProgressDialog;
import com.webinfotech.salonease.threading.MainThreadImpl;

import org.jetbrains.annotations.NotNull;

public class TopSalonsActivity extends AppCompatActivity implements TopSalonsPresenter.View, SalonListAdapter.Callback {

    TopSalonsPresenterImpl mPresenter;
    CustomProgressDialog customProgressDialog;
    @BindView(R.id.recycler_view_top_salons)
    RecyclerView recyclerViewTopSalons;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_top_salons);
        ButterKnife.bind(this);
        initialisePresenter();
        setUpProgressDialog();
        AndroidApplication androidApplication = (AndroidApplication) getApplicationContext();
        mPresenter.fetchTopSalons(androidApplication.getLatitude(), androidApplication.getLongitude());
    }

    private void initialisePresenter() {
        mPresenter = new TopSalonsPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    private void setUpProgressDialog() {
        customProgressDialog = new CustomProgressDialog(this, this);
        customProgressDialog.setUpDialog();
    }

    @Override
    public void loadDate(SalonUser[] salonUsers) {
        SalonListAdapter salonListAdapter = new SalonListAdapter(this, salonUsers, this);
        recyclerViewTopSalons.setAdapter(salonListAdapter);
        recyclerViewTopSalons.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public void showLoader() {
        customProgressDialog.showDialog();
    }

    @Override
    public void hideLoader() {
        customProgressDialog.dismissDialog();
    }

    @Override
    public void onSalonClicked(int salonId) {
        Intent intent = new Intent(this, SalonDetailsActivity.class);
        intent.putExtra("salonId", salonId);
        startActivity(intent);
    }

    @OnClick(R.id.btn_back) void onBackClicked()
    {
        finish();
    }

}