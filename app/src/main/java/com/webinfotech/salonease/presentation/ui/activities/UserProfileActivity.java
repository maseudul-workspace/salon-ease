package com.webinfotech.salonease.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.webinfotech.salonease.R;
import com.webinfotech.salonease.domain.executors.impl.ThreadExecutor;
import com.webinfotech.salonease.domain.models.UserInfo;
import com.webinfotech.salonease.presentation.presenters.UserProfilePresenter;
import com.webinfotech.salonease.presentation.presenters.impl.UserProfilePresenterImpl;
import com.webinfotech.salonease.threading.MainThreadImpl;
import com.webinfotech.salonease.util.GlideHelper;

public class UserProfileActivity extends AppCompatActivity implements UserProfilePresenter.View {

    @BindView(R.id.edit_text_name)
    EditText editTextName;
    @BindView(R.id.edit_text_phone)
    EditText editTextPhone;
    @BindView(R.id.edit_text_email)
    EditText editTextEmail;
    @BindView(R.id.edit_text_state)
    EditText editTextState;
    @BindView(R.id.edit_text_city)
    EditText editTextCity;
    @BindView(R.id.edit_text_pin)
    EditText editTextPin;
    @BindView(R.id.edit_text_address)
    EditText editTextAddress;
    @BindView(R.id.radio_group_gender)
    RadioGroup radioGroupGender;
    @BindView(R.id.radio_btn_male)
    RadioButton radioButtonMale;
    @BindView(R.id.radio_btn_female)
    RadioButton radioButtonFemale;
    UserProfilePresenterImpl mPresenter;
    ProgressDialog progressDialog;
    String gender = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);
        ButterKnife.bind(this);
        initialisePresenter();
        setUpProgressDialog();
        setRadioGroupGender();
        mPresenter.fetchUserProfile();
    }

    private void initialisePresenter() {
        mPresenter = new UserProfilePresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this,this);
    }

    @Override
    public void loadData(UserInfo userInfo) {
        editTextName.setText(userInfo.name);
        editTextEmail.setText(userInfo.email);
        editTextAddress.setText(userInfo.address);
        editTextCity.setText(userInfo.city);
        editTextPhone.setText(userInfo.mobile);
        editTextPin.setText(userInfo.pin);
        editTextState.setText(userInfo.state);
        if (gender != null) {
            if (gender.equals("M")) {
                radioButtonMale.setChecked(true);
            } else {
                radioButtonFemale.setChecked(true);
            }
        }
    }

    private void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    private void setRadioGroupGender() {
        radioGroupGender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton rb=(RadioButton)findViewById(checkedId);
                gender = rb.getText().toString();
                if (gender.equals("Male")) {
                    gender = "M";
                } else {
                    gender = "F";
                }
            }
        });
    }

    @OnClick(R.id.btn_submit) void onSubmitClicked() {
        if (    editTextName.getText().toString().trim().isEmpty() ||
                editTextState.getText().toString().trim().isEmpty() ||
                editTextCity.getText().toString().trim().isEmpty() ||
                editTextPin.getText().toString().trim().isEmpty() ||
                editTextEmail.getText().toString().trim().isEmpty() ||
                editTextAddress.getText().toString().trim().isEmpty()
        ) {
            Toasty.warning(this, "Please fill all the fields").show();
        } else if (gender == null) {
            Toasty.warning(this, "Please enter your gender", Toast.LENGTH_LONG).show();
        } else {
            mPresenter.updateProfile(editTextName.getText().toString(), editTextPhone.getText().toString(), editTextState.getText().toString(), editTextCity.getText().toString(), gender, editTextAddress.getText().toString(), editTextPin.getText().toString(), editTextEmail.getText().toString());
        }
    }

    @Override
    public void onProfileUpdateSuccess() {
        finish();
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @OnClick(R.id.img_view_back) void onBackClicked() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

}