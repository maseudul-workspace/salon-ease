package com.webinfotech.salonease.presentation.presenters;

import com.webinfotech.salonease.domain.models.Category;
import com.webinfotech.salonease.domain.models.Subcategory;
import com.webinfotech.salonease.domain.models.ThirdCategory;

public interface EnquiryActivityPresenter {
    void fetchCategories();
    void fetchSubCategories(int categoryId);
    void fetchThirdCategories(int subcategoryId);
    void sendMessage(int categoryId,
                     int subcategoryId,
                     int thirdcategoryId,
                    String name,
                    String mobile,
                    String message,
                    String booking_date
    );
    interface View {
        void loadMaincategories(Category[] categories);
        void loadSubCategories(Subcategory[] subcategories);
        void loadThirdCategories(ThirdCategory[] thirdcategories);
        void showLoader();
        void showAnimationSuccess();
        void hideLoader();
        void loginError();
    }
}
