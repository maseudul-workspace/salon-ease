package com.webinfotech.salonease.presentation.presenters.impl;

import android.content.Context;
import android.graphics.Color;
import android.widget.Toast;

import com.webinfotech.salonease.AndroidApplication;
import com.webinfotech.salonease.R;
import com.webinfotech.salonease.domain.executors.Executor;
import com.webinfotech.salonease.domain.executors.MainThread;
import com.webinfotech.salonease.domain.interactors.FetchAppLoadDataInteractor;
import com.webinfotech.salonease.domain.interactors.FetchCityListInteractor;
import com.webinfotech.salonease.domain.interactors.FetchOffersInteractor;
import com.webinfotech.salonease.domain.interactors.MessageListInteractor;
import com.webinfotech.salonease.domain.interactors.UpdateFirebaseInteractor;
import com.webinfotech.salonease.domain.interactors.impl.FetchAppLoadDataInteractorImpl;
import com.webinfotech.salonease.domain.interactors.impl.FetchCityListInteractorImpl;
import com.webinfotech.salonease.domain.interactors.impl.FetchOffersInteractorImpl;
import com.webinfotech.salonease.domain.interactors.impl.MessageListInteractorImpl;
import com.webinfotech.salonease.domain.interactors.impl.UpdateFirebaseInteractorImpl;
import com.webinfotech.salonease.domain.models.AppLoadData;
import com.webinfotech.salonease.domain.models.Category;
import com.webinfotech.salonease.domain.models.City;
import com.webinfotech.salonease.domain.models.MessageData;
import com.webinfotech.salonease.domain.models.OffersData;
import com.webinfotech.salonease.domain.models.SalonUser;
import com.webinfotech.salonease.domain.models.SalonUserContainer;
import com.webinfotech.salonease.domain.models.UserInfo;
import com.webinfotech.salonease.domain.models.testing.CategoryContainer;
import com.webinfotech.salonease.presentation.presenters.MainPresenter;
import com.webinfotech.salonease.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.salonease.presentation.ui.adapters.CategoryViewpagerAdapter;
import com.webinfotech.salonease.presentation.ui.adapters.FreelancerAdapter;
import com.webinfotech.salonease.presentation.ui.adapters.HomeSlidingImagesAdapter;
import com.webinfotech.salonease.presentation.ui.adapters.TopSalonViewpagerAdapter;
import com.webinfotech.salonease.repository.AppRepositoryImpl;

import java.util.ArrayList;

import es.dmoral.toasty.Toasty;

public class MainPresenterImpl extends AbstractPresenter implements MainPresenter,
                                                                    FetchAppLoadDataInteractor.Callback,
                                                                    FetchCityListInteractor.Callback,
                                                                    UpdateFirebaseInteractor.Callback,
                                                                    FetchOffersInteractor.Callback,
                                                                    MessageListInteractor.Callback

{

    Context mContext;
    MainPresenter.View mView;

    public MainPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchAppData(double latitude, double longitude) {
        FetchAppLoadDataInteractorImpl fetchAppLoadDataInteractor = new FetchAppLoadDataInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, latitude, longitude);
        fetchAppLoadDataInteractor.execute();
        mView.showLoader();
    }

    @Override
    public void fetchCities() {
        FetchCityListInteractorImpl fetchCityListInteractor = new FetchCityListInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this);
        fetchCityListInteractor.execute();
    }

    @Override
    public void updateFirebaseToken(String token) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo user = androidApplication.getUserInfo(mContext);
        if (user == null) {
        } else {
            UpdateFirebaseInteractorImpl updateFirebaseInteractor = new UpdateFirebaseInteractorImpl(mExecutor, mMainThread, this, new AppRepositoryImpl(), user.apiToken, user.userId, token);
            updateFirebaseInteractor.execute();
        }
    }

    @Override
    public void fetchOffers() {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo user = androidApplication.getUserInfo(mContext);
        if (user != null && user.isRegistered == 2) {
            FetchOffersInteractorImpl fetchOffersInteractor = new FetchOffersInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, user.apiToken);
            fetchOffersInteractor.execute();
        }
    }

    @Override
    public void onAppLoadDataFetchSuccess(AppLoadData appLoadData) {
        mView.loadData(appLoadData);
        mView.hideLoader();
    }

    @Override
    public void onAppLoadDataFetchFail(String errorMsg) {
        mView.hideLoader();
        Toasty.custom(mContext, "Please check your internet connection", mContext.getResources().getDrawable(R.drawable.ic_baseline_error_outline_24),  Color.parseColor("#a58430"), Color.parseColor("#FFFFFF"), Toast.LENGTH_LONG, true, true).show();
    }

    @Override
    public void onGettingCityListSuccess(City[] cities) {
        mView.loadCities(cities);
    }

    @Override
    public void onGettingCityListFail(String errorMsg) {

    }

    @Override
    public void onFirebaseTokenUpdateSuccess() {

    }

    @Override
    public void onFirebaseTokenUpdateFail() {

    }

    @Override
    public void onOffersFetchSuccess(OffersData offersData) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        androidApplication.setOffersData(offersData);
    }

    @Override
    public void onOffersFetchFail(String errorMsg) {

    }

    @Override
    public void onMessageListFetchSuccess(MessageData[] messageData, int totalPage) {
        mView.loadMessage(messageData);
        mView.hideLoader();

    }

    @Override
    public void onMessageListFetchFail(String errorMsg, int loginError) {
        mView.hideLoader();
        if(loginError == 1)
        {
            AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
            androidApplication.setUserInfo(mContext,null);
            mView.loginError();
        }
        else
        {
            Toasty.warning(mContext,errorMsg,Toasty.LENGTH_SHORT).show();
        }
    }

    @Override
    public void fetchMessage() {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo user = androidApplication.getUserInfo(mContext);
        if (user != null) {
            MessageListInteractorImpl messageListInteractorImpl = new MessageListInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, user.apiToken, 1);
            messageListInteractorImpl.execute();
        }
    }
}
