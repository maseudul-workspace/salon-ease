package com.webinfotech.salonease.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.webinfotech.salonease.AndroidApplication;
import com.webinfotech.salonease.R;
import com.webinfotech.salonease.domain.models.UserInfo;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class PaymentResponseActivity extends AppCompatActivity {

    @BindView(R.id.layout_success_payment)
    View layoutSuccess;
    @BindView(R.id.layout_failed_payment)
    View layoutFailed;
    @BindView(R.id.txt_view_order_id)
    TextView txtViewOrderId;
    @BindView(R.id.txt_view_order_date)
    TextView txtViewOrderDate;
    @BindView(R.id.txt_view_transaction_id)
    TextView txtViewPaymentId;
    @BindView(R.id.txt_view_amount)
    TextView txtViewAmount;
    @BindView(R.id.txt_view_name)
    TextView txtViewName;
    String orderId;
    int orderStatus;
    String transactionId;
    String amount;
    String paymentId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_response);
        ButterKnife.bind(this);
        orderId = getIntent().getStringExtra("orderId");
        paymentId = getIntent().getStringExtra("paymentId");
        orderStatus = getIntent().getIntExtra("orderStatus", 0);
        transactionId = getIntent().getStringExtra("transactionId");
        amount = getIntent().getStringExtra("amount");
        setData();
    }

    private void setData() {
        txtViewOrderId.setText(orderId);
        txtViewOrderDate.setText(getDate());
        txtViewAmount.setText("Rs. " + amount);
        txtViewPaymentId.setText(paymentId);
        AndroidApplication androidApplication = (AndroidApplication) getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(this);
        txtViewName.setText(userInfo.name);
        if (orderStatus == 1) {
            layoutSuccess.setVisibility(View.VISIBLE);
        } else {
            layoutFailed.setVisibility(View.VISIBLE);
        }
    }

    public String getDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat(
                "MMMM dd, yyyy", Locale.getDefault());
        Date date = new Date();
        return dateFormat.format(date);
    }

    @OnClick(R.id.btn_order_history) void onOrderHistoryClicked() {
        Intent intent = new Intent(this, OrderHistoryActivity.class);
        startActivity(intent);
    }

}