package com.webinfotech.salonease.presentation.ui.dialogs;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.webinfotech.salonease.AndroidApplication;
import com.webinfotech.salonease.R;
import com.webinfotech.salonease.domain.models.Coupon;
import com.webinfotech.salonease.presentation.ui.fragments.CartFragment;
import com.webinfotech.salonease.presentation.ui.fragments.ServiceListFragment;


public class CouponDialogFragment extends DialogFragment {

    public interface Callback {
        void onCouponApplyClicked();
    }

    @BindView(R.id.txt_view_coupon_desc)
    TextView txtViewCouponDesc;
    @BindView(R.id.txt_view_save_amt)
    TextView txtViewSaveAmt;
    Callback mCallback;

    public CouponDialogFragment() {
        // Required empty public constructor
    }

    // TODO: Rename and change types and number of parameters
    public static CouponDialogFragment newInstance(String param1, String param2) {
        CouponDialogFragment fragment = new CouponDialogFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            mCallback = (Callback) getParentFragment();
        } catch (ClassCastException e) {
            throw new ClassCastException("Calling fragment must implement Callback interface");
        }
        setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Material_NoActionBar_Fullscreen);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_coupon_dialog, container, false);
        ButterKnife.bind(this, view);
        setData();
        return view;
    }

    private void setData() {
        AndroidApplication androidApplication = (AndroidApplication) getContext().getApplicationContext();
        Coupon coupon = androidApplication.getOffersData().coupon;
        txtViewCouponDesc.setText(coupon.name);
        txtViewSaveAmt.setText(coupon.amount + "% OFF On Total Amount");
    }

    @Override
    public void onActivityCreated(Bundle arg0) {
        super.onActivityCreated(arg0);
        getDialog().getWindow()
                .getAttributes().windowAnimations = R.style.DialogAnimation;
    }

    @OnClick(R.id.btn_apply_coupon) void onApplyCouponClicked() {
        mCallback.onCouponApplyClicked();
        dismiss();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (getParentFragment() instanceof ServiceListFragment.Callback)
        {
            mCallback = (Callback) getParentFragment();
        }
        if (context instanceof Callback)
        {
            mCallback = (Callback)context;
        }
    }

}