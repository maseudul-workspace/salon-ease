package com.webinfotech.salonease.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import com.webinfotech.salonease.R;
import com.webinfotech.salonease.domain.executors.impl.ThreadExecutor;
import com.webinfotech.salonease.presentation.presenters.AddressListPresenter;
import com.webinfotech.salonease.presentation.presenters.impl.AddressListPresenterImpl;
import com.webinfotech.salonease.presentation.ui.adapters.ShippingAddressAdapter;
import com.webinfotech.salonease.threading.MainThreadImpl;
import com.webinfotech.salonease.util.Helper;

public class AddressListActivity extends AppCompatActivity implements AddressListPresenter.View {

    @BindView(R.id.recycler_view_address)
    RecyclerView recyclerViewAddress;
    AddressListPresenterImpl mPresenter;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_address_list);
        ButterKnife.bind(this);
        initialisePresenter();
        setUpProgressDialog();
    }

    private void initialisePresenter() {
        mPresenter = new AddressListPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    private void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @Override
    public void loadAdapter(ShippingAddressAdapter adapter) {
        recyclerViewAddress.setVisibility(View.VISIBLE);
        recyclerViewAddress.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewAddress.setAdapter(adapter);
    }

    @Override
    public void goToAddressDetails(int addressId) {
        Intent intent = new Intent(this, EditAddressActivity.class);
        intent.putExtra("addressId", addressId);
        startActivity(intent);
    }

    @Override
    public void onAddressFetchFailed() {
        recyclerViewAddress.setVisibility(View.GONE);
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public void showLoginBottomSheet() {
        Helper.showLoginBottomSheet(getSupportFragmentManager());
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.fetchShippingAddress();
    }

    @OnClick(R.id.txt_view_add_address) void onAddAddressClicked() {
        Intent intent = new Intent(this, AddAddressActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.btn_back) void onBackClicked()
    {
        finish();
    }

}