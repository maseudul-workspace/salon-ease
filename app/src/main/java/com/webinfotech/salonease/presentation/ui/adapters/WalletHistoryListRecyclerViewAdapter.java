package com.webinfotech.salonease.presentation.ui.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.webinfotech.salonease.R;
import com.webinfotech.salonease.domain.models.WalletResponseData;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import butterknife.BindView;
import butterknife.ButterKnife;

public class WalletHistoryListRecyclerViewAdapter extends RecyclerView.Adapter<WalletHistoryListRecyclerViewAdapter.ViewHolder> {

    Context mContext;
    WalletResponseData[] walletResponseData;


    public WalletHistoryListRecyclerViewAdapter(Context mContext, WalletResponseData[] walletResponseData) {
        this.mContext = mContext;
        this.walletResponseData = walletResponseData;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
           View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycle_view_wallet_history_list_item, parent, false);
           ViewHolder holder = new ViewHolder(view);
           return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {


        if (position > 0 && convertDatetoYear(walletResponseData[position].created_at).equals(convertDatetoYear(walletResponseData[position - 1].created_at))){
            //make sure it is not the first one, and make sure it has the same ID as previous.
            holder.txtViewYear.setVisibility(View.GONE); //hide it, you need to set the reference first.
        }else holder.txtViewYear.setVisibility(View.VISIBLE);
        //if somehow order gets changed.(e.g. previous row is deleted).


        holder.txtViewYear.setText(convertDatetoYear(walletResponseData[position].created_at));
        holder.txtViewDay.setText(convertDateToDay(walletResponseData[position].created_at));
        holder.txtViewMonth.setText(convertDatetoMonth(walletResponseData[position].created_at));
        holder.txtViewComment.setText(walletResponseData[position].comment);
        if(walletResponseData[position].transaction_type.equals("1")) {
            holder.txtViewStatus.setText("Debited");
            holder.txtViewStatus.setTextColor(Color.parseColor("#c7251a"));
        } else if(walletResponseData[position].transaction_type.equals("2")) {
            holder.txtViewStatus.setText("Credited");
            holder.txtViewStatus.setTextColor(Color.parseColor("#2E7D32"));
        }

        holder.txtViewAmount.setText("₹ "+walletResponseData[position].amount);
    }

    @Override
    public int getItemCount() {
        return walletResponseData.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.text_view_day)
        TextView txtViewDay;
        @BindView(R.id.txt_view_status)
        TextView txtViewStatus;
        @BindView(R.id.txt_view_year)
        TextView txtViewYear;
        @BindView(R.id.text_view_wallet_history_list_amount)
        TextView txtViewAmount;
        @BindView(R.id.text_view_comment)
        TextView txtViewComment;
        @BindView(R.id.text_view_month)
        TextView txtViewMonth;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void updateData(WalletResponseData[] walletResponseData) {
        this.walletResponseData = walletResponseData;
        notifyDataSetChanged();
    }

    private String convertDateToDay(String date)
    {
        SimpleDateFormat spf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date newDate= null;
        try {
            newDate = spf.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        spf= new SimpleDateFormat("dd");
        date = spf.format(newDate);
        return date;
    }

    private String convertDatetoMonth(String date)
    {
        SimpleDateFormat spf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date newDate= null;
        try {
            newDate = spf.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        spf= new SimpleDateFormat("MMM");
        date = spf.format(newDate);
        return date;
    }

    private String convertDatetoYear(String date)
    {
        SimpleDateFormat spf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date newDate= null;
        try {
            newDate = spf.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        spf= new SimpleDateFormat("yyyy");
        date = spf.format(newDate);
        return date;
    }
}
