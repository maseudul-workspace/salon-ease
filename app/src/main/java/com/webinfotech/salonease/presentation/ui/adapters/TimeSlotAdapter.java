package com.webinfotech.salonease.presentation.ui.adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.GradientDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.webinfotech.salonease.R;
import com.webinfotech.salonease.domain.models.TimeSlots;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class TimeSlotAdapter extends RecyclerView.Adapter<TimeSlotAdapter.ViewHolder> {

    public interface Callback {
        void onTimeSlotSelected(int id);
    }

    Context mContext;
    ArrayList<TimeSlots> timeSlots;
    Callback mCallback;

    public TimeSlotAdapter(Context mContext, ArrayList<TimeSlots> timeSlots, Callback mCallback) {
        this.mContext = mContext;
        this.timeSlots = timeSlots;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_time_slots, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewTimeSlot.setText(changeTimeFormat(timeSlots.get(position).timeSlot));
        Drawable background = holder.txtViewTimeSlot.getBackground();
        GradientDrawable gradientDrawable = (GradientDrawable) background;
        if (timeSlots.get(position).isTimeSelected) {
            gradientDrawable.setColor(mContext.getResources().getColor(R.color.colorAccent));
        } else {
            gradientDrawable.setColor(mContext.getResources().getColor(R.color.md_grey_900));
        }
        holder.txtViewTimeSlot.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onTimeSlotSelected(timeSlots.get(position).id);
            }
        });
    }

    @Override
    public int getItemCount() {
        return timeSlots.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_time_slot)
        TextView txtViewTimeSlot;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void updateData(ArrayList<TimeSlots> timeSlots) {
        this.timeSlots = timeSlots;
        notifyDataSetChanged();
    }

    public String changeTimeFormat(String time) {
        String inputPattern = "HH:mm:ss";
        String outputPattern = "h:mm a";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

}
