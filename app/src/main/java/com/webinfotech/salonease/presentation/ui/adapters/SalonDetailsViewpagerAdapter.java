package com.webinfotech.salonease.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.webinfotech.salonease.R;
import com.webinfotech.salonease.domain.models.SalonImages;
import com.webinfotech.salonease.util.GlideHelper;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

public class SalonDetailsViewpagerAdapter extends PagerAdapter  {

    Context mContext;
    SalonImages[] salonImages;

    public SalonDetailsViewpagerAdapter(Context mContext, SalonImages[] salonImages) {
        this.mContext = mContext;
        this.salonImages = salonImages;
    }

    @Override
    public int getCount() {
        return salonImages.length;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.layout_viewpager_salon_details, container, false);
        ImageView imageView = (ImageView) layout.findViewById(R.id.img_view_viewpager);
        GlideHelper.setImageViewCustomRoundedCorners(mContext, imageView, mContext.getResources().getString(R.string.base_url) + "images/client/" + salonImages[position].image, 10);
        container.addView(layout);
        return layout;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }

}
