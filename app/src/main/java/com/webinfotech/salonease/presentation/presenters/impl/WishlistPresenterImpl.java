package com.webinfotech.salonease.presentation.presenters.impl;

import android.content.Context;

import com.webinfotech.salonease.AndroidApplication;
import com.webinfotech.salonease.domain.executors.Executor;
import com.webinfotech.salonease.domain.executors.MainThread;
import com.webinfotech.salonease.domain.interactors.FetchWishlistInteractor;
import com.webinfotech.salonease.domain.interactors.impl.FetchWishlistInteractorImpl;
import com.webinfotech.salonease.domain.models.SalonUser;
import com.webinfotech.salonease.domain.models.UserInfo;
import com.webinfotech.salonease.domain.models.Wishlist;
import com.webinfotech.salonease.presentation.presenters.WishlistPresenter;
import com.webinfotech.salonease.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.salonease.repository.AppRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class WishlistPresenterImpl extends AbstractPresenter implements WishlistPresenter, FetchWishlistInteractor.Callback {

    Context mContext;
    WishlistPresenter.View mView;

    public WishlistPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchWishlist() {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo != null) {
            FetchWishlistInteractorImpl fetchWishlistInteractor = new FetchWishlistInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken);
            fetchWishlistInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void onFetchWishlistSuccess(Wishlist[] wishlists) {
        SalonUser[] salonUsers = new SalonUser[wishlists.length];
        for (int i = 0; i < wishlists.length; i++) {
            salonUsers[i] = wishlists[i].salonUser;
        }
        mView.loadData(salonUsers);
        mView.hideLoader();
    }

    @Override
    public void onFetchWishlistFail(String errorMsg, int loginError) {
        Toasty.warning(mContext, errorMsg).show();
        mView.hideLoader();
    }
}
