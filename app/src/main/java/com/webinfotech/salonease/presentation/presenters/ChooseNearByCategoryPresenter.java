package com.webinfotech.salonease.presentation.presenters;

import com.webinfotech.salonease.domain.models.AppLoadData;
import com.webinfotech.salonease.domain.models.Category;
import com.webinfotech.salonease.domain.models.City;
import com.webinfotech.salonease.domain.models.Subcategory;

public interface ChooseNearByCategoryPresenter {

    void fetchCategories();
    void fetchSubcategories(int categoryId);
    void fetchCities();
    interface View {
        void loadMaincategories(Category[] categories);
        void loadSubcategories(Subcategory[] subcategories);
        void loadCities(City[] cities);
        void showLoader();
        void hideLoader();
    }
}
