package com.webinfotech.salonease.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.webinfotech.salonease.R;
import com.webinfotech.salonease.domain.models.ComboService;
import com.webinfotech.salonease.domain.models.ComboServiceData;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class CombosAdapter extends RecyclerView.Adapter<CombosAdapter.ViewHolder> {

    public interface Callback {
        void onComboSelected(int position);
        void onComboRemoved(int position);
    }

    Context mContext;
    ComboServiceData[] comboServiceData;
    Callback mCallback;

    public CombosAdapter(Context mContext, ComboServiceData[] comboServiceData, Callback mCallback) {
        this.mContext = mContext;
        this.comboServiceData = comboServiceData;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_combos, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewComboName.setText(comboServiceData[position].comboName);
        holder.txtViewPrice.setText("Rs. " + comboServiceData[position].price);
        ComboServiceAdapter adapter = new ComboServiceAdapter(mContext, comboServiceData[position].comboServices);
        holder.recyclerViewCombos.setAdapter(adapter);
        holder.recyclerViewCombos.setLayoutManager(new LinearLayoutManager(mContext));
        holder.txtViewAddNotSelected.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onComboSelected(position);
            }
        });
        holder.txtViewAddSelected.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onComboRemoved(position);
            }
        });
        if (comboServiceData[position].isComboSelected) {
            holder.txtViewAddSelected.setVisibility(View.VISIBLE);
            holder.txtViewAddNotSelected.setVisibility(View.GONE);
        } else {
            holder.txtViewAddNotSelected.setVisibility(View.VISIBLE);
            holder.txtViewAddSelected.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        return comboServiceData.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_price)
        TextView txtViewPrice;
        @BindView(R.id.txt_view_combo_name)
        TextView txtViewComboName;
        @BindView(R.id.recycler_view_combos)
        RecyclerView recyclerViewCombos;
        @BindView(R.id.txt_view_add_selected)
        TextView txtViewAddSelected;
        @BindView(R.id.txt_view_add_not_selected)
        TextView txtViewAddNotSelected;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void setComboServiceData(ComboServiceData[] comboServiceData) {
        this.comboServiceData = comboServiceData;
        notifyDataSetChanged();
    }

}
