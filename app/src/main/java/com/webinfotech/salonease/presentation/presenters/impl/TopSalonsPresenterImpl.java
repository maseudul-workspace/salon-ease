package com.webinfotech.salonease.presentation.presenters.impl;

import android.content.Context;

import com.webinfotech.salonease.domain.executors.Executor;
import com.webinfotech.salonease.domain.executors.MainThread;
import com.webinfotech.salonease.domain.interactors.FetchTopSalonsInteractor;
import com.webinfotech.salonease.domain.interactors.impl.FetchTopSalonsInteractorImpl;
import com.webinfotech.salonease.domain.models.SalonUser;
import com.webinfotech.salonease.presentation.presenters.TopSalonsPresenter;
import com.webinfotech.salonease.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.salonease.repository.AppRepositoryImpl;

public class TopSalonsPresenterImpl extends AbstractPresenter implements TopSalonsPresenter, FetchTopSalonsInteractor.Callback {

    Context mContext;
    TopSalonsPresenter.View mView;

    public TopSalonsPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchTopSalons(double latitude, double longitude) {
        FetchTopSalonsInteractorImpl fetchTopSalonsinteractor = new FetchTopSalonsInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, latitude, longitude);
        fetchTopSalonsinteractor.execute();
        mView.showLoader();
    }

    @Override
    public void onFetchTopSalonsSuccess(SalonUser[] salonUsers) {
        mView.hideLoader();
        mView.loadDate(salonUsers);
    }

    @Override
    public void onFetchTopSalonsFail(String errorMsg) {
        mView.hideLoader();
    }
}
