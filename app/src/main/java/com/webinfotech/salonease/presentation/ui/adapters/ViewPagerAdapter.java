package com.webinfotech.salonease.presentation.ui.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

import com.webinfotech.salonease.R;
import com.webinfotech.salonease.domain.models.Slider;
import com.webinfotech.salonease.util.GlideHelper;


public class ViewPagerAdapter extends PagerAdapter {

    Context mContext;
    Slider[] sliders;

    public ViewPagerAdapter(Context mContext, Slider[] sliders) {
        this.mContext = mContext;
        this.sliders = sliders;
    }

    @Override
    public int getCount() {
        return sliders.length;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }


    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.viewpager_item, container, false);
        ImageView imageView = layout.findViewById(R.id.item_imageView);
        GlideHelper.setImageView(mContext, imageView, mContext.getResources().getString(R.string.base_url) + "images/slider/thumb/" + sliders[position].image);
        container.addView(layout);
        return layout;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }
}
