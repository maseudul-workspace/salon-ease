package com.webinfotech.salonease.presentation.presenters.impl;

import android.content.Context;

import com.webinfotech.salonease.AndroidApplication;
import com.webinfotech.salonease.domain.executors.Executor;
import com.webinfotech.salonease.domain.executors.MainThread;
import com.webinfotech.salonease.domain.interactors.FetchAddressDetailsInteractor;
import com.webinfotech.salonease.domain.interactors.UpdateAddressInteractor;
import com.webinfotech.salonease.domain.interactors.impl.FetchAddressDetailsInteractorImpl;
import com.webinfotech.salonease.domain.interactors.impl.UpdateAddressInteractorImpl;
import com.webinfotech.salonease.domain.models.ShippingAddress;
import com.webinfotech.salonease.domain.models.UserInfo;
import com.webinfotech.salonease.presentation.presenters.EditAddressPresenter;
import com.webinfotech.salonease.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.salonease.repository.AppRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class EditAddressPresenterImpl extends AbstractPresenter implements EditAddressPresenter,
                                                                            FetchAddressDetailsInteractor.Callback,
                                                                            UpdateAddressInteractor.Callback
{

    Context mContext;
    EditAddressPresenter.View mView;
    FetchAddressDetailsInteractorImpl fetchShippingAddressDetailsInteractor;
    UpdateAddressInteractorImpl updateAddressInteractorImpl;

    public EditAddressPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchAddressDetails(int addressId) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo user = androidApplication.getUserInfo(mContext);
        if (user == null) {
            mView.showLoginBottomSheet();
            Toasty.warning(mContext, "Your Session Expired!! Please Login Again").show();
        } else {
            fetchShippingAddressDetailsInteractor = new FetchAddressDetailsInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, user.apiToken, user.userId, addressId);
            fetchShippingAddressDetailsInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void updateAddress(int addressId, String name, String email, String mobile, String city, String state, String pin, String address, double latitude, double longitude) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo user = androidApplication.getUserInfo(mContext);
        if (user == null) {
            Toasty.warning(mContext, "Your Session Expired!! Please Login Again").show();
        } else {
            updateAddressInteractorImpl = new UpdateAddressInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, user.apiToken, addressId, name, email, mobile, state, city, pin, address, latitude, longitude);
            updateAddressInteractorImpl.execute();
            mView.showLoader();
        }
    }

    @Override
    public void onGettingAddressDetailsSuccess(ShippingAddress shippingAddress) {
        mView.loadAddressDetails(shippingAddress);
        mView.hideLoader();
    }

    @Override
    public void onGettingAddressDetailsFail(String errorMsg, int loginError) {
        mView.hideLoader();
        if (loginError == 1) {
            mView.showLoginBottomSheet();
        }
        Toasty.warning(mContext, errorMsg).show();
    }

    @Override
    public void onUpdateAddressSuccess() {
        mView.hideLoader();
        Toasty.success(mContext, "Address Edited Successfully").show();
        mView.onAddressEditSuccess();
    }

    @Override
    public void onUpdateAddressFail(String errorMsg, int loginError) {
        mView.hideLoader();
        if (loginError == 1) {
            mView.showLoginBottomSheet();
        }
        Toasty.warning(mContext, errorMsg).show();
    }
}
