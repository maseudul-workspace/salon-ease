package com.webinfotech.salonease.presentation.presenters;

public interface ChangePasswordPresenter {
    void changePassword(String oldPassword, String newPassword);
    interface View {
        void showLoader();
        void hideLoader();
        void showLoginBottomSheet();
    }
}
