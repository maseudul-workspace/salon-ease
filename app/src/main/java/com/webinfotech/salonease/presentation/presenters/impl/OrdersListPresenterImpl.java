package com.webinfotech.salonease.presentation.presenters.impl;

import android.content.Context;
import android.util.Log;

import com.webinfotech.salonease.AndroidApplication;
import com.webinfotech.salonease.domain.executors.Executor;
import com.webinfotech.salonease.domain.executors.MainThread;
import com.webinfotech.salonease.domain.interactors.CancelOrderInteractor;
import com.webinfotech.salonease.domain.interactors.ChangeVendorInteractor;
import com.webinfotech.salonease.domain.interactors.FetchBankListInteractor;
import com.webinfotech.salonease.domain.interactors.FetchOrderHistoryInteractor;
import com.webinfotech.salonease.domain.interactors.SubmitReviewInteractor;
import com.webinfotech.salonease.domain.interactors.impl.CancelOrderInteractorImpl;
import com.webinfotech.salonease.domain.interactors.impl.ChangeVendorInteractorImpl;
import com.webinfotech.salonease.domain.interactors.impl.FetchBankListInteractorImpl;
import com.webinfotech.salonease.domain.interactors.impl.FetchOrderHistoryInteractorImpl;
import com.webinfotech.salonease.domain.interactors.impl.SubmitReviewInteractorImpl;
import com.webinfotech.salonease.domain.models.BankList;
import com.webinfotech.salonease.domain.models.Orders;
import com.webinfotech.salonease.domain.models.UserInfo;
import com.webinfotech.salonease.presentation.presenters.OrdersListPresenter;
import com.webinfotech.salonease.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.salonease.presentation.ui.adapters.OrdersAdapter;
import com.webinfotech.salonease.repository.AppRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class OrdersListPresenterImpl extends AbstractPresenter implements   OrdersListPresenter,
                                                                            FetchOrderHistoryInteractor.Callback,
                                                                            OrdersAdapter.Callback,
                                                                            FetchBankListInteractor.Callback,
                                                                            CancelOrderInteractor.Callback,
                                                                            SubmitReviewInteractor.Callback,
                                                                            ChangeVendorInteractor.Callback
{

    Context mContext;
    OrdersListPresenter.View mView;
    int orderId;
    int isRefund;
    int clientId;

    public OrdersListPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchOrderHistories() {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo user = androidApplication.getUserInfo(mContext);
        if (user == null) {
            mView.showLoginBottomSheet();
            Toasty.warning(mContext, "Your Session Expired!! Please Login Again").show();
        } else {
            FetchOrderHistoryInteractorImpl fetchOrderHistoryInteractor = new FetchOrderHistoryInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, user.userId, user.apiToken);
            fetchOrderHistoryInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void submitRating(int rating, String feedback) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo user = androidApplication.getUserInfo(mContext);
        if (user == null) {
            Toasty.warning(mContext, "Your Session Expired!! Please Login Again").show();
        } else {
            SubmitReviewInteractorImpl submitReviewInteractor = new SubmitReviewInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, user.apiToken, user.userId, clientId, rating, feedback);
            submitReviewInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void onGettingOrderHistorySuccess(Orders[] orders) {
        OrdersAdapter adapter = new OrdersAdapter(mContext, orders, this);
        mView.loadAdapter(adapter);
        mView.hideLoader();
    }

    @Override
    public void onGettingOrderHistoryFail(String errorMsg, int loginError) {
        mView.hideLoader();
        if (loginError == 1) {
           // mView.showLoginBottomSheet();
            Toasty.warning(mContext, "Your Session Expired!! Please Login Again").show();
        }
    }

    @Override
    public void onCancelClicked(int orderId, int isRefund) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo user = androidApplication.getUserInfo(mContext);
        if (user == null) {
            Toasty.warning(mContext, "Your Session Expired!! Please Login Again").show();
        } else {
            this.orderId = orderId;
            this.isRefund = isRefund;
            if (this.isRefund == 1) {
                mView.showLoader();
                cancelOrder(0);
            } else {
                FetchBankListInteractorImpl fetchBankListInteractor = new FetchBankListInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, user.userId, user.apiToken);
                fetchBankListInteractor.execute();
                mView.showLoader();
            }
        }
    }

    @Override
    public void onFeedBackClicked(int clientId) {
        this.clientId = clientId;
        mView.onFeedbackClicked();
    }

    @Override
    public void changeOrderStatus(int orderId, int status) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo user = androidApplication.getUserInfo(mContext);
        if (user == null) {
            Toasty.warning(mContext, "Your Session Expired!! Please Login Again").show();
        } else {
            ChangeVendorInteractorImpl changeVendorInteractor = new ChangeVendorInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, user.apiToken, orderId, status);
            changeVendorInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void onGettingBankListSuccess(BankList[] bankLists) {
        if (bankLists.length == 0) {
            mView.hideLoader();
            mView.showBankSnackbar();
        } else {
            cancelOrder(bankLists[0].id);
        }
    }

    private void cancelOrder(int bankId) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo user = androidApplication.getUserInfo(mContext);
        if (user == null) {
            Toasty.warning(mContext, "Your Session Expired!! Please Login Again").show();
        } else {
            CancelOrderInteractorImpl cancelOrderInteractor = new CancelOrderInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, user.apiToken, this.orderId, isRefund, bankId);
            cancelOrderInteractor.execute();
        }
    }

    @Override
    public void onGettingBankListFail(String errorMsg, int loginError) {
        mView.hideLoader();
        if (loginError == 1) {
            mView.showLoginBottomSheet();
        }
        Toasty.warning(mContext, errorMsg).show();
    }

    @Override
    public void onOrderCancelSuccess() {
        mView.hideLoader();
        fetchOrderHistories();
    }

    @Override
    public void onCancelOrderFailed(String errorMsg, int loginError) {
        mView.hideLoader();
        if (loginError == 1) {
            mView.showLoginBottomSheet();
        }
        Toasty.warning(mContext, errorMsg).show();
    }

    @Override
    public void onReviewSubmitSuccess() {
        mView.hideLoader();
        Toasty.success(mContext, "Review Submitted").show();
    }

    @Override
    public void onReviewSubmitFail(String errorMsg, int loginError) {
        mView.hideLoader();
        if (loginError == 1) {
            mView.showLoginBottomSheet();
        }
        Toasty.warning(mContext, errorMsg).show();
    }

    @Override
    public void onVendorChangeSuccess() {
        Toasty.success(mContext, "Success").show();
        fetchOrderHistories();
    }

    @Override
    public void onVendorChangeFail(String errorMsg, int loginError) {
        mView.hideLoader();
        if (loginError == 1) {
            mView.showLoginBottomSheet();
        }
        Toasty.warning(mContext, errorMsg).show();
    }
}
