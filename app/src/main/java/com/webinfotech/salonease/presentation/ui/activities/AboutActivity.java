package com.webinfotech.salonease.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.webinfotech.salonease.R;
import com.webinfotech.salonease.util.GlideHelper;

public class AboutActivity extends AppCompatActivity {

    @BindView(R.id.img_view_about)
    ImageView imgViewAbout;
    @BindView(R.id.txt_view_about)
    TextView txtViewAbout;
    @BindView(R.id.layout_refund_policy)
    View layoutRefundPolicy;
    @BindView(R.id.txt_view_disclaimer)
    TextView txtViewDisclaimer;
    @BindView(R.id.layout_privacy_policy)
    View layoutPrivacyPolicy;
    @BindView(R.id.layout_terms_condition)
    View layoutTermsCondition;
    @BindView(R.id.layout_faq)
    View layoutFaq;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        ButterKnife.bind(this);
        GlideHelper.setImageView(this, imgViewAbout, "https://qph.fs.quoracdn.net/main-qimg-1ad4a4ca6565989eed77555be4d6e86e.webp");
    }

    @OnClick(R.id.img_view_back) void onBackClicked() {
        onBackPressed();
    }

    @OnClick(R.id.layout_about_us_header) void onAboutUsClicked() {
        if (txtViewAbout.getVisibility() == View.VISIBLE) {
            txtViewAbout.setVisibility(View.GONE);
        } else {
            txtViewAbout.setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.layout_refund_cancellation_policy_header) void onLayoutRefundClicked() {
        if (layoutRefundPolicy.getVisibility() == View.VISIBLE) {
            layoutRefundPolicy.setVisibility(View.GONE);
        } else {
            layoutRefundPolicy.setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.layout_disclaimer_header) void onDisclaimerClicked() {
        if (txtViewDisclaimer.getVisibility() == View.VISIBLE) {
            txtViewDisclaimer.setVisibility(View.GONE);
        } else {
            txtViewDisclaimer.setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.layout_privacy_policy_header) void onPrivacyPolicyClicked() {
        if (layoutPrivacyPolicy.getVisibility() == View.VISIBLE){
            layoutPrivacyPolicy.setVisibility(View.GONE);
        } else {
            layoutPrivacyPolicy.setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.layout_terms_condition_header) void onermsConsitionClicked() {
        if (layoutTermsCondition.getVisibility() == View.VISIBLE) {
            layoutTermsCondition.setVisibility(View.GONE);
        } else {
            layoutTermsCondition.setVisibility(View.VISIBLE);
        }
    }

    @OnClick(R.id.layout_faq_header) void onLayoutFaqHeaderClicked() {
        if (layoutFaq.getVisibility() == View.VISIBLE) {
            layoutFaq.setVisibility(View.GONE);
        } else {
            layoutFaq.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

}