package com.webinfotech.salonease.presentation.presenters.impl;

import android.content.Context;

import com.webinfotech.salonease.domain.executors.Executor;
import com.webinfotech.salonease.domain.executors.MainThread;
import com.webinfotech.salonease.domain.interactors.FetchSalonDataInteractor;
import com.webinfotech.salonease.domain.interactors.impl.FetchSalonDataInteractorImpl;
import com.webinfotech.salonease.domain.models.SalonData;
import com.webinfotech.salonease.presentation.presenters.SalonListFragmentPresenter;
import com.webinfotech.salonease.presentation.presenters.SalonListPresenter;
import com.webinfotech.salonease.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.salonease.presentation.ui.adapters.SalonDataAdapter;
import com.webinfotech.salonease.repository.AppRepositoryImpl;

public class SalonListFragmentPresenterImpl extends AbstractPresenter implements SalonListFragmentPresenter, FetchSalonDataInteractor.Callback, SalonDataAdapter.Callback {

    Context mContext;
    SalonListFragmentPresenter.View mView;
    SalonData[] newSalonData;
    SalonDataAdapter adapter;

    public SalonListFragmentPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchSalon(int serviceCityId, int categoryId, int page, int type, double latitude, double longitude, int clientType, String priceFrom, String priceTo, int ac, int parking, int wifi, int music, int sortBy, String refresh) {
        if (refresh.equals("refresh")) {
            newSalonData = null;
        }
        FetchSalonDataInteractorImpl fetchSalonDataInteractor = new FetchSalonDataInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, serviceCityId, categoryId, page, type, latitude, longitude, clientType, priceFrom, priceTo, ac, parking, wifi, music, sortBy);
        fetchSalonDataInteractor.execute();
    }

    @Override
    public void onGettingSalonDataSuccess(SalonData[] salonData, int totalPage) {
        SalonData[] tempSalonData;
        tempSalonData = newSalonData;
        try {
            int len1 = tempSalonData.length;
            int len2 = salonData.length;
            newSalonData = new SalonData[len1 + len2];
            System.arraycopy(tempSalonData, 0, newSalonData, 0, len1);
            System.arraycopy(salonData, 0, newSalonData, len1, len2);
            adapter.updateDataSet(newSalonData);
        }catch (NullPointerException e){
            adapter = new SalonDataAdapter(mContext, salonData, this);
            mView.loadData(adapter, totalPage);
        }
        mView.hideProgressDialog();
    }

    @Override
    public void onGettingSalonDataFail(String errorMsg) {
        mView.hideProgressDialog();
    }

    @Override
    public void onSalonClicked(int salonId, int serviceId) {
        mView.goToSalonDetails(salonId, serviceId);
    }
}
