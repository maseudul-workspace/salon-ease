package com.webinfotech.salonease.presentation.presenters.impl;

import android.content.Context;

import com.webinfotech.salonease.AndroidApplication;
import com.webinfotech.salonease.domain.executors.Executor;
import com.webinfotech.salonease.domain.executors.MainThread;
import com.webinfotech.salonease.domain.interactors.FetchWalletHistoryInteractor;
import com.webinfotech.salonease.domain.interactors.PlaceOrderInteractor;
import com.webinfotech.salonease.domain.interactors.VerifyPaymentInteractor;
import com.webinfotech.salonease.domain.interactors.impl.FetchWalletHistoryInteractorImpl;
import com.webinfotech.salonease.domain.interactors.impl.PlaceOrderInteractorImpl;
import com.webinfotech.salonease.domain.interactors.impl.VerifyPaymentInteractorImpl;
import com.webinfotech.salonease.domain.models.PaymentDataMain;
import com.webinfotech.salonease.domain.models.UserInfo;
import com.webinfotech.salonease.domain.models.WalletResponse;
import com.webinfotech.salonease.domain.models.WalletResponseData;
import com.webinfotech.salonease.presentation.presenters.PaymentPresenter;
import com.webinfotech.salonease.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.salonease.repository.AppRepositoryImpl;

import java.util.ArrayList;

import es.dmoral.toasty.Toasty;

public class PaymentPresenterImpl extends AbstractPresenter implements  PaymentPresenter,
                                                                        PlaceOrderInteractor.Callback,
                                                                        VerifyPaymentInteractor.Callback,
                                                                        FetchWalletHistoryInteractor.Callback
{

    Context mContext;
    PaymentPresenter.View mView;

    public PaymentPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchWalletHistory() {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo != null && userInfo.isRegistered == 2) {
            FetchWalletHistoryInteractorImpl fetchWalletHistoryInteractor = new FetchWalletHistoryInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, 1);
            fetchWalletHistoryInteractor.execute();
        }
    }

    @Override
    public void placeOrder(int vendorId,
                           ArrayList<Integer> serviceIds,
                           ArrayList<Integer> quantities,
                           String serviceTime,
                           String addressId,
                           String offerId,
                           String offerJobId,
                           String couponId,
                           int isWallet) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo user = androidApplication.getUserInfo(mContext);
        if (user == null) {
            Toasty.warning(mContext, "Your Session Expired!! Please Login Again").show();
        } else {
            PlaceOrderInteractorImpl placeOrderInteractor = new PlaceOrderInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, user.apiToken, vendorId, serviceIds, quantities, serviceTime, addressId, offerId, offerJobId, couponId, isWallet);
            placeOrderInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void verifyPayment(String razorpayOrderId, String razorpayPaymentId, String razorpaySignature, int orderId) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo user = androidApplication.getUserInfo(mContext);
        if (user == null) {
            Toasty.warning(mContext, "Your Session Expired!! Please Login Again").show();
        } else {
            VerifyPaymentInteractorImpl verifyPaymentInteractor = new VerifyPaymentInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, user.apiToken, user.userId, razorpayOrderId, razorpayPaymentId, razorpaySignature, orderId);
            verifyPaymentInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void onPlaceOrderSuccess(PaymentDataMain paymentDataMain) {
        mView.hideLoader();
        mView.initiatePayment(paymentDataMain.paymentData, paymentDataMain.orderId);
    }

    @Override
    public void onPlaceOrderFail(String errorMsg, int loginError) {
        mView.hideLoader();
        Toasty.warning(mContext, errorMsg).show();
    }

    @Override
    public void onVerifyPaymentSuccess() {
        mView.hideLoader();
        mView.onPaymentVerificationSuccess();
    }

    @Override
    public void onVerifyPaymentFail(String errorMsg) {
        mView.hideLoader();
        mView.onPaymentVerificationSuccess();
    }

    @Override
    public void onWalletHistoryFetchSuccess(WalletResponse walletResponse, int totalPage) {
        mView.loadWalletAmount(walletResponse.walletAmount);
    }

    @Override
    public void onWalletHistoryFetchFail(String errorMsg, int loginError) {

    }
}
