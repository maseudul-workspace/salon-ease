package com.webinfotech.salonease.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.SearchView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AbsListView;

import com.webinfotech.salonease.R;
import com.webinfotech.salonease.domain.executors.impl.ThreadExecutor;
import com.webinfotech.salonease.presentation.presenters.SearchListPresenter;
import com.webinfotech.salonease.presentation.presenters.impl.SearchListPresenterImpl;
import com.webinfotech.salonease.presentation.ui.adapters.SalonListAdapter;
import com.webinfotech.salonease.threading.MainThreadImpl;

public class SearchActivity extends AppCompatActivity implements SearchListPresenter.View {

    SearchView searchView;
    Boolean isScrolling = false;
    Integer currentItems;
    Integer totalItems;
    Integer scrollOutItems;
    int pageNo = 1;
    int totalPage = 1;
    String searchKey;
    LinearLayoutManager layoutManager;
    SearchListPresenterImpl mPresenter;
    @BindView(R.id.recycler_view_search_list)
    RecyclerView recyclerViewSearchList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        ButterKnife.bind(this);
        initialisePresenter();
    }

    private void initialisePresenter() {
        mPresenter = new SearchListPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    @Override
    public void loadData(SalonListAdapter adapter, int totalPage) {
        this.totalPage = totalPage;
        layoutManager = new LinearLayoutManager(this);
        recyclerViewSearchList.setLayoutManager(layoutManager);
        recyclerViewSearchList.setAdapter(adapter);
        recyclerViewSearchList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if(newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL)
                {
                    isScrolling= true;
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                currentItems = layoutManager.getChildCount();
                totalItems  = layoutManager.getItemCount();
                scrollOutItems = layoutManager.findFirstCompletelyVisibleItemPosition();
                if(!recyclerView.canScrollVertically(1))
                {
                    if(pageNo < totalPage) {
                        isScrolling = false;
                        pageNo = pageNo + 1;
                        mPresenter.fetchSearchList(searchKey, 26.14, 91.79, pageNo, "");
                    }
                }
            }
        });
    }

    @Override
    public void goToSalonDetails(int salonId) {
        Intent intent = new Intent(this, SalonDetailsActivity.class);
        intent.putExtra("salonId", salonId);
        startActivity(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate( R.menu.search_menu, menu);

        MenuItem myActionMenuItem = menu.findItem( R.id.action_search);
        searchView = (SearchView) myActionMenuItem.getActionView();
        myActionMenuItem.expandActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                if(!query.isEmpty()) {
                    pageNo = 1;
                    totalPage = 1;
                    searchKey = query;
                    mPresenter.fetchSearchList(searchKey, 26.14, 91.79, pageNo, "refresh");
                }
                return false;
            }
            @Override
            public boolean onQueryTextChange(String s) {
                // UserFeedback.show( "SearchOnQueryTextChanged: " + s);
                if(!s.isEmpty()){
                    pageNo = 1;
                    totalPage = 1;
                    searchKey = s;
                    mPresenter.fetchSearchList(searchKey, 26.14, 91.79, pageNo, "refresh");
                }
                return false;
            }
        });
        return true;
    }

}