package com.webinfotech.salonease.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import com.webinfotech.salonease.domain.executors.Executor;
import com.webinfotech.salonease.domain.executors.MainThread;
import com.webinfotech.salonease.domain.interactors.RequestPasswordChangeInteractor;
import com.webinfotech.salonease.domain.interactors.ResetPasswordInteractor;
import com.webinfotech.salonease.domain.interactors.impl.RequestPasswordChangeInteractorImpl;
import com.webinfotech.salonease.domain.interactors.impl.ResetPasswordInteractorImpl;
import com.webinfotech.salonease.presentation.presenters.ForgetPasswordPresenter;
import com.webinfotech.salonease.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.salonease.repository.AppRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class ForgetPasswordPresenterImpl extends AbstractPresenter implements ForgetPasswordPresenter, RequestPasswordChangeInteractor.Callback {

    Context mContext;
    ForgetPasswordPresenter.View mView;

    public ForgetPasswordPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void requestPasswordChange(String mobile) {
        RequestPasswordChangeInteractorImpl requestPasswordChangeInteractor = new RequestPasswordChangeInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, mobile);
        requestPasswordChangeInteractor.execute();
        mView.showLoader();
    }

    @Override
    public void onRequestPasswordChangedSuccess() {
        mView.hideLoader();
        mView.onPasswordRequestSuccess();
    }

    @Override
    public void onRequestPasswordChangedFail(String errorMsg) {
        mView.hideLoader();
        Toasty.warning(mContext, errorMsg).show();
    }


}
