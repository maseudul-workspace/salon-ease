package com.webinfotech.salonease.presentation.ui.fragments;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.webinfotech.salonease.R;
import com.webinfotech.salonease.domain.models.ComboServiceData;
import com.webinfotech.salonease.domain.models.Service;
import com.webinfotech.salonease.domain.models.UserInfo;
import com.webinfotech.salonease.presentation.ui.adapters.FragmentsAdapter;

public class ServiceFragment extends Fragment implements ServiceListFragment.Callback, CombosFragment.Callback{

    @Override
    public void onComboSelect(int comboId) {
        mCallback.onComboSelected(comboId);
    }

    @Override
    public void onComboRemoved(int comboId) {
        mCallback.onComboRemoved(comboId);
    }

    @Override
    public void onServiceSelected(int serviceId) {
        mCallback.onServiceSelected(serviceId);
    }

    @Override
    public void onServiceRemoved(int serviceId) {
        mCallback.onServiceRemoved(serviceId);
    }

    public interface Callback {
        void onServiceSelected(int serviceId);
        void onServiceRemoved(int serviceId);
        void onComboSelected(int comboId);
        void onComboRemoved(int comboId);
    }

    @BindView(R.id.view_pager)
    ViewPager viewPager;
    @BindView(R.id.tab_layout)
    TabLayout tabLayout;
    Service[] services;
    ComboServiceData[] comboServiceData;
    Callback mCallback;
    Service[] deals;

    public ServiceFragment() {
        // Required empty public constructor
    }

    public static ServiceFragment newInstance(String param1, String param2) {
        ServiceFragment fragment = new ServiceFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_service, container, false);
        ButterKnife.bind(this, view);
        if (savedInstanceState != null) {
            services = new Gson().fromJson(savedInstanceState.getString("service"), Service[].class);
            comboServiceData = new Gson().fromJson(savedInstanceState.getString("combos"), ComboServiceData[].class);
        }

        ServiceListFragment serviceListFragment = new ServiceListFragment();
        serviceListFragment.setRecyclerViewServices(services);
        CombosFragment combosFragment = new CombosFragment();
        combosFragment.setCombos(comboServiceData);
        FragmentsAdapter adapter = new FragmentsAdapter(getChildFragmentManager());
        adapter.AddFragment(serviceListFragment, "Services");
        adapter.AddFragment(combosFragment, "Combos");
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
        return view;
    }

    public void setData(Service[] services, ComboServiceData[] comboServiceData, Service[] deals) {
        this.services = services;
        this.comboServiceData = comboServiceData;
        this.deals = deals;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof Callback)
        {
            mCallback = (Callback)context;
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("service", new Gson().toJson(services));
        outState.putString("combos", new Gson().toJson(comboServiceData));
    }
}