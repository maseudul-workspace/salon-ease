package com.webinfotech.salonease.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.webinfotech.salonease.R;
import com.webinfotech.salonease.domain.executors.impl.ThreadExecutor;
import com.webinfotech.salonease.domain.models.WalletResponse;
import com.webinfotech.salonease.domain.models.WalletResponseData;
import com.webinfotech.salonease.presentation.presenters.WalletHistoryPresenter;
import com.webinfotech.salonease.presentation.presenters.impl.WalletHistoryPresenterImpl;
import com.webinfotech.salonease.presentation.ui.adapters.WalletHistoryListRecyclerViewAdapter;
import com.webinfotech.salonease.threading.MainThreadImpl;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class WalletActivity extends AppCompatActivity implements WalletHistoryPresenter.View {

    @BindView(R.id.recycler_view_wallet_History_list)
    RecyclerView recyclerViewWalletList;
    @BindView(R.id.text_view_total_balance)
    TextView textViewTotalBalance;

    WalletHistoryPresenterImpl mPresenter;
    LinearLayoutManager layoutManager;
    ViewGroup progressView;
    protected boolean isProgressShowing = false;
    LottieAnimationView lottieAnimationView;

    Boolean isScrolling = false;
    Integer currentItems;
    Integer totalItems;
    Integer scrollOutItems;
    int pageNo = 1;
    int totalPage = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wallet);
        ButterKnife.bind(this);
        initialsiePresenter();
        mPresenter.fetchWalletHistory(pageNo, "refresh");
    }

    private void initialsiePresenter() {
        mPresenter = new WalletHistoryPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    @Override
    public void loadAdapter(WalletHistoryListRecyclerViewAdapter adapter, int totalPage) {
        this.totalPage = totalPage;
        layoutManager = new LinearLayoutManager(this);
        recyclerViewWalletList.setLayoutManager(layoutManager);
        recyclerViewWalletList.setAdapter(adapter);
        recyclerViewWalletList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {

                super.onScrollStateChanged(recyclerView, newState);
                if(newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL)
                {
                    isScrolling= true;
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                currentItems = layoutManager.getChildCount();
                totalItems  = layoutManager.getItemCount();
                scrollOutItems = layoutManager.findFirstCompletelyVisibleItemPosition();
                if(!recyclerView.canScrollVertically(1))
                {
                    if(pageNo < totalPage) {
                        isScrolling = false;
                        pageNo = pageNo + 1;
                        mPresenter.fetchWalletHistory(pageNo,"");
                    }
                }
            }
        });
    }

    @Override
    public void loadData(WalletResponse walletResponse) {
        textViewTotalBalance.setText("₹ "+Integer.toString((int) walletResponse.walletAmount));
    }

    @Override
    public void showLoader() {
        isProgressShowing = true;
        progressView = (ViewGroup) getLayoutInflater().inflate(R.layout.progressbar_layout, null);
        View v = this.findViewById(android.R.id.content).getRootView();
        ViewGroup viewGroup = (ViewGroup) v;
        viewGroup.addView(progressView);
        lottieAnimationView = findViewById(R.id.progressBar);
        lottieAnimationView.playAnimation();
    }

    @Override
    public void hideLoader() {
        View v = this.findViewById(android.R.id.content).getRootView();
        ViewGroup viewGroup = (ViewGroup) v;
        viewGroup.removeView(progressView);
        lottieAnimationView.cancelAnimation();
        isProgressShowing = false;
    }

    @Override
    public void hidePaginationLoader() {

    }

    @Override
    public void onloginError() {
        Toast.makeText(this, "Your session has expired !!", Toast.LENGTH_LONG).show();
        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    @OnClick(R.id.btn_back) void onBackClicked()
    {
        finish();
    }
}