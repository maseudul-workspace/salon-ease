package com.webinfotech.salonease.presentation.presenters.impl;

import android.content.Context;

import com.webinfotech.salonease.AndroidApplication;
import com.webinfotech.salonease.domain.executors.Executor;
import com.webinfotech.salonease.domain.executors.MainThread;
import com.webinfotech.salonease.domain.interactors.AddAddressInteractor;
import com.webinfotech.salonease.domain.interactors.impl.AddAddressInteractorImpl;
import com.webinfotech.salonease.domain.models.UserInfo;
import com.webinfotech.salonease.presentation.presenters.AddAddressPresenter;
import com.webinfotech.salonease.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.salonease.repository.AppRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class AddAddressPresenterImpl extends AbstractPresenter implements AddAddressPresenter, AddAddressInteractor.Callback {

    Context mContext;
    AddAddressPresenter.View mView;
    AddAddressInteractorImpl addShippingAddressInteractor;

    public AddAddressPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void addAddress(String name, String email, String mobile, String city, String state, String pin, String address, double latitude, double longitude) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo user = androidApplication.getUserInfo(mContext);
        if (user == null) {
            mView.showLoginBottomSheet();
            Toasty.warning(mContext, "Your Session Expired!! Please Login Again").show();
        } else {
            addShippingAddressInteractor = new AddAddressInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, user.apiToken, user.userId, name, email, mobile, state, city, pin, address, latitude, longitude);
            addShippingAddressInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void onAddShippingAddressSuccess() {
        mView.hideLoader();
        mView.onAddAddressSuccess();
    }

    @Override
    public void onAddShippingAddressFail(String errorMsg, int loginError) {
        mView.hideLoader();
        if (loginError == 1) {
            mView.showLoginBottomSheet();
        }
        Toasty.warning(mContext, errorMsg).show();
    }
}
