package com.webinfotech.salonease.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import com.webinfotech.salonease.AndroidApplication;
import com.webinfotech.salonease.domain.executors.Executor;
import com.webinfotech.salonease.domain.executors.MainThread;
import com.webinfotech.salonease.domain.interactors.FetchUserProfileInteractor;
import com.webinfotech.salonease.domain.interactors.UpdateProfileInteractor;
import com.webinfotech.salonease.domain.interactors.impl.FetchUserProfileInteractorImpl;
import com.webinfotech.salonease.domain.interactors.impl.UpdateProfileInteractorImpl;
import com.webinfotech.salonease.domain.models.UserInfo;
import com.webinfotech.salonease.presentation.presenters.UserProfilePresenter;
import com.webinfotech.salonease.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.salonease.repository.AppRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class UserProfilePresenterImpl extends AbstractPresenter implements  UserProfilePresenter,
                                                                            UpdateProfileInteractor.Callback,
                                                                            FetchUserProfileInteractor.Callback
{

    Context mContext;
    UserProfilePresenter.View mView;

    public UserProfilePresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }
    
    @Override
    public void fetchUserProfile() {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo == null) {

        } else {
            FetchUserProfileInteractorImpl fetchUserProfileInteractor = new FetchUserProfileInteractorImpl(mExecutor, mMainThread, this, new AppRepositoryImpl(), userInfo.apiToken, userInfo.userId);
            fetchUserProfileInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void updateProfile(String name, String mobile, String state, String city, String gender, String address, String pin, String email) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo == null) {
            Toasty.warning(mContext, "Your Session Expired!! Please Login Again").show();
        } else {
            UpdateProfileInteractorImpl updateProfileInteractor = new UpdateProfileInteractorImpl(mExecutor, mMainThread, this, new AppRepositoryImpl(), userInfo.apiToken, userInfo.userId, name, mobile, state, city, gender, address, pin, email);
            updateProfileInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void onProfileUpdateSuccess() {
        Toasty.success(mContext, "Profile Updated").show();
        fetchUserProfile();
    }

    @Override
    public void onProfileUpdateFail(String errorMsg, int loginError) {
        mView.hideLoader();
        Toast.makeText(mContext, errorMsg, loginError).show();
    }

    @Override
    public void onFetchUserProfileSuccess(UserInfo userInfo) {
        mView.loadData(userInfo);
        mView.hideLoader();
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo localUserInfo = androidApplication.getUserInfo(mContext);
        localUserInfo.email = userInfo.email;
        localUserInfo.name = userInfo.name;
        localUserInfo.mobile = userInfo.mobile;
        androidApplication.setUserInfo(mContext, localUserInfo);
    }

    @Override
    public void onFetchUserProfileFail(String errorMsg, int loginError) {
        mView.hideLoader();
    }
}
