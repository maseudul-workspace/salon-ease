package com.webinfotech.salonease.presentation.presenters;

public interface ReviewsPresenter {
    void submitReview(int clientId, int rating, String feedback);
    interface View {
        void showLoader();
        void hideLoader();
    }
}
