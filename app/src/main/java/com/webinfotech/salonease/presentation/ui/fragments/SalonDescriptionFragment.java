package com.webinfotech.salonease.presentation.ui.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.webinfotech.salonease.R;
import com.webinfotech.salonease.domain.models.ComboServiceData;
import com.webinfotech.salonease.domain.models.Service;

public class SalonDescriptionFragment extends Fragment implements OnMapReadyCallback {

    @BindView(R.id.txt_view_description)
    TextView txtViewDescription;
    @BindView(R.id.txt_view_address)
    TextView txtViewAddress;
    private GoogleMap mMap;
    String about;
    String address;
    double latitude;
    double longitude;

    public SalonDescriptionFragment() {
        // Required empty public constructor
    }

    public static SalonDescriptionFragment newInstance(String param1, String param2) {
        SalonDescriptionFragment fragment = new SalonDescriptionFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_salon_description, container, false);
        ButterKnife.bind(this, view);
        if (savedInstanceState != null) {
            about = savedInstanceState.getString("about");
            address = savedInstanceState.getString("address");
            latitude = savedInstanceState.getDouble("latitude");
            longitude = savedInstanceState.getDouble("longitude");
        }
        txtViewDescription.setText(about);
        txtViewAddress.setText(address);
        initialiseMap();
        return view;
    }

    private void initialiseMap() {
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    public void setTxtViewDescription(String about, String address,  double latitude, double longitude) {
        this.about = about;
        this.latitude = latitude;
        this.longitude = longitude;
        this.address = address;
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        //Place current location marker
        LatLng latLng = new LatLng(latitude, longitude);
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        googleMap.addMarker(markerOptions);
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        googleMap.animateCamera(CameraUpdateFactory.zoomTo(15));
        markerOptions.draggable(false);
        googleMap.getUiSettings().setScrollGesturesEnabled(false);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("about", about);
        outState.putString("address", address);
        outState.putDouble("latitude", latitude);
        outState.putDouble("longitude", longitude);
    }
}