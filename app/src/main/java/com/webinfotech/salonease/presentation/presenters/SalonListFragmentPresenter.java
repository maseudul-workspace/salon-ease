package com.webinfotech.salonease.presentation.presenters;

import com.webinfotech.salonease.presentation.ui.adapters.SalonDataAdapter;

public interface SalonListFragmentPresenter {
    void fetchSalon(int serviceCityId, int categoryId, int page, int type, double latitude, double longitude, int clientType, String priceFrom, String priceTo, int ac, int parking, int wifi, int music, int sortBy, String refresh);
    interface View {
        void loadData(SalonDataAdapter adapter, int totalPage);
        void hideRecyclerView();
        void hideProgressDialog();
        void goToSalonDetails(int salonId, int serviceId);
    }
}
