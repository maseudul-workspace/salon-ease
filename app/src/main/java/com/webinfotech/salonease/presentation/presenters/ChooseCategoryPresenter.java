package com.webinfotech.salonease.presentation.presenters;

import com.webinfotech.salonease.domain.models.City;
import com.webinfotech.salonease.domain.models.Subcategory;

public interface ChooseCategoryPresenter {
    void fetchSubcategories(int categoryId);
    void fetchCities();
    interface View {
        void loadSubcategories(Subcategory[] subcategories);
        void loadCities(City[] cities);
    }
}
