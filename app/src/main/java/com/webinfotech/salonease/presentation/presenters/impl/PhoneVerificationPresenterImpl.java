package com.webinfotech.salonease.presentation.presenters.impl;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.webinfotech.salonease.AndroidApplication;
import com.webinfotech.salonease.domain.executors.Executor;
import com.webinfotech.salonease.domain.executors.MainThread;
import com.webinfotech.salonease.domain.interactors.SendOtpInteractor;
import com.webinfotech.salonease.domain.interactors.VerifyOtpInteractor;
import com.webinfotech.salonease.domain.interactors.impl.SendOtpInteractorImpl;
import com.webinfotech.salonease.domain.interactors.impl.VerifyOtpInteractorImpl;
import com.webinfotech.salonease.domain.models.UserInfo;
import com.webinfotech.salonease.presentation.presenters.PhoneVerificationPresenter;
import com.webinfotech.salonease.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.salonease.repository.AppRepositoryImpl;

public class PhoneVerificationPresenterImpl extends AbstractPresenter implements PhoneVerificationPresenter, SendOtpInteractor.Callback, VerifyOtpInteractor.Callback {

    Context mContext;
    PhoneVerificationPresenter.View mView;

    public PhoneVerificationPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void sendOtp(String phone) {
        SendOtpInteractorImpl sendOtpInteractor = new SendOtpInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, phone);
        sendOtpInteractor.execute();
        mView.showLoader();
    }

    @Override
    public void verifyOtp(String phone, String otp) {
        VerifyOtpInteractorImpl verifyOtpInteractor = new VerifyOtpInteractorImpl(mExecutor, mMainThread, this, new AppRepositoryImpl(), phone, otp);
        verifyOtpInteractor.execute();
        mView.showLoader();
    }

    @Override
    public void onOtpSendSuccess() {
        mView.hideLoader();
        mView.onOtpSendSuccess();
    }

    @Override
    public void onOtpSendFail(String errorMsg) {
        mView.hideLoader();
        Toast.makeText(mContext, errorMsg, Toast.LENGTH_LONG).show();
    }

    @Override
    public void onVerifyOtpSuccess(UserInfo userInfo) {
        mView.hideLoader();
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        androidApplication.setUserInfo(mContext, userInfo);
        if (userInfo.isRegistered == 2) {
            mView.goToMainActivity();
        } else {
            mView.goToRegisterActivity();
        }
    }

    @Override
    public void onVerifyOtpFail(String errorMsg) {
        mView.hideLoader();
    }

}
