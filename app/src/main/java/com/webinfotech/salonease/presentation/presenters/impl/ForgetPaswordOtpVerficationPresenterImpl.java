package com.webinfotech.salonease.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import com.webinfotech.salonease.domain.executors.Executor;
import com.webinfotech.salonease.domain.executors.MainThread;
import com.webinfotech.salonease.domain.interactors.SendOtpInteractor;
import com.webinfotech.salonease.domain.interactors.impl.SendForgetPasswordInteractorImpl;
import com.webinfotech.salonease.presentation.presenters.ForgetPasswordOtpVerificationPresenter;
import com.webinfotech.salonease.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.salonease.repository.AppRepositoryImpl;

public class ForgetPaswordOtpVerficationPresenterImpl extends AbstractPresenter implements ForgetPasswordOtpVerificationPresenter, SendOtpInteractor.Callback {

    Context mContext;
    ForgetPasswordOtpVerificationPresenter.View mView;

    public ForgetPaswordOtpVerficationPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void sendOtp(String phone) {
        SendForgetPasswordInteractorImpl sendOtpInteractor = new SendForgetPasswordInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, phone);
        sendOtpInteractor.execute();
        mView.showLoader();
    }

    @Override
    public void onOtpSendSuccess() {
        mView.hideLoader();
        mView.onOtpSendSuccess("");
    }

    @Override
    public void onOtpSendFail(String errorMsg) {
        mView.hideLoader();
        Toast.makeText(mContext, errorMsg, Toast.LENGTH_LONG).show();
    }
}
