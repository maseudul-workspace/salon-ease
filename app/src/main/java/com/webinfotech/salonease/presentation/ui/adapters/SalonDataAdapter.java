package com.webinfotech.salonease.presentation.ui.adapters;

import android.content.Context;
import android.graphics.Paint;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.webinfotech.salonease.R;
import com.webinfotech.salonease.domain.models.SalonData;
import com.webinfotech.salonease.domain.models.SalonUser;
import com.webinfotech.salonease.util.GlideHelper;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class SalonDataAdapter extends RecyclerView.Adapter<SalonDataAdapter.ViewHolder> {

    public interface Callback {
        void onSalonClicked(int salonId, int serviceId);
    }
    Context mContext;
    SalonData[] salonData;
    Callback mCallback;

    public SalonDataAdapter(Context mContext, SalonData[] salonData, Callback mCallback) {
        this.mContext = mContext;
        this.salonData = salonData;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_salon_data, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        SalonUser salonUser = salonData[position].salonUser;
        holder.txtViewSalonName.setText(salonUser.name);
        GlideHelper.setImageViewCustomRoundedCorners(mContext, holder.imgViewSalonName, mContext.getResources().getString(R.string.base_url) + "images/client/" + salonUser.image, 5);
        holder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onSalonClicked(salonUser.id, salonData[position].id);
            }
        });
        holder.txtViewDistance.setText(Math.round(salonData[position].distance) + "km");
        holder.txtViewPrice.setText(salonData[position].price);
        String facilities = "";
        if (salonUser.ac == 2) {
            facilities = "AC";
        }
        if (salonUser.parking == 2) {
            facilities = facilities + ", Parking";
        }
        if (salonUser.wifi == 2) {
            facilities = facilities + ", Wifi";
        }
        if (salonUser.music == 2) {
            facilities = facilities + ", Music";
        }
        holder.txtViewFacilities.setText(facilities);
        holder.ratingBar.setRating(Math.round(salonUser.rating));
        holder.txtViewAddress.setText(salonUser.address);
    }

    @Override
    public int getItemCount() {
        return salonData.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_view_salon)
        ImageView imgViewSalonName;
        @BindView(R.id.txt_view_salon_name)
        TextView txtViewSalonName;
        @BindView(R.id.txt_view_address)
        TextView txtViewAddress;
        @BindView(R.id.txt_view_facilites)
        TextView txtViewFacilities;
        @BindView(R.id.txt_view_distance)
        TextView txtViewDistance;
        @BindView(R.id.txt_view_price)
        TextView txtViewPrice;
        @BindView(R.id.rating_bar_salon)
        RatingBar ratingBar;
        @BindView(R.id.main_layout)
        View mainLayout;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void updateDataSet(SalonData[] salonData) {
        this.salonData = salonData;
    }

}
