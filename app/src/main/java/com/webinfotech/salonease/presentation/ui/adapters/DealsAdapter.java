package com.webinfotech.salonease.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.webinfotech.salonease.R;
import com.webinfotech.salonease.domain.models.SalonUser;
import com.webinfotech.salonease.util.GlideHelper;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class DealsAdapter extends RecyclerView.Adapter<DealsAdapter.ViewHolder> {

    public interface Callback {
        void onSalonClicked(int id);
    }

    Context mContext;
    SalonUser[] salonUsers;
    Callback mCallback;

    public DealsAdapter(Context mContext, SalonUser[] salonUsers, Callback mCallback) {
        this.mContext = mContext;
        this.salonUsers = salonUsers;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_deal_of_the_day, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewSalonName.setText(salonUsers[position].name);
        DealServiceAdapter adapter = new DealServiceAdapter(mContext, salonUsers[position].services);
        holder.recyclerViewDeals.setAdapter(adapter);
        holder.recyclerViewDeals.setLayoutManager(new LinearLayoutManager(mContext));
        GlideHelper.setImageViewCustomRoundedCorners(mContext, holder.imgViewSalon, mContext.getResources().getString(R.string.base_url) + "images/client/" + salonUsers[position].image, 100);
        holder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onSalonClicked(salonUsers[position].id);
            }
        });
        if (salonUsers[position].clientType == 1) {
            holder.imgViewSalonType.setImageDrawable(mContext.getResources().getDrawable(R.drawable.home_small_icon));
            holder.txtViewSalonType.setText("Home Service");
        } else {
            holder.imgViewSalonType.setImageDrawable(mContext.getResources().getDrawable(R.drawable.barber_chair_small));
            holder.txtViewSalonType.setText("On Site Salon");
        }
    }

    @Override
    public int getItemCount() {
        return salonUsers.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.recycler_view_deal_of_the_day)
        RecyclerView recyclerViewDeals;
        @BindView(R.id.txt_view_salon_name)
        TextView txtViewSalonName;
        @BindView(R.id.img_view_salon)
        ImageView imgViewSalon;
        @BindView(R.id.main_layout)
        View mainLayout;
        @BindView(R.id.img_view_salon_type)
        ImageView imgViewSalonType;
        @BindView(R.id.txt_view_salon_type)
        TextView txtViewSalonType;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
