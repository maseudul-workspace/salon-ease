package com.webinfotech.salonease.presentation.presenters;

import com.webinfotech.salonease.domain.models.ComboServiceData;
import com.webinfotech.salonease.domain.models.SalonUser;

public interface CombosPresenter {
    void fetchCombos(double latitude, double longitude);
    interface View {
        void loadDate(ComboServiceData[] comboServiceData);
        void showLoader();
        void hideLoader();
    }
}
