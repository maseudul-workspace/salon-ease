package com.webinfotech.salonease.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;

import com.webinfotech.salonease.AndroidApplication;
import com.webinfotech.salonease.R;
import com.webinfotech.salonease.util.GlideHelper;

public class UserActivity extends AppCompatActivity {

    @BindView(R.id.img_view_user_activity)
    ImageView imgViewUserActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user);
        ButterKnife.bind(this);
        GlideHelper.setImageView(this, imgViewUserActivity, "https://hairstylecamp.com/wp-content/uploads/korean-men-haircut.jpg");
    }

    @OnClick(R.id.layout_change_pswd) void onChangePasswordClicked() {
        Intent intent = new Intent(this, ChangePasswordActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.btn_log_out) void onLogoutClicked() {
        AndroidApplication androidApplication = (AndroidApplication) getApplicationContext();
        androidApplication.setUserInfo(this, null);
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finishAffinity();
    }

    @OnClick(R.id.address_view) void onAddressViewClicked() {
        Intent intent = new Intent(this, AddressListActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.orders_view) void onOrdersClicked() {
        Intent intent = new Intent(this, OrderHistoryActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.banks_view) void onBanksClicked() {
        Intent intent = new Intent(this, BankListActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.view_user_profile) void onUserProfileClicked() {
        Intent intent = new Intent(this, UserProfileActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.img_view_back) void onBackClicked() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

}