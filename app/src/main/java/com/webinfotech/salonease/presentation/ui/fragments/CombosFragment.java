package com.webinfotech.salonease.presentation.ui.fragments;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.webinfotech.salonease.R;
import com.webinfotech.salonease.domain.models.ComboService;
import com.webinfotech.salonease.domain.models.ComboServiceData;
import com.webinfotech.salonease.domain.models.Service;
import com.webinfotech.salonease.presentation.ui.adapters.CombosAdapter;

public class CombosFragment extends Fragment implements CombosAdapter.Callback {

    public interface Callback {
        void onComboSelect(int comboId);
        void onComboRemoved(int comboId);
    }

    @BindView(R.id.recycler_view_combos)
    RecyclerView recyclerViewCombos;
    ComboServiceData[] combos;
    Callback mCallback;
    CombosAdapter adapter;

    public CombosFragment() {
        // Required empty public constructor
    }


    public static CombosFragment newInstance(String param1, String param2) {
        CombosFragment fragment = new CombosFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_combos, container, false);
        ButterKnife.bind(this, view);
        if (savedInstanceState != null) {
            combos = new Gson().fromJson(savedInstanceState.getString("combos"), ComboServiceData[].class);
        }
        adapter = new CombosAdapter(getContext(), combos, this);
        recyclerViewCombos.setLayoutManager(new LinearLayoutManager(getContext(), RecyclerView.HORIZONTAL, false));
        recyclerViewCombos.setAdapter(adapter);
        return view;
    }

    public void setCombos(ComboServiceData[] combos) {
        this.combos = combos;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (getParentFragment() instanceof Callback)
        {
            mCallback = (Callback) getParentFragment();
        }
    }

    @Override
    public void onComboSelected(int position) {
        combos[position].isComboSelected = true;
        adapter.setComboServiceData(combos);
        mCallback.onComboSelect(combos[position].id);
    }

    @Override
    public void onComboRemoved(int position) {
        combos[position].isComboSelected = false;
        adapter.setComboServiceData(combos);
        mCallback.onComboRemoved(combos[position].id);
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("combos", new Gson().toJson(combos));
    }

}