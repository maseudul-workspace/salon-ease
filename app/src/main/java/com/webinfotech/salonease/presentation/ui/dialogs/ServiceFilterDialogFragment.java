package com.webinfotech.salonease.presentation.ui.dialogs;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;
import android.widget.Toast;

import com.webinfotech.salonease.R;
import com.webinfotech.salonease.domain.models.Category;
import com.webinfotech.salonease.domain.models.City;

public class ServiceFilterDialogFragment extends DialogFragment implements SelectCityDialog.Callback, CategoriesListDialog.Callback {

    public interface Callback {
        void onAddClicked(int categoryId, int serviceCity, String clientType, String categoryName);
    }

    Context mContext;
    Callback mCallback;
    CategoriesListDialog categoriesListDialog;
    SelectCityDialog selectCityDialog;
    @BindView(R.id.txt_view_service)
    TextView txtViewService;
    @BindView(R.id.txt_view_city)
    TextView txtViewCity;
    @BindView(R.id.check_box_salon)
    CheckBox checkBoxSalon;
    @BindView(R.id.check_box_home_service)
    CheckBox checkBoxHomeService;
    int categoryId;
    Activity mActivity;
    Category[] categories;
    City[] cities;
    String categoryName;
    String clientType = null;
    int cityId = 0;

    public ServiceFilterDialogFragment() {
        // Required empty public constructor
    }

    public static ServiceFilterDialogFragment newInstance() {
        ServiceFilterDialogFragment fragment = new ServiceFilterDialogFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(DialogFragment.STYLE_NO_TITLE, android.R.style.Theme_Material_Light_NoActionBar_Fullscreen);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_service_filter_dialog, container, false);
        ButterKnife.bind(this, view);
        setCategoriesListDialog();
        categoriesListDialog.setCategories(categories);
        txtViewService.setText(categoryName);
        setSelectCityDialog();
        selectCityDialog.setRecyclerView(cities);
        return view;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mContext = context;
        if (context instanceof Callback)
        {
            mCallback = (Callback)context;
        }
        if (context instanceof Activity){
            mActivity =(Activity) context;
        }
    }

    private void setCategoriesListDialog() {
        categoriesListDialog = new CategoriesListDialog(mContext, mActivity, this);
        categoriesListDialog.setUpDialogView();
    }

    private void setSelectCityDialog() {
        selectCityDialog = new SelectCityDialog(mContext, mActivity, this);
        selectCityDialog.setUpDialogView();
    }

    public void setCities(City[] cities) {
       this.cities = cities;
    }

    public void setCategories(Category[] categories, String categoryName, int categoryId) {
        this.categories = categories;
        this.categoryName = categoryName;
        this.categoryId = categoryId;
    }

    @Override
    public void onCitySelected(int id, String cityName) {
        txtViewCity.setText(cityName);
        cityId = id;
    }

    @Override
    public void onCategorySelected(int categoryId, String categoryName) {
        txtViewService.setText(categoryName);
        this.categoryId = categoryId;
    }

    @OnClick(R.id.view_service_layout) void onServiceClicked() {
        categoriesListDialog.showDialog();
    }

    @OnClick(R.id.view_location) void onLocationClicked() {
        selectCityDialog.showDialog();
    }

    @Override
    public void onActivityCreated(Bundle arg0) {
        super.onActivityCreated(arg0);
        getDialog().getWindow()
                .getAttributes().windowAnimations = R.style.DialogAnimation;
    }

    @OnClick(R.id.btn_submit) void onSubmitClicked() {
        if (checkBoxSalon.isChecked() && checkBoxHomeService.isChecked()) {
            clientType = null;
            initiate();
        } else if (checkBoxSalon.isChecked()) {
            clientType = "2";
            initiate();
        } else if (checkBoxHomeService.isChecked()) {
            clientType = "1";
            initiate();
        } else {
            Toast.makeText(mContext, "Please select at least one service for", Toast.LENGTH_SHORT).show();
        }
    }

    private void initiate() {
        if (cityId == 0) {
            Toast.makeText(mContext, "Please Select a City", Toast.LENGTH_SHORT).show();
        } else {
            mCallback.onAddClicked(categoryId, cityId,clientType, categoryName);
            dismiss();
        }
    }

}