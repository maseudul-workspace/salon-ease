package com.webinfotech.salonease.presentation.presenters;

import com.webinfotech.salonease.domain.models.ShippingAddress;

public interface CartFragmentPresenter {
    void checkOffer( int offerId, int jobId, int vendorId);
    void checkCoupon(int couponId);
    void fetchShippingAddress();
    interface View {
        void isOfferValid(boolean isVaild);
        void isCouponValid(boolean isValid);
        void loadShippingAddress(ShippingAddress[] addresses);
        void showLoader();
        void hideLoader();
    }
}
