package com.webinfotech.salonease.presentation.ui.adapters;

import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.webinfotech.salonease.R;
import com.webinfotech.salonease.domain.models.ComboServiceData;
import com.webinfotech.salonease.util.GlideHelper;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ComboListAdapter extends RecyclerView.Adapter<ComboListAdapter.ViewHolder> {

    public interface Callback {
        void onSalonClicked(int id);
    }

    Context mContext;
    ComboServiceData[] comboServiceData;
    Callback mCallback;

    public ComboListAdapter(Context mContext, ComboServiceData[] comboServiceData, Callback mCallback) {
        this.mContext = mContext;
        this.comboServiceData = comboServiceData;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_combo_list, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewComboName.setText(comboServiceData[position].comboName);
        long discount = Math.round(comboServiceData[position].price / comboServiceData[position].mrp * 100);
        holder.txtViewPrice.setText("Rs. " + comboServiceData[position].price);
        holder.txtViewMrp.setText("Rs. " + comboServiceData[position].mrp);
        holder.txtViewDiscount.setText("Extra " + discount + " off");
        holder.txtViewMrp.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
        ComboServiceAdapter adapter = new ComboServiceAdapter(mContext, comboServiceData[position].comboServices);
        holder.recyclerViewCombos.setAdapter(adapter);
        holder.recyclerViewCombos.setLayoutManager(new LinearLayoutManager(mContext));
        holder.txtViewSalonName.setText(comboServiceData[position].clientName);
        GlideHelper.setImageViewCustomRoundedCorners(mContext, holder.imgViewSalon, mContext.getResources().getString(R.string.base_url) + "images/client/" + comboServiceData[position].client_image, 100);
        holder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onSalonClicked(comboServiceData[position].clientId);
            }
        });
        if (comboServiceData[position].clientType == 1) {
            holder.imgViewSalonType.setImageDrawable(mContext.getResources().getDrawable(R.drawable.home_small_icon));
            holder.txtViewSalonType.setText("Home Service");
        } else {
            holder.imgViewSalonType.setImageDrawable(mContext.getResources().getDrawable(R.drawable.barber_chair_small));
            holder.txtViewSalonType.setText("On Site Salon");
        }
    }

    @Override
    public int getItemCount() {
        return comboServiceData.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_combo_name)
        TextView txtViewComboName;
        @BindView(R.id.recycler_view_combos)
        RecyclerView recyclerViewCombos;
        @BindView(R.id.txt_view_salon_name)
        TextView txtViewSalonName;
        @BindView(R.id.img_view_salon)
        ImageView imgViewSalon;
        @BindView(R.id.txt_view_price)
        TextView txtViewPrice;
        @BindView(R.id.txt_view_mrp)
        TextView txtViewMrp;
        @BindView(R.id.txt_view_discount)
        TextView txtViewDiscount;
        @BindView(R.id.main_layout)
        View mainLayout;
        @BindView(R.id.img_view_salon_type)
        ImageView imgViewSalonType;
        @BindView(R.id.txt_view_salon_type)
        TextView txtViewSalonType;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
