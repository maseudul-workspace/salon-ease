package com.webinfotech.salonease.presentation.ui.dialogs;

import android.app.Activity;
import android.content.Context;
import android.view.View;

import com.webinfotech.salonease.R;
import com.webinfotech.salonease.domain.models.Subcategory;
import com.webinfotech.salonease.presentation.ui.adapters.SubcategoriesAdapter;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class SubcategoriesListDialog implements SubcategoriesAdapter.Callback {

    public interface Callback {
        void onSubcategoryClicked(int position);
    }

    Context mContext;
    View dialogContainer;
    AlertDialog.Builder builder;
    AlertDialog dialog;
    Activity mActivity;
    Callback mCallback;
    @BindView(R.id.recycler_view_categories)
    RecyclerView recyclerViewCategories;

    public SubcategoriesListDialog(Context mContext, Activity mActivity, Callback mCallback) {
        this.mContext = mContext;
        this.mActivity = mActivity;
        this.mCallback = mCallback;
    }

    @Override
    public void onSubcategoryClicked(int position) {
        hideDialog();
        mCallback.onSubcategoryClicked(position);
    }

    public void setUpDialogView() {
        dialogContainer = mActivity.getLayoutInflater().inflate(R.layout.category_list_dialog, null);
        builder = new AlertDialog.Builder(mContext, android.R.style.Theme_Material_Light_Dialog_NoActionBar_MinWidth);
        builder.setView(dialogContainer);
        dialog = builder.create();
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        ButterKnife.bind(this, dialogContainer);
    }

    public void setCategories(Subcategory[] categories) {
        SubcategoriesAdapter adapter = new SubcategoriesAdapter(mContext, categories, this);
        recyclerViewCategories.setAdapter(adapter);
        recyclerViewCategories.setLayoutManager(new LinearLayoutManager(mContext));
        recyclerViewCategories.addItemDecoration(new DividerItemDecoration(recyclerViewCategories.getContext(), DividerItemDecoration.VERTICAL));
    }

    public void showDialog() {
        dialog.show();
    }

    public void hideDialog() {
        dialog.dismiss();
    }

}
