package com.webinfotech.salonease.presentation.presenters;

import com.webinfotech.salonease.domain.models.ComboServiceData;
import com.webinfotech.salonease.domain.models.SalonUser;

public interface DealofthedayPresenter {
    void fetchDeals(double latitude, double longitude);
    interface View {
        void loadDate(SalonUser[] salonUsers);
        void showLoader();
        void hideLoader();
    } }
