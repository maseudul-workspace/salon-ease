package com.webinfotech.salonease.presentation.ui.adapters;

import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.webinfotech.salonease.R;
import com.webinfotech.salonease.domain.models.ServiceCart;
import com.webinfotech.salonease.util.GlideHelper;

import org.w3c.dom.Text;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class CartServiceAdapter extends RecyclerView.Adapter<CartServiceAdapter.ViewHolder> {

    public interface Callback{
        void onServiceQtyClicked(int position);
        void onOfferApplyClicked(int position);
    }

    Context mContext;
    ArrayList<ServiceCart> serviceCarts;
    Callback mCallback;

    public CartServiceAdapter(Context mContext, ArrayList<ServiceCart> serviceCarts, Callback mCallback) {
        this.mContext = mContext;
        this.serviceCarts = serviceCarts;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_cart_service, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        GlideHelper.setImageViewCustomRoundedCorners(mContext, holder.imgViewService, mContext.getResources().getString(R.string.base_url) + "admin/service_category/sub_category/thumb/" + serviceCarts.get(position).image, 5);
        holder.txtViewService.setText(serviceCarts.get(position).serviceName);
        holder.viewApplyOffer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onOfferApplyClicked(position);
            }
        });
        if (serviceCarts.get(position).offerApplied) {
            holder.viewApplyOffer.setVisibility(View.GONE);
            holder.txtViewOfferApplied.setVisibility(View.VISIBLE);
            holder.txtViewQty.setText("Qty: 1");
            holder.txtViewPrice.setText("Rs. " + serviceCarts.get(position).offerPrice);
            holder.layoutQty.setOnClickListener(null);
            holder.txtViewSpecialPrice.setVisibility(View.GONE);
        } else {
            holder.txtViewPrice.setText("Rs. " + (serviceCarts.get(position).price * serviceCarts.get(position).qty) );
            holder.txtViewOfferApplied.setVisibility(View.GONE);
            holder.txtViewQty.setText("Qty: " + serviceCarts.get(position).qty);
            holder.layoutQty.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mCallback.onServiceQtyClicked(position);
                }
            });
            if (serviceCarts.get(position).offerPrice < 1) {
                holder.viewApplyOffer.setVisibility(View.GONE);
            } else {
                holder.viewApplyOffer.setVisibility(View.VISIBLE);
            }
            if (serviceCarts.get(position).isDeal.equals("Y")) {
                holder.txtViewSpecialPrice.setVisibility(View.VISIBLE);
            } else {
                holder.txtViewSpecialPrice.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public int getItemCount() {
        return serviceCarts.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_view_service)
        ImageView imgViewService;
        @BindView(R.id.txt_view_service)
        TextView txtViewService;
        @BindView(R.id.txt_view_price)
        TextView txtViewPrice;
        @BindView(R.id.layout_qty)
        View layoutQty;
        @BindView(R.id.txt_view_qty)
        TextView txtViewQty;
        @BindView(R.id.view_apply_offer)
        View viewApplyOffer;
        @BindView(R.id.txt_view_offer_applied)
        TextView txtViewOfferApplied;
        @BindView(R.id.txt_view_special_price)
        TextView txtViewSpecialPrice;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void setServiceCarts(ArrayList<ServiceCart> serviceCarts) {
        this.serviceCarts = serviceCarts;
        notifyDataSetChanged();
    }

}
