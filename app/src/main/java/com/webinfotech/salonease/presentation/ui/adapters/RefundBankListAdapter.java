package com.webinfotech.salonease.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.webinfotech.salonease.R;
import com.webinfotech.salonease.domain.models.BankList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class RefundBankListAdapter extends RecyclerView.Adapter<RefundBankListAdapter.ViewHolder> {

    public interface Callback {
        void onBankEditClicked(BankList bankList);
    }

    Context mContext;
    BankList[] bankLists;
    Callback mCallback;

    public RefundBankListAdapter(Context mContext, BankList[] bankLists, Callback mCallback) {
        this.mContext = mContext;
        this.bankLists = bankLists;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_refund_bank_list, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
       holder.txtViewBankName.setText(bankLists[position].bankName);
       holder.txtViewAccountNo.setText(bankLists[position].accountNo);
       holder.txtViewBranch.setText(bankLists[position].branchName);
       holder.txtViewIfscCode.setText(bankLists[position].ifscCode);
       holder.btnEdit.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               mCallback.onBankEditClicked(bankLists[position]);
           }
       });
    }

    @Override
    public int getItemCount() {
        return bankLists.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_bank_name)
        TextView txtViewBankName;
        @BindView(R.id.txt_view_account_holder)
        TextView txtViewAccountHolder;
        @BindView(R.id.txt_view_ifsc_code)
        TextView txtViewIfscCode;
        @BindView(R.id.txt_view_branch)
        TextView txtViewBranch;
        @BindView(R.id.txt_view_account_no)
        TextView txtViewAccountNo;
        @BindView(R.id.btn_edit)
        Button btnEdit;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
