package com.webinfotech.salonease.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.avatarfirst.avatargenlib.AvatarConstants;
import com.avatarfirst.avatargenlib.AvatarGenerator;
import com.webinfotech.salonease.R;
import com.webinfotech.salonease.domain.models.Reviews;
import com.webinfotech.salonease.util.GlideHelper;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ReviewsAdapter extends RecyclerView.Adapter<ReviewsAdapter.ViewHolder> {

    Context mContext;
    Reviews[] reviews;

    public ReviewsAdapter(Context mContext, Reviews[] reviews) {
        this.mContext = mContext;
        this.reviews = reviews;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_reviews, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewUsername.setText(reviews[position].customerName);
        holder.txtViewReview.setText(reviews[position].comment);
        holder.ratingBarrReview.setRating(reviews[position].rating);
        holder.imgViewUser.setImageDrawable(AvatarGenerator.Companion.avatarImage(mContext, 200, AvatarConstants.Companion.getCIRCLE(), reviews[position].customerName, AvatarConstants.Companion.getCOLOR400()));
    }

    @Override
    public int getItemCount() {
        return reviews.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_view_user)
        ImageView imgViewUser;
        @BindView(R.id.txt_view_user_name)
        TextView txtViewUsername;
        @BindView(R.id.txt_view_review)
        TextView txtViewReview;
        @BindView(R.id.rating_bar_review)
        RatingBar ratingBarrReview;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
