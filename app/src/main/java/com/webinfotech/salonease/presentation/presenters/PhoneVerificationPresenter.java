package com.webinfotech.salonease.presentation.presenters;

public interface PhoneVerificationPresenter {
    void sendOtp(String phone);
    void verifyOtp(String phone, String otp);
    interface View {
        void onOtpSendSuccess();
        void goToMainActivity();
        void goToRegisterActivity();
        void showLoader();
        void hideLoader();
    }
}
