package com.webinfotech.salonease.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.webinfotech.salonease.R;
import com.webinfotech.salonease.domain.executors.impl.ThreadExecutor;
import com.webinfotech.salonease.presentation.presenters.ChangePasswordPresenter;
import com.webinfotech.salonease.presentation.presenters.impl.ChangePasswordPresneterImpl;
import com.webinfotech.salonease.threading.MainThreadImpl;
import com.webinfotech.salonease.util.GlideHelper;
import com.webinfotech.salonease.util.Helper;

public class ChangePasswordActivity extends AppCompatActivity implements ChangePasswordPresenter.View{

    @BindView(R.id.img_view_change_pswd)
    ImageView imgViewChangePswd;
    @BindView(R.id.edit_text_new_pswd)
    EditText editTextNewPswd;
    @BindView(R.id.edit_text_old_pswd)
    EditText editTextOldPswd;
    @BindView(R.id.edit_text_confirm_pswd)
    EditText editTextConfirmPswd;
    ProgressDialog progressDialog;
    ChangePasswordPresneterImpl mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        ButterKnife.bind(this);
        GlideHelper.setImageView(this, imgViewChangePswd, "https://img.etimg.com/thumb/msid-63861163,width-650,imgsize-155002,,resizemode-4,quality-100/curly-hair-.jpg");
        setUpProgressDialog();
        initialisePresenter();
    }

    private void initialisePresenter() {
        mPresenter = new ChangePasswordPresneterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    private void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @OnClick(R.id.btn_submit) void onChangePasswordClicked() {
        if (editTextOldPswd.getText().toString().trim().isEmpty()) {
            Toast.makeText(this,"Please Enter Your Current Password", Toast.LENGTH_LONG).show();
        } else if (editTextNewPswd.getText().toString().trim().isEmpty()) {
            Toast.makeText(this,"Please Enter New Password", Toast.LENGTH_LONG).show();
        } else if (editTextConfirmPswd.getText().toString().trim().isEmpty()) {
            Toast.makeText(this,"Please Confirm Password", Toast.LENGTH_LONG).show();
        } else if (!editTextNewPswd.getText().toString().trim().equals(editTextConfirmPswd.getText().toString().trim())) {
            Toast.makeText(this,"Password Mismatch", Toast.LENGTH_LONG).show();
        } else {
            mPresenter.changePassword(editTextOldPswd.getText().toString(), editTextNewPswd.getText().toString());
        }
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public void showLoginBottomSheet() {
        Helper.showLoginBottomSheet(getSupportFragmentManager());
    }

}