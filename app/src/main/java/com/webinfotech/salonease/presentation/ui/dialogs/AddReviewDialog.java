package com.webinfotech.salonease.presentation.ui.dialogs;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.Toast;

import com.webinfotech.salonease.R;

import androidx.appcompat.app.AlertDialog;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;

public class AddReviewDialog {

    public interface Callback {
        void submitRating(int rating, String feedback);
    }

    Context mContext;
    View dialogContainer;
    AlertDialog.Builder builder;
    AlertDialog dialog;
    Activity mActivity;
    Callback mCallback;
    @BindView(R.id.rating_bar_review)
    RatingBar ratingBar;
    @BindView(R.id.edit_text_feedback)
    EditText editTextFeedback;

    public AddReviewDialog(Context mContext, Activity mActivity, Callback mCallback) {
        this.mContext = mContext;
        this.mActivity = mActivity;
        this.mCallback = mCallback;
    }

    public void setUpDialog() {
        dialogContainer = mActivity.getLayoutInflater().inflate(R.layout.layout_add_rating_dialog, null);
        builder = new AlertDialog.Builder(mContext, android.R.style.Theme_Material_Light_Dialog_NoActionBar_MinWidth);
        builder.setView(dialogContainer);
        dialog = builder.create();
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        ButterKnife.bind(this, dialogContainer);
    }

    public void showDialog() {
        dialog.show();
    }

    public void hideDialog() {
        dialog.dismiss();
    }

    @OnClick(R.id.btn_submit) void onSubmitClicked() {
        if (ratingBar.getRating() < 1) {
            Toasty.normal(mContext, "Please give a rating", Toast.LENGTH_SHORT).show();
        } else if (editTextFeedback.getText().toString().trim().isEmpty()) {
            Toasty.normal(mContext, "Please give your feedback", Toast.LENGTH_SHORT).show();
        } else {
            mCallback.submitRating(Math.round(ratingBar.getRating()), editTextFeedback.getText().toString());
            hideDialog();
        }
    }

}
