package com.webinfotech.salonease.presentation.presenters.impl;

import android.content.Context;

import com.webinfotech.salonease.domain.executors.Executor;
import com.webinfotech.salonease.domain.executors.MainThread;
import com.webinfotech.salonease.domain.interactors.FetchSearchListInteractor;
import com.webinfotech.salonease.domain.interactors.impl.FetchSearchListInteractorImpl;
import com.webinfotech.salonease.domain.models.SalonUser;
import com.webinfotech.salonease.presentation.presenters.SearchListPresenter;
import com.webinfotech.salonease.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.salonease.presentation.ui.adapters.SalonListAdapter;
import com.webinfotech.salonease.repository.AppRepositoryImpl;

public class SearchListPresenterImpl extends AbstractPresenter implements SearchListPresenter, FetchSearchListInteractor.Callback, SalonListAdapter.Callback{

    Context mContext;
    SearchListPresenter.View mView;
    SalonUser[] newSalonUsers;
    SalonListAdapter adapter;
    String searchKey;

    public SearchListPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchSearchList(String searchKey, double latitude, double longitude, int page, String refresh) {
        this.searchKey = searchKey;
        if (refresh.equals("refresh")) {
            newSalonUsers = null;
        }
        FetchSearchListInteractorImpl fetchSearchListInteractor = new FetchSearchListInteractorImpl(mExecutor, mMainThread, this, new AppRepositoryImpl(), searchKey, latitude, longitude, page);
        fetchSearchListInteractor.execute();
    }

    @Override
    public void onGettingSearchListSuccess(SalonUser[] salonUsers, int totalPage, String searchKey) {
        if (this.searchKey.equals(searchKey)) {
            if(salonUsers.length == 0){
                adapter = new SalonListAdapter(mContext, salonUsers, this);
                mView.loadData(adapter, totalPage);
            }else{
                SalonUser[] tempSalonUsers;
                tempSalonUsers = newSalonUsers;
                try {
                    int len1 = tempSalonUsers.length;
                    int len2 = salonUsers.length;
                    newSalonUsers = new SalonUser[len1 + len2];
                    System.arraycopy(tempSalonUsers, 0, newSalonUsers, 0, len1);
                    System.arraycopy(salonUsers, 0, newSalonUsers, len1, len2);
                    adapter.updateDataSet(newSalonUsers);
                } catch (NullPointerException e) {
                    newSalonUsers = salonUsers;
                    adapter = new SalonListAdapter(mContext, salonUsers, this);
                    mView.loadData(adapter, totalPage);
                }
            }
        }
    }

    @Override
    public void onGettingSearchListFail(String errorMsg) {
        newSalonUsers = null;
    }

    @Override
    public void onSalonClicked(int salonId) {
        mView.goToSalonDetails(salonId);
    }
}
