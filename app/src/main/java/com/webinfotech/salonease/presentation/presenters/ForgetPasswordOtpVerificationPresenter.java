package com.webinfotech.salonease.presentation.presenters;

public interface ForgetPasswordOtpVerificationPresenter {
    void sendOtp(String phone);
    interface View {
        void onOtpSendSuccess(String otp);
        void showLoader();
        void hideLoader();
    }
}
