package com.webinfotech.salonease.presentation.presenters.impl;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.webinfotech.salonease.AndroidApplication;
import com.webinfotech.salonease.domain.executors.Executor;
import com.webinfotech.salonease.domain.executors.MainThread;
import com.webinfotech.salonease.domain.interactors.RegisterUserInteractor;
import com.webinfotech.salonease.domain.interactors.impl.RegisterUserInteractorImpl;
import com.webinfotech.salonease.domain.models.UserInfo;
import com.webinfotech.salonease.presentation.presenters.RegisterUserPresenter;
import com.webinfotech.salonease.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.salonease.repository.AppRepositoryImpl;

public class RegisterUserPresenterImpl extends AbstractPresenter implements RegisterUserPresenter,
                                                                            RegisterUserInteractor.Callback {

    Context mContext;
    RegisterUserPresenter.View mView;

    public RegisterUserPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void registerUser(String name, String email, String gender) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        String referalId = androidApplication.getReferUserId(mContext);
        if (userInfo != null) {
            RegisterUserInteractorImpl registerUserInteractor = new RegisterUserInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.userId, name, email, gender, referalId);
            registerUserInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void onRegisterUserSuccess(UserInfo userInfo) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        androidApplication.setUserInfo(mContext, userInfo);
        mView.hideLoader();
        mView.onUserRegistrationSuccess();
    }

    @Override
    public void onRegisterUserFail(String errorMsg) {
        mView.hideLoader();
        Toast.makeText(mContext, errorMsg, Toast.LENGTH_LONG).show();
    }


}
