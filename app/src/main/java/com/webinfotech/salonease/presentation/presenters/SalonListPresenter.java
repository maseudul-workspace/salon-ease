package com.webinfotech.salonease.presentation.presenters;

import com.webinfotech.salonease.domain.models.Category;
import com.webinfotech.salonease.domain.models.City;
import com.webinfotech.salonease.domain.models.ThirdCategory;
import com.webinfotech.salonease.presentation.ui.adapters.SalonDataAdapter;

public interface SalonListPresenter {
    void fetchSalonList(int serviceCityId, int categoryId, int page, String clientType, String refresh);
    void fetchCityList();
    void fetchCategories();
    void fetchThirdCategories(int categoryId);
    interface View {
        void loadData(SalonDataAdapter adapter, int totalPage);
        void loadCities(City[] cities);
        void loadCategories(Category[] categories);
        void showLoader();
        void hideLoader();
        void goToSalonDetails(int serviceId);
        void hideRecyclerView();
        void loadThirdCategories(ThirdCategory[] thirdCategories);
    }
}
