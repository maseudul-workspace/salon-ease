package com.webinfotech.salonease.presentation.presenters.impl;

import android.content.Context;
import android.util.Log;

import com.webinfotech.salonease.AndroidApplication;
import com.webinfotech.salonease.domain.executors.Executor;
import com.webinfotech.salonease.domain.executors.MainThread;
import com.webinfotech.salonease.domain.interactors.AddToWishlistInteractor;
import com.webinfotech.salonease.domain.interactors.FetchSalonDetailsInteractor;
import com.webinfotech.salonease.domain.interactors.FetchShippingAddressInteractor;
import com.webinfotech.salonease.domain.interactors.FetchWishlistInteractor;
import com.webinfotech.salonease.domain.interactors.RemoveFromWishlistInteractor;
import com.webinfotech.salonease.domain.interactors.impl.AddToWishlistInteractorImpl;
import com.webinfotech.salonease.domain.interactors.impl.FetchSalonDetailsInteractorImpl;
import com.webinfotech.salonease.domain.interactors.impl.FetchShippingAddressInteractorImpl;
import com.webinfotech.salonease.domain.interactors.impl.FetchWishlistInteractorImpl;
import com.webinfotech.salonease.domain.interactors.impl.RemoveFromWishlistInteractorImpl;
import com.webinfotech.salonease.domain.models.SalonData;
import com.webinfotech.salonease.domain.models.SalonUser;
import com.webinfotech.salonease.domain.models.ShippingAddress;
import com.webinfotech.salonease.domain.models.UserInfo;
import com.webinfotech.salonease.domain.models.Wishlist;
import com.webinfotech.salonease.presentation.presenters.SalonDetailsPresenter;
import com.webinfotech.salonease.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.salonease.presentation.ui.adapters.BookingAddressAdapter;
import com.webinfotech.salonease.repository.AppRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class SalonDetailsPresenterImpl extends AbstractPresenter implements SalonDetailsPresenter,
                                                                            FetchSalonDetailsInteractor.Callback,
                                                                            FetchShippingAddressInteractor.Callback,
                                                                            BookingAddressAdapter.Callback,
                                                                            FetchWishlistInteractor.Callback,
                                                                            AddToWishlistInteractor.Callback,
                                                                            RemoveFromWishlistInteractor.Callback
{

    Context mContext;
    SalonDetailsPresenter.View mView;
    BookingAddressAdapter addressAdapter;
    ShippingAddress[] shippingAddresses;


    public SalonDetailsPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchProductDetails(int serviceId) {
        FetchSalonDetailsInteractorImpl fetchSalonDetailsInteractor = new FetchSalonDetailsInteractorImpl(mExecutor, mMainThread, this, new AppRepositoryImpl(), serviceId);
        fetchSalonDetailsInteractor.execute();
        mView.showLoader();
    }

    @Override
    public void fetchShippingAddressList() {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo == null) {
        } else {
            FetchShippingAddressInteractorImpl fetchShippingAddressInteractor = new FetchShippingAddressInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, userInfo.userId);
            fetchShippingAddressInteractor.execute();
        }
    }

    @Override
    public void fetchWishlist() {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo == null) {
        } else {
            FetchWishlistInteractorImpl fetchWishlistInteractor = new FetchWishlistInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken);
            fetchWishlistInteractor.execute();
        }
    }

    @Override
    public void addToWishlist(int vendorId) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo == null) {
        } else {
            AddToWishlistInteractorImpl addToWishlistInteractor = new AddToWishlistInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, vendorId);
            addToWishlistInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void removeFromWishlist(int wishlistId) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo == null) {
        } else {
            RemoveFromWishlistInteractorImpl removeFromWishlistInteractor = new RemoveFromWishlistInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, wishlistId);
            removeFromWishlistInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void onGettingSalonDetailsSuccess(SalonUser salonUser) {
        mView.loadSalonData(salonUser);
        mView.hideLoader();
    }

    @Override
    public void onGettingSalonDetailsFail(String errorMsg) {
        mView.hideLoader();
    }

    @Override
    public void onGettingAddressListSucces(ShippingAddress[] shippingAddresses) {
        if (shippingAddresses.length > 0) {
            shippingAddresses[0].isSelected = true;
        }
        this.shippingAddresses = shippingAddresses;
        addressAdapter = new BookingAddressAdapter(mContext, this.shippingAddresses, this);
        mView.loadAddress(addressAdapter);
    }

    @Override
    public void onGettingAddressListFail(String errorMsg, int loginError) {
    }

    @Override
    public void onAddressSelected(int id) {
        for (int i = 0; i < shippingAddresses.length; i++) {
            if (shippingAddresses[i].id == id) {
                shippingAddresses[i].isSelected = true;
            } else {
                shippingAddresses[i].isSelected = false;
            }
        }
        addressAdapter.updateDataSet(this.shippingAddresses);
    }

    @Override
    public void onEditClicked(int id) {

    }

    @Override
    public void onDeliverButtonClicked(int addressId) {
        mView.onBookNowClicked(addressId);
    }

    @Override
    public void onFetchWishlistSuccess(Wishlist[] wishlists) {
        mView.loadWishlists(wishlists);
    }

    @Override
    public void onFetchWishlistFail(String errorMsg, int loginError) {

    }

    @Override
    public void onAddToWishlistSuccess() {
        mView.hideLoader();
        mView.onAddToWishlistSuccess();
    }

    @Override
    public void onAddToWishlistFail(String errorMsg, int loginError) {
        mView.hideLoader();
        Toasty.warning(mContext, errorMsg).show();
    }

    @Override
    public void onRemoveFromWishlistSuccess() {
        mView.hideLoader();
        mView.onRemoveFromWishlistSuccess();
    }

    @Override
    public void onRemoveFromWishlistFail(String errorMsg, int loginError) {
        mView.hideLoader();
        Toasty.warning(mContext, errorMsg).show();
    }

}
