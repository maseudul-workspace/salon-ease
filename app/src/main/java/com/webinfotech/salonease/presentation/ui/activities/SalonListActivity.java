package com.webinfotech.salonease.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.util.Log;

import com.google.android.material.tabs.TabLayout;
import com.webinfotech.salonease.R;
import com.webinfotech.salonease.domain.executors.impl.ThreadExecutor;
import com.webinfotech.salonease.domain.models.Category;
import com.webinfotech.salonease.domain.models.City;
import com.webinfotech.salonease.domain.models.ThirdCategory;
import com.webinfotech.salonease.presentation.presenters.SalonListPresenter;
import com.webinfotech.salonease.presentation.presenters.impl.SalonListPresenterImpl;
import com.webinfotech.salonease.presentation.ui.adapters.FragmentsAdapter;
import com.webinfotech.salonease.presentation.ui.adapters.SalonDataAdapter;
import com.webinfotech.salonease.presentation.ui.dialogs.CustomProgressDialog;
import com.webinfotech.salonease.presentation.ui.fragments.SalonListFragment;
import com.webinfotech.salonease.threading.MainThreadImpl;

public class SalonListActivity extends AppCompatActivity implements SalonListPresenter.View {

    @BindView(R.id.tab_layout)
    TabLayout tabLayout;
    @BindView(R.id.view_pager)
    ViewPager viewPager;
    CustomProgressDialog progressDialog;
    SalonListPresenterImpl mPresenter;
    int categoryId;
    int serviceCityId;
    int clientType;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_salon_list2);
        categoryId = getIntent().getIntExtra("categoryId", 0);
        serviceCityId = getIntent().getIntExtra("serviceCityId", 0);
        clientType = getIntent().getIntExtra("clientType", 0);
        ButterKnife.bind(this);
        setUpProgressDialog();
        initialisePresenter();
        mPresenter.fetchThirdCategories(categoryId);
    }

    private void initialisePresenter() {
        mPresenter = new SalonListPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    public void setUpProgressDialog() {
        progressDialog = new CustomProgressDialog(this, this);
        progressDialog.setUpDialog();
    }

    @Override
    public void loadData(SalonDataAdapter adapter, int totalPage) {

    }

    @Override
    public void loadCities(City[] cities) {

    }

    @Override
    public void loadCategories(Category[] categories) {

    }

    @Override
    public void showLoader() {
        progressDialog.showDialog();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismissDialog();
    }

    @Override
    public void goToSalonDetails(int serviceId) {

    }

    @Override
    public void hideRecyclerView() {

    }

    @Override
    public void loadThirdCategories(ThirdCategory[] thirdCategories) {
        FragmentsAdapter fragmentsAdapter = new FragmentsAdapter(getSupportFragmentManager());
        for (int i = 0; i < thirdCategories.length; i++) {
            Bundle bundle = new Bundle();
            bundle.putInt("categoryId", thirdCategories[i].id);
            bundle.putInt("serviceCityId", serviceCityId);
            bundle.putInt("clientType", clientType);
            SalonListFragment salonListFragment = new SalonListFragment();
            salonListFragment.setArguments(bundle);
            fragmentsAdapter.AddFragment(salonListFragment, thirdCategories[i].name);
        }

        viewPager.setAdapter(fragmentsAdapter);
        tabLayout.setupWithViewPager(viewPager);

    }
}