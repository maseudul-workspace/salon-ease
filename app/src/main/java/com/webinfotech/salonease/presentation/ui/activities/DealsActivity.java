package com.webinfotech.salonease.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.content.Intent;
import android.os.Bundle;

import com.webinfotech.salonease.AndroidApplication;
import com.webinfotech.salonease.R;
import com.webinfotech.salonease.domain.executors.MainThread;
import com.webinfotech.salonease.domain.executors.impl.ThreadExecutor;
import com.webinfotech.salonease.domain.models.SalonUser;
import com.webinfotech.salonease.presentation.presenters.DealofthedayPresenter;
import com.webinfotech.salonease.presentation.presenters.impl.DealofthedayPresenterImpl;
import com.webinfotech.salonease.presentation.ui.adapters.DealsAdapter;
import com.webinfotech.salonease.presentation.ui.adapters.DealsListAdapter;
import com.webinfotech.salonease.presentation.ui.dialogs.CustomProgressDialog;
import com.webinfotech.salonease.threading.MainThreadImpl;

public class DealsActivity extends AppCompatActivity implements DealofthedayPresenter.View, DealsListAdapter.Callback {

    @BindView(R.id.recycler_view_deals)
    RecyclerView recyclerViewDeals;
    DealofthedayPresenterImpl mPresenter;
    CustomProgressDialog customProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deals);
        ButterKnife.bind(this);
        initialisePresenter();
        setUpProgressDialog();
        AndroidApplication androidApplication = (AndroidApplication) getApplicationContext();
        mPresenter.fetchDeals(androidApplication.getLatitude(), androidApplication.getLongitude());
    }

    private void initialisePresenter() {
        mPresenter = new DealofthedayPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    private void setUpProgressDialog() {
        customProgressDialog = new CustomProgressDialog(this, this);
        customProgressDialog.setUpDialog();
    }

    @Override
    public void loadDate(SalonUser[] salonUsers) {
        DealsListAdapter dealsAdapter = new DealsListAdapter(this, salonUsers, this::onSalonClicked);
        recyclerViewDeals.setAdapter(dealsAdapter);
        recyclerViewDeals.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public void showLoader() {
        customProgressDialog.showDialog();
    }

    @Override
    public void hideLoader() {
        customProgressDialog.dismissDialog();
    }

    @Override
    public void onSalonClicked(int id) {
        Intent intent = new Intent(this, SalonDetailsActivity.class);
        intent.putExtra("salonId", id);
        startActivity(intent);
    }

    @OnClick(R.id.btn_back) void onBackClicked()
    {
        finish();
    }
}