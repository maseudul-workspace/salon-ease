package com.webinfotech.salonease.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.goodiebag.pinview.Pinview;
import com.webinfotech.salonease.R;
import com.webinfotech.salonease.domain.executors.impl.ThreadExecutor;
import com.webinfotech.salonease.presentation.presenters.ForgetPasswordOtpVerificationPresenter;
import com.webinfotech.salonease.presentation.presenters.impl.ForgetPaswordOtpVerficationPresenterImpl;
import com.webinfotech.salonease.threading.MainThreadImpl;
import com.webinfotech.salonease.util.GlideHelper;

public class ForgetPasswordOtpVerificationActivity extends AppCompatActivity implements ForgetPasswordOtpVerificationPresenter.View {

    @BindView(R.id.img_view_otp)
    ImageView imgViewOtp;
    @BindView(R.id.edit_text_phone)
    EditText editTextPhone;
    @BindView(R.id.layout_otp)
    View layoutOtp;
    @BindView(R.id.layout_phone)
    View layoutPhone;
    @BindView(R.id.pinview)
    Pinview pinview;
    ForgetPaswordOtpVerficationPresenterImpl mPresenter;
    ProgressDialog progressDialog;
    String otp;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password_otp_verification);
        ButterKnife.bind(this);
        GlideHelper.setImageView(this, imgViewOtp, "https://resize.indiatvnews.com/en/resize/newbucket/1200_-/2020/04/pixel-4-1586243178.jpg");
        setUpProgressDialog();
        initialisePresenter();
    }

    private void initialisePresenter() {
        mPresenter = new ForgetPaswordOtpVerficationPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    public void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @Override
    public void onOtpSendSuccess(String otp) {
        this.otp = otp;
        layoutPhone.setVisibility(View.GONE);
        layoutOtp.setVisibility(View.VISIBLE);
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @OnClick(R.id.btn_submit) void onSubmitClicked() {
        if (editTextPhone.getText().toString().trim().isEmpty()) {
            Toast.makeText(this, "Please Enter Your 10 Digits Phone No", Toast.LENGTH_LONG).show();
        } else if (editTextPhone.getText().toString().trim().length() < 10) {
            Toast.makeText(this, "Phone No Must Be 10 Digits", Toast.LENGTH_LONG).show();
        } else {
            mPresenter.sendOtp(editTextPhone.getText().toString());
        }
    }

    @OnClick(R.id.btn_check_otp) void onOtpCheckClicked() {
        if (pinview.getValue().length() < 5) {
            Toast.makeText(this, "OTP should be 5 digit", Toast.LENGTH_SHORT).show();
        } else {
            if (pinview.getValue().toString().equals(otp)) {
                Intent intent = new Intent(this, ForgetPasswordActivity.class);
                intent.putExtra("mobile", editTextPhone.getText().toString());
                intent.putExtra("otp", otp);
                startActivity(intent);
                finish();
            } else {
                Toast.makeText(this, "Wrong Otp", Toast.LENGTH_SHORT).show();
            }
        }
    }

    @OnClick(R.id.img_view_back) void onBackClicked() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

}