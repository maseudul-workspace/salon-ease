package com.webinfotech.salonease.presentation.presenters.impl;

import android.content.Context;

import com.webinfotech.salonease.domain.executors.Executor;
import com.webinfotech.salonease.domain.executors.MainThread;
import com.webinfotech.salonease.domain.interactors.FetchDealsInteractor;
import com.webinfotech.salonease.domain.interactors.impl.FetchDealsInteractorImpl;
import com.webinfotech.salonease.domain.models.SalonUser;
import com.webinfotech.salonease.presentation.presenters.DealofthedayPresenter;
import com.webinfotech.salonease.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.salonease.repository.AppRepositoryImpl;

public class DealofthedayPresenterImpl extends AbstractPresenter implements DealofthedayPresenter, FetchDealsInteractor.Callback {

    Context mContext;
    DealofthedayPresenter.View mView;

    public DealofthedayPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchDeals(double latitude, double longitude) {
        FetchDealsInteractorImpl fetchDealsInteractor = new FetchDealsInteractorImpl(mExecutor, mMainThread, this, new AppRepositoryImpl(), latitude, longitude);
        fetchDealsInteractor.execute();
        mView.showLoader();
    }

    @Override
    public void onGettingDealsSuccess(SalonUser[] salonUsers) {
        mView.hideLoader();
        mView.loadDate(salonUsers);
    }

    @Override
    public void onGettingDealsFail(String errorMsg) {
        mView.hideLoader();
    }
}
