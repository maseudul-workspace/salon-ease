package com.webinfotech.salonease.presentation.ui.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.webinfotech.salonease.R;
import com.webinfotech.salonease.domain.models.ShippingAddress;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ShippingAddressAdapter extends RecyclerView.Adapter<ShippingAddressAdapter.ViewHolder> {

    public interface Callback {
        void onEditClicked(int id);
        void onDeleteClicked(int id);
    }

    Context mContext;
    ShippingAddress[] addresses;
    Callback mCallback;

    public ShippingAddressAdapter(Context mContext, ShippingAddress[] addresses, Callback mCallback) {
        this.mContext = mContext;
        this.addresses = addresses;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_shipping_address, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewUserName.setText(addresses[position].name);
        holder.txtViewVillage.setText(addresses[position].city);
        holder.txtViewAddress.setText(addresses[position].address);
        holder.txtViewDistPin.setText("Postal Code - " + addresses[position].pin);
        holder.txtViewMobileEmail.setText("Mobile - " + addresses[position].mobile + ",  Email - " + addresses[position].email);
        holder.txtViewEditAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onEditClicked(addresses[position].id);
            }
        });
        holder.txtViewDeleteAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                builder.setTitle("Confirm dialog demo !");
                builder.setMessage("You are about to delete an address. Do you really want to proceed ?");
                builder.setCancelable(false);
                builder.setPositiveButton(Html.fromHtml("<font color='#000000'>Yes</font>"), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mCallback.onDeleteClicked(addresses[position].id);
                    }
                });

                builder.setNegativeButton(Html.fromHtml("<font color='#000000'>No</font>"), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                    }
                });

                builder.show();
            }
        });
    }

    @Override
    public int getItemCount() {
        return addresses.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_user_name)
        TextView txtViewUserName;
        @BindView(R.id.txt_view_address)
        TextView txtViewAddress;
        @BindView(R.id.txt_view_village)
        TextView txtViewVillage;
        @BindView(R.id.txt_view_dist_pin)
        TextView txtViewDistPin;
        @BindView(R.id.txt_view_mobile_email)
        TextView txtViewMobileEmail;
        @BindView(R.id.txt_view_edit_address)
        TextView txtViewEditAddress;
        @BindView(R.id.txt_view_delete_address)
        TextView txtViewDeleteAddress;
        @BindView(R.id.main_layout)
        View mainLayout;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void updateDataSet(ShippingAddress[] shippingAddresses) {
        this.addresses = shippingAddresses;
        notifyDataSetChanged();
    }

}
