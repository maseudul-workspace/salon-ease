package com.webinfotech.salonease.presentation.ui.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.webinfotech.salonease.R;
import com.webinfotech.salonease.domain.models.Orders;
import com.webinfotech.salonease.util.GlideHelper;
import com.webinfotech.salonease.util.Helper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class OrdersAdapter extends RecyclerView.Adapter<OrdersAdapter.ViewHolder> {

    public interface Callback {
        void onCancelClicked(int orderId, int isRefund);
        void onFeedBackClicked(int clientId);
        void changeOrderStatus(int orderId, int status);
    }

    Context mContext;
    Orders[] orders;
    Callback mCallback;

    public OrdersAdapter(Context mContext, Orders[] orders, Callback mCallback) {
        this.mContext = mContext;
        this.orders = orders;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_orders, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        try {
            int isRefund;
            holder.txtViewOrderNo.setText("ORDER NO " + orders[position].id);
            holder.txtViewOrderDate.setText("Service Date: " + changeDataFormat(orders[position].serviceTime));
            GlideHelper.setImageView(mContext, holder.imgViewSalon, mContext.getResources().getString(R.string.base_url) + "images/client/" + orders[position].vendorDetails.image);
            holder.txtViewPayableAmount.setText("Rs. " + (orders[position].amount - orders[position].advanceAmount - orders[position].discount));
            holder.txtViewAdvanceAmount.setText("Rs. " + orders[position].advanceAmount);
            holder.txtViewSalonName.setText(orders[position].vendorDetails.name);
            if (orders[position].paymentStatus == 1) {
                holder.txtViewPaymentStatus.setText("Failed");
                isRefund = 1;
            } else {
                holder.txtViewPaymentStatus.setText("Paid");
                isRefund = 2;
            }

            switch (orders[position].orderStatus) {
                case 1:
                    holder.txtViewOrderStatus.setText("Processing");
                    holder.txtViewOrderStatus.setTextColor(mContext.getResources().getColor(R.color.md_blue_500));
                    if (orders[position].vendorDetails.clientType == 1) {
                        if (Helper.getHoursDifference(orders[position].serviceTime) > 2) {
                            isRefund = 2;
                        } else {
                            isRefund = 1;
                        }
                    } else {
                        if (Helper.getMinuteDifference(orders[position].serviceTime) > 30) {
                            isRefund = 2;
                        } else {
                            isRefund = 1;
                        }
                    }
                    holder.btnCancel.setVisibility(View.VISIBLE);
                    holder.btnFeedback.setVisibility(View.GONE);
                    holder.layoutCancellation.setVisibility(View.GONE);
                    break;
                case 2:
                    holder.txtViewOrderStatus.setText("Accepted");
                    holder.txtViewOrderStatus.setTextColor(mContext.getResources().getColor(R.color.md_teal_A700));
                    holder.btnCancel.setVisibility(View.VISIBLE);
                    holder.btnFeedback.setVisibility(View.GONE);
                    holder.layoutCancellation.setVisibility(View.GONE);
                    break;
                case 3:
                    holder.txtViewOrderStatus.setText("Rescheduled");
                    holder.txtViewOrderStatus.setTextColor(mContext.getResources().getColor(R.color.md_blue_500));
                    if (orders[position].vendorDetails.clientType == 1) {
                        if (Helper.getHoursDifference(orders[position].serviceTime) > 2) {
                            isRefund = 2;
                        } else {
                            isRefund = 1;
                        }
                    } else {
                        if (Helper.getMinuteDifference(orders[position].serviceTime) > 30) {
                            isRefund = 2;
                        } else {
                            isRefund = 1;
                        }
                    }
                    holder.btnCancel.setVisibility(View.VISIBLE);
                    holder.btnFeedback.setVisibility(View.GONE);
                    holder.layoutCancellation.setVisibility(View.GONE);
                    break;
                case 4:
                    holder.txtViewOrderStatus.setText("Completed");
                    holder.txtViewOrderStatus.setTextColor(mContext.getResources().getColor(R.color.md_green_500));
                    holder.btnCancel.setVisibility(View.GONE);
                    holder.btnFeedback.setVisibility(View.VISIBLE);
                    holder.layoutCancellation.setVisibility(View.GONE);
                    break;
                case 5:
                    holder.txtViewOrderStatus.setText("Cancelled");
                    holder.txtViewOrderStatus.setTextColor(mContext.getResources().getColor(R.color.md_red_400));
                    holder.btnCancel.setVisibility(View.GONE);
                    holder.btnFeedback.setVisibility(View.GONE);
                    break;
            }

            int finalIsRefund = isRefund;
            holder.btnCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String cancelMsg = "";

                    if (finalIsRefund == 1) {
                        if (orders[position].vendorDetails.clientType == 1) {
                            cancelMsg = "You have cross the limit of refund period of before 2 hours. Do you really want to proceed ?";
                        } else {
                            cancelMsg = "You have cross the limit of refund period of before 30 minutes. Do you really want to proceed ?";
                        }
                    } else {
                        cancelMsg = "You are about to cancel a order. Do you really want to proceed ?";
                    }
                    AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                    builder.setTitle("Confirmation dialog");
                    builder.setMessage(cancelMsg);
                    builder.setCancelable(false);
                    builder.setPositiveButton(Html.fromHtml("<font color='black'><b>Confirm</b></font>"), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            mCallback.onCancelClicked(orders[position].id, finalIsRefund);
                        }
                    });
                    builder.setNegativeButton(Html.fromHtml("<font color='#FF1744'><b>No</b></font>"), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    builder.show();
                }
            });

            holder.btnFeedback.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mCallback.onFeedBackClicked(orders[position].vendorDetails.id);
                }
            });

            OrderServicesAdapter adapter = new OrderServicesAdapter(mContext, orders[position].serviceOrders);
            holder.recyclerViewOrderServices.setLayoutManager(new LinearLayoutManager(mContext));
            holder.recyclerViewOrderServices.setAdapter(adapter);
            holder.recyclerViewOrderServices.addItemDecoration(new DividerItemDecoration(mContext, DividerItemDecoration.VERTICAL));

            holder.layoutServicesHeader.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (holder.recyclerViewOrderServices.getVisibility() == View.VISIBLE) {
                        holder.recyclerViewOrderServices.setVisibility(View.GONE);
                    } else {
                        holder.recyclerViewOrderServices.setVisibility(View.VISIBLE);
                    }
                }
            });

        } catch (Exception e) {

        }

        if (orders[position].discount < 0.1) {
            holder.txtViewDiscount.setText("Rs. 0");
        } else {
            holder.txtViewDiscount.setText("Rs. " + orders[position].discount);
        }

        if (orders[position].paymentMethod == 1) {
            holder.txtViewPaymentMethod.setText("Online");
        } else {
            holder.txtViewPaymentMethod.setText("Wallet Pay");
        }

        if (orders[position].paymentId == null || orders[position].paymentId.isEmpty()) {
            holder.layoutPaymentId.setVisibility(View.GONE);
        } else {
            holder.txtViewPaymentId.setText(orders[position].paymentId);
            holder.layoutPaymentId.setVisibility(View.VISIBLE);
        }

        if (orders[position].vendorCancelStatus == 2 ) {
            if (orders[position].orderStatus == 6) {
                holder.btnChangeVendor.setVisibility(View.GONE);
                holder.btnCancel.setVisibility(View.GONE);
                holder.btnFeedback.setVisibility(View.GONE);
                holder.txtViewStatus.setText("Request for changing vendor sent");
            } else if ( orders[position].orderStatus == 5) {
                holder.btnChangeVendor.setVisibility(View.GONE);
                holder.btnCancel.setVisibility(View.GONE);
                holder.btnFeedback.setVisibility(View.GONE);
                holder.txtViewStatus.setText("");
            } else {
                holder.btnChangeVendor.setVisibility(View.VISIBLE);
                holder.btnCancel.setVisibility(View.GONE);
                holder.btnFeedback.setVisibility(View.GONE);
                holder.layoutCancellation.setVisibility(View.VISIBLE);
                if (orders[position].vendorCancelReason == null) {
                    holder.txtViewCancelReason.setText("Reason is not specified");
                } else {
                    holder.txtViewCancelReason.setText(orders[position].vendorCancelReason);
                }
                holder.txtViewStatus.setText("Vendor cancelled the order");
            }
        } else {
            holder.btnChangeVendor.setVisibility(View.GONE);
            holder.layoutCancellation.setVisibility(View.GONE);
            holder.txtViewStatus.setText("");
            holder.layoutCancellation.setVisibility(View.GONE);
        }

        holder.btnChangeVendor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                builder.setTitle("Confirmation Message");
                builder.setMessage("Do you want to change vendor or cancel the order ?");
                builder.setCancelable(false);
                builder.setPositiveButton(Html.fromHtml("<font color='#00BFA5'>Change</font>"), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mCallback.changeOrderStatus(orders[position].id, 2);
                    }
                });

                builder.setNegativeButton(Html.fromHtml("<font color='#FF1744'>Cancel</font>"), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        mCallback.changeOrderStatus(orders[position].id, 1);
                    }
                });

                builder.show();
            }
        });

        if (orders[position].vendorDetails.clientType == 1) {
            holder.imgViewSalonType.setImageDrawable(mContext.getResources().getDrawable(R.drawable.home_small_icon));
            holder.txtViewSalonType.setText("Home Service");
        } else {
            holder.imgViewSalonType.setImageDrawable(mContext.getResources().getDrawable(R.drawable.barber_chair_small));
            holder.txtViewSalonType.setText("On Site Salon");
        }

    }

    @Override
    public int getItemCount() {
        return orders.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_order_date)
        TextView txtViewOrderDate;
        @BindView(R.id.txt_view_order_no)
        TextView txtViewOrderNo;
        @BindView(R.id.txt_view_payable_amount)
        TextView txtViewPayableAmount;
        @BindView(R.id.txt_view_advance_amount)
        TextView txtViewAdvanceAmount;
        @BindView(R.id.txt_view_order_status)
        TextView txtViewOrderStatus;
        @BindView(R.id.img_view_salon)
        ImageView imgViewSalon;
        @BindView(R.id.txt_view_salon_name)
        TextView txtViewSalonName;
        @BindView(R.id.txt_view_payment_status)
        TextView txtViewPaymentStatus;
        @BindView(R.id.txt_view_cancel_reason)
        TextView txtViewCancelReason;
        @BindView(R.id.btn_cancel)
        Button btnCancel;
        @BindView(R.id.btn_feedback)
        Button btnFeedback;
        @BindView(R.id.btn_change_vendor)
        Button btnChangeVendor;
        @BindView(R.id.layout_cancellation)
        View layoutCancellation;
        @BindView(R.id.recycler_view_order_services)
        RecyclerView recyclerViewOrderServices;
        @BindView(R.id.layout_services_header)
        View layoutServicesHeader;
        @BindView(R.id.txt_view_coupon_discount)
        TextView txtViewDiscount;
        @BindView(R.id.txt_view_payment_method)
        TextView txtViewPaymentMethod;
        @BindView(R.id.txt_view_payment_id)
        TextView txtViewPaymentId;
        @BindView(R.id.layout_payment_id)
        View layoutPaymentId;
        @BindView(R.id.txt_view_status)
        TextView txtViewStatus;
        @BindView(R.id.img_view_salon_type)
        ImageView imgViewSalonType;
        @BindView(R.id.txt_view_salon_type)
        TextView txtViewSalonType;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    private String changeDataFormat(String dateString) {
        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        String outputPattern = "EEEE, dd MMM, yyyy hh:mm a";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(dateString);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

}
