package com.webinfotech.salonease.presentation.presenters;

import com.webinfotech.salonease.domain.models.PaymentData;

import java.util.ArrayList;

public interface PaymentPresenter {
    void fetchWalletHistory();
    void placeOrder( int vendorId,
                     ArrayList<Integer> serviceIds,
                     ArrayList<Integer> quantities,
                     String serviceTime,
                     String addressId,
                     String offerId,
                     String offerJobId,
                     String couponId,
                     int isWallet);
    void verifyPayment( String razorpayOrderId,
                        String razorpayPaymentId,
                        String razorpaySignature,
                        int orderId);
    interface View {
        void goToOrderHistory();
        void initiatePayment(PaymentData paymentData, int orderId);
        void onPaymentVerificationSuccess();
        void showLoader();
        void hideLoader();
        void loadWalletAmount(double amount);
    }
}
