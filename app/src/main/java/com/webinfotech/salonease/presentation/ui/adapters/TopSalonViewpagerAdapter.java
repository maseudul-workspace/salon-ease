package com.webinfotech.salonease.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.webinfotech.salonease.R;
import com.webinfotech.salonease.domain.models.SalonUser;
import com.webinfotech.salonease.domain.models.SalonUserContainer;
import com.webinfotech.salonease.util.GlideHelper;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import es.dmoral.toasty.Toasty;

public class TopSalonViewpagerAdapter extends PagerAdapter {

    public interface Callback {
        void onSalonClicked(int salonId);
    }

    Context mContext;
    ArrayList<SalonUserContainer> salonUserContainers;
    Callback mCallback;

    public TopSalonViewpagerAdapter(Context mContext, ArrayList<SalonUserContainer> salonUserContainers, Callback mCallback) {
        this.mContext = mContext;
        this.salonUserContainers = salonUserContainers;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.layout_viewpager_category, container, false);
        ImageView imageViewCategory1 = (ImageView) layout.findViewById(R.id.img_view_category1);
        ImageView imageViewCategory2 = (ImageView) layout.findViewById(R.id.img_view_category2);
        TextView txtViewCategoryName1 = (TextView) layout.findViewById(R.id.txt_view_category1_name);
        TextView txtViewCategoryName2 = (TextView) layout.findViewById(R.id.txt_view_category2_name);
        View viewCategory1 = (View) layout.findViewById(R.id.view_category_1);
        View viewCategory2 = (View) layout.findViewById(R.id.view_category_2);
        ArrayList<SalonUser> salonUsers = salonUserContainers.get(position).salonUsers;
        GlideHelper.setImageViewCustomRoundedCorners(mContext, imageViewCategory1, mContext.getResources().getString(R.string.base_url) + "images/client/" + salonUsers.get(0).image, 8);
        if (salonUsers.size() > 1) {
            GlideHelper.setImageViewCustomRoundedCorners(mContext, imageViewCategory2, mContext.getResources().getString(R.string.base_url) + "images/client/" + salonUsers.get(1).image, 8);
            txtViewCategoryName2.setText(salonUsers.get(1).name);
            viewCategory2.setVisibility(View.VISIBLE);
            viewCategory2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (salonUsers.get(1).services.length == 0 || salonUsers.get(1).services == null) {
                        Toasty.warning(mContext, "No services provided by this salon").show();
                    } else {
                        mCallback.onSalonClicked(salonUsers.get(1).services[0].id);
                    }
                }
            });
        } else {
            viewCategory2.setVisibility(View.INVISIBLE);
        }
        viewCategory1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (salonUsers.get(0).services.length == 0 || salonUsers.get(0).services == null) {
                    Toasty.warning(mContext, "No services provided by this salon").show();
                } else {
                    mCallback.onSalonClicked(salonUsers.get(0).services[0].id);
                }
            }
        });
        txtViewCategoryName1.setText(salonUsers.get(0).name);
        container.addView(layout);
        return layout;
    }

    @Override
    public int getCount() {
        return salonUserContainers.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }

}
