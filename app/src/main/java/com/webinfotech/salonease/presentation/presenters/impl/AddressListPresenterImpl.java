package com.webinfotech.salonease.presentation.presenters.impl;

import android.content.Context;

import com.webinfotech.salonease.AndroidApplication;
import com.webinfotech.salonease.domain.executors.Executor;
import com.webinfotech.salonease.domain.executors.MainThread;
import com.webinfotech.salonease.domain.interactors.FetchShippingAddressInteractor;
import com.webinfotech.salonease.domain.interactors.impl.FetchShippingAddressInteractorImpl;
import com.webinfotech.salonease.domain.models.ShippingAddress;
import com.webinfotech.salonease.domain.models.UserInfo;
import com.webinfotech.salonease.presentation.presenters.AddressListPresenter;
import com.webinfotech.salonease.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.salonease.presentation.ui.adapters.ShippingAddressAdapter;
import com.webinfotech.salonease.repository.AppRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class AddressListPresenterImpl extends AbstractPresenter implements  AddressListPresenter,
                                                                            FetchShippingAddressInteractor.Callback,
                                                                            ShippingAddressAdapter.Callback
{

    Context mContext;
    AddressListPresenter.View mView;

    public AddressListPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchShippingAddress() {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo user = androidApplication.getUserInfo(mContext);
        if (user == null) {
            mView.showLoginBottomSheet();
            Toasty.warning(mContext, "Your Session Expired!! Please Login Again").show();
        } else {
            FetchShippingAddressInteractorImpl fetchShippingAddressInteractor = new FetchShippingAddressInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, user.apiToken, user.userId);
            fetchShippingAddressInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void onGettingAddressListSucces(ShippingAddress[] shippingAddresses) {
        if (shippingAddresses.length > 0) {
            ShippingAddressAdapter shippingAddressAdapter = new ShippingAddressAdapter(mContext, shippingAddresses, this);
            mView.loadAdapter(shippingAddressAdapter);
        }
        mView.hideLoader();
    }

    @Override
    public void onGettingAddressListFail(String errorMsg, int loginError) {
        mView.hideLoader();
        if (loginError == 1) {
            Toasty.warning(mContext, errorMsg).show();
            mView.showLoginBottomSheet();
        } else {
            mView.onAddressFetchFailed();
        }
    }

    @Override
    public void onEditClicked(int id) {
        mView.goToAddressDetails(id);
    }

    @Override
    public void onDeleteClicked(int id) {

    }
}
