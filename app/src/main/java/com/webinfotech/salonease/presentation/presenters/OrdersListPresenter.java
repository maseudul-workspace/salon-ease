package com.webinfotech.salonease.presentation.presenters;

import com.webinfotech.salonease.presentation.ui.adapters.OrdersAdapter;

public interface OrdersListPresenter {
    void fetchOrderHistories();
    void submitRating(int rating, String feedback);
    interface View {
        void loadAdapter(OrdersAdapter ordersAdapter);
        void showBankSnackbar();
        void onFeedbackClicked();
        void showLoader();
        void hideLoader();
        void showLoginBottomSheet();
    }
}
