package com.webinfotech.salonease.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.webinfotech.salonease.R;
import com.webinfotech.salonease.domain.models.CombosCart;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class CartCombosAdapter extends RecyclerView.Adapter<CartCombosAdapter.ViewHolder> {

    public interface Callback {
        void onComboQtyClicked(int position);
    }

    Context mContext;
    ArrayList<CombosCart> combosCarts;
    Callback mCallback;

    public CartCombosAdapter(Context mContext, ArrayList<CombosCart> combosCarts, Callback mCallback) {
        this.mContext = mContext;
        this.combosCarts = combosCarts;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_cart_combos, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewComboName.setText(combosCarts.get(position).comboName);
        holder.txtViewPrice.setText("Rs. " + (combosCarts.get(position).price * combosCarts.get(position).qty));
        holder.txtViewQty.setText("Qty: " + combosCarts.get(position).qty);
        holder.layoutQty.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onComboQtyClicked(position);
            }
        });
        ComboServiceAdapter adapter = new ComboServiceAdapter(mContext, combosCarts.get(position).comboServices);
        holder.recyclerViewCombos.setAdapter(adapter);
        holder.recyclerViewCombos.setLayoutManager(new LinearLayoutManager(mContext));
    }

    @Override
    public int getItemCount() {
        return combosCarts.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_combo_name)
        TextView txtViewComboName;
        @BindView(R.id.recycler_view_combos)
        RecyclerView recyclerViewCombos;
        @BindView(R.id.txt_view_price)
        TextView txtViewPrice;
        @BindView(R.id.layout_qty)
        View layoutQty;
        @BindView(R.id.txt_view_qty)
        TextView txtViewQty;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void setCombosCarts(ArrayList<CombosCart> combosCarts) {
        this.combosCarts = combosCarts;
        notifyDataSetChanged();
    }

}
