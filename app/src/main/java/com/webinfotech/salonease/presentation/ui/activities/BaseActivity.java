package com.webinfotech.salonease.presentation.ui.activities;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.dynamiclinks.DynamicLink;
import com.google.firebase.dynamiclinks.FirebaseDynamicLinks;
import com.google.firebase.dynamiclinks.ShortDynamicLink;
import com.webinfotech.salonease.AndroidApplication;
import com.webinfotech.salonease.R;
import com.webinfotech.salonease.domain.models.UserInfo;
import com.webinfotech.salonease.util.Helper;

import butterknife.BindView;
import butterknife.ButterKnife;

public class BaseActivity extends AppCompatActivity  {

    @BindView(R.id.toolbar) @Nullable
    Toolbar toolbar;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    ActionBarDrawerToggle actionBarDrawerToggle;
    @BindView(R.id.navigation)
    NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);
    }

    protected void inflateContent(@LayoutRes int inflateResID){
        setContentView(R.layout.activity_base);
        FrameLayout contentFramelayout = (FrameLayout) findViewById(R.id.content_frame);
        getLayoutInflater().inflate(inflateResID, contentFramelayout);
        ButterKnife.bind(this);
        setToolbar();
        setUpNavigationView();
        assert toolbar != null;
        toolbar.findViewById(R.id.notification_bell).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(isLoggedIn())
                {
                  goToNotificationActivity();
                }
                else
                {
                    Helper.showLoginBottomSheet(getSupportFragmentManager());
                }
            }
        });
    }


    @Override
    protected void onResume() {
        super.onResume();
        setToolbar();
    }

    public void setToolbar() {
        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.app_name, R.string.app_name);
    }

    public void setUpNavigationView() {
        Menu navMenu = navigationView.getMenu();
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {

            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();
                switch (id) {
                    case R.id.nav_home:
                        break;

                    case R.id.nav_search:
                        goToSearchActivity();
                        break;

                    case R.id.nav_about:
                        goToAboutActivity();
                        break;

                    case R.id.nav_booking:
                        if(isLoggedIn()) {
                            goToBookingListActivity();
                        }else {
                            Helper.showLoginBottomSheet(getSupportFragmentManager());
                        }
                        break;
                    case R.id.nav_wishlist:
                        if(isLoggedIn()) {
                            goToWishlistActivity();
                        }else {
                            Helper.showLoginBottomSheet(getSupportFragmentManager());
                        }
                        break;

                    case R.id.nav_user:
                        if(isLoggedIn()) {
                            goToUserActivity();
                        }else {
                            Helper.showLoginBottomSheet(getSupportFragmentManager());
                        }
                        break;

                    case R.id.nav_contact_us:
                        goToContactUsActivity();
                        break;
                    case R.id.nav_feedback:
                        if(isLoggedIn()) {
                            goToFeedbackActivity();
                        }else {
                            Helper.showLoginBottomSheet(getSupportFragmentManager());
                        }
                        break;
                    case R.id.nav_wallet:
                        if(isLoggedIn()) {
                            goToWalletActivity();
                        }else {
                            Helper.showLoginBottomSheet(getSupportFragmentManager());
                        }
                        break;
                    case R.id.nav_refer_earn:
                        if (isLoggedIn()) {
                            gotoReferlEarnActivity();
                        } else {
                            Helper.showLoginBottomSheet(getSupportFragmentManager());
                        }
                        break;
                }
                return false;
            }
        });
    }

    private void gotoReferlEarnActivity() {
        Intent intent = new Intent(this, ReferalEarnActivity.class);
        closeDrawer();
        startActivity(intent);
    }

    private void goToFeedbackActivity() {
        Intent intent = new Intent(this, EnquiryActivity.class);
        closeDrawer();
        startActivity(intent);
    }

    private void goToAboutActivity() {
        Intent intent = new Intent(this, AboutActivity.class);
        closeDrawer();
        startActivity(intent);
    }

    private void goToContactUsActivity() {
        Intent intent = new Intent(this, ContactUsActivity.class);
        closeDrawer();
        startActivity(intent);
    }

    private void goToWishlistActivity() {
        Intent intent = new Intent(this, WishListActivity.class);
        closeDrawer();
        startActivity(intent);
    }

    private void goToWalletActivity() {
        Intent intent = new Intent(this, WalletActivity.class);
        closeDrawer();
        startActivity(intent);
    }

    private void goToBookingListActivity() {
        Intent intent = new Intent(this, OrderHistoryActivity.class);
        startActivity(intent);
    }

    private void goToSearchActivity() {
        Intent intent = new Intent(this, SearchActivity.class);
        startActivity(intent);
    }

    private void goToUserActivity() {
        Intent intent = new Intent(this, UserActivity.class);
        startActivity(intent);
    }

    private void goToNotificationActivity() {
        Intent intent = new Intent(getApplicationContext(), MessageListActivity.class);
        startActivity(intent);
    }

    private void closeDrawer()
    {
        drawerLayout.closeDrawer(Gravity.LEFT);
    }

    private boolean isLoggedIn() {
        AndroidApplication androidApplication = (AndroidApplication) getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(this);
        if (userInfo != null) {
//            Log.e("LogMsg", "User Id: " + userInfo.userId);
//            Log.e("LogMsg", "Api token: " + userInfo.apiToken);
            return true;
        } else {
            return false;
        }
    }

}