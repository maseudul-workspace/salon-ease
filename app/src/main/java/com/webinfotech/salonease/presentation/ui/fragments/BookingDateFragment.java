package com.webinfotech.salonease.presentation.ui.fragments;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.google.gson.Gson;
import com.webinfotech.salonease.R;
import com.webinfotech.salonease.domain.models.TimeSlots;
import com.webinfotech.salonease.presentation.ui.adapters.TimeSlotAdapter;
import com.webinfotech.salonease.presentation.ui.dialogs.CouponDialogFragment;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class BookingDateFragment extends DialogFragment implements TimeSlotAdapter.Callback{

    public interface Callback {
        void onTimeSelected(String dateTime);
    }

    @BindView(R.id.radio_group_dates)
    RadioGroup radioGroupDates;
    @BindView(R.id.radio_btn_today)
    RadioButton radioBtnToday;
    @BindView(R.id.radio_btn_tomorrow)
    RadioButton radioBtnTomorrow;
    @BindView(R.id.radio_btn_day_after_tomorrow)
    RadioButton radioButtonDayAfterTomorrow;
    @BindView(R.id.recycler_view_time_slots)
    RecyclerView recyclerViewTimeSlots;
    String openingTime;
    String closingTime;
    ArrayList<TimeSlots> timeSlots = new ArrayList<>();
    TimeSlotAdapter timeSlotAdapter;
    String bookingDate;
    String bookingTime;
    Callback mCallback;

    public BookingDateFragment() {
        // Required empty public constructor
    }

    public static BookingDateFragment newInstance(String param1, String param2) {
        BookingDateFragment fragment = new BookingDateFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            mCallback = (Callback) getParentFragment();
        } catch (ClassCastException e) {
            throw new ClassCastException("Calling fragment must implement Callback interface");
        }
        setStyle(DialogFragment.STYLE_NORMAL, android.R.style.Theme_Material_NoActionBar_Fullscreen);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_booking_date, container, false);
        ButterKnife.bind(this, view);
        if (savedInstanceState != null) {
            openingTime = savedInstanceState.getString("openingTime");
            closingTime = savedInstanceState.getString("closingTime");
        }
        setRadioGroupDates();
        String time = openingTime;
        int id = 0;
        timeSlots = new ArrayList<>();
        while (true) {
            time = convertToCalendar(time);
            if (checkTime(time)) {
                timeSlots.add(new TimeSlots(id, time, false));
            } else {
                break;
            }
            id++;
        }
        timeSlotAdapter = new TimeSlotAdapter(getContext(), timeSlots, this);
        recyclerViewTimeSlots.setAdapter(timeSlotAdapter);
        recyclerViewTimeSlots.setLayoutManager(new GridLayoutManager(getContext(), 4));
        return view;
    }

    public void setData(String openingTime, String closingTime) {
        this.openingTime = openingTime;
        this.closingTime = closingTime;
    }

    private void setRadioGroupDates() {
        radioGroupDates.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.radio_btn_today:
                        bookingDate = getDate(0);
                        break;
                    case R.id.radio_btn_tomorrow:
                        bookingDate = getDate(1);
                        break;
                    case R.id.radio_btn_day_after_tomorrow:
                        bookingDate = getDate(2);
                        break;
                }
            }
        });
    }

    private String convertToCalendar(String time) {
        DateFormat formatter = new SimpleDateFormat("HH:mm:ss");
        try {
            Date date = (Date)formatter.parse(time);
            Calendar cal = Calendar.getInstance();
            cal.setTime(date);
            cal.add(Calendar.MINUTE, 30);
            SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
            return df.format(cal.getTime());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private boolean checkTime(String time) {
        try {
            Date closedDate = new SimpleDateFormat("HH:mm:ss").parse(closingTime);
            Date date = new SimpleDateFormat("HH:mm:ss").parse(time);
            if (date.equals(closedDate) || date.before(closedDate)) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public void onActivityCreated(Bundle arg0) {
        super.onActivityCreated(arg0);
        getDialog().getWindow()
                .getAttributes().windowAnimations = R.style.DialogAnimation;
    }

    @Override
    public void onTimeSlotSelected(int id) {
        for (int i = 0; i < timeSlots.size(); i++) {
            if (timeSlots.get(i).id == id) {
                timeSlots.get(i).isTimeSelected = true;
                bookingTime = timeSlots.get(i).timeSlot;
            } else {
                timeSlots.get(i).isTimeSelected = false;
            }
        }
        timeSlotAdapter.updateData(timeSlots);
    }

    @OnClick(R.id.btn_confirm) void onConfirmClicked() {
        if (bookingDate == null) {
            YoYo.with(Techniques.Shake).duration(1200).playOn(radioGroupDates);
        } else if (bookingTime == null) {
            YoYo.with(Techniques.Shake).duration(1200).playOn(recyclerViewTimeSlots);
        } else {
            mCallback.onTimeSelected(bookingDate + " " + bookingTime);
            dismiss();
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("openingTime", openingTime);
        outState.putString("closingTime", closingTime);
    }

    private String getDate(int day) {
        Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DAY_OF_YEAR, day);
        Date date = calendar.getTime();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        return df.format(date);
    }

}