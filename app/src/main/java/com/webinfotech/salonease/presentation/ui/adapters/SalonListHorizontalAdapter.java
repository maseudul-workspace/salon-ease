package com.webinfotech.salonease.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.webinfotech.salonease.R;
import com.webinfotech.salonease.domain.models.SalonUser;
import com.webinfotech.salonease.util.GlideHelper;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class SalonListHorizontalAdapter extends RecyclerView.Adapter<SalonListHorizontalAdapter.ViewHolder> {

    public interface Callback {
        void onSalonClicked(int salonId);
    }

    Context mContext;
    SalonUser[] salonUsers;
    Callback mCallback;

    public SalonListHorizontalAdapter(Context mContext, SalonUser[] salonUsers, Callback mCallback) {
        this.mContext = mContext;
        this.salonUsers = salonUsers;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_salon_list_horizontal, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewSalonName.setText(salonUsers[position].name);
        holder.txtViewLocation.setText(salonUsers[position].address);
        GlideHelper.setImageViewCustomRoundedCorners(mContext, holder.imgViewSalon, mContext.getResources().getString(R.string.base_url) + "images/client/" + salonUsers[position].image, 10);
        holder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onSalonClicked(salonUsers[position].id);
            }
        });
        holder.txtViewRating.setText(Double.toString(salonUsers[position].rating));
    }

    @Override
    public int getItemCount() {
        return salonUsers.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_salon_name)
        TextView txtViewSalonName;
        @BindView(R.id.txt_view_location)
        TextView txtViewLocation;
        @BindView(R.id.img_view_salon)
        ImageView imgViewSalon;
        @BindView(R.id.txt_view_rating)
        TextView txtViewRating;
        @BindView(R.id.main_layout)
        View mainLayout;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
