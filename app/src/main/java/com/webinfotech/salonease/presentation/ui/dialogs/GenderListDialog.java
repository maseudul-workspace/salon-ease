package com.webinfotech.salonease.presentation.ui.dialogs;

import android.app.Activity;
import android.content.Context;
import android.view.View;

import androidx.appcompat.app.AlertDialog;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.webinfotech.salonease.R;
import com.webinfotech.salonease.domain.models.Category;

import com.webinfotech.salonease.presentation.ui.activities.GenderAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GenderListDialog implements GenderAdapter.Callback {

    public interface Callback {
        void onCategoryClicked(int position);
    }

    Context mContext;
    View dialogContainer;
    AlertDialog.Builder builder;
    AlertDialog dialog;
    Activity mActivity;
    Callback mCallback;
    @BindView(R.id.recycler_view_gender)
    RecyclerView recyclerViewMainCategories;

    public GenderListDialog(Context mContext, Activity mActivity, Callback mCallback) {
        this.mContext = mContext;
        this.mActivity = mActivity;
        this.mCallback = mCallback;
    }

    @Override
    public void onCategoryClicked(int categoryId) {
        hideDialog();
        mCallback.onCategoryClicked(categoryId);
    }

    public void setUpDialogView() {
        dialogContainer = mActivity.getLayoutInflater().inflate(R.layout.gender_list_dialog, null);
        builder = new AlertDialog.Builder(mContext, android.R.style.Theme_Material_Light_Dialog_NoActionBar_MinWidth);
        builder.setView(dialogContainer);
        dialog = builder.create();
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        ButterKnife.bind(this, dialogContainer);
    }

    public void setCategories(Category[] categories) {
        GenderAdapter adapter = new GenderAdapter(mContext, categories, this);
        recyclerViewMainCategories.setAdapter(adapter);
        recyclerViewMainCategories.setLayoutManager(new LinearLayoutManager(mContext));
        recyclerViewMainCategories.addItemDecoration(new DividerItemDecoration(recyclerViewMainCategories.getContext(), DividerItemDecoration.VERTICAL));
    }

    public void showDialog() {
        dialog.show();
    }

    public void hideDialog() {
        dialog.dismiss();
    }
}
