package com.webinfotech.salonease.presentation.presenters;

public interface ForgetPasswordPresenter {
    void requestPasswordChange(String mobile);
    interface View {
        void onPasswordRequestSuccess();
        void showLoader();
        void hideLoader();
    }
}
