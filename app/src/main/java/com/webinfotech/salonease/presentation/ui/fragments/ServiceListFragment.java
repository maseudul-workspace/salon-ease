package com.webinfotech.salonease.presentation.ui.fragments;

import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.gson.Gson;
import com.webinfotech.salonease.R;
import com.webinfotech.salonease.domain.models.ComboServiceData;
import com.webinfotech.salonease.domain.models.Service;
import com.webinfotech.salonease.presentation.ui.adapters.ServiceSalonDetailsAdapter;

public class ServiceListFragment extends Fragment implements ServiceSalonDetailsAdapter.Callback {

    public interface Callback {
        void onServiceSelected(int comboId);
        void onServiceRemoved(int comboId);
    }

    @BindView(R.id.recycler_view_services)
    RecyclerView recyclerViewServices;
    Service[] services;
    Callback mCallback;
    ServiceSalonDetailsAdapter adapter;

    public ServiceListFragment() {
        // Required empty public constructor
    }


    public static ServiceListFragment newInstance(String param1, String param2) {
        ServiceListFragment fragment = new ServiceListFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_service_list, container, false);
        ButterKnife.bind(this, view);
        if (savedInstanceState != null) {
            services = new Gson().fromJson(savedInstanceState.getString("service"), Service[].class);
        }
        adapter = new ServiceSalonDetailsAdapter(getContext(), services, this);
        recyclerViewServices.setAdapter(adapter);
        recyclerViewServices.setLayoutManager(new LinearLayoutManager(getContext()));
        return view;
    }

    public void setRecyclerViewServices(Service[] services) {
        this.services = services;
    }

    @Override
    public void onServiceSelected(int position) {
        services[position].isServiceSelected = true;
        adapter.updateDataSet(this.services);
        mCallback.onServiceSelected(services[position].id);
    }

    @Override
    public void onServiceRemoved(int position) {
        services[position].isServiceSelected = false;
        adapter.updateDataSet(this.services);
        mCallback.onServiceRemoved(services[position].id);
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (getParentFragment() instanceof Callback)
        {
            mCallback = (Callback) getParentFragment();
        }
    }

    @Override
    public void onSaveInstanceState(@NonNull Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString("service", new Gson().toJson(services));
    }

}