package com.webinfotech.salonease.presentation.ui.fragments;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.webinfotech.salonease.R;
import com.webinfotech.salonease.util.GlideHelper;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link SplashSliderFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class SplashSliderFragment extends Fragment {


    @BindView(R.id.img_view_splash)
    ImageView imgViewSplash;
    @BindView(R.id.img_view_splash_small)
    ImageView imgViewSplashSmall;
    @BindView(R.id.txt_view_splash)
    TextView txtViewSplash;
    @BindView(R.id.txt_view_splash_small)
    TextView txtViewSplashSmall;
    Context mContext;
    String image;
    Drawable smallImageDrawable;
    String splashText;
    String splashTextSmall;

    public SplashSliderFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static SplashSliderFragment newInstance(String param1, String param2) {
        SplashSliderFragment fragment = new SplashSliderFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_splash_slider, container, false);
        ButterKnife.bind(this, view);
        GlideHelper.setImageView(mContext, imgViewSplash, image);
        imgViewSplashSmall.setImageDrawable(smallImageDrawable);
        txtViewSplash.setText(splashText);
        txtViewSplashSmall.setText(splashTextSmall);
        return view;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mContext = context;
    }

    public void setData(String image, Drawable smallImageDrawable, String splashText, String splashTextSmall) {
        this.image = image;
        this.smallImageDrawable = smallImageDrawable;
        this.splashText = splashText;
        this.splashTextSmall = splashTextSmall;
    }

}