package com.webinfotech.salonease.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.webinfotech.salonease.R;
import com.webinfotech.salonease.domain.models.MessageData;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;

public class NotificationListRecyclerViewAdapter extends RecyclerView.Adapter<NotificationListRecyclerViewAdapter.ViewHolder> {

    Context mContext;
    MessageData[] messageData;



    public NotificationListRecyclerViewAdapter(Context mContext, MessageData[] messageData) {
        this.mContext = mContext;
        this.messageData = messageData;
    }

    @NonNull
    @Override
    public NotificationListRecyclerViewAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recycle_view_message_list_item, parent, false);
        NotificationListRecyclerViewAdapter.ViewHolder holder = new NotificationListRecyclerViewAdapter.ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull NotificationListRecyclerViewAdapter.ViewHolder holder, int position) {

        holder.txtViewTitle.setText(messageData[position].title);
        holder.txtViewDescription.setText(messageData[position].message);
        if(messageData[position].created_at != null)
        {
            holder.txtViewTime.setText(convertDate(messageData[position].created_at));
        }
    }

    @Override
    public int getItemCount() {
        return messageData.length;
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_title)
        TextView txtViewTitle;
        @BindView(R.id.txt_view_description)
        TextView txtViewDescription;
        @BindView(R.id.txt_view_time)
        TextView txtViewTime;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void updateData(MessageData[] newMessageData) {
        this.messageData = newMessageData;
        notifyDataSetChanged();
    }

    private String convertDate(String date)
    {
        SimpleDateFormat spf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date newDate= null;
        try {
            newDate = spf.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        spf= new SimpleDateFormat("EEE, HH:mm a");
        date = spf.format(newDate);
        return date;
    }
}
