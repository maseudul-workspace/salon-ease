package com.webinfotech.salonease.presentation.ui.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.text.method.PasswordTransformationMethod;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.webinfotech.salonease.R;
import com.webinfotech.salonease.domain.executors.impl.ThreadExecutor;
import com.webinfotech.salonease.presentation.presenters.RegisterUserPresenter;
import com.webinfotech.salonease.presentation.presenters.impl.RegisterUserPresenterImpl;
import com.webinfotech.salonease.presentation.ui.dialogs.AddAddressDialogFragment;
import com.webinfotech.salonease.threading.MainThreadImpl;
import com.webinfotech.salonease.util.GlideHelper;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class RegisterActivity extends AppCompatActivity implements RegisterUserPresenter.View, AddAddressDialogFragment.Callback {

    @BindView(R.id.img_view_register)
    ImageView imgViewRegister;
    @BindView(R.id.edit_text_name)
    EditText editTextName;
    @BindView(R.id.edit_text_email)
    EditText editTextEmail;
    @BindView(R.id.radio_group_gender)
    RadioGroup radioGroupGender;
    String gender;
    ProgressDialog progressDialog;
    RegisterUserPresenterImpl mPresenter;
    String[] appPremisions = {
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
    };
    private static final int PERMISSIONS_REQUEST_CODE = 1240;
    DialogFragment addressDialogFragment;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);
        GlideHelper.setImageView(this, imgViewRegister, "https://image.freepik.com/free-photo/handsome-man-cutting-beard-barber-shop-salon_1303-20934.jpg");
        setRadioGroupGender();
        initialisePresenter();
        setUpProgressDialog();
    }

    private void initialisePresenter() {
        mPresenter = new RegisterUserPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    public void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    private void setRadioGroupGender() {
        radioGroupGender.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton rb=(RadioButton)findViewById(checkedId);
                gender = rb.getText().toString();
                if (gender.equals("Male")) {
                    gender = "M";
                } else {
                    gender = "F";
                }
            }
        });
    }

    @OnClick(R.id.btn_submit) void onSubmitClicked() {

        if ( editTextName.getText().toString().trim().isEmpty()
        ) {
            Toast.makeText(this, "Name Required", Toast.LENGTH_LONG).show();
        } else if (gender == null) {
            Toast.makeText(this, "Please enter your gender", Toast.LENGTH_LONG).show();
        } else {
            mPresenter.registerUser(
                    editTextName.getText().toString(),
                    editTextEmail.getText().toString(),
                    gender
            );
        }
    }


    @Override
    public void onUserRegistrationSuccess() {
        new MaterialAlertDialogBuilder(this)
                .setTitle("Confirmation Dialog")
                .setMessage("Do you want to add address?")
                .setNegativeButton("Skip", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        finish();
                    }
                })
                .setPositiveButton("Add", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (checkAndRequestPermissions()) {
                            goToAddAddressActivity();
                        }
                    }
                })
                .show();
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    private boolean checkAndRequestPermissions() {
        List<String> listPermissionsNeeded = new ArrayList<>();
        for (String perm : appPremisions) {
            if (ContextCompat.checkSelfPermission(this, perm) != PackageManager.PERMISSION_GRANTED) {
                listPermissionsNeeded.add(perm);
            }
        }
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), PERMISSIONS_REQUEST_CODE);
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST_CODE) {
            HashMap<String, Integer> permissionResults = new HashMap<>();
            int deniedCount = 0;
            for (int i = 0; i < grantResults.length; i++) {
                if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                    permissionResults.put(permissions[i], grantResults[i]);
                    deniedCount++;
                }
            }

            if (deniedCount == 0) {
                goToAddAddressActivity();
            } else {
                for (Map.Entry<String, Integer> entry : permissionResults.entrySet()) {
                    String permName = entry.getKey();
                    int permResult = entry.getValue();
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, permName)) {
                        this.showAlertDialog("", "This app needs read and write storage permissions",
                                "Yes, Grant permissions",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        checkAndRequestPermissions();
                                    }
                                },
                                "Cancel",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        Toast.makeText(getApplicationContext(), "Permissions are required ", Toast.LENGTH_SHORT).show();
                                    }
                                },
                                false);
                    } else {
                        this.showAlertDialog("", "You have denied some permissions",
                                "Go to settings",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                                Uri.fromParts("package", getPackageName(), null));
                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                    }
                                },
                                "Cancel",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        Toast.makeText(getApplicationContext(), "Permissions are required", Toast.LENGTH_SHORT).show();
                                    }
                                },
                                false);
                        break;
                    }

                }
            }
        }
    }

    public AlertDialog showAlertDialog(
            String title, String msg, String positiveLabel,
            DialogInterface.OnClickListener positiveOnClick,
            String negativeLabel, DialogInterface.OnClickListener negativeOnClick,
            boolean isCancelable
    ) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setCancelable(isCancelable);
        builder.setMessage(msg);
        builder.setPositiveButton(positiveLabel, positiveOnClick);
        builder.setNegativeButton(negativeLabel, negativeOnClick);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        return alertDialog;
    }

    @Override
    public void submitData(String latitude, String longitude, String state, String city, String address, String pin, String email) {
        addressDialogFragment.dismiss();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    private void goToAddAddressActivity() {
        Intent intent = new Intent(this, AddAddressActivity.class);
        startActivity(intent);
        finish();
    }

}