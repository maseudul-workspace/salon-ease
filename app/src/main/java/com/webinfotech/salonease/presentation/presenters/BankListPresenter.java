package com.webinfotech.salonease.presentation.presenters;

import com.webinfotech.salonease.domain.models.BankList;
import com.webinfotech.salonease.presentation.ui.adapters.RefundBankListAdapter;

public interface BankListPresenter {
    void addBankAccount(String name, String bankName, String accountNo, String ifsc, String branchName);
    void fetchBankList();
    void updateBankAccount(String name, String bankName, String accountNo, String ifsc, String branchName);
    interface View {
        void loadData(RefundBankListAdapter adapter);
        void setData(BankList bankList);
        void hideBankAddBtn();
        void showLoader();
        void hideLoader();
        void showLoginBottomSheet();
    }
}
