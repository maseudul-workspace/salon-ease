package com.webinfotech.salonease.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.webinfotech.salonease.R;
import com.webinfotech.salonease.domain.executors.impl.ThreadExecutor;
import com.webinfotech.salonease.domain.models.City;
import com.webinfotech.salonease.domain.models.Subcategory;
import com.webinfotech.salonease.presentation.presenters.ChooseCategoryPresenter;
import com.webinfotech.salonease.presentation.presenters.impl.ChooseCategoryPresenterImpl;
import com.webinfotech.salonease.presentation.ui.dialogs.SelectCityDialog;
import com.webinfotech.salonease.presentation.ui.dialogs.SubcategoriesListDialog;
import com.webinfotech.salonease.threading.MainThreadImpl;

public class ChooseCategoryActivity extends AppCompatActivity implements ChooseCategoryPresenter.View, SubcategoriesListDialog.Callback, SelectCityDialog.Callback {

    @BindView(R.id.layout_choose_category)
    View layoutChooseCategory;
    @BindView(R.id.layout_choose_city)
    View layoutChooseCity;
    @BindView(R.id.txt_view_category)
    TextView txtViewCategory;
    @BindView(R.id.txt_view_city)
    TextView txtViewCity;
    @BindView(R.id.radio_group_service)
    RadioGroup radioGroupService;
    int categoryId;
    ChooseCategoryPresenterImpl mPresenter;
    SubcategoriesListDialog subcategoriesListDialog;
    SelectCityDialog selectCityDialog;
    Subcategory[] subcategories;
    int clientType = 0;
    int cityId = 0;
    int subcategoryId = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_category);
        categoryId = getIntent().getIntExtra("categoryId", 0);
        ButterKnife.bind(this);
        initialisePresenter();
        initialiseDialogs();
        setRadioGroupService();
        mPresenter.fetchCities();
        mPresenter.fetchSubcategories(categoryId);
    }

    private void initialisePresenter() {
        mPresenter = new ChooseCategoryPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    private void initialiseDialogs() {
        subcategoriesListDialog = new SubcategoriesListDialog(this, this, this);
        subcategoriesListDialog.setUpDialogView();

        selectCityDialog = new SelectCityDialog(this, this, this);
        selectCityDialog.setUpDialogView();

    }

    private void setRadioGroupService() {
        radioGroupService.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.radio_btn_salon:
                        clientType = 2;
                        break;
                    case R.id.radio_btn_home_service:
                        clientType = 1;
                        break;
                }
            }
        });
    }

    @OnClick(R.id.layout_choose_category) void onChooseCategoryClicked() {
        subcategoriesListDialog.showDialog();
    }

    @OnClick(R.id.layout_choose_city) void onCityClicked() {
        selectCityDialog.showDialog();
    }

    @OnClick(R.id.btn_search) void onSearchClicked() {
        if (subcategoryId == 0) {
            YoYo.with(Techniques.Shake).duration(1200).playOn(layoutChooseCategory);
        } else if (cityId == 0) {
            YoYo.with(Techniques.Shake).duration(1200).playOn(layoutChooseCity);
        } else if (clientType == 0) {
            YoYo.with(Techniques.Shake).duration(1200).playOn(radioGroupService);
        } else {
           Intent intent = new Intent(this, SalonListActivity.class);
           intent.putExtra("categoryId", subcategoryId);
           intent.putExtra("serviceCityId", cityId);
           intent.putExtra("clientType", clientType);
           startActivity(intent);
        }
    }

    @Override
    public void loadSubcategories(Subcategory[] subcategories) {
        this.subcategories = subcategories;
        subcategoriesListDialog.setCategories(subcategories);
    }

    @Override
    public void loadCities(City[] cities) {
        selectCityDialog.setRecyclerView(cities);
    }

    @Override
    public void onCitySelected(int id, String cityName) {
        cityId = id;
        txtViewCity.setText(cityName);
    }

    @Override
    public void onSubcategoryClicked(int position) {
        subcategoryId = this.subcategories[position].id;
        txtViewCategory.setText(this.subcategories[position].name);
    }

    @OnClick(R.id.btn_back) void onBackClicked()
    {
        finish();
    }
}