package com.webinfotech.salonease.presentation.presenters;

import com.webinfotech.salonease.domain.models.SalonData;
import com.webinfotech.salonease.domain.models.SalonUser;
import com.webinfotech.salonease.domain.models.Wishlist;
import com.webinfotech.salonease.presentation.ui.adapters.BookingAddressAdapter;

public interface SalonDetailsPresenter {
    void fetchProductDetails(int serviceId);
    void fetchShippingAddressList();
    void fetchWishlist();
    void addToWishlist(int vendorId);
    void removeFromWishlist(int wishlistId);
    interface View {
        void loadSalonData(SalonUser salonUser);
        void showLoader();
        void hideLoader();
        void loadAddress(BookingAddressAdapter bookingAddressAdapter);
        void onBookNowClicked(int addressId);
        void loadWishlists(Wishlist[] wishlists);
        void onAddToWishlistSuccess();
        void onRemoveFromWishlistSuccess();
    }
}
