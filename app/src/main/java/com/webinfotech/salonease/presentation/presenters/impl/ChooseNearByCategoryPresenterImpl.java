package com.webinfotech.salonease.presentation.presenters.impl;

import android.content.Context;

import com.webinfotech.salonease.domain.executors.Executor;
import com.webinfotech.salonease.domain.executors.MainThread;
import com.webinfotech.salonease.domain.interactors.FetchCategoriesInteractor;
import com.webinfotech.salonease.domain.interactors.FetchCityListInteractor;
import com.webinfotech.salonease.domain.interactors.FetchSubcategoryInteractor;
import com.webinfotech.salonease.domain.interactors.impl.FetchCategoriesInteractorImpl;
import com.webinfotech.salonease.domain.interactors.impl.FetchCityListInteractorImpl;
import com.webinfotech.salonease.domain.interactors.impl.FetchSubcategoryInteractorImpl;
import com.webinfotech.salonease.domain.models.Category;
import com.webinfotech.salonease.domain.models.City;
import com.webinfotech.salonease.domain.models.Subcategory;
import com.webinfotech.salonease.presentation.presenters.ChooseCategoryPresenter;
import com.webinfotech.salonease.presentation.presenters.ChooseNearByCategoryPresenter;
import com.webinfotech.salonease.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.salonease.repository.AppRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class ChooseNearByCategoryPresenterImpl extends AbstractPresenter implements ChooseNearByCategoryPresenter, FetchSubcategoryInteractor.Callback, FetchCityListInteractor.Callback, FetchCategoriesInteractor.Callback {

    Context mContext;
    View mView;

    public ChooseNearByCategoryPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchCategories() {
        FetchCategoriesInteractorImpl fetchCategoriesInteractor = new FetchCategoriesInteractorImpl(mExecutor, mMainThread, this, new AppRepositoryImpl());
        fetchCategoriesInteractor.execute();
    }

    @Override
    public void fetchSubcategories(int categoryId) {
        FetchSubcategoryInteractorImpl  fetchSubcategoryInteractor = new FetchSubcategoryInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, categoryId);
        fetchSubcategoryInteractor.execute();
    }

    @Override
    public void fetchCities() {
        FetchCityListInteractorImpl fetchCityListInteractor = new FetchCityListInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this);
        fetchCityListInteractor.execute();
    }

    @Override
    public void onGettingCityListSuccess(City[] cities) {
        mView.loadCities(cities);
    }

    @Override
    public void onGettingCityListFail(String errorMsg) {
        Toasty.warning(mContext, errorMsg, Toasty.LENGTH_SHORT).show();
    }

    @Override
    public void onGettingSubcategorySuccess(Subcategory[] subcategories) {
        mView.loadSubcategories(subcategories);
    }

    @Override
    public void ongGettingSubcategoryFail(String errorMsg) {
        Toasty.warning(mContext, errorMsg, Toasty.LENGTH_SHORT).show();

    }

    @Override
    public void onGettingCategoriesSuccess(Category[] categories) {
        mView.loadMaincategories(categories);
    }

    @Override
    public void onGettingCategoriesFail() {
        Toasty.warning(mContext, "Please check your internet connection", Toasty.LENGTH_SHORT).show();

    }
}
