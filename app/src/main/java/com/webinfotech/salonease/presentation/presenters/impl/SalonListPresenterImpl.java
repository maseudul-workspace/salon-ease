package com.webinfotech.salonease.presentation.presenters.impl;

import android.content.Context;

import com.webinfotech.salonease.domain.executors.Executor;
import com.webinfotech.salonease.domain.executors.MainThread;
import com.webinfotech.salonease.domain.interactors.FetchCategoriesInteractor;
import com.webinfotech.salonease.domain.interactors.FetchCityListInteractor;
import com.webinfotech.salonease.domain.interactors.FetchSalonDataInteractor;
import com.webinfotech.salonease.domain.interactors.FetchThirdCategoryInteractor;
import com.webinfotech.salonease.domain.interactors.impl.FetchCategoriesInteractorImpl;
import com.webinfotech.salonease.domain.interactors.impl.FetchCityListInteractorImpl;
import com.webinfotech.salonease.domain.interactors.impl.FetchSalonDataInteractorImpl;
import com.webinfotech.salonease.domain.interactors.impl.FetchThirdCategoryInteractorImpl;
import com.webinfotech.salonease.domain.models.Category;
import com.webinfotech.salonease.domain.models.City;
import com.webinfotech.salonease.domain.models.SalonData;
import com.webinfotech.salonease.domain.models.ThirdCategory;
import com.webinfotech.salonease.presentation.presenters.SalonListPresenter;
import com.webinfotech.salonease.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.salonease.presentation.ui.adapters.SalonDataAdapter;
import com.webinfotech.salonease.repository.AppRepositoryImpl;

public class SalonListPresenterImpl extends AbstractPresenter implements    SalonListPresenter,
                                                                            FetchSalonDataInteractor.Callback,
                                                                            SalonDataAdapter.Callback,
                                                                            FetchCityListInteractor.Callback,
                                                                            FetchCategoriesInteractor.Callback,
                                                                            FetchThirdCategoryInteractor.Callback

{

    Context mContext;
    SalonListPresenter.View mView;
    SalonData[] newSalonData;
    SalonDataAdapter adapter;

    public SalonListPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchSalonList(int serviceCityId, int categoryId, int page, String clientType, String refresh) {
        if (refresh.equals("refresh")) {
            newSalonData = null;
        }
    }

    @Override
    public void fetchCityList() {
        FetchCityListInteractorImpl fetchCityListInteractor = new FetchCityListInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this);
        fetchCityListInteractor.execute();
    }

    @Override
    public void fetchCategories() {
        FetchCategoriesInteractorImpl fetchCategoriesInteractor = new FetchCategoriesInteractorImpl(mExecutor, mMainThread, this, new AppRepositoryImpl());
        fetchCategoriesInteractor.execute();
    }

    @Override
    public void fetchThirdCategories(int categoryId) {
        FetchThirdCategoryInteractorImpl fetchThirdCategoryInteractor = new FetchThirdCategoryInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, categoryId);
        fetchThirdCategoryInteractor.execute();
        mView.showLoader();
    }

    @Override
    public void onGettingSalonDataSuccess(SalonData[] salonData, int totalPage) {
        if (salonData.length > 0) {
            SalonData[] tempSalonData;
            tempSalonData = newSalonData;
            try {
                int len1 = tempSalonData.length;
                int len2 = salonData.length;
                newSalonData = new SalonData[len1 + len2];
                System.arraycopy(tempSalonData, 0, newSalonData, 0, len1);
                System.arraycopy(salonData, 0, newSalonData, len1, len2);
                adapter.updateDataSet(newSalonData);
            }catch (NullPointerException e){
                adapter = new SalonDataAdapter(mContext, salonData, this);
                mView.loadData(adapter, totalPage);
                mView.hideLoader();
            }
        } else {
            mView.hideRecyclerView();
        }
    }

    @Override
    public void onGettingSalonDataFail(String errorMsg) {
        mView.hideLoader();
        mView.hideRecyclerView();
    }

    @Override
    public void onGettingCityListSuccess(City[] cities) {
        mView.loadCities(cities);
    }

    @Override
    public void onGettingCityListFail(String errorMsg) {
        mView.hideLoader();
    }

    @Override
    public void onGettingCategoriesSuccess(Category[] categories) {
        mView.loadCategories(categories);
    }

    @Override
    public void onGettingCategoriesFail() {
    }

    @Override
    public void onGettingThirdCategorySuccess(ThirdCategory[] thirdCategories) {
        mView.hideLoader();
        mView.loadThirdCategories(thirdCategories);
    }

    @Override
    public void ongGettingThirdCategoryFail(String errorMsg) {
        mView.hideLoader();
    }

    @Override
    public void onSalonClicked(int salonId, int serviceId) {

    }
}
