package com.webinfotech.salonease.presentation.presenters;

import com.webinfotech.salonease.domain.models.UserInfo;

public interface UserProfilePresenter {
    void fetchUserProfile();
    void updateProfile(String name, String mobile, String state, String city, String gender, String address, String pin, String email);
    interface View {
        void loadData(UserInfo userInfo);
        void onProfileUpdateSuccess();
        void showLoader();
        void hideLoader();
    }
}
