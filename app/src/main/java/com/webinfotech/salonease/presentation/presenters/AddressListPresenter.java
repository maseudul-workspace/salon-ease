package com.webinfotech.salonease.presentation.presenters;

import com.webinfotech.salonease.presentation.ui.adapters.ShippingAddressAdapter;

public interface AddressListPresenter {
    void fetchShippingAddress();
    interface View {
        void loadAdapter(ShippingAddressAdapter adapter);
        void goToAddressDetails(int addressId);
        void onAddressFetchFailed();
        void showLoader();
        void hideLoader();
        void showLoginBottomSheet();
    }
}
