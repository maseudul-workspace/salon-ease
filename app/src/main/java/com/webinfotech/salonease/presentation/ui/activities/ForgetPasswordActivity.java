package com.webinfotech.salonease.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.webinfotech.salonease.R;
import com.webinfotech.salonease.domain.executors.impl.ThreadExecutor;
import com.webinfotech.salonease.presentation.presenters.ForgetPasswordPresenter;
import com.webinfotech.salonease.presentation.presenters.impl.ForgetPasswordPresenterImpl;
import com.webinfotech.salonease.presentation.ui.dialogs.CustomProgressDialog;
import com.webinfotech.salonease.presentation.ui.dialogs.SuccessDialog;
import com.webinfotech.salonease.threading.MainThreadImpl;
import com.webinfotech.salonease.util.GlideHelper;

public class ForgetPasswordActivity extends AppCompatActivity implements ForgetPasswordPresenter.View, SuccessDialog.Callback {

    @BindView(R.id.edit_text_phone)
    EditText editTextPhone;
    @BindView(R.id.img_view_forget_password)
    ImageView imgViewForgetPassword;
    ForgetPasswordPresenterImpl mPresenter;
    CustomProgressDialog progressDialog;
    SuccessDialog successDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forget_password);
        ButterKnife.bind(this);
        setSuccessDialog();
        GlideHelper.setImageView(this, imgViewForgetPassword, "https://italiansbarber.com/wp-content/uploads/2016/05/mens-haircuts-brown-short-hair_1024x1024-2.jpg");
        setUpProgressDialog();
        initilaisePresenter();
    }

    private void initilaisePresenter() {
        mPresenter = new ForgetPasswordPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    private void setUpProgressDialog() {
        progressDialog = new CustomProgressDialog(this, this);
        progressDialog.setUpDialog();
    }

    private void setSuccessDialog() {
        successDialog = new SuccessDialog(this, this, this);
        successDialog.setUpDialogView();
    }

    @OnClick(R.id.btn_submit) void onSubmitClicked() {
        if ( editTextPhone.getText().toString().trim().isEmpty()) {
            Toasty.warning(this,"Please Insert Your Phone", Toast.LENGTH_LONG).show();
        } else if (editTextPhone.getText().toString().trim().length() != 10) {
            Toasty.warning(this,"Phone No Must Be 10 digit", Toast.LENGTH_LONG).show();
        } else {
            mPresenter.requestPasswordChange(editTextPhone.getText().toString());
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onPasswordRequestSuccess() {
        successDialog.setSuccessMessage("Request Sent To Admin");
        successDialog.showDialog();
    }

    @Override
    public void showLoader() {
        progressDialog.showDialog();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismissDialog();
    }

    @Override
    public void onOkayClicked() {
        finish();
    }
}