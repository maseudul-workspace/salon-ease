package com.webinfotech.salonease.presentation.ui.activities;

import android.animation.Animator;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.airbnb.lottie.LottieAnimationView;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.webinfotech.salonease.AndroidApplication;
import com.webinfotech.salonease.R;
import com.webinfotech.salonease.domain.executors.impl.ThreadExecutor;
import com.webinfotech.salonease.domain.models.Category;
import com.webinfotech.salonease.domain.models.Subcategory;
import com.webinfotech.salonease.domain.models.ThirdCategory;
import com.webinfotech.salonease.domain.models.UserInfo;
import com.webinfotech.salonease.presentation.presenters.EnquiryActivityPresenter;
import com.webinfotech.salonease.presentation.presenters.impl.EnquiryActivityPresenterImpl;
import com.webinfotech.salonease.presentation.ui.dialogs.GenderListDialog;
import com.webinfotech.salonease.presentation.ui.dialogs.SubcategoriesListDialog;
import com.webinfotech.salonease.presentation.ui.dialogs.ThirdcategoriesListDialog;
import com.webinfotech.salonease.threading.MainThreadImpl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class EnquiryActivity extends AppCompatActivity implements EnquiryActivityPresenter.View, GenderListDialog.Callback, SubcategoriesListDialog.Callback, ThirdcategoriesListDialog.Callback {

    @BindView(R.id.txt_view_booking)
    TextView textViewBookingDate;
    @BindView(R.id.txt_view_success_message)
    TextView textViewSuccessMessage;
    @BindView(R.id.edit_text_phone)
    EditText editTextPhone;
    @BindView(R.id.edit_text_name)
    EditText editTextName;
    @BindView(R.id.txt_view_category)
    TextView txtViewCategory;
    @BindView(R.id.txt_view_sub_category)
    TextView txtViewSubCategory;
    @BindView(R.id.txt_view_third_category)
    TextView txtViewThirdCategory;
    @BindView(R.id.feedback_title)
    TextView textViewFeedbackTitle;
    @BindView(R.id.textArea_message)
    EditText editTextMessage;
    @BindView(R.id.layout_choose_category_feedback)
    LinearLayout linearLayoutCategoryFeedback;
    @BindView(R.id.layout_choose_sub_category_feedback)
    LinearLayout linearLayoutSubCategoryFeedback;
    @BindView(R.id.layout_choose_third_category_feedback)
    LinearLayout linearLayoutThirdCategoryFeedback;
    @BindView(R.id.layout_choose_booking_date)
    LinearLayout linearLayoutBookingDate;
    @BindView(R.id.feedback_linear)
    LinearLayout linearLayoutFeedbackLinear;
    @BindView(R.id.done_linearlayout)
    LinearLayout linearLayoutDoneLinear;
    @BindView(R.id.btn_home)
    Button btn_home;
    @BindView(R.id.btn_back)
    ImageView btn_back;



    ViewGroup progressView;
    protected boolean isProgressShowing = false;
    LottieAnimationView lottieAnimationView;
    LottieAnimationView lottieAnimationView2;
    EnquiryActivityPresenterImpl mPresenter;
    GenderListDialog genderListDialog;
    SubcategoriesListDialog subcategoriesListDialog;
    ThirdcategoriesListDialog thirdcategoriesListDialog;
    Category[] categories;
    Subcategory[] subcategories;
    ThirdCategory[] thirdCategories;
    int categoryId = 0;
    int subcategoryId = 0;
    int thirdcategoryId = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_enquiry);
        ButterKnife.bind(this);
        initialisePresenter();
        initialiseDialogs();
        mPresenter.fetchCategories();
        AndroidApplication androidApplication = (AndroidApplication) getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(this);
        editTextPhone.setText(userInfo.mobile);
        editTextPhone.setEnabled(false);
    }

    private void initialisePresenter() {
        mPresenter = new EnquiryActivityPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    private void initialiseDialogs() {
        genderListDialog = new GenderListDialog(this, this, this);
        genderListDialog.setUpDialogView();

        subcategoriesListDialog = new SubcategoriesListDialog(this, this, this);
        subcategoriesListDialog.setUpDialogView();

        thirdcategoriesListDialog = new ThirdcategoriesListDialog(this, this, this);
        thirdcategoriesListDialog.setUpDialogView();
    }

    @Override
    public void onCategoryClicked(int position) {
        categoryId = this.categories[position].id;
        txtViewCategory.setText(this.categories[position].name);
        mPresenter.fetchSubCategories(categoryId);
    }

    @Override
    public void onSubcategoryClicked(int position) {
        subcategoryId = this.subcategories[position].id;
        txtViewSubCategory.setText(this.subcategories[position].name);
        mPresenter.fetchThirdCategories(subcategoryId);
    }

    @Override
    public void onThirdcategoryClicked(int position) {
        thirdcategoryId = this.thirdCategories[position].id;
        txtViewThirdCategory.setText(this.thirdCategories[position].name);
    }

    @Override
    public void loadMaincategories(Category[] categories) {
        this.categories = categories;
        genderListDialog.setCategories(categories);
    }

    @Override
    public void loadSubCategories(Subcategory[] subcategories) {
        this.subcategories = subcategories;
        subcategoriesListDialog.setCategories(subcategories);
    }

    @Override
    public void loadThirdCategories(ThirdCategory[] thirdcategories) {
        this.thirdCategories = thirdcategories;
        thirdcategoriesListDialog.setCategories(thirdcategories);
    }

    @OnClick(R.id.layout_choose_booking_date) void onBookingDateClicked()
    {
            Calendar cal = Calendar.getInstance();
            int year = cal.get(Calendar.YEAR);
            int month = cal.get(Calendar.MONTH);
            int day = cal.get(Calendar.DAY_OF_MONTH);

            DatePickerDialog dialog = new DatePickerDialog(
                    this,
                    android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                    new DatePickerDialog.OnDateSetListener() {
                        @Override
                        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
                            month = month + 1;

                            String date = year + "-" + month + "-" + dayOfMonth;
                            date = convertDate(date);
                            textViewBookingDate.setText(convertDateForViewing(date));
                        }
                    },
                    year,month,day);

            dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            dialog.show();
    }

    @OnClick(R.id.layout_choose_category_feedback) void onCategoryClicked()
    {
        genderListDialog.showDialog();
    }

    @OnClick(R.id.layout_choose_sub_category_feedback) void onSubCategoryClicked()
    {
        subcategoriesListDialog.showDialog();
    }

    @OnClick(R.id.layout_choose_third_category_feedback) void onThirdCategoryClicked()
    {
        thirdcategoriesListDialog.showDialog();
    }

    @OnClick(R.id.btn_submit_feedback) void onSubmitFeedbackClicked()
    {
        if(categoryId == 0) {
            YoYo.with(Techniques.Shake).duration(1200).playOn(linearLayoutCategoryFeedback);
        } else if (textViewBookingDate.getText().toString().equals("Select Booking date")) {
            YoYo.with(Techniques.Shake).duration(1200).playOn(linearLayoutBookingDate);
        } else if (editTextName.getText().toString().isEmpty()) {
            YoYo.with(Techniques.Shake).duration(1200).playOn(editTextName);
        } else if (editTextPhone.getText().toString().isEmpty()) {
            YoYo.with(Techniques.Shake).duration(1200).playOn(editTextPhone);
        }  else if (editTextMessage.getText().toString().isEmpty()) {
            YoYo.with(Techniques.Shake).duration(1200).playOn(editTextMessage);
        } else {
            mPresenter.sendMessage(categoryId, subcategoryId, thirdcategoryId, editTextName.getText().toString(), editTextPhone.getText().toString(), editTextMessage.getText().toString(), convertDateForSending(textViewBookingDate.getText().toString()));
        }
    }

    @Override
    public void showLoader() {
        if (!isProgressShowing) {
            isProgressShowing = true;
            progressView = (ViewGroup) getLayoutInflater().inflate(R.layout.progressbar_layout, null);
            View v = this.findViewById(android.R.id.content).getRootView();
            ViewGroup viewGroup = (ViewGroup) v;
            viewGroup.addView(progressView);
            lottieAnimationView = findViewById(R.id.progressBar);
            lottieAnimationView.playAnimation();
        }
    }

    @Override
    public void showAnimationSuccess() {
        linearLayoutFeedbackLinear.setVisibility(View.GONE);
        linearLayoutDoneLinear.setVisibility(View.VISIBLE);
        lottieAnimationView2 = findViewById(R.id.lotte_success);
        btn_back.setVisibility(View.INVISIBLE);
        textViewFeedbackTitle.setVisibility(View.INVISIBLE);
        lottieAnimationView2.playAnimation();
        lottieAnimationView2.addAnimatorListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animation) {

            }

            @Override
            public void onAnimationEnd(Animator animation) {
                textViewSuccessMessage.setVisibility(View.VISIBLE);
                btn_home.setVisibility(View.VISIBLE);
                AlphaAnimation fadeIn = new AlphaAnimation(0.0f , 1.0f ) ;
                textViewSuccessMessage.startAnimation(fadeIn);
                btn_home.setAnimation(fadeIn);
                fadeIn.setDuration(1200);
                fadeIn.setFillAfter(true);
            }

            @Override
            public void onAnimationCancel(Animator animation) {

            }

            @Override
            public void onAnimationRepeat(Animator animation) {

            }
        });
    }

    @Override
    public void hideLoader() {
        View v = this.findViewById(android.R.id.content).getRootView();
        ViewGroup viewGroup = (ViewGroup) v;
        viewGroup.removeView(progressView);
        lottieAnimationView.cancelAnimation();
        isProgressShowing = false;
    }

    @Override
    public void loginError() {
        Toast.makeText(this, "Your session has expired !!", Toast.LENGTH_LONG).show();
        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    private String convertDate(String date) {
        SimpleDateFormat spf=new SimpleDateFormat("yyyy-M-d");
        Date newDate= null;
        try {
            newDate = spf.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        spf= new SimpleDateFormat("yyyy-MM-dd");
        date = spf.format(newDate);
        return date;
    }

    private String convertDateForViewing(String date) {
        SimpleDateFormat spf=new SimpleDateFormat("yyyy-M-d");
        Date newDate= null;
        try {
            newDate = spf.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        spf= new SimpleDateFormat("EEE, MMM d, ''yy");
        date = spf.format(newDate);
        return date;
    }

    private String convertDateForSending(String date) {
        SimpleDateFormat spf=new SimpleDateFormat("EEE, MMM d, ''yy");
        Date newDate= null;
        try {
            newDate = spf.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        spf= new SimpleDateFormat("yyyy-MM-dd");
        date = spf.format(newDate);
        return date;
    }

    @OnClick(R.id.btn_back) void onBackClicked()
    {
        finish();
    }

    @OnClick(R.id.btn_home) void onHomeClicked() {
        finish();
    }

}
