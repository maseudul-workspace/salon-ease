package com.webinfotech.salonease.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.webinfotech.salonease.R;
import com.webinfotech.salonease.domain.models.ComboService;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ComboServiceAdapter extends RecyclerView.Adapter<ComboServiceAdapter.ViewHolder> {

    Context mContext;
    ComboService[] comboServices;

    public ComboServiceAdapter(Context mContext, ComboService[] comboServices) {
        this.mContext = mContext;
        this.comboServices = comboServices;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_combo_services, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewComboService.setText(comboServices[position].name);
        holder.txtViewPrice.setText("Rs " + comboServices[position].price);
    }

    @Override
    public int getItemCount() {
        return comboServices.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_combo_service)
        TextView txtViewComboService;
        @BindView(R.id.txt_view_price)
        TextView txtViewPrice;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
