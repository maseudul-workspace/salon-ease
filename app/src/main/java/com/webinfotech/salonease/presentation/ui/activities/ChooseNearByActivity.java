package com.webinfotech.salonease.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.webinfotech.salonease.R;
import com.webinfotech.salonease.domain.executors.impl.ThreadExecutor;
import com.webinfotech.salonease.domain.models.AppLoadData;
import com.webinfotech.salonease.domain.models.Category;
import com.webinfotech.salonease.domain.models.City;
import com.webinfotech.salonease.domain.models.Subcategory;
import com.webinfotech.salonease.presentation.presenters.ChooseNearByCategoryPresenter;
import com.webinfotech.salonease.presentation.presenters.impl.ChooseNearByCategoryPresenterImpl;
import com.webinfotech.salonease.presentation.ui.dialogs.GenderListDialog;
import com.webinfotech.salonease.presentation.ui.dialogs.SelectCityDialog;
import com.webinfotech.salonease.presentation.ui.dialogs.SubcategoriesListDialog;
import com.webinfotech.salonease.threading.MainThreadImpl;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ChooseNearByActivity extends AppCompatActivity implements ChooseNearByCategoryPresenter.View, GenderListDialog.Callback, SubcategoriesListDialog.Callback, SelectCityDialog.Callback {

    @BindView(R.id.layout_choose_main_category)
    View layoutChooseMainCategory;
    @BindView(R.id.layout_choose_category)
    View layoutChooseCategory;
    @BindView(R.id.layout_choose_city)
    View layoutChooseCity;
    @BindView(R.id.txt_view_main_category)
    TextView txtViewMainCategory;
    @BindView(R.id.txt_view_category)
    TextView txtViewCategory;
    @BindView(R.id.txt_view_city)
    TextView txtViewCity;
    @BindView(R.id.radio_group_service)
    RadioGroup radioGroupService;
    ViewGroup progressView;
    protected boolean isProgressShowing = false;
    LottieAnimationView lottieAnimationView;
    ChooseNearByCategoryPresenterImpl mPresenter;
    GenderListDialog genderListDialog;
    SubcategoriesListDialog subcategoriesListDialog;
    SelectCityDialog selectCityDialog;
    Category[] categories;
    Subcategory[] subcategories;
    int categoryId = 0;
    int clientType = 0;
    int cityId = 0;
    int subcategoryId = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_near_by);
        ButterKnife.bind(this);
        initialisePresenter();
        initialiseDialogs();
        setRadioGroupService();
        mPresenter.fetchCategories();
        mPresenter.fetchCities();
    }


    private void initialisePresenter() {
        mPresenter = new ChooseNearByCategoryPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    private void initialiseDialogs() {
        genderListDialog = new GenderListDialog(this, this, this);
        genderListDialog.setUpDialogView();

        subcategoriesListDialog = new SubcategoriesListDialog(this, this, this);
        subcategoriesListDialog.setUpDialogView();

        selectCityDialog = new SelectCityDialog(this, this, this);
        selectCityDialog.setUpDialogView();

    }

    private void setRadioGroupService() {
        radioGroupService.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.radio_btn_salon:
                        clientType = 2;
                        break;
                    case R.id.radio_btn_home_service:
                        clientType = 1;
                        break;
                }
            }
        });
    }

    @OnClick(R.id.layout_choose_main_category) void onChooseMainCategoryClicked() {
       genderListDialog.showDialog();
    }

    @OnClick(R.id.layout_choose_category) void onChooseCategoryClicked() {
        subcategoriesListDialog.showDialog();
    }

    @OnClick(R.id.layout_choose_city) void onCityClicked() {
        selectCityDialog.showDialog();
    }

    @OnClick(R.id.btn_search) void onSearchClicked() {
        if(categoryId == 0) {
            YoYo.with(Techniques.Shake).duration(1200).playOn(layoutChooseMainCategory);
        } else if (subcategoryId == 0) {
            YoYo.with(Techniques.Shake).duration(1200).playOn(layoutChooseCategory);
        } else if (cityId == 0) {
            YoYo.with(Techniques.Shake).duration(1200).playOn(layoutChooseCity);
        } else if (clientType == 0) {
            YoYo.with(Techniques.Shake).duration(1200).playOn(radioGroupService);
        } else {
            Intent intent = new Intent(this, SalonListActivity.class);
            intent.putExtra("categoryId", subcategoryId);
            intent.putExtra("serviceCityId", cityId);
            intent.putExtra("clientType", clientType);
            startActivity(intent);
        }
    }

    @Override
    public void loadMaincategories(Category[] categories) {
        this.categories = categories;
        genderListDialog.setCategories(categories);
    }

    @Override
    public void loadSubcategories(Subcategory[] subcategories) {
        this.subcategories = subcategories;
        subcategoriesListDialog.setCategories(subcategories);
    }

    @Override
    public void loadCities(City[] cities) {
        selectCityDialog.setRecyclerView(cities);
    }

    @Override
    public void showLoader() {
        if (!isProgressShowing) {
            isProgressShowing = true;
            progressView = (ViewGroup) getLayoutInflater().inflate(R.layout.progressbar_layout, null);
            View v = this.findViewById(android.R.id.content).getRootView();
            ViewGroup viewGroup = (ViewGroup) v;
            viewGroup.addView(progressView);
            lottieAnimationView = findViewById(R.id.progressBar);
            lottieAnimationView.playAnimation();
        }
    }

    @Override
    public void hideLoader() {
        View v = this.findViewById(android.R.id.content).getRootView();
        ViewGroup viewGroup = (ViewGroup) v;
        viewGroup.removeView(progressView);
        lottieAnimationView.cancelAnimation();
        isProgressShowing = false;
    }

    @Override
    public void onCitySelected(int id, String cityName) {
        cityId = id;
        txtViewCity.setText(cityName);
    }

    @Override
    public void onCategoryClicked(int position) {
        categoryId = this.categories[position].id;
        txtViewMainCategory.setText(this.categories[position].name);
        mPresenter.fetchSubcategories(categoryId);
    }

    @Override
    public void onSubcategoryClicked(int position) {
        subcategoryId = this.subcategories[position].id;
        txtViewCategory.setText(this.subcategories[position].name);
    }

    @OnClick(R.id.btn_back) void onBackClicked()
    {
        finish();
    }

}