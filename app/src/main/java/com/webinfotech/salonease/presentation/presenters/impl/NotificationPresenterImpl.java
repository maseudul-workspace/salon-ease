package com.webinfotech.salonease.presentation.presenters.impl;

import android.content.Context;

import com.webinfotech.salonease.AndroidApplication;
import com.webinfotech.salonease.domain.executors.Executor;
import com.webinfotech.salonease.domain.executors.MainThread;
import com.webinfotech.salonease.domain.interactors.MessageListInteractor;
import com.webinfotech.salonease.domain.interactors.impl.MessageListInteractorImpl;
import com.webinfotech.salonease.domain.models.MessageData;
import com.webinfotech.salonease.domain.models.UserInfo;
import com.webinfotech.salonease.presentation.presenters.NotificationPresenter;
import com.webinfotech.salonease.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.salonease.presentation.ui.adapters.NotificationListRecyclerViewAdapter;
import com.webinfotech.salonease.repository.AppRepositoryImpl;

public class NotificationPresenterImpl extends AbstractPresenter implements NotificationPresenter, MessageListInteractor.Callback {

    Context mContext;
    NotificationPresenter.View mView;
    MessageData[] newMessageData = null;
    NotificationListRecyclerViewAdapter adapter;
    int position;

    public NotificationPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void onMessageListFetchSuccess(MessageData[] messageData, int totalPage) {
        MessageData[] tempMessageDatas;
        tempMessageDatas = newMessageData;
        try {
            int len1 = tempMessageDatas.length;
            int len2 = messageData.length;
            newMessageData = new MessageData[len1 + len2];
            System.arraycopy(tempMessageDatas, 0, newMessageData, 0, len1);
            System.arraycopy(messageData, 0, newMessageData, len1, len2);
            adapter.updateData(newMessageData);
        }catch (NullPointerException e){
            newMessageData = messageData;
            adapter = new NotificationListRecyclerViewAdapter(mContext,messageData);
            mView.loadAdapter(adapter, totalPage);
        }
        mView.hideLoader();
    }

    @Override
    public void onMessageListFetchFail(String errorMsg, int loginError) {
        mView.hideLoader();
        if(loginError == 1)
        {
            AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
            androidApplication.setUserInfo(mContext,null);
            mView.onLoginError();
        }
    }

    @Override
    public void fetchMessageList(int page, String refresh) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (refresh.equals("refresh")) {
            newMessageData = null;
        }
        if (userInfo != null) {
            MessageListInteractorImpl messageListInteractorImpl = new MessageListInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, page);
            messageListInteractorImpl.execute();
            mView.showLoader();
        }
    }
}
