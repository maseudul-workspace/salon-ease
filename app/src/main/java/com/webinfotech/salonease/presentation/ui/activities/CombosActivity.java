package com.webinfotech.salonease.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import com.webinfotech.salonease.AndroidApplication;
import com.webinfotech.salonease.R;
import com.webinfotech.salonease.domain.executors.impl.ThreadExecutor;
import com.webinfotech.salonease.domain.models.ComboServiceData;
import com.webinfotech.salonease.presentation.presenters.CombosPresenter;
import com.webinfotech.salonease.presentation.presenters.impl.CombosPresenterImpl;
import com.webinfotech.salonease.presentation.ui.adapters.ComboListAdapter;
import com.webinfotech.salonease.presentation.ui.adapters.CombosAdapter;
import com.webinfotech.salonease.presentation.ui.dialogs.CustomProgressDialog;
import com.webinfotech.salonease.threading.MainThreadImpl;

public class CombosActivity extends AppCompatActivity implements CombosPresenter.View, ComboListAdapter.Callback {

    @BindView(R.id.recycler_view_combos)
    RecyclerView recyclerViewCombos;
    CombosPresenterImpl mPresenter;
    CustomProgressDialog customProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_combos);
        ButterKnife.bind(this);
        initialisePresenter();
        setUpProgressDialog();
        AndroidApplication androidApplication = (AndroidApplication) getApplicationContext();
        mPresenter.fetchCombos(androidApplication.getLatitude(), androidApplication.getLongitude());
    }

    private void initialisePresenter() {
        mPresenter = new CombosPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    private void setUpProgressDialog() {
        customProgressDialog = new CustomProgressDialog(this, this);
        customProgressDialog.setUpDialog();
    }

    @Override
    public void loadDate(ComboServiceData[] comboServiceData) {
        ComboListAdapter comboListAdapter = new ComboListAdapter(this, comboServiceData, this);
        recyclerViewCombos.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewCombos.setAdapter(comboListAdapter);
    }

    @Override
    public void showLoader() {
        customProgressDialog.showDialog();
    }

    @Override
    public void hideLoader() {
        customProgressDialog.dismissDialog();
    }

    @Override
    public void onSalonClicked(int id) {
        Intent intent = new Intent(this, SalonDetailsActivity.class);
        intent.putExtra("salonId", id);
        startActivity(intent);
    }

    @OnClick(R.id.btn_back) void onBackClicked()
    {
        finish();
    }
}