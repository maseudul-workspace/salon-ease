package com.webinfotech.salonease.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.webinfotech.salonease.R;
import com.webinfotech.salonease.domain.executors.impl.ThreadExecutor;
import com.webinfotech.salonease.presentation.presenters.NotificationPresenter;
import com.webinfotech.salonease.presentation.presenters.impl.NotificationPresenterImpl;
import com.webinfotech.salonease.presentation.ui.adapters.NotificationListRecyclerViewAdapter;
import com.webinfotech.salonease.threading.MainThreadImpl;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class MessageListActivity extends AppCompatActivity implements NotificationPresenter.View {

    @BindView(R.id.recycler_view_message_list)
    RecyclerView recyclerViewMessageList;

    NotificationPresenterImpl mPresenter;
    LinearLayoutManager layoutManager;
    ViewGroup progressView;
    protected boolean isProgressShowing = false;
    LottieAnimationView lottieAnimationView;

    Boolean isScrolling = false;
    Integer currentItems;
    Integer totalItems;
    Integer scrollOutItems;
    int pageNo = 1;
    int totalPage = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message_list);
        ButterKnife.bind(this);
        initialsiePresenter();
        mPresenter.fetchMessageList(pageNo, "refresh");
    }

    private void initialsiePresenter() {
        mPresenter = new NotificationPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    @Override
    public void loadAdapter(NotificationListRecyclerViewAdapter adapter, int totalPage) {
        this.totalPage = totalPage;
        layoutManager = new LinearLayoutManager(this);
        recyclerViewMessageList.setLayoutManager(layoutManager);
        recyclerViewMessageList.setAdapter(adapter);
        recyclerViewMessageList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {

                super.onScrollStateChanged(recyclerView, newState);
                if(newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL)
                {
                    isScrolling= true;
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                currentItems = layoutManager.getChildCount();
                totalItems  = layoutManager.getItemCount();
                scrollOutItems = layoutManager.findFirstCompletelyVisibleItemPosition();
                if(!recyclerView.canScrollVertically(1))
                {
                    if(pageNo < totalPage) {
                        isScrolling = false;
                        pageNo = pageNo + 1;
                        mPresenter.fetchMessageList(pageNo,"");
                    }
                }
            }
        });
    }

    @Override
    public void showLoader() {
        isProgressShowing = true;
        progressView = (ViewGroup) getLayoutInflater().inflate(R.layout.progressbar_layout, null);
        View v = this.findViewById(android.R.id.content).getRootView();
        ViewGroup viewGroup = (ViewGroup) v;
        viewGroup.addView(progressView);
        lottieAnimationView = findViewById(R.id.progressBar);
        lottieAnimationView.playAnimation();
    }

    @Override
    public void hideLoader() {
        View v = this.findViewById(android.R.id.content).getRootView();
        ViewGroup viewGroup = (ViewGroup) v;
        viewGroup.removeView(progressView);
        lottieAnimationView.cancelAnimation();
        isProgressShowing = false;
    }


    @Override
    public void onLoginError() {
        Toast.makeText(this, "Your session has expired !!", Toast.LENGTH_LONG).show();
        Intent intent = new Intent(this, LoginActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }

    @OnClick(R.id.btn_back) void onBackClicked()
    {
        finish();
    }
}