package com.webinfotech.salonease.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;

import com.webinfotech.salonease.R;
import com.webinfotech.salonease.domain.models.SalonImages;
import com.webinfotech.salonease.util.GlideHelper;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ImageGalleryAdapter extends RecyclerView.Adapter<ImageGalleryAdapter.ViewHolder> {

    public interface Callback {
        void onImageClicked(String image);
    }

    Context mContext;
    SalonImages[] salonImages;
    Callback mCallback;

    public ImageGalleryAdapter(Context mContext, SalonImages[] salonImages, Callback mCallback) {
        this.mContext = mContext;
        this.salonImages = salonImages;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_image_gallery, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        GlideHelper.setImageView(mContext, holder.imgViewGallery, mContext.getResources().getString(R.string.base_url) + "images/client/" + salonImages[position].image);
        holder.imgViewGallery.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCallback.onImageClicked(salonImages[position].image);
            }
        });
    }

    @Override
    public int getItemCount() {
        return salonImages.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_view_gallery)
        ImageView imgViewGallery;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
