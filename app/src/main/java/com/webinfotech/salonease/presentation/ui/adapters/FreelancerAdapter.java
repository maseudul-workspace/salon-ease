package com.webinfotech.salonease.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.webinfotech.salonease.R;
import com.webinfotech.salonease.domain.models.SalonUser;
import com.webinfotech.salonease.util.GlideHelper;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import es.dmoral.toasty.Toasty;

public class FreelancerAdapter extends RecyclerView.Adapter<FreelancerAdapter.ViewHolder> {

    public interface Callback {
        void onSalonClicked(int salonId);
    }

    Context mContext;
    SalonUser[] salonUsers;
    Callback mCallback;

    public FreelancerAdapter(Context mContext, SalonUser[] salonUsers, Callback mCallback) {
        this.mContext = mContext;
        this.salonUsers = salonUsers;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_freelancer, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewFreelancerName.setText(salonUsers[position].name);
        GlideHelper.setImageViewCustomRoundedCorners(mContext, holder.imgViewFreelancer, mContext.getResources().getString(R.string.base_url) + "images/client/" + salonUsers[position].image, 100);
        holder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onSalonClicked(salonUsers[position].id);
            }
        });
    }

    @Override
    public int getItemCount() {
        return salonUsers.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_freelancer_name)
        TextView txtViewFreelancerName;
        @BindView(R.id.img_view_freelancer)
        ImageView imgViewFreelancer;
        @BindView(R.id.main_layout)
        View mainLayout;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
