package com.webinfotech.salonease.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import com.webinfotech.salonease.AndroidApplication;
import com.webinfotech.salonease.R;
import com.webinfotech.salonease.domain.executors.impl.ThreadExecutor;
import com.webinfotech.salonease.domain.models.SalonUser;
import com.webinfotech.salonease.presentation.presenters.TopFreelancerPresenter;
import com.webinfotech.salonease.presentation.presenters.impl.TopFreeLancerPresenterImpl;
import com.webinfotech.salonease.presentation.ui.adapters.SalonListAdapter;
import com.webinfotech.salonease.presentation.ui.dialogs.CustomProgressDialog;
import com.webinfotech.salonease.threading.MainThreadImpl;

public class TopFreelancersActivity extends AppCompatActivity implements TopFreelancerPresenter.View, SalonListAdapter.Callback {

    @BindView(R.id.recycler_view_top_freelancers)
    RecyclerView recyclerViewTopFreelancers;
    private TopFreeLancerPresenterImpl mPresenter;
    CustomProgressDialog customProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_top_freelancers);
        ButterKnife.bind(this);
        initialisePresenter();
        setUpProgressDialog();
        AndroidApplication androidApplication = (AndroidApplication) getApplicationContext();
        mPresenter.fetchTopFreelancers(androidApplication.getLatitude(), androidApplication.getLongitude());
    }

    private void initialisePresenter() {
        mPresenter = new TopFreeLancerPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    private void setUpProgressDialog() {
        customProgressDialog = new CustomProgressDialog(this, this);
        customProgressDialog.setUpDialog();
    }

    @Override
    public void loadDate(SalonUser[] salonUsers) {
        SalonListAdapter salonListAdapter = new SalonListAdapter(this, salonUsers, this);
        recyclerViewTopFreelancers.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewTopFreelancers.setAdapter(salonListAdapter);
    }

    @Override
    public void showLoader() {
        customProgressDialog.showDialog();
    }

    @Override
    public void hideLoader() {
        customProgressDialog.dismissDialog();
    }

    @Override
    public void onSalonClicked(int salonId) {
        Intent intent = new Intent(this, SalonDetailsActivity.class);
        intent.putExtra("salonId", salonId);
        startActivity(intent);
    }

    @OnClick(R.id.btn_back) void onBackClicked()
    {
        finish();
    }
}