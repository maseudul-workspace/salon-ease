package com.webinfotech.salonease.presentation.presenters.impl;

import android.content.Context;

import com.webinfotech.salonease.domain.executors.Executor;
import com.webinfotech.salonease.domain.executors.MainThread;
import com.webinfotech.salonease.domain.interactors.FetchCombosInteractor;
import com.webinfotech.salonease.domain.interactors.impl.FetchCombosInteractorImpl;
import com.webinfotech.salonease.domain.models.ComboServiceData;
import com.webinfotech.salonease.presentation.presenters.CombosPresenter;
import com.webinfotech.salonease.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.salonease.repository.AppRepositoryImpl;

public class CombosPresenterImpl extends AbstractPresenter implements CombosPresenter, FetchCombosInteractor.Callback {

    Context mContext;
    CombosPresenter.View mView;

    public CombosPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchCombos(double latitude, double longitude) {
        FetchCombosInteractorImpl fetchCombosInteractor = new FetchCombosInteractorImpl(mExecutor, mMainThread, this, new AppRepositoryImpl(), latitude, longitude);
        fetchCombosInteractor.execute();
        mView.showLoader();
    }

    @Override
    public void onGettingComboListSuccess(ComboServiceData[] comboServiceData) {
        mView.loadDate(comboServiceData);
        mView.hideLoader();
    }

    @Override
    public void onGettingComboListFail(String errorMsg) {
        mView.hideLoader();
    }
}
