package com.webinfotech.salonease.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.webinfotech.salonease.R;
import com.webinfotech.salonease.presentation.ui.adapters.FragmentsAdapter;
import com.webinfotech.salonease.presentation.ui.fragments.SplashSliderFragment;
import com.webinfotech.salonease.util.GlideHelper;

public class SplashSliderActivity extends AppCompatActivity {

    @BindView(R.id.viewpager_splash_slider)
    ViewPager viewPager;
    @BindView(R.id.dots_indicator)
    LinearLayout dotsIndicatorLayout;
    @BindView(R.id.btn_get_started)
    Button btnGetStarted;
    SharedPreferences preferences;
    String pref_show_intro = "Intro";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_slider);
        ButterKnife.bind(this);
        preferences = getSharedPreferences("IntroSlider", Context.MODE_PRIVATE);

        if(!preferences.getBoolean(pref_show_intro, true))
        {
            Intent intent = new Intent(this, LocationPermissionActivity.class);
            startActivity(intent);
            finish();
        }
        setData();
    }

    private void setData() {
        SplashSliderFragment splashSliderFragment1 = new SplashSliderFragment();
        splashSliderFragment1.setData("https://www.viu.ca/sites/default/files/styles/full_bg_focal_breakpoints_theme_viu_theme_bg_xxl_landscape_1x/public/hairstyle-foundation-certificate-program.jpg?itok=_rw6ycbL&timestamp=1553532443", getDrawable(R.drawable.scissors_dark), "Every Client is Special", "Perfect salon booking app for your beauty and comfortable life");

        SplashSliderFragment splashSliderFragment2 = new SplashSliderFragment();
        splashSliderFragment2.setData("https://i.pinimg.com/originals/08/f5/dd/08f5dd7f30140a7c6aa619e929507f0f.jpg", getDrawable(R.drawable.trimmer_dark), "Embrace Technology", "Perfect salon booking app for your beauty and comfortable life");
        SplashSliderFragment splashSliderFragment3 = new SplashSliderFragment();
        splashSliderFragment3.setData("https://www.ringmystylist.com/wp-content/uploads/2019/09/shutterstock_1198611691-1024x683.png", getDrawable(R.drawable.comb_dark), "Keep it Clean", "Perfect salon booking app for your beauty and comfortable life");
        FragmentsAdapter adapter = new FragmentsAdapter(getSupportFragmentManager());
        adapter.AddFragment(splashSliderFragment1, "");
        adapter.AddFragment(splashSliderFragment2, "");
        adapter.AddFragment(splashSliderFragment3, "");
        prepareViewpagerDotsIndicator(0, 3);
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if (position == 2) {
                    btnGetStarted.setVisibility(View.VISIBLE);
                    dotsIndicatorLayout.setVisibility(View.GONE);
                } else {
                    btnGetStarted.setVisibility(View.INVISIBLE);
                    dotsIndicatorLayout.setVisibility(View.VISIBLE);
                }
                prepareViewpagerDotsIndicator(position, 3);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
    }

    private void prepareViewpagerDotsIndicator(int sliderPosition, int length) {
        if(dotsIndicatorLayout.getChildCount() > 0){
            dotsIndicatorLayout.removeAllViews();
        }

        ImageView dots[] = new ImageView[length];
        for(int i = 0; i < length; i++){
            dots[i] = new ImageView(this);
            if(i == sliderPosition){
                GlideHelper.setImageViewWithDrawable(this, dots[i], R.drawable.active_dot);
            } else {
                GlideHelper.setImageViewWithDrawable(this, dots[i], R.drawable.inactive_dot);
            }

            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(25, 15);
            layoutParams.setMargins(7, 0, 7, 0);
            dotsIndicatorLayout.addView(dots[i], layoutParams);

        }
    }

    @OnClick(R.id.btn_get_started) void onGetStartedClicked() {
        Intent intent = new Intent(this, LocationPermissionActivity.class);
        startActivity(intent);
        finish();
        SharedPreferences.Editor editor = preferences.edit();
        editor.putBoolean(pref_show_intro, false);
        editor.apply();
    }

}