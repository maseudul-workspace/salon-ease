package com.webinfotech.salonease.presentation.presenters;

import com.webinfotech.salonease.domain.models.AppLoadData;
import com.webinfotech.salonease.domain.models.Category;
import com.webinfotech.salonease.domain.models.City;
import com.webinfotech.salonease.domain.models.MessageData;
import com.webinfotech.salonease.presentation.ui.adapters.CategoryViewpagerAdapter;
import com.webinfotech.salonease.presentation.ui.adapters.FreelancerAdapter;
import com.webinfotech.salonease.presentation.ui.adapters.HomeSlidingImagesAdapter;
import com.webinfotech.salonease.presentation.ui.adapters.TopSalonViewpagerAdapter;

public interface MainPresenter {
    void fetchAppData(double latitude, double longitude);
    void fetchCities();
    void fetchMessage();
    void updateFirebaseToken(String token);
    void fetchOffers();
    interface View {
        void loadData(AppLoadData appLoadData);
        void loadMessage(MessageData[] messageData);
        void loadCities(City[] cities);
        void showLoader();
        void hideLoader();
        void loginError();
    }
}
