package com.webinfotech.salonease.presentation.presenters;

import com.webinfotech.salonease.presentation.ui.adapters.SalonListAdapter;

public interface SearchListPresenter {
    void fetchSearchList(String searchKey, double latitude, double longitude, int page, String refresh);
    interface View {
        void loadData(SalonListAdapter adapter, int totalPage);
        void goToSalonDetails(int serviceId);
    }
}
