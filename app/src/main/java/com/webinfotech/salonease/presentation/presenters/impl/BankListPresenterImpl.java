package com.webinfotech.salonease.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import com.webinfotech.salonease.AndroidApplication;
import com.webinfotech.salonease.domain.executors.Executor;
import com.webinfotech.salonease.domain.executors.MainThread;
import com.webinfotech.salonease.domain.interactors.AddBankAccountInteractor;
import com.webinfotech.salonease.domain.interactors.FetchBankListInteractor;
import com.webinfotech.salonease.domain.interactors.UpdateBankInteractor;
import com.webinfotech.salonease.domain.interactors.impl.AddBankAccountInteractorImpl;
import com.webinfotech.salonease.domain.interactors.impl.FetchBankListInteractorImpl;
import com.webinfotech.salonease.domain.interactors.impl.UpdateBankInteractorImpl;
import com.webinfotech.salonease.domain.models.BankList;
import com.webinfotech.salonease.domain.models.UserInfo;
import com.webinfotech.salonease.presentation.presenters.BankListPresenter;
import com.webinfotech.salonease.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.salonease.presentation.ui.adapters.RefundBankListAdapter;
import com.webinfotech.salonease.repository.AppRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class BankListPresenterImpl extends AbstractPresenter implements BankListPresenter,
                                                                        AddBankAccountInteractor.Callback,
                                                                        FetchBankListInteractor.Callback,
                                                                        RefundBankListAdapter.Callback,
                                                                        UpdateBankInteractor.Callback
{

    Context mContext;
    BankListPresenter.View mView;
    int bankAccountId;

    public BankListPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void addBankAccount(String name, String bankName, String accountNo, String ifsc, String branchName) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo user = androidApplication.getUserInfo(mContext);
        if (user == null) {
            mView.showLoginBottomSheet();
            Toasty.warning(mContext, "Your Session Expired!! Please Login Again").show();
        } else {
            AddBankAccountInteractorImpl addBankAccountInteractor = new AddBankAccountInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, user.apiToken, user.userId, name, bankName, accountNo, ifsc, branchName);
            addBankAccountInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void fetchBankList() {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo user = androidApplication.getUserInfo(mContext);
        if (user == null) {
            mView.showLoginBottomSheet();
            Toasty.warning(mContext, "Your Session Expired!! Please Login Again").show();
        } else {
            FetchBankListInteractorImpl fetchBankListInteractor = new FetchBankListInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, user.userId, user.apiToken);
            fetchBankListInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void updateBankAccount(String name, String bankName, String accountNo, String ifsc, String branchName) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo user = androidApplication.getUserInfo(mContext);
        if (user == null) {
            mView.showLoginBottomSheet();
            Toasty.warning(mContext, "Your Session Expired!! Please Login Again").show();
        } else {
            UpdateBankInteractorImpl updateBankInteractor = new UpdateBankInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, user.apiToken, bankAccountId, name, bankName, accountNo, ifsc, branchName);
            updateBankInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void onAddAccountSuccess() {
        mView.hideLoader();
    }

    @Override
    public void onAddAccountFail(String errorMsg, int loginError) {
        mView.hideLoader();
        if (loginError == 1) {
            mView.showLoginBottomSheet();
        }
        Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onGettingBankListSuccess(BankList[] bankLists) {
        if (bankLists.length == 0) {
            mView.hideBankAddBtn();
        } else {
            RefundBankListAdapter adapter = new RefundBankListAdapter(mContext, bankLists, this);
            mView.loadData(adapter);
        }
        mView.hideLoader();
    }

    @Override
    public void onGettingBankListFail(String errorMsg, int loginError) {
        mView.hideLoader();
        if (loginError == 1) {
            mView.showLoginBottomSheet();
        }
        Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onBankEditClicked(BankList bankList) {
        bankAccountId = bankList.id;
        mView.setData(bankList);
    }

    @Override
    public void onBankUpdateSuccess() {
        mView.hideLoader();
        fetchBankList();
    }

    @Override
    public void onBankUpdateFail(String errorMsg, int loginError) {
        mView.hideLoader();
        if (loginError == 1) {
            mView.showLoginBottomSheet();
        }
        Toast.makeText(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }
}
