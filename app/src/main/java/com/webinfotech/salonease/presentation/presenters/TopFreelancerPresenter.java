package com.webinfotech.salonease.presentation.presenters;

import com.webinfotech.salonease.domain.models.SalonUser;

public interface TopFreelancerPresenter {
    void fetchTopFreelancers(double latitude, double longitude);
    interface View {
        void loadDate(SalonUser[] salonUsers);
        void showLoader();
        void hideLoader();
    }
}
