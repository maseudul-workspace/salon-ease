package com.webinfotech.salonease.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.webinfotech.salonease.R;
import com.webinfotech.salonease.domain.models.testing.Category;
import com.webinfotech.salonease.domain.models.testing.CategoryContainer;
import com.webinfotech.salonease.util.GlideHelper;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

public class CategoryViewpagerAdapter extends PagerAdapter {

    public interface Callback {
        void onCategoryClicked(int categoryId, String categoryName);
    }

    Context mContext;
    ArrayList<CategoryContainer> categoryContainers;
    Callback mCallback;

    public CategoryViewpagerAdapter(Context mContext, ArrayList<CategoryContainer> categoryContainers, Callback mCallback) {
        this.mContext = mContext;
        this.categoryContainers = categoryContainers;
        this.mCallback = mCallback;
    }

    @Override
    public int getCount() {
        return categoryContainers.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.layout_viewpager_category, container, false);
        ImageView imageViewCategory1 = (ImageView) layout.findViewById(R.id.img_view_category1);
        ImageView imageViewCategory2 = (ImageView) layout.findViewById(R.id.img_view_category2);
        TextView txtViewCategoryName1 = (TextView) layout.findViewById(R.id.txt_view_category1_name);
        TextView txtViewCategoryName2 = (TextView) layout.findViewById(R.id.txt_view_category2_name);
        View viewCategory1 = (View) layout.findViewById(R.id.view_category_1);
        View viewCategory2 = (View) layout.findViewById(R.id.view_category_2);
        ArrayList<Category> categories = categoryContainers.get(position).categories;
        GlideHelper.setImageViewCustomRoundedCorners(mContext, imageViewCategory1, mContext.getResources().getString(R.string.base_url) + "admin/service_category/" + categories.get(0).image, 8);
        if (categories.size() > 1) {
            GlideHelper.setImageViewCustomRoundedCorners(mContext, imageViewCategory2, mContext.getResources().getString(R.string.base_url) + "admin/service_category/" + categories.get(1).image, 8);
            txtViewCategoryName2.setText(categories.get(1).name);
            viewCategory2.setVisibility(View.VISIBLE);
            viewCategory2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mCallback.onCategoryClicked(categories.get(1).id, categories.get(1).name);
                }
            });
        } else {
            viewCategory2.setVisibility(View.INVISIBLE);
        }
        viewCategory1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onCategoryClicked(categories.get(0).id, categories.get(0).name);
            }
        });
        txtViewCategoryName1.setText(categories.get(0).name);
        container.addView(layout);
        return layout;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }
}
