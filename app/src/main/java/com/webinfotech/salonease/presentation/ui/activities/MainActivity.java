package com.webinfotech.salonease.presentation.ui.activities;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.viewpager.widget.ViewPager;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.content.Intent;
import android.content.IntentSender;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.airbnb.lottie.LottieAnimationView;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.play.core.appupdate.AppUpdateInfo;
import com.google.android.play.core.appupdate.AppUpdateManager;
import com.google.android.play.core.appupdate.AppUpdateManagerFactory;
import com.google.android.play.core.install.model.AppUpdateType;
import com.google.android.play.core.install.model.UpdateAvailability;
import com.google.android.play.core.tasks.OnFailureListener;
import com.google.android.play.core.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.tbuonomo.viewpagerdotsindicator.DotsIndicator;
import com.webinfotech.salonease.AndroidApplication;
import com.webinfotech.salonease.R;
import com.webinfotech.salonease.domain.executors.impl.ThreadExecutor;
import com.webinfotech.salonease.domain.models.AppLoadData;
import com.webinfotech.salonease.domain.models.Category;
import com.webinfotech.salonease.domain.models.City;
import com.webinfotech.salonease.domain.models.MessageData;
import com.webinfotech.salonease.domain.models.UserInfo;
import com.webinfotech.salonease.presentation.presenters.MainPresenter;
import com.webinfotech.salonease.presentation.presenters.impl.MainPresenterImpl;
import com.webinfotech.salonease.presentation.ui.adapters.CategoryAdapter;
import com.webinfotech.salonease.presentation.ui.adapters.CategoryViewpagerAdapter;
import com.webinfotech.salonease.presentation.ui.adapters.ComboHomeAdapter;
import com.webinfotech.salonease.presentation.ui.adapters.DealsAdapter;
import com.webinfotech.salonease.presentation.ui.adapters.FreelancerAdapter;
import com.webinfotech.salonease.presentation.ui.adapters.HomeSlidingImagesAdapter;
import com.webinfotech.salonease.presentation.ui.adapters.SalonListHorizontalAdapter;
import com.webinfotech.salonease.presentation.ui.adapters.TopSalonViewpagerAdapter;
import com.webinfotech.salonease.presentation.ui.adapters.ViewPagerAdapter;
import com.webinfotech.salonease.presentation.ui.dialogs.ServiceFilterDialogFragment;
import com.webinfotech.salonease.presentation.ui.dialogs.UpdateDialog;
import com.webinfotech.salonease.threading.MainThreadImpl;
import com.webinfotech.salonease.util.GlideHelper;
import com.webinfotech.salonease.util.Helper;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends BaseActivity implements MainPresenter.View, ServiceFilterDialogFragment.Callback, CategoryAdapter.Callback, SalonListHorizontalAdapter.Callback, FreelancerAdapter.Callback, ComboHomeAdapter.Callback, DealsAdapter.Callback {

    @BindView(R.id.view_pager_main)
    ViewPager viewPager;
    @BindView(R.id.notification_number)
    TextView textViewNotificationNumber;
    @BindView(R.id.recycler_view_freelancers)
    RecyclerView recyclerViewFreelancers;
    @BindView(R.id.recycler_view_categories)
    RecyclerView recyclerViewCategories;
    @BindView(R.id.recycler_view_salon_list)
    RecyclerView recyclerViewSalonList;
    @BindView(R.id.bottom_navigation)
    BottomNavigationView bottomNavigationView;
    @BindView(R.id.recycler_view_combos)
    RecyclerView recyclerViewCombos;
    @BindView(R.id.recycler_view_deal_of_the_day)
    RecyclerView recyclerViewDealofTheDay;
    ViewGroup progressView;
    protected boolean isProgressShowing = false;
    LottieAnimationView lottieAnimationView;
    SwipeRefreshLayout swipeRefreshLayout;
    MainPresenterImpl mPresenter;
    Category[] categories;
    ServiceFilterDialogFragment serviceFilterDialogFragment;
    City[] cities;
    String categoryName;
    int UPDATE_REQUEST_CODE = 1200;
    double latitude = 26.14;
    double longitude = 91.73;
    int currentPage = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        inflateContent(R.layout.activity_main);
        ButterKnife.bind(this);

//        Set Latitude and Longitude
        AndroidApplication androidApplication = (AndroidApplication) getApplicationContext();
        latitude = androidApplication.getLatitude();
        longitude = androidApplication.getLongitude();
        checkLogin();
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        serviceFilterDialogFragment = new ServiceFilterDialogFragment();
        setBottomNavigationView();
        initialisePresenter();
        checkUpdate();
        refresh();
        mPresenter.updateFirebaseToken(refreshedToken);
        mPresenter.fetchAppData(latitude, longitude);
        mPresenter.fetchCities();
        mPresenter.fetchOffers();
       if(isLoggedIn()) {
            mPresenter.fetchMessage();
       }
      //  textViewNotificationNumber.setVisibility(View.VISIBLE);
    }

    private void initialisePresenter() {
        mPresenter = new MainPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    private void setBottomNavigationView() {
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.nav_user:
                        if (isLoggedIn()) {
                            goToUserActivity();
                        } else {
                            Helper.showLoginBottomSheet(getSupportFragmentManager());
                        }
                        break;
                    case R.id.nav_search:
                        goToSearchActivity();
                        break;
                    case R.id.nav_about:
                        goToAboutActivity();
                        break;
                    case R.id.nav_booking:
                        if(isLoggedIn()) {
                            goToBookingListActivity();
                        }
                        else {
                            Helper.showLoginBottomSheet(getSupportFragmentManager());
                        }
                        break;
                }
                return false;
            }
        });
    }

    private boolean isLoggedIn() {
        AndroidApplication androidApplication = (AndroidApplication) getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(this);
        if (userInfo != null) {
            return true;
        } else {
            return false;
        }
    }

    private void goToUserActivity() {
        Intent intent = new Intent(this, UserActivity.class);
        startActivity(intent);
    }

    private void goToAboutActivity() {
        Intent intent = new Intent(this, AboutActivity.class);
        startActivity(intent);
    }

    private void goToBookingListActivity() {
        Intent intent = new Intent(this, OrderHistoryActivity.class);
        startActivity(intent);
    }


    private void checkUpdate() {
        AppUpdateManager appUpdateManager = AppUpdateManagerFactory.create(this);

        Task<AppUpdateInfo> appUpdateInfoTask = appUpdateManager.getAppUpdateInfo();


        appUpdateInfoTask.addOnSuccessListener(appUpdateInfo -> {
            if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE) {
                try {
                    appUpdateManager.startUpdateFlowForResult(appUpdateInfo, AppUpdateType.FLEXIBLE, this, UPDATE_REQUEST_CODE);
                } catch (IntentSender.SendIntentException e) {
                    e.printStackTrace();
                }
            }
        });

        appUpdateInfoTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(Exception e) {
            }
        });

    }

    @Override
    public void loadData(AppLoadData appLoadData) {

        CategoryAdapter categoryAdapter = new CategoryAdapter(this, appLoadData.categories, this);
        recyclerViewCategories.setAdapter(categoryAdapter);
        recyclerViewCategories.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));

        FreelancerAdapter freelancerAdapter = new FreelancerAdapter(this, appLoadData.topFreeLaunchers, this);
        SalonListHorizontalAdapter salonListHorizontalAdapter = new SalonListHorizontalAdapter(this, appLoadData.topSaloons, this);
        ComboHomeAdapter comboHomeAdapter = new ComboHomeAdapter(this, appLoadData.comboServiceData, this);
        DealsAdapter dealsAdapter = new DealsAdapter(this, appLoadData.dealOfTheDays, this);

        recyclerViewFreelancers.setAdapter(freelancerAdapter);
        recyclerViewFreelancers.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));

        recyclerViewSalonList.setAdapter(salonListHorizontalAdapter);
        recyclerViewSalonList.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        recyclerViewCombos.setAdapter(comboHomeAdapter);
        recyclerViewCombos.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        
        recyclerViewDealofTheDay.setAdapter(dealsAdapter);
        recyclerViewDealofTheDay.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));

        ViewPagerAdapter adapter = new ViewPagerAdapter(this, appLoadData.sliders);
        viewPager.setAdapter(adapter);


        Handler handler = new Handler();
        Runnable update = new Runnable()  {

            public void run() {
                if ( currentPage == appLoadData.sliders.length ) {

                    currentPage = 0;
                }
                viewPager.setCurrentItem(currentPage++, true);
            }
        };


        new Timer().schedule(new TimerTask() {

            @Override
            public void run() {
                handler.post(update);
            }
        }, 2000, 4000);
    }

    @Override
    public void loadMessage(MessageData[] messageData) {
        textViewNotificationNumber.setText(Integer.toString(messageData.length));
    }

    @Override
    public void loadCities(City[] cities) {
        this.cities = cities;
    }

    @Override
    public void showLoader() {
        if (!isProgressShowing) {
            isProgressShowing = true;
            progressView = (ViewGroup) getLayoutInflater().inflate(R.layout.progressbar_layout, null);
            View v = this.findViewById(android.R.id.content).getRootView();
            ViewGroup viewGroup = (ViewGroup) v;
            viewGroup.addView(progressView);
            lottieAnimationView = findViewById(R.id.progressBar);
            lottieAnimationView.playAnimation();
        }
    }

    @Override
    public void hideLoader() {
        View v = this.findViewById(android.R.id.content).getRootView();
        ViewGroup viewGroup = (ViewGroup) v;
        viewGroup.removeView(progressView);
        lottieAnimationView.cancelAnimation();
        isProgressShowing = false;
    }

    @Override
    public void loginError() {
        Toast.makeText(this, "Your session has expired !!", Toast.LENGTH_LONG).show();
        Helper.showLoginBottomSheet(getSupportFragmentManager());
        finish();
    }

    @Override
    public void onAddClicked(int categoryId, int serviceCity, String clientType, String categoryName) {
        Intent intent = new Intent(this, SalonListActivity.class);
        intent.putExtra("categoryId", categoryId);
        intent.putExtra("serviceCityId", serviceCity);
        intent.putExtra("clientType", clientType);
        intent.putExtra("categoryName", categoryName);
        startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.fetchCities();
        mPresenter.fetchOffers();
    }

    private void goToSearchActivity() {
        Intent intent = new Intent(this, SearchActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onCategoryClicked(String categoryName, int id) {
        Intent intent = new Intent(this, ChooseCategoryActivity.class);
        intent.putExtra("categoryId", id);
        startActivity(intent);
    }

    @Override
    public void onSalonClicked(int salonId) {
        Intent intent = new Intent(this, SalonDetailsActivity.class);
        intent.putExtra("salonId", salonId);
        startActivity(intent);
    }

    @OnClick(R.id.layout_search) void onSearchClicked() {
        Intent intent = new Intent(this, SearchActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.view_top_salons) void onTopSalonsClicked() {
        Intent intent = new Intent(this, TopSalonsActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.view_top_home_services) void onTopHomeServicesClicked() {
        Intent intent = new Intent(this, TopFreelancersActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.view_deal_of_the_day) void onDealOfTheDayClicked() {
        Intent intent = new Intent(this, DealsActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.view_combos) void onCombosClicked() {
        Intent intent = new Intent(this, CombosActivity.class);
        startActivity(intent);
    }
    @OnClick(R.id.relative_layout_near_by) void onNearByClicked()
    {
        Intent intent = new Intent(this, ChooseNearByActivity.class);
        startActivity(intent);
    }

    private void checkLogin() {
        AndroidApplication androidApplication = (AndroidApplication) getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(this);
        if (userInfo != null) {

        }
    }

    private void refresh()
    {
        swipeRefreshLayout = findViewById(R.id.swiperefresh);
        swipeRefreshLayout.setOnRefreshListener(() -> {
            onResume();
            swipeRefreshLayout.setRefreshing(false);
        });
    }

}