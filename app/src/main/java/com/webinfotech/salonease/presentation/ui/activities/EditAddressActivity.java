package com.webinfotech.salonease.presentation.ui.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.app.ProgressDialog;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.textfield.TextInputLayout;
import com.webinfotech.salonease.R;
import com.webinfotech.salonease.domain.executors.impl.ThreadExecutor;
import com.webinfotech.salonease.domain.models.ShippingAddress;
import com.webinfotech.salonease.presentation.presenters.EditAddressPresenter;
import com.webinfotech.salonease.presentation.presenters.impl.EditAddressPresenterImpl;
import com.webinfotech.salonease.threading.MainThreadImpl;
import com.webinfotech.salonease.util.Helper;

import java.io.IOException;
import java.util.List;
import java.util.Locale;

public class EditAddressActivity extends AppCompatActivity implements  OnMapReadyCallback, EditAddressPresenter.View{

    @BindView(R.id.layout_bottomsheet)
    View layoutBottomsheet;
    @BindView(R.id.txt_view_location_address)
    TextView txtViewLocationAddress;
    @BindView(R.id.edit_text_city)
    EditText editTextCity;
    @BindView(R.id.edit_text_state)
    EditText editTextState;
    @BindView(R.id.edit_text_address)
    EditText editTextAddress;
    @BindView(R.id.edit_text_pin)
    EditText editTextPin;
    @BindView(R.id.edit_text_email)
    EditText editTextEmail;
    @BindView(R.id.edit_text_phone)
    EditText editTextPhone;
    @BindView(R.id.edit_text_name)
    EditText editTextName;
    @BindView(R.id.txt_input_name_layout)
    TextInputLayout textInputNameLayout;
    @BindView(R.id.txt_input_phone_layout)
    TextInputLayout textInputPhoneLayout;
    BottomSheetBehavior sheetBehavior;
    private GoogleMap mMap;
    ProgressDialog progressDialog;
    EditAddressPresenterImpl mPresenter;
    int addressId;
    Marker mCurrLocationMarker;
    double latitude;
    double longitude;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_address);
        getSupportActionBar().setTitle("Edit Address");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ButterKnife.bind(this);
        setUpProgressDialog();
        addressId = getIntent().getIntExtra("addressId", 0);
        setLayoutBottomsheet();
        textInputNameLayout.setVisibility(View.VISIBLE);
        textInputPhoneLayout.setVisibility(View.VISIBLE);
        initialisePresenter();
        mPresenter.fetchAddressDetails(addressId);
    }

    private void initialisePresenter() {
        mPresenter = new EditAddressPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    private void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }


    private void setLayoutBottomsheet() {
        sheetBehavior = BottomSheetBehavior.from(layoutBottomsheet);
        sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
        sheetBehavior.setPeekHeight(400);
        sheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
            @Override
            public void onStateChanged(@NonNull View bottomSheet, int newState) {
                switch (newState) {
                    case BottomSheetBehavior.STATE_HIDDEN:
                        break;
                    case BottomSheetBehavior.STATE_EXPANDED:
                        break;
                    case BottomSheetBehavior.STATE_COLLAPSED: {
                        break;
                    }
                    case BottomSheetBehavior.STATE_DRAGGING:
                        break;
                    case BottomSheetBehavior.STATE_SETTLING:
                        break;
                }
            }

            @Override
            public void onSlide(@NonNull View bottomSheet, float slideOffset) {

            }
        });
    }

    private void initialiseMap() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        LatLng latLng = new LatLng(latitude, longitude);
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.title("Current Position");
        markerOptions.draggable(true);
        markerOptions.visible(false);
        mCurrLocationMarker = mMap.addMarker(markerOptions);

        //move map camera
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(18));
        mMap.setOnCameraIdleListener(new GoogleMap.OnCameraIdleListener() {
            @Override
            public void onCameraIdle() {
                LatLng centerLatLang = mMap.getProjection().getVisibleRegion().latLngBounds.getCenter();
                Geocoder geocoder;
                latitude = centerLatLang.latitude;
                longitude = centerLatLang.longitude;
                List<Address> addresses = null;
                geocoder = new Geocoder(getApplicationContext(), Locale.getDefault());
                try {
                    addresses = geocoder.getFromLocation(centerLatLang.latitude, centerLatLang.longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
                } catch (IOException e) {
                    e.printStackTrace();
                }

                String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
                String city = addresses.get(0).getLocality();
                String state = addresses.get(0).getAdminArea();
                String postalCode = addresses.get(0).getPostalCode();
                editTextCity.setText(city);
                editTextState.setText(state);
                editTextPin.setText(postalCode);
                editTextAddress.setText(address);
                txtViewLocationAddress.setText(address);
            }
        });

        mMap.setOnCameraMoveListener(new GoogleMap.OnCameraMoveListener() {
            @Override
            public void onCameraMove() {
                sheetBehavior.setState(BottomSheetBehavior.STATE_COLLAPSED);
            }
        });
    }

    @Override
    public void loadAddressDetails(ShippingAddress address) {
        editTextAddress.setText(address.address);
        editTextCity.setText(address.city);
        editTextEmail.setText(address.email);
        editTextName.setText(address.name);
        editTextPhone.setText(address.mobile);
        editTextPin.setText(address.pin);
        editTextState.setText(address.state);
        this.latitude = address.latitude;
        this.longitude = address.longitude;
        initialiseMap();
    }

    @OnClick(R.id.btn_view_add_address) void onAddAddressClicked() {
        if (    editTextCity.getText().toString().trim().isEmpty() ||
                editTextPin.getText().toString().trim().isEmpty() ||
                editTextState.getText().toString().trim().isEmpty() ||
                editTextAddress.getText().toString().trim().isEmpty() ||
                editTextName.getText().toString().trim().isEmpty() ||
                editTextPhone.getText().toString().trim().isEmpty()
        ) {
            Toast.makeText(this, "Some fields are empty", Toast.LENGTH_LONG).show();
        } else {
            mPresenter.updateAddress(addressId, editTextName.getText().toString(), editTextEmail.getText().toString(), editTextPhone.getText().toString(), editTextCity.getText().toString(), editTextState.getText().toString(), editTextPin.getText().toString(), editTextAddress.getText().toString(), latitude, longitude);
        }
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public void onAddressEditSuccess() {
        finish();
    }

    @Override
    public void showLoginBottomSheet() {
        Helper.showLoginBottomSheet(getSupportFragmentManager());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}