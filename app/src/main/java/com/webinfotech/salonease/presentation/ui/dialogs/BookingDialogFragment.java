package com.webinfotech.salonease.presentation.ui.dialogs;

import android.os.Bundle;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.webinfotech.salonease.R;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link BookingDialogFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class BookingDialogFragment extends DialogFragment {



    public BookingDialogFragment() {
        // Required empty public constructor
    }


    public static BookingDialogFragment newInstance(String param1, String param2) {
        BookingDialogFragment fragment = new BookingDialogFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_booking_dialog, container, false);
    }
}