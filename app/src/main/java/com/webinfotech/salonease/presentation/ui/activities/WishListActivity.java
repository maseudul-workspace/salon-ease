package com.webinfotech.salonease.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import com.webinfotech.salonease.R;
import com.webinfotech.salonease.domain.executors.impl.ThreadExecutor;
import com.webinfotech.salonease.domain.models.SalonUser;
import com.webinfotech.salonease.presentation.presenters.WishlistPresenter;
import com.webinfotech.salonease.presentation.presenters.impl.WishlistPresenterImpl;
import com.webinfotech.salonease.presentation.ui.adapters.SalonDataAdapter;
import com.webinfotech.salonease.presentation.ui.adapters.SalonListAdapter;
import com.webinfotech.salonease.presentation.ui.dialogs.CustomProgressDialog;
import com.webinfotech.salonease.threading.MainThreadImpl;

public class WishListActivity extends AppCompatActivity implements WishlistPresenter.View, SalonListAdapter.Callback {

    @BindView(R.id.recycler_view_wish_list)
    RecyclerView recyclerViewWishlist;
    WishlistPresenterImpl mPresenter;
    CustomProgressDialog customProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wish_list);
        ButterKnife.bind(this);
        initialisePresenter();
        setCustomProgressDialog();
    }

    private void initialisePresenter() {
        mPresenter = new WishlistPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    private void setCustomProgressDialog() {
        customProgressDialog = new CustomProgressDialog(this, this);
        customProgressDialog.setUpDialog();
    }

    @Override
    public void loadData(SalonUser[] salonUsers) {
        SalonListAdapter salonListAdapter = new SalonListAdapter(this, salonUsers, this);
        recyclerViewWishlist.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewWishlist.setAdapter(salonListAdapter);
    }

    @Override
    public void showLoader() {
        customProgressDialog.showDialog();
    }

    @Override
    public void hideLoader() {
        customProgressDialog.dismissDialog();
    }

    @Override
    public void onSalonClicked(int salonId) {
        Intent intent = new Intent(this, SalonDetailsActivity.class);
        intent.putExtra("salonId", salonId);
        startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.fetchWishlist();
    }

    @OnClick(R.id.btn_back) void onBackClicked()
    {
        finish();
    }
}