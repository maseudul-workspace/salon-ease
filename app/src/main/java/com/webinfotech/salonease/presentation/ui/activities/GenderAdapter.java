package com.webinfotech.salonease.presentation.ui.activities;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.webinfotech.salonease.R;
import com.webinfotech.salonease.domain.models.Category;
import com.webinfotech.salonease.domain.models.Subcategory;
import com.webinfotech.salonease.util.GlideHelper;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GenderAdapter extends RecyclerView.Adapter<GenderAdapter.ViewHolder> {

    public interface Callback {
        void onCategoryClicked(int categoryId);
    }

    Context mContext;
    Category[] categories;
    Callback mCallback;

    public GenderAdapter(Context mContext, Category[] categories, Callback mCallback) {
        this.mContext = mContext;
        this.categories = categories;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_gender_item, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewCategory.setText(categories[position].name);
        GlideHelper.setImageViewCustomRoundedCorners(mContext, holder.imgViewCategory, mContext.getResources().getString(R.string.base_url) + "admin/service_category/" + categories[position].image, 100);
        holder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onCategoryClicked(position);
            }
        });
    }

    @Override
    public int getItemCount() {
        return categories.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_category_name)
        TextView txtViewCategory;
        @BindView(R.id.img_view_category)
        ImageView imgViewCategory;
        @BindView(R.id.main_layout)
        View mainLayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
