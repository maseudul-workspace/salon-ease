package com.webinfotech.salonease.presentation.presenters;

import com.webinfotech.salonease.domain.models.SalonUser;

public interface WishlistPresenter {
    void fetchWishlist();
    interface View {
        void loadData(SalonUser[] salonUsers);
        void showLoader();
        void hideLoader();
    }
}
