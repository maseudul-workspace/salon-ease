package com.webinfotech.salonease.presentation.presenters.impl;

import android.content.Context;

import com.webinfotech.salonease.AndroidApplication;
import com.webinfotech.salonease.domain.executors.Executor;
import com.webinfotech.salonease.domain.executors.MainThread;
import com.webinfotech.salonease.domain.interactors.FetchCategoriesInteractor;
import com.webinfotech.salonease.domain.interactors.FetchSubcategoryInteractor;
import com.webinfotech.salonease.domain.interactors.FetchThirdCategoryInteractor;
import com.webinfotech.salonease.domain.interactors.SendEnquiryInteractor;
import com.webinfotech.salonease.domain.interactors.impl.FetchCategoriesInteractorImpl;
import com.webinfotech.salonease.domain.interactors.impl.FetchSubcategoryInteractorImpl;
import com.webinfotech.salonease.domain.interactors.impl.FetchThirdCategoryInteractorImpl;
import com.webinfotech.salonease.domain.interactors.impl.SendEnquiryInteractorImpl;
import com.webinfotech.salonease.domain.models.Category;
import com.webinfotech.salonease.domain.models.Subcategory;
import com.webinfotech.salonease.domain.models.ThirdCategory;
import com.webinfotech.salonease.domain.models.UserInfo;
import com.webinfotech.salonease.presentation.presenters.EnquiryActivityPresenter;
import com.webinfotech.salonease.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.salonease.repository.AppRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class EnquiryActivityPresenterImpl extends AbstractPresenter implements EnquiryActivityPresenter, FetchCategoriesInteractor.Callback, SendEnquiryInteractor.Callback, FetchSubcategoryInteractor.Callback, FetchThirdCategoryInteractor.Callback {

    Context mContext;
    View mView;

    public EnquiryActivityPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void onGettingCategoriesSuccess(Category[] categories) {
        mView.loadMaincategories(categories);
        mView.hideLoader();
    }

    @Override
    public void onGettingCategoriesFail() {
        Toasty.warning(mContext, "Please check your internet connection", Toasty.LENGTH_SHORT).show();
        mView.hideLoader();
    }

    @Override
    public void fetchCategories() {
        FetchCategoriesInteractorImpl fetchCategoriesInteractor = new FetchCategoriesInteractorImpl(mExecutor, mMainThread, this, new AppRepositoryImpl());
        fetchCategoriesInteractor.execute();
        mView.showLoader();
    }

    @Override
    public void fetchSubCategories(int categoryId) {
        FetchSubcategoryInteractorImpl fetchSubcategoryInteractor = new FetchSubcategoryInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, categoryId);
        fetchSubcategoryInteractor.execute();
        mView.showLoader();
    }

    @Override
    public void fetchThirdCategories(int subcategoryId) {
        FetchThirdCategoryInteractorImpl fetchThirdCategoryInteractor = new FetchThirdCategoryInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, subcategoryId);
        fetchThirdCategoryInteractor.execute();
        mView.showLoader();
    }

    @Override
    public void sendMessage(int categoryId, int subcategoryId, int thirdcategoryId, String name, String mobile, String message, String booking_date) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        SendEnquiryInteractorImpl sendEnquiryInteractorImpl = new SendEnquiryInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, categoryId, subcategoryId, thirdcategoryId, name, mobile, message, booking_date);
        sendEnquiryInteractorImpl.execute();
        mView.showLoader();
    }

    @Override
    public void onSendFeedbackSuccess() {
        mView.hideLoader();
        mView.showAnimationSuccess();
    }

    @Override
    public void onSendFeedbackFail(String errorMsg, int loginError) {
        mView.hideLoader();
        if(loginError == 1)
        {
            AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
            androidApplication.setUserInfo(mContext,null);
            mView.loginError();
        }
        else
        {
            Toasty.warning(mContext,errorMsg,Toasty.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onGettingSubcategorySuccess(Subcategory[] subcategories) {
        mView.loadSubCategories(subcategories);
        mView.hideLoader();
    }

    @Override
    public void ongGettingSubcategoryFail(String errorMsg) {
        Toasty.warning(mContext, "Please check your internet connection", Toasty.LENGTH_SHORT).show();
        mView.hideLoader();
    }

    @Override
    public void onGettingThirdCategorySuccess(ThirdCategory[] thirdCategories) {
        mView.loadThirdCategories(thirdCategories);
        mView.hideLoader();
    }

    @Override
    public void ongGettingThirdCategoryFail(String errorMsg) {
        Toasty.warning(mContext, "Please check your internet connection", Toasty.LENGTH_SHORT).show();
        mView.hideLoader();
    }
}
