package com.webinfotech.salonease.presentation.ui.adapters;

import android.content.Context;
import android.graphics.Paint;
import android.os.CountDownTimer;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.webinfotech.salonease.R;
import com.webinfotech.salonease.domain.models.Service;
import com.webinfotech.salonease.util.GlideHelper;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class ServiceSalonDetailsAdapter extends RecyclerView.Adapter<ServiceSalonDetailsAdapter.ViewHolder> {

    public interface Callback {
        void onServiceSelected(int position);
        void onServiceRemoved(int position);
    }

    Context mContext;
    Service[] services;
    Callback mCallback;

    public ServiceSalonDetailsAdapter(Context mContext, Service[] services, Callback mCallback) {
        this.mContext = mContext;
        this.services = services;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_services_salon_details, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewService.setText(services[position].mainCategory + ": " + services[position].lastCategory);
        holder.txtViewMrp.setText("Rs. " + services[position].mrp);
        holder.txtViewPrice.setText("Rs. " + services[position].price);
        holder.txtViewMrp.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
        GlideHelper.setImageViewCustomRoundedCorners(mContext, holder.imgViewService, mContext.getResources().getString(R.string.base_url) + "admin/service_category/sub_category/thumb/" + services[position].lastCategoryImage, 5);
        holder.txtViewAddNotSelected.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onServiceSelected(position);
            }
        });
        holder.txtViewAddSelected.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onServiceRemoved(position);
            }
        });
        if (services[position].isServiceSelected) {
            holder.txtViewAddSelected.setVisibility(View.VISIBLE);
            holder.txtViewAddNotSelected.setVisibility(View.GONE);
        } else {
            holder.txtViewAddNotSelected.setVisibility(View.VISIBLE);
            holder.txtViewAddSelected.setVisibility(View.GONE);
        }
        if (services[position].isDeal.equals("Y")) {
            holder.layoutSpecialPrice.setVisibility(View.VISIBLE);
            double price = services[position].price - services[position].price * services[position].discount / 100;
            holder.txtViewPrice.setText("Rs. " + price);
            try {
                SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
                long diff;
                long oldLong;
                long NewLong;
                Date oldDate, newDate;
                String oldTime = getCurrentDate();
                String NewTime = services[position].expiryDate;
                oldDate = formatter.parse(oldTime);
                newDate = formatter.parse(NewTime);
                oldLong = oldDate.getTime();
                NewLong = newDate.getTime();
                diff = NewLong - oldLong;

                CountDownTimer countDownTimer = new CountDownTimer(diff, 1000) {
                    @Override
                    public void onTick(long millisUntilFinished) {
                        long millis = millisUntilFinished;
                        String hms = (TimeUnit.MILLISECONDS.toDays(millis)) + "d "
                                + (TimeUnit.MILLISECONDS.toHours(millis) - TimeUnit.DAYS.toHours(TimeUnit.MILLISECONDS.toDays(millis))) + "h "
                                + (TimeUnit.MILLISECONDS.toMinutes(millis) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(millis))) + "m "
                                + (TimeUnit.MILLISECONDS.toSeconds(millis) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millis))) + "s";
                        holder.txtViewEndTime.setText(hms);
                    }
                    @Override
                    public void onFinish() {
                    }
                }.start();
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else {
            holder.layoutSpecialPrice.setVisibility(View.GONE);
        }

        if (services[position].status == 1) {
            holder.txtViewNotAvailable.setVisibility(View.GONE);
        } else {
            holder.txtViewNotAvailable.setVisibility(View.VISIBLE);
            holder.txtViewAddSelected.setVisibility(View.GONE);
            holder.txtViewAddNotSelected.setVisibility(View.GONE);
        }

    }

    @Override
    public int getItemCount() {
        return services.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_view_service)
        ImageView imgViewService;
        @BindView(R.id.txt_view_service)
        TextView txtViewService;
        @BindView(R.id.txt_view_mrp)
        TextView txtViewMrp;
        @BindView(R.id.txt_view_price)
        TextView txtViewPrice;
        @BindView(R.id.txt_view_add_selected)
        TextView txtViewAddSelected;
        @BindView(R.id.txt_view_add_not_selected)
        TextView txtViewAddNotSelected;
        @BindView(R.id.layout_special_price)
        View layoutSpecialPrice;
        @BindView(R.id.txt_view_end_time)
        TextView txtViewEndTime;
        @BindView(R.id.txt_view_not_available)
        TextView txtViewNotAvailable;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void updateDataSet(Service[] services) {
        this.services = services;
        notifyDataSetChanged();
    }

    private String getCurrentDate() {
        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        String formattedDate = df.format(c);
        return formattedDate;
    }

}
