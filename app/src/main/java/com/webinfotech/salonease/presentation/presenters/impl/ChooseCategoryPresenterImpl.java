package com.webinfotech.salonease.presentation.presenters.impl;

import android.content.Context;

import com.webinfotech.salonease.domain.executors.Executor;
import com.webinfotech.salonease.domain.executors.MainThread;
import com.webinfotech.salonease.domain.interactors.FetchCityListInteractor;
import com.webinfotech.salonease.domain.interactors.FetchSubcategoryInteractor;
import com.webinfotech.salonease.domain.interactors.impl.FetchCityListInteractorImpl;
import com.webinfotech.salonease.domain.interactors.impl.FetchSubcategoryInteractorImpl;
import com.webinfotech.salonease.domain.models.City;
import com.webinfotech.salonease.domain.models.Subcategory;
import com.webinfotech.salonease.presentation.presenters.ChooseCategoryPresenter;
import com.webinfotech.salonease.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.salonease.repository.AppRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class ChooseCategoryPresenterImpl extends AbstractPresenter implements ChooseCategoryPresenter, FetchSubcategoryInteractor.Callback, FetchCityListInteractor.Callback {

    Context mContext;
    ChooseCategoryPresenter.View mView;

    public ChooseCategoryPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchSubcategories(int categoryId) {
        FetchSubcategoryInteractorImpl  fetchSubcategoryInteractor = new FetchSubcategoryInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, categoryId);
        fetchSubcategoryInteractor.execute();
    }

    @Override
    public void fetchCities() {
        FetchCityListInteractorImpl fetchCityListInteractor = new FetchCityListInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this);
        fetchCityListInteractor.execute();
    }

    @Override
    public void onGettingCityListSuccess(City[] cities) {
        mView.loadCities(cities);
    }

    @Override
    public void onGettingCityListFail(String errorMsg) {
        Toasty.warning(mContext, errorMsg, Toasty.LENGTH_SHORT).show();
    }

    @Override
    public void onGettingSubcategorySuccess(Subcategory[] subcategories) {
        mView.loadSubcategories(subcategories);
    }

    @Override
    public void ongGettingSubcategoryFail(String errorMsg) {
        Toasty.warning(mContext, errorMsg, Toasty.LENGTH_SHORT).show();

    }
}
