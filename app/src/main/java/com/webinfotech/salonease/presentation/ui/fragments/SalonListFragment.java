package com.webinfotech.salonease.presentation.ui.fragments;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RadioGroup;

import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton;
import com.google.android.material.slider.RangeSlider;
import com.webinfotech.salonease.AndroidApplication;
import com.webinfotech.salonease.R;
import com.webinfotech.salonease.domain.executors.impl.ThreadExecutor;
import com.webinfotech.salonease.presentation.presenters.SalonListFragmentPresenter;
import com.webinfotech.salonease.presentation.presenters.impl.SalonListFragmentPresenterImpl;
import com.webinfotech.salonease.presentation.ui.activities.SalonDetailsActivity;
import com.webinfotech.salonease.presentation.ui.adapters.SalonDataAdapter;
import com.webinfotech.salonease.presentation.ui.dialogs.CustomProgressDialog;
import com.webinfotech.salonease.threading.MainThreadImpl;

import java.util.List;

public class SalonListFragment extends Fragment implements SalonListFragmentPresenter.View {

    private int categoryId;
    private int serviceCityId;
    private int clientType;
    @BindView(R.id.recycler_view_salon_list)
    RecyclerView recyclerViewViewSalonList;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    @BindView(R.id.btn_filter)
    ExtendedFloatingActionButton btnFilter;
    @BindView(R.id.range_slider)
    RangeSlider rangeSlider;
    @BindView(R.id.radio_group_sort)
    RadioGroup radioGroupSort;
    @BindView(R.id.check_box_ac)
    CheckBox checkBoxAc;
    @BindView(R.id.check_box_music)
    CheckBox checkBoxMusic;
    @BindView(R.id.check_box_parking)
    CheckBox checkBoxParking;
    @BindView(R.id.check_box_wifi)
    CheckBox checkBoxWifi;
    Context mContext;
    SalonListFragmentPresenterImpl mPresenter;
    Boolean isScrolling = false;
    Integer currentItems;
    Integer totalItems;
    Integer scrollOutItems;
    int pageNo = 1;
    int totalPage = 1;
    int sortBy = 1;
    int ac = 1;
    int parking = 1;
    int wifi = 1;
    int music = 1;
    String priceFrom = null;
    String priceTo = null;
    CustomProgressDialog customProgressDialog;


    public SalonListFragment() {
        // Required empty public constructor
    }

    public static SalonListFragment newInstance(String param1, String param2) {
        SalonListFragment fragment = new SalonListFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            categoryId = getArguments().getInt("categoryId");
            serviceCityId = getArguments().getInt("serviceCityId");
            clientType = getArguments().getInt("clientType");
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_salon_list, container, false);
        ButterKnife.bind(this, view);
        setUpCustomProgressDialog();
        setUpViews();
        rangeSlider.setValues(1.0f,5000.0f);
        btnFilter.shrink();
        fetchData();
        return view;
    }

    private void setUpCustomProgressDialog() {
        customProgressDialog = new CustomProgressDialog(mContext, getActivity());
        customProgressDialog.setUpDialog();
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        mContext = context;
        initialisePresenter();
    }

    private void initialisePresenter() {
        mPresenter = new SalonListFragmentPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), mContext, this);
    }

    private void setUpViews() {
        radioGroupSort.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.radio_btn_distance_low_to_high:
                        sortBy = 1;
                        break;
                    case R.id.radio_btn_distance_high_to_low:
                        sortBy = 2;
                        break;
                    case R.id.radio_btn_price_low_to_high:
                        sortBy = 3;
                        break;
                    case R.id.radio_btn_price_high_to_low:
                        sortBy = 4;
                        break;
                }
            }
        });

        checkBoxAc.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    ac = 2;
                } else {
                    ac = 1;
                }
            }
        });

        checkBoxMusic.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    music = 2;
                } else {
                    music = 1;
                }
            }
        });

        checkBoxParking.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    parking = 2;
                } else {
                    parking = 1;
                }
            }
        });

        checkBoxWifi.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    wifi = 2;
                } else {
                    wifi = 1;
                }
            }
        });



    }

    private void fetchData() {
        AndroidApplication androidApplication = (AndroidApplication) getContext().getApplicationContext();

        mPresenter.fetchSalon(serviceCityId, categoryId, pageNo, 3, androidApplication.getLatitude(), androidApplication.getLongitude(), clientType, priceFrom, priceTo, ac, parking, wifi, music, sortBy, "refresh");
    }

    @Override
    public void loadData(SalonDataAdapter adapter, int totalPage) {
        this.totalPage = totalPage;
        LinearLayoutManager layoutManager = new LinearLayoutManager(mContext);
        recyclerViewViewSalonList.setAdapter(adapter);
        recyclerViewViewSalonList.setLayoutManager(layoutManager);
        recyclerViewViewSalonList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if(newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL)
                {
                    isScrolling= true;
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                currentItems = layoutManager.getChildCount();
                totalItems  = layoutManager.getItemCount();
                scrollOutItems = layoutManager.findFirstCompletelyVisibleItemPosition();
                if(!recyclerView.canScrollVertically(1))
                {
                    if(pageNo < totalPage) {
                        isScrolling = false;
                        pageNo = pageNo + 1;
                    }
                }
            }
        });
    }

    @Override
    public void hideRecyclerView() {

    }

    @Override
    public void hideProgressDialog() {
        customProgressDialog.dismissDialog();
    }

    @Override
    public void goToSalonDetails(int salonId, int serviceId) {
        Intent intent = new Intent(getContext(), SalonDetailsActivity.class);
        intent.putExtra("serviceId", serviceId);
        intent.putExtra("salonId", salonId);
        startActivity(intent);
    }

    @OnClick(R.id.btn_filter) void onNavigationClicked() {
        if (btnFilter.isExtended()) {
            btnFilter.shrink();
            drawerLayout.openDrawer(Gravity.LEFT);
        } else {
            btnFilter.extend();
        }
    }

    @OnClick(R.id.txt_view_done) void onDoneClicked() {
        isScrolling = false;
        pageNo = 1;
        totalPage = 1;
        drawerLayout.closeDrawer(Gravity.LEFT);
        List<Float> priceValues = rangeSlider.getValues();
        priceFrom = priceValues.get(0).toString();
        priceTo = priceValues.get(1).toString();
        fetchData();
        customProgressDialog.showDialog();
    }

}