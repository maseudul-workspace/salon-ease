package com.webinfotech.salonease.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;

import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.card.MaterialCardView;
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton;
import com.google.android.material.switchmaterial.SwitchMaterial;
import com.google.android.material.tabs.TabLayout;
import com.google.gson.Gson;
import com.tbuonomo.viewpagerdotsindicator.DotsIndicator;
import com.webinfotech.salonease.AndroidApplication;
import com.webinfotech.salonease.R;
import com.webinfotech.salonease.domain.executors.impl.ThreadExecutor;
import com.webinfotech.salonease.domain.models.ComboServiceData;
import com.webinfotech.salonease.domain.models.CombosCart;
import com.webinfotech.salonease.domain.models.OfferSalons;
import com.webinfotech.salonease.domain.models.Offers;
import com.webinfotech.salonease.domain.models.OffersData;
import com.webinfotech.salonease.domain.models.SalonData;
import com.webinfotech.salonease.domain.models.SalonImages;
import com.webinfotech.salonease.domain.models.SalonUser;
import com.webinfotech.salonease.domain.models.Service;
import com.webinfotech.salonease.domain.models.ServiceCart;
import com.webinfotech.salonease.domain.models.UserInfo;
import com.webinfotech.salonease.domain.models.Wishlist;
import com.webinfotech.salonease.domain.models.testing.BookingServices;
import com.webinfotech.salonease.presentation.presenters.SalonDetailsPresenter;
import com.webinfotech.salonease.presentation.presenters.impl.SalonDetailsPresenterImpl;
import com.webinfotech.salonease.presentation.ui.adapters.BookingAddressAdapter;
import com.webinfotech.salonease.presentation.ui.adapters.FragmentsAdapter;
import com.webinfotech.salonease.presentation.ui.adapters.ImageGalleryAdapter;
import com.webinfotech.salonease.presentation.ui.adapters.ReviewsAdapter;
import com.webinfotech.salonease.presentation.ui.adapters.SalonDetailsViewpagerAdapter;
import com.webinfotech.salonease.presentation.ui.adapters.ServiceSalonDetailsAdapter;
import com.webinfotech.salonease.presentation.ui.adapters.ServicesAdapter;
import com.webinfotech.salonease.presentation.ui.dialogs.CustomProgressDialog;
import com.webinfotech.salonease.presentation.ui.fragments.BookingDateFragment;
import com.webinfotech.salonease.presentation.ui.fragments.CartFragment;
import com.webinfotech.salonease.presentation.ui.fragments.GalleryListFragment;
import com.webinfotech.salonease.presentation.ui.fragments.ReviewsFragment;
import com.webinfotech.salonease.presentation.ui.fragments.SalonDescriptionFragment;
import com.webinfotech.salonease.presentation.ui.fragments.ServiceFragment;
import com.webinfotech.salonease.presentation.ui.fragments.ServiceListFragment;
import com.webinfotech.salonease.threading.MainThreadImpl;
import com.webinfotech.salonease.util.GlideHelper;
import com.webinfotech.salonease.util.Helper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class SalonDetailsActivity extends AppCompatActivity implements  SalonDetailsPresenter.View,
                                                                        OnMapReadyCallback,
                                                                        ServiceSalonDetailsAdapter.Callback,
                                                                        ServiceFragment.Callback,
                                                                        CartFragment.Callback
{

    @BindView(R.id.img_view_header)
    ImageView imgViewHeader;
    @BindView(R.id.txt_view_address)
    TextView txtViewAddress;
    @BindView(R.id.rating_bar_salon)
    RatingBar ratingBarSalon;
    @BindView(R.id.img_view_ac_on)
    ImageView imgViewAcOn;
    @BindView(R.id.img_view_ac_of)
    ImageView imgViewAcOf;
    @BindView(R.id.txt_view_ac)
    TextView txtViewAc;
    @BindView(R.id.img_view_wifi_on)
    ImageView imgViewWifiOn;
    @BindView(R.id.img_view_wifi_of)
    ImageView imgViewWifiOf;
    @BindView(R.id.txt_view_wifi)
    TextView txtViewWifi;
    @BindView(R.id.img_view_music_of)
    ImageView imgViewMusicOf;
    @BindView(R.id.img_view_music_on)
    ImageView imgViewMusicOn;
    @BindView(R.id.txt_view_music)
    TextView txtViewMusic;
    @BindView(R.id.img_view_parking_of)
    ImageView imgViewParkingOf;
    @BindView(R.id.img_view_parking_on)
    ImageView imgViewParkingOn;
    @BindView(R.id.txt_view_parking)
    TextView txtViewParking;
    @BindView(R.id.features_layout)
    View featuresLayout;
    @BindView(R.id.view_pager)
    ViewPager viewPager;
    @BindView(R.id.tab_layout)
    TabLayout tabLayout;
    @BindView(R.id.img_view_heart_stroke)
    ImageView imgViewHeartStroke;
    @BindView(R.id.img_view_heart_fill)
    ImageView imgViewHeartFill;
    double latitude;
    double longitude;
    SalonDetailsPresenterImpl mPresenter;
    Service[] services;
    ServicesAdapter servicesAdapter;
    @BindView(R.id.txt_view_salon_name)
    TextView txtViewSalonName;
    RecyclerView recyclerViewShippingAddress;
    BottomSheetDialog addressBottomSheetDialog;
    TextView txtViewAddAddress;
    ImageView imgViewAddAddress;
    String bookingTime = null;
    MaterialCardView cardViewFirstDate;
    MaterialCardView cardViewSecondDate;
    MaterialCardView cardViewThirdDate;
    TextView txtViewFirstDate;
    TextView txtViewSecondDate;
    TextView txtViewThirdDate;
    int serviceFor = 0;
    RadioGroup radioGroupServiceFor;
    View viewAddAddress;
    int price = 0;
    int serviceId = 0;
    int addressId = 0;
    int vendorId;
    ArrayList<BookingServices> bookingServices = new ArrayList<>();
    SalonUser salonUser;
    CustomProgressDialog customProgressDialog;
    ArrayList<Integer> serviceIds = new ArrayList<>();
    ArrayList<Integer> comboIds = new ArrayList<>();
    @BindView(R.id.btn_book_now)
    Button btnBookNow;
    ArrayList<ServiceCart> serviceCarts = new ArrayList<>();
    ArrayList<CombosCart> combosCarts = new ArrayList<>();
    ComboServiceData[] comboServiceData;
    BookingDateFragment bookingDateFragment = new BookingDateFragment();
    Offers[] offers;
    int salonId;
    int wishListId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_salon_details);
        ButterKnife.bind(this);
        AndroidApplication androidApplication = (AndroidApplication) getApplicationContext();
        if (androidApplication.getOffersData() != null) {
            offers = androidApplication.getOffersData().offers;
        }
        serviceId = getIntent().getIntExtra("serviceId", 0);
        salonId = getIntent().getIntExtra("salonId", 0);
        if (serviceId != 0) {
            serviceIds.add(serviceId);
        }
        initialisePresenter();
        setUpProgressDialog();
        setAddressBottomSheetDialog();
        mPresenter.fetchProductDetails(salonId);
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
        int deviceHeight = displayMetrics.heightPixels;
        viewPager.getLayoutParams().height = deviceHeight - 150;
    }

    private void initialisePresenter() {
        mPresenter = new SalonDetailsPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    private void setAddressBottomSheetDialog() {
        if (addressBottomSheetDialog == null) {
            View view = LayoutInflater.from(this).inflate(R.layout.layout_booking_bottom_sheet, null);
            addressBottomSheetDialog = new BottomSheetDialog(this);
            txtViewAddAddress = (TextView) view.findViewById(R.id.txt_view_add_address);
            imgViewAddAddress = (ImageView) view.findViewById(R.id.img_view_add_address);
            recyclerViewShippingAddress = (RecyclerView) view.findViewById(R.id.recycler_view_address);
            cardViewFirstDate = (MaterialCardView) view.findViewById(R.id.card_view_first_date);
            cardViewSecondDate = (MaterialCardView) view.findViewById(R.id.card_view_second_date);
            cardViewThirdDate = (MaterialCardView) view.findViewById(R.id.card_view_third_date);
            txtViewFirstDate = (TextView) view.findViewById(R.id.txt_view_first_date);
            txtViewSecondDate = (TextView) view.findViewById(R.id.txt_view_second_date);
            txtViewThirdDate = (TextView) view.findViewById(R.id.txt_view_third_date);
            viewAddAddress = (View) view.findViewById(R.id.view_add_adddress);
            txtViewFirstDate.setText(getDateAhead(1));
            txtViewSecondDate.setText(getDateAhead(2));
            txtViewThirdDate.setText(getDateAhead(3));
            cardViewFirstDate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    cardViewFirstDate.setCardBackgroundColor(getResources().getColor(R.color.colorAccent));
                    cardViewSecondDate.setCardBackgroundColor(getResources().getColor(R.color.white));
                    cardViewThirdDate.setCardBackgroundColor(getResources().getColor(R.color.white));
                    txtViewFirstDate.setTextColor(getResources().getColor(R.color.white));
                    txtViewSecondDate.setTextColor(getResources().getColor(R.color.md_black_1000));
                    txtViewThirdDate.setTextColor(getResources().getColor(R.color.md_black_1000));
                    bookingTime = getDateAheadFormat(1);
                }
            });
            cardViewSecondDate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    cardViewSecondDate.setCardBackgroundColor(getResources().getColor(R.color.colorAccent));
                    cardViewFirstDate.setCardBackgroundColor(getResources().getColor(R.color.white));
                    cardViewThirdDate.setCardBackgroundColor(getResources().getColor(R.color.white));
                    txtViewFirstDate.setTextColor(getResources().getColor(R.color.md_black_1000));
                    txtViewSecondDate.setTextColor(getResources().getColor(R.color.white));
                    txtViewThirdDate.setTextColor(getResources().getColor(R.color.md_black_1000));
                    bookingTime = getDateAheadFormat(2);
                }
            });
            cardViewThirdDate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    cardViewThirdDate.setCardBackgroundColor(getResources().getColor(R.color.colorAccent));
                    cardViewSecondDate.setCardBackgroundColor(getResources().getColor(R.color.white));
                    cardViewFirstDate.setCardBackgroundColor(getResources().getColor(R.color.white));
                    txtViewFirstDate.setTextColor(getResources().getColor(R.color.md_black_1000));
                    txtViewSecondDate.setTextColor(getResources().getColor(R.color.md_black_1000));
                    txtViewThirdDate.setTextColor(getResources().getColor(R.color.white));
                    bookingTime = getDateAheadFormat(3);
                }
            });
            txtViewAddAddress.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    Intent intent = new Intent(getApplicationContext(), AddAddressActivity.class);
//                    startActivity(intent);
                }
            });

            viewAddAddress.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getApplicationContext(), AddAddressActivity.class);
                    startActivity(intent);
                }
            });

            addressBottomSheetDialog.setContentView(view);
        }
    }

    @Override
    public void loadSalonData(SalonUser salonUser) {
        this.salonUser = salonUser;
        ratingBarSalon.setRating(Math.round(salonUser.rating));
        vendorId =salonUser.id;
        SalonImages[] images = salonUser.images;
        GlideHelper.setImageView(this, imgViewHeader, getResources().getString(R.string.base_url) + "images/client/" + salonUser.image);
        txtViewAddress.setText(salonUser.address);
        txtViewSalonName.setText(salonUser.name);

        latitude = salonUser.latitude;
        longitude = salonUser.longitude;
        services = salonUser.services;
        comboServiceData = salonUser.comboServiceData;
//        ratingBarSalon.setRating(salonUser.rating);

        ReviewsAdapter reviewsAdapter = new ReviewsAdapter(this, salonUser.reviews);

        if (salonUser.clientType == 2) {
            if (salonUser.ac == 2) {
                imgViewAcOn.setVisibility(View.VISIBLE);
                txtViewAc.setTextColor(getResources().getColor(R.color.white));
            } else {
                imgViewAcOf.setVisibility(View.VISIBLE);
                txtViewAc.setTextColor(getResources().getColor(R.color.md_grey_600));
            }
            if (salonUser.parking == 2) {
                imgViewParkingOn.setVisibility(View.VISIBLE);
                txtViewParking.setTextColor(getResources().getColor(R.color.white));
            } else {
                imgViewParkingOf.setVisibility(View.VISIBLE);
                txtViewParking.setTextColor(getResources().getColor(R.color.md_grey_600));
            }
            if (salonUser.wifi == 2) {
                imgViewWifiOn.setVisibility(View.VISIBLE);
                txtViewWifi.setTextColor(getResources().getColor(R.color.white));
            } else {
                imgViewWifiOf.setVisibility(View.VISIBLE);
                txtViewWifi.setTextColor(getResources().getColor(R.color.md_grey_600));
            }
            if (salonUser.music == 2) {
                imgViewMusicOn.setVisibility(View.VISIBLE);
                txtViewMusic.setTextColor(getResources().getColor(R.color.white));
            } else {
                imgViewMusicOf.setVisibility(View.VISIBLE);
                txtViewMusic.setTextColor(getResources().getColor(R.color.md_grey_600));
            }
        } else {
            featuresLayout.setVisibility(View.GONE);
        }

        for (Service service : services) {
            if (service.id == serviceId) {
                service.isServiceSelected = true;
                btnBookNow.setVisibility(View.VISIBLE);
                break;
            }
        }

        SalonDescriptionFragment salonDescriptionFragment = new SalonDescriptionFragment();
        salonDescriptionFragment.setTxtViewDescription(salonUser.description, salonUser.address, salonUser.latitude, salonUser.longitude);

        ServiceFragment serviceListFragment = new ServiceFragment();
        serviceListFragment.setData(services, salonUser.comboServiceData, salonUser.deals);

        GalleryListFragment galleryListFragment = new GalleryListFragment();
        galleryListFragment.setRecyclerViewGallery(salonUser.images);

        ReviewsFragment reviewsFragment = new ReviewsFragment();
        reviewsFragment.setRecyclerViewReviews(salonUser.id, salonUser.reviews, salonUser.rating);

        FragmentsAdapter fragmentsAdapter = new FragmentsAdapter(getSupportFragmentManager());
        fragmentsAdapter.AddFragment(serviceListFragment, "Services");
        fragmentsAdapter.AddFragment(salonDescriptionFragment, "About");
        fragmentsAdapter.AddFragment(galleryListFragment, "Gallery");
        fragmentsAdapter.AddFragment(reviewsFragment, "Reviews");

        bookingDateFragment.setData(salonUser.openingTime, salonUser.closing_time);
        viewPager.setAdapter(fragmentsAdapter);
        tabLayout.setupWithViewPager(viewPager);

        mPresenter.fetchWishlist();

    }

    private void setUpProgressDialog() {
        customProgressDialog = new CustomProgressDialog(this, this);
        customProgressDialog.setUpDialog();
    }

    public String changeTimeFormat(String time) {
        String inputPattern = "HH:mm:ss";
        String outputPattern = "h:mm a";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    @Override
    public void showLoader() {
        customProgressDialog.showDialog();
    }

    @Override
    public void hideLoader() {
        customProgressDialog.dismissDialog();
    }

    @Override
    public void loadAddress(BookingAddressAdapter bookingAddressAdapter) {
        recyclerViewShippingAddress.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewShippingAddress.setAdapter(bookingAddressAdapter);
        recyclerViewShippingAddress.addItemDecoration(new DividerItemDecoration(this, LinearLayout.VERTICAL));
    }

    @Override
    public void onBookNowClicked(int addressId) {
        this.addressId = addressId;
        if (bookingTime == null) {
            Toast.makeText(this, "Please Choose A Day", Toast.LENGTH_SHORT).show();
        } else {
            showTimePicker();
        }
    }

    @Override
    public void loadWishlists(Wishlist[] wishlists) {
        for (Wishlist wishlist : wishlists) {
            if (wishlist.salonUser.id == vendorId) {
                imgViewHeartFill.setVisibility(View.VISIBLE);
                imgViewHeartStroke.setVisibility(View.GONE);
                this.wishListId = wishlist.id;
                break;
            }
        }
    }

    @Override
    public void onAddToWishlistSuccess() {
        imgViewHeartFill.setVisibility(View.VISIBLE);
        imgViewHeartStroke.setVisibility(View.GONE);
    }

    @Override
    public void onRemoveFromWishlistSuccess() {
        imgViewHeartFill.setVisibility(View.GONE);
        imgViewHeartStroke.setVisibility(View.VISIBLE);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        LatLng latLng = new LatLng(latitude, longitude);
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.title("Current Position");
        markerOptions.draggable(true);
        googleMap.addMarker(markerOptions);
        //move map camera
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        googleMap.animateCamera(CameraUpdateFactory.zoomTo(18));
    }


    private void showTimePicker() {
        final Calendar c = Calendar.getInstance();
        int mHour = c.get(Calendar.HOUR_OF_DAY);
        int mMinute = c.get(Calendar.MINUTE);

        // Launch Time Picker Dialog
        final TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {

                        Calendar calendar = Calendar.getInstance();
                        calendar.set(Calendar.HOUR_OF_DAY,hourOfDay);
                        calendar.set(Calendar.MINUTE,minute);
                        String finalTime = bookingTime + " " + new SimpleDateFormat("HH:mm:ss").format(calendar.getTime());
                        double payableAmount = price * 0.2;
                        Intent intent = new Intent(getApplicationContext(), PaymentActivity.class);
                        Bundle args = new Bundle();
                        intent.putExtra("serviceObject", new Gson().toJson(bookingServices) );
                        intent.putExtra("serviceTime", finalTime);
                        intent.putExtra("addressId", addressId);
                        intent.putExtra("vendorId", vendorId);
                        intent.putExtra("payableAmount", payableAmount);
                        intent.putExtra("BUNDLE",args);
                        startActivity(intent);
                    }
                }, mHour, mMinute, false);
        timePickerDialog.show();
    }

    private void showAddressBottomSheet() {
        addressBottomSheetDialog.show();
    }

    private String getDateAhead(int days) {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DATE, days);
        return new SimpleDateFormat("EEEE").format(c.getTime());
    }

    private String getDateAheadFormat(int days) {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DATE, days);
        return new SimpleDateFormat("yyyy-MM-dd").format(c.getTime());
    }

    private boolean isLoggedIn() {
        AndroidApplication androidApplication = (AndroidApplication) getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(this);
        if (userInfo != null) {
//            Log.e("LogMsg", "User Id: " + userInfo.userId);
//            Log.e("LogMsg", "Api token: " + userInfo.apiToken);
            return true;
        } else {
            return false;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.fetchShippingAddressList();
    }

    @OnClick(R.id.img_view_back) void onBackClicked() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onServiceSelected(int serviceId) {
        serviceIds.add(serviceId);
        btnBookNow.setVisibility(View.VISIBLE);
    }

    @Override
    public void onServiceRemoved(int serviceId) {
        for (int i = 0; i < serviceIds.size(); i++) {
            if (serviceIds.get(i) == serviceId) {
                serviceIds.remove(i);
                break;
            }
        }
        if (serviceIds.size() == 0 && comboIds.size() == 0) {
            btnBookNow.setVisibility(View.GONE);
        }
    }

    @Override
    public void onComboSelected(int comboId) {
        comboIds.add(comboId);
        btnBookNow.setVisibility(View.VISIBLE);
    }

    @Override
    public void onComboRemoved(int comboId) {
        for (int i = 0; i < comboIds.size(); i++) {
            if (comboIds.get(i) == comboId) {
                comboIds.remove(i);
                break;
            }
        }
        if (serviceIds.size() == 0 && comboIds.size() == 0) {
            btnBookNow.setVisibility(View.GONE);
        }
    }

    @OnClick(R.id.btn_book_now) void onBookNowClicked() {
        if (isLoggedIn()) {
            serviceCarts = new ArrayList<>();
            combosCarts = new ArrayList<>();
            for (int i = 0; i < serviceIds.size(); i++) {
                for (Service service : services) {
                    if (serviceIds.get(i) == service.id) {
                        double price;
                        if (service.isDeal.equals("Y")) {
                            price = service.price - service.price * service.discount / 100;
                        } else {
                            price = service.price;
                        }
                        serviceCarts.add(new ServiceCart(service.id, service.lastCategory, price, service.lastCategoryImage, 1, getOfferId(service.lastCategoryId), offerCheck(service.lastCategoryId), false, service.isDeal));
                        break;
                    }
                }
            }
            for (int i = 0; i < comboIds.size(); i++) {
                for (ComboServiceData comboServiceDatum : comboServiceData) {
                    if (comboIds.get(i) == comboServiceDatum.id) {
                        combosCarts.add(new CombosCart(comboServiceDatum.id, comboServiceDatum.comboName, comboServiceDatum.price, 1, comboServiceDatum.comboServices));
                        break;
                    }
                }
            }
            CartFragment cartFragment = new CartFragment();
            cartFragment.setData(serviceCarts, combosCarts, salonUser.id, salonUser.openingTime, salonUser.closing_time, salonUser.clientType);
            cartFragment.show(getSupportFragmentManager(), "Cart Fragment");
        } else {
            Helper.showLoginBottomSheet(getSupportFragmentManager());
        }
    }

    private double offerCheck(int categoryId) {
        if (offers == null) {
            return 0;
        } else {
            double price = 0;
            for (Offers offer : offers) {
                if (offer.thirdCategoryId == categoryId) {
                    OfferSalons[] offerSalons = offer.offerSalons;
                    for (OfferSalons offerSalon : offerSalons) {
                        if (offerSalon.client_id == salonUser.id) {
                            price = offer.price;
                            break;
                        }
                    }
                    break;
                }
            }
            return price;
        }
    }

    private int getOfferId(int categoryId) {
        if (offers == null) {
            return 0;
        } else {
            int offerId = 0;
            for (Offers offer : offers) {
                if (offer.thirdCategoryId == categoryId) {
                    OfferSalons[] offerSalons = offer.offerSalons;
                    for (OfferSalons offerSalon : offerSalons) {
                        if (offerSalon.client_id == salonUser.id) {
                            offerId = offer.id;
                            break;
                        }
                    }
                    break;
                }
            }
            return offerId;
        }
    }

    @Override
    public void onBookingConfirm(ArrayList<ServiceCart> serviceCarts, ArrayList<CombosCart> combosCarts, double payableAmount, int couponId, String bookingTime, int addressId) {
        Intent intent = new Intent(this, PaymentActivity.class);
        intent.putExtra("services", new Gson().toJson(serviceCarts));
        intent.putExtra("combos", new Gson().toJson(combosCarts));
        intent.putExtra("couponId", couponId);
        intent.putExtra("serviceTime", bookingTime);
        intent.putExtra("payableAmount", payableAmount);
        intent.putExtra("vendorId", vendorId);
        intent.putExtra("addressId", addressId);
        startActivity(intent);
    }

    @OnClick(R.id.img_view_heart_stroke) void onHeartStrokeClicked() {
        mPresenter.addToWishlist(vendorId);
    }

    @OnClick(R.id.img_view_heart_fill) void onHeartFillClicked() {
        mPresenter.removeFromWishlist(wishListId);
    }

}