package com.webinfotech.salonease.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;

import com.airbnb.lottie.LottieAnimationView;
import com.webinfotech.salonease.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ContactUsActivity extends AppCompatActivity {

    LottieAnimationView lottieAnimationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contact_us);
        ButterKnife.bind(this);
        lottieAnimationView = findViewById(R.id.lottie_contact_us);
        lottieAnimationView.playAnimation();
    }

    @OnClick(R.id.material_card_view_call) void onCallClicked()
    {
        Intent intent = new Intent(Intent.ACTION_DIAL);
        intent.setData(Uri.parse("tel:+918638155802"));
        startActivity(intent);
    }

    @OnClick(R.id.material_card_view_text) void onTextClicked()
    {
        String url = "https://api.whatsapp.com/send?phone="+"+918638155802";
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(url));
        startActivity(i);
    }

    @OnClick(R.id.material_card_view_email) void onEmailClicked()
    {
        Intent email= new Intent(Intent.ACTION_SENDTO);
        email.setData(Uri.parse("mailto:salonease234@gmail.com"));
        email.putExtra(Intent.EXTRA_SUBJECT, "Subject");
        email.putExtra(Intent.EXTRA_TEXT, "My Email message");
        startActivity(email);
    }

    @OnClick(R.id.btn_back) void onBackClicked()
    {
        finish();
    }

}