package com.webinfotech.salonease.presentation.presenters.impl;

import android.content.Context;

import com.webinfotech.salonease.domain.executors.Executor;
import com.webinfotech.salonease.domain.executors.MainThread;
import com.webinfotech.salonease.domain.interactors.FetchTopFreelancerInteractor;
import com.webinfotech.salonease.domain.interactors.impl.FetchTopFreelancerInteractorImpl;
import com.webinfotech.salonease.domain.models.SalonUser;
import com.webinfotech.salonease.presentation.presenters.TopFreelancerPresenter;
import com.webinfotech.salonease.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.salonease.repository.AppRepositoryImpl;

public class TopFreeLancerPresenterImpl extends AbstractPresenter implements TopFreelancerPresenter, FetchTopFreelancerInteractor.Callback {

    Context mContext;
    TopFreelancerPresenter.View mView;

    public TopFreeLancerPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchTopFreelancers(double latitude, double longitude) {
        FetchTopFreelancerInteractorImpl fetchTopFreelancerInteractor = new FetchTopFreelancerInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, latitude, longitude);
        fetchTopFreelancerInteractor.execute();
        mView.showLoader();
    }

    @Override
    public void onFetchTopFreelancerSuccess(SalonUser[] salonUsers) {
        mView.loadDate(salonUsers);
        mView.hideLoader();
    }

    @Override
    public void onFetchTopFreelancerFail(String errorMsg) {
        mView.hideLoader();
    }
}
