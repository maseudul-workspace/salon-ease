package com.webinfotech.salonease.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.method.PasswordTransformationMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.webinfotech.salonease.R;
import com.webinfotech.salonease.domain.executors.impl.ThreadExecutor;
import com.webinfotech.salonease.presentation.presenters.LoginPresenter;
import com.webinfotech.salonease.presentation.presenters.impl.LoginPresenterImpl;
import com.webinfotech.salonease.presentation.ui.dialogs.CustomProgressDialog;
import com.webinfotech.salonease.threading.MainThreadImpl;
import com.webinfotech.salonease.util.GlideHelper;

public class LoginActivity extends AppCompatActivity implements LoginPresenter.View {

    @BindView(R.id.img_view_login_1)
    ImageView imgViewLogin1;
    @BindView(R.id.edit_text_phone)
    EditText editTextPhone;
    @BindView(R.id.edit_text_password)
    EditText editTextPassword;
    @BindView(R.id.txt_view_password_show)
    ImageView txtViewPasswordShow;
    @BindView(R.id.txt_view_password_hide)
    ImageView txtViewPasswordHide;
    CustomProgressDialog progressDialog;
    LoginPresenterImpl mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
        initialisePresenter();
        setUpProgressDialog();
        GlideHelper.setImageView(this, imgViewLogin1, "https://image.freepik.com/free-photo/handsome-man-cutting-beard-barber-shop-salon_1303-20934.jpg");
    }

    private void initialisePresenter() {
        mPresenter = new LoginPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    @OnClick(R.id.btn_login) void onLoginClicked() {
        if (editTextPhone.getText().toString().trim().isEmpty()) {
            Toast.makeText(this, "Please enter phone no", Toast.LENGTH_LONG).show();
        } else if (editTextPassword.getText().toString().trim().isEmpty()) {
            Toast.makeText(this, "Please enter password", Toast.LENGTH_LONG).show();
        } else {
            mPresenter.checkLogin(  editTextPhone.getText().toString(),
                                    editTextPassword.getText().toString()
            );
            showLoader();
        }
    }

    public void setUpProgressDialog() {
        progressDialog = new CustomProgressDialog(this, this);
        progressDialog.setUpDialog();
    }

    @OnClick(R.id.layout_sign_up) void onSignUpClicked() {
        Intent intent = new Intent(this, RegisterActivity.class);
        startActivity(intent);
    }

    @Override
    public void showLoader() {
        progressDialog.showDialog();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismissDialog();
    }

    @Override
    public void goToMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.txt_view_forget_password) void onForgetPasswordClicked() {
        Intent intent = new Intent(this, ForgetPasswordActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.txt_view_password_show) void onPasswordShowClicked() {
        editTextPassword.setTransformationMethod(null);
        txtViewPasswordShow.setVisibility(View.GONE);
        txtViewPasswordHide.setVisibility(View.VISIBLE);
    }

    @OnClick(R.id.txt_view_password_hide) void onPasswordHideClicked() {
        editTextPassword.setTransformationMethod(new PasswordTransformationMethod());
        txtViewPasswordShow.setVisibility(View.VISIBLE);
        txtViewPasswordHide.setVisibility(View.GONE);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

}