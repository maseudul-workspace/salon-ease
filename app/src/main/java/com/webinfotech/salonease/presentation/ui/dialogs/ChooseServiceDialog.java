package com.webinfotech.salonease.presentation.ui.dialogs;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.CheckBox;

import com.webinfotech.salonease.R;

import androidx.appcompat.app.AlertDialog;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class ChooseServiceDialog {

    public interface Callback {
        void onServiceSelected(String serviceType);
    }

    View dialogContainer;
    AlertDialog.Builder builder;
    AlertDialog dialog;
    Context mContext;
    Activity mActivity;
    Callback mCallback;
    @BindView(R.id.check_box_home_service)
    CheckBox checkBoxHomeService;
    @BindView(R.id.check_box_salon)
    CheckBox checkBoxSalon;

    public ChooseServiceDialog(Context mContext, Activity mActivity, Callback mCallback) {
        this.mContext = mContext;
        this.mActivity = mActivity;
        this.mCallback = mCallback;
    }

    public void setUpDialog() {
        dialogContainer = mActivity.getLayoutInflater().inflate(R.layout.choose_service_type_dialog, null);
        builder = new AlertDialog.Builder(mContext, android.R.style.Theme_Material_Light_Dialog_NoActionBar_MinWidth);
        builder.setView(dialogContainer);
        dialog = builder.create();
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        ButterKnife.bind(this, dialogContainer);
    }

    @OnClick(R.id.btn_submit) void onSubmitClicked() {
        if (checkBoxSalon.isChecked() && checkBoxHomeService.isChecked()) {
            mCallback.onServiceSelected(null);
        } else if (checkBoxSalon.isChecked()) {
            mCallback.onServiceSelected("2");
        } else {
            mCallback.onServiceSelected("1");
        }
    }

    public void showDialog() {
        dialog.show();
    }

    public void hideDialog() {
        dialog.dismiss();
    }

}
