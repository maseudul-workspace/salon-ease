package com.webinfotech.salonease.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import com.google.android.material.snackbar.Snackbar;
import com.webinfotech.salonease.R;
import com.webinfotech.salonease.domain.executors.impl.ThreadExecutor;
import com.webinfotech.salonease.presentation.presenters.OrdersListPresenter;
import com.webinfotech.salonease.presentation.presenters.impl.OrdersListPresenterImpl;
import com.webinfotech.salonease.presentation.ui.adapters.OrdersAdapter;
import com.webinfotech.salonease.presentation.ui.dialogs.AddReviewDialog;
import com.webinfotech.salonease.presentation.ui.dialogs.CustomProgressDialog;
import com.webinfotech.salonease.threading.MainThreadImpl;
import com.webinfotech.salonease.util.Helper;

public class OrderHistoryActivity extends AppCompatActivity implements OrdersListPresenter.View , AddReviewDialog.Callback {

    OrdersListPresenterImpl mPresenter;
    @BindView(R.id.recycler_view_orders)
    RecyclerView recyclerViewOrders;
    @BindView(R.id.main_layout)
    View mainLayout;
    CustomProgressDialog progressDialog;
    AddReviewDialog addReviewDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_history);
        ButterKnife.bind(this);
        setUpProgressDialog();
        setAddReviewDialog();
        initialisePresenter();
        mPresenter.fetchOrderHistories();
    }

    private void setAddReviewDialog() {
        addReviewDialog = new AddReviewDialog(this, this, this);
        addReviewDialog.setUpDialog();
    }

    public void setUpProgressDialog() {
        progressDialog = new CustomProgressDialog(this, this);
        progressDialog.setUpDialog();
    }

    private void initialisePresenter() {
        mPresenter = new OrdersListPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    @Override
    public void loadAdapter(OrdersAdapter ordersAdapter) {
        recyclerViewOrders.setAdapter(ordersAdapter);
        recyclerViewOrders.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public void showBankSnackbar() {
        Snackbar.make(mainLayout, "You don't have an bank account !! Please add one", Snackbar.LENGTH_LONG)
                .setAction("Add Account", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getApplicationContext(), BankListActivity.class);
                        startActivity(intent);
                    }
                }).show();
    }

    @Override
    public void onFeedbackClicked() {
        addReviewDialog.showDialog();
    }

    @Override
    public void showLoader() {
        progressDialog.showDialog();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismissDialog();
    }

    @Override
    public void showLoginBottomSheet() {
      //  Helper.showLoginBottomSheet(getSupportFragmentManager());
    }

    @Override
    public void submitRating(int rating, String feedback) {
        mPresenter.submitRating(rating, feedback);
    }

    @OnClick(R.id.btn_back) void onBackClicked()
    {
        finish();
    }
}