package com.webinfotech.salonease;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.webinfotech.salonease.domain.models.OffersData;
import com.webinfotech.salonease.domain.models.UserInfo;


public class AndroidApplication extends Application {

    UserInfo userInfo;
    OffersData offersData = null;
    double latitude;
    double longitude;
    String referUserId;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    public void setUserInfo(Context context, UserInfo userInfo){
        this.userInfo = userInfo;
        SharedPreferences sharedPref = context.getSharedPreferences(
                "SALON EASE", Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPref.edit();

        if(userInfo != null) {
            editor.putString("USER", new Gson().toJson(userInfo));
        } else {
            editor.putString("USER", "");
        }

        editor.commit();
    }

    public UserInfo getUserInfo(Context context){
        UserInfo user;
        if(userInfo != null){
            user = userInfo;
        }else{
            SharedPreferences sharedPref = context.getSharedPreferences(
                    "SALON EASE", Context.MODE_PRIVATE);
            String userInfoJson = sharedPref.getString("USER","");
            if(userInfoJson.isEmpty()){
                user = null;
            }else{
                try {
                    user = new Gson().fromJson(userInfoJson, UserInfo.class);
                    this.userInfo = user;
                }catch (Exception e){
                    user = null;
                }
            }
        }
        return user;
    }

    public void setReferUserId(Context context, String referId){
        this.referUserId = referId;
        SharedPreferences sharedPref = context.getSharedPreferences(
                "SALON EASE", Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPref.edit();

        if(this.referUserId != null) {
            editor.putString("REFERID", referUserId);
        } else {
            editor.putString("REFERID", "");
        }

        editor.commit();
    }

    public String getReferUserId(Context context){
        String referId;
        if(referUserId != null){
            referId = referUserId;
        }else{
            SharedPreferences sharedPref = context.getSharedPreferences(
                    "SALON EASE", Context.MODE_PRIVATE);
            String referIdString = sharedPref.getString("REFERID","");
            if(referIdString.isEmpty()){
                referId = null;
            }else{
               referId = referIdString;
            }
        }
        return referId;
    }

    public void setOffersData(OffersData offersData) {
        this.offersData = offersData;
    }

    public OffersData getOffersData() {
        return offersData;
    }

    public void setCoordinates(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public double getLongitude() {
        return longitude;
    }

}
