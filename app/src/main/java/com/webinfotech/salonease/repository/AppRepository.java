package com.webinfotech.salonease.repository;

import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface AppRepository {

    @POST("send/otp")
    @FormUrlEncoded
    Call<ResponseBody> sendOtp(@Field("mobile") String mobile);

    @POST("customer/registration/update/details")
    @FormUrlEncoded
    Call<ResponseBody> registerUser(@Field("user_id") int userId,
                                    @Field("name") String name,
                                    @Field("gender") String gender,
                                    @Field("email") String email,
                                    @Field("referral_id") String referalId
                                    );

    @POST("customer/login")
    @FormUrlEncoded
    Call<ResponseBody> checkLogin(@Field("mobile") String userId,
                                  @Field("password") String password
    );

    @PUT("customer/password/change/{id}")
    @FormUrlEncoded
    Call<ResponseBody> changePassword(  @Header("Authorization") String authorization,
                                        @Path("id") int userId,
                                        @Field("current_pass") String currentPassword,
                                        @Field("new_password") String newPassword,
                                        @Field("confirm_password") String confirmPassword
    );

    @GET("customer/forgot/otp/send/{mobile}")
    Call<ResponseBody> sendForgetPasswordOtp(@Path("mobile") String mobile);

    @POST("customer/forgot/password/change")
    @FormUrlEncoded
    Call<ResponseBody> resetPassword(   @Field("mobile") String mobile,
                                        @Field("otp") String otp,
                                        @Field("password") String password,
                                        @Field("confirm_password") String confirmPassword
    );

    @POST("app/loade/api")
    @FormUrlEncoded
    Call<ResponseBody> fetchAppLoadData(    @Field("latitude") double latitude,
                                            @Field("longitude") double longitude
                                        );


    @POST("service/list")
    @FormUrlEncoded
    Call<ResponseBody> fetchSalons(@Field("service_city") int serviceCityId,
                                   @Field("category_id") int categoryId,
                                   @Field("type") int type,
                                   @Field("latitude") double latitude,
                                   @Field("longitude") double longitude,
                                   @Field("client_type") int clientType,
                                   @Query("page") int page,
                                   @Field("price_from") String priceFrom,
                                   @Field("price_to") String priceTo,
                                   @Field("ac") int ac,
                                   @Field("parking") int parking,
                                   @Field("wifi") int wifi,
                                   @Field("music") int music,
                                   @Field("sort_by") int sort_by
    );

    @GET("service/city/list")
    Call<ResponseBody> fetchCityList();

    @GET("service/details/{client_id}")
    Call<ResponseBody> fetchSalonDetails(@Path("client_id") int client_id);

    @GET("customer/address/list")
    Call<ResponseBody> fetchShippingAddressList(@Header("Authorization") String authorization);

    @POST("customer/address/add")
    @FormUrlEncoded
    Call<ResponseBody> addAddress(  @Header("Authorization") String authorization,
                                    @Field("user_id") int userId,
                                    @Field("name") String name,
                                    @Field("email") String email,
                                    @Field("mobile") String mobile,
                                    @Field("state") String state,
                                    @Field("city") String city,
                                    @Field("pin") String pin,
                                    @Field("address") String address,
                                    @Field("latitude") double latitude,
                                    @Field("longitude") double longitude
    );

    @GET("customer/address/fetch/{id}")
    Call<ResponseBody> fetchAddressDetails(@Header("Authorization") String authorization,
                                           @Path("id") int userId
    );

    @PUT("customer/address/update/{id}")
    @FormUrlEncoded
    Call<ResponseBody> updateAddress(   @Header("Authorization") String authorization,
                                                @Path("id") int addressId,
                                                @Field("name") String name,
                                                @Field("email") String email,
                                                @Field("mobile") String mobile,
                                                @Field("state") String state,
                                                @Field("city") String city,
                                                @Field("pin") String pin,
                                                @Field("address") String address,
                                                @Field("latitude") double latitude,
                                                @Field("longitude") double longitude
    );

    @GET("service/list/category")
    Call<ResponseBody> fetchCategories();

    @POST("customer/order/place")
    @FormUrlEncoded
    Call<ResponseBody> placeOrder(@Header("Authorization") String authorization,
                                  @Field("vendor_id") int vendorId,
                                  @Field("job_id[]") ArrayList<Integer> serviceIds,
                                  @Field("quantity[]") ArrayList<Integer> quantities,
                                  @Field("service_time") String serviceTime,
                                  @Field("address_id") String addressId,
                                  @Field("offer_id") String offerId,
                                  @Field("offer_job_id") String offerJobId,
                                  @Field("coupon_id") String couponId,
                                  @Field("is_wallet") int isWallet
                                  );

    @POST("customer/payment/verify")
    @FormUrlEncoded
    Call<ResponseBody> verifyPayment(   @Header("Authorization") String authorization,
                                        @Field("user_id") int userId,
                                        @Field("razorpay_order_id") String razorpayOrderId,
                                        @Field("razorpay_payment_id") String razorpayPaymentId,
                                        @Field("razorpay_signature") String razorpaySignature,
                                        @Field("order_id") int orderId
    );

    @GET("customer/order/history")
    Call<ResponseBody> fetchOrderHistory(@Header("Authorization") String authorization
    );

    @PUT("customer/bank/info/add/{user_id}")
    @FormUrlEncoded
    Call<ResponseBody> addBankAccount(  @Header("Authorization") String authorization,
                                        @Path("user_id") int userId,
                                        @Field("bank_name") String bankName,
                                        @Field("ac_no") String accountNo,
                                        @Field("ifsc") String ifsc,
                                        @Field("branch_name") String branchName
    );

    @GET("customer/bank/info/list/{user_id}")
    Call<ResponseBody> fetchBankList(   @Header("Authorization") String authorization,
                                        @Path("user_id") int userId
    );

    @PUT("customer/bank/info/update/{bank_info_id}")
    @FormUrlEncoded
    Call<ResponseBody> updateBankAccount(  @Header("Authorization") String authorization,
                                           @Path("bank_info_id") int bankInfoId,
                                           @Field("bank_name") String bankName,
                                           @Field("ac_no") String accountNo,
                                           @Field("ifsc") String ifsc,
                                           @Field("branch_name") String branchName
    );

    @POST("customer/order/cancel")
    @FormUrlEncoded
    Call<ResponseBody> cancelOrder(  @Header("Authorization") String authorization,
                                     @Field("order_id") int orderId,
                                     @Field("is_refund") int isRefund,
                                     @Field("account_id") int accountId
    );

    @POST("customer/service/review/insert")
    @FormUrlEncoded
    Call<ResponseBody> submitReview(  @Header("Authorization") String authorization,
                                      @Field("customer_id") int userId,
                                      @Field("client_id") int clientId,
                                      @Field("comment") String comment,
                                      @Field("rating") int rating
                                      );

    @POST("customer/profile/update")
    @FormUrlEncoded
    Call<ResponseBody> updateProfile(   @Header("Authorization") String authorization,
                                        @Field("user_id") int userId,
                                        @Field("name") String name,
                                        @Field("email") String email,
                                        @Field("mobile") String mobile,
                                        @Field("state") String state,
                                        @Field("address") String address,
                                        @Field("city") String city,
                                        @Field("pin") String pin,
                                        @Field("gender") String gender,
                                        @Field("dob") String dob
    );

    @GET("customer/profile/{id}")
    Call<ResponseBody> fetchUserProfile(   @Header("Authorization") String authorization,
                                           @Path("id") int userId
    );

    @POST("service/search")
    @FormUrlEncoded
    Call<ResponseBody> fetchSalonList( @Field("search_key") String searchKey,
                                       @Field("latitude") double latitude,
                                       @Field("longitude") double longitude,
                                       @Query("page") int page
    );

    @GET("customer/update/firebase_token/{id}/{token}")
    Call<ResponseBody> updateFirebaseToken( @Header("Authorization") String authorization,
                                            @Path("id") int userId,
                                            @Path("token") String apiToken
    );

    @POST("password/request/send")
    @FormUrlEncoded
    Call<ResponseBody> requestPassword (  @Field("mobile_number") String mobileNumber,
                                          @Field("user_type") int userType );

    @GET("sub/category/{main_category_id}")
    Call<ResponseBody> fetchSecondCategory(@Path("main_category_id") int mainCategoryId);

    @GET("third/category/{sub_category_id}")
    Call<ResponseBody> fetchThirdCategory(@Path("sub_category_id") int subCategoryId);

    @GET("customer/offer/list")
    Call<ResponseBody> fetchOfferList(@Header("Authorization") String authorization);

    @POST("offer/check")
    @FormUrlEncoded
    Call<ResponseBody> checkOffer(  @Header("Authorization") String authorization,
                                    @Field("offer_id") int offerId,
                                    @Field("job_id") int jobId,
                                    @Field("vendor_id") int vendorId
    );

    @POST("otp/verify")
    @FormUrlEncoded
    Call<ResponseBody> verifyOtp (  @Field("mobile") String mobileNumber,
                                    @Field("otp") String otp );

    @POST("customer/offer/coupon/check")
    @FormUrlEncoded
    Call<ResponseBody> checkCoupon( @Header("Authorization") String authorization,
                                    @Field("coupon_id") int couponId
                                   );

    @GET("customer/wallet/history")
    Call<ResponseBody> fetchWalletHistory(   @Header("Authorization") String authorization,
                                             @Query("page") int page
    );
    @POST("top/salon/view/all")
    @FormUrlEncoded
    Call<ResponseBody> fetchTopSalons( @Field("latitude") double latitude,
                                       @Field("longitude") double longitude
    );

    @POST("top/freelancer/view/all")
    @FormUrlEncoded
    Call<ResponseBody> fetchTopFreelancer( @Field("latitude") double latitude,
                                           @Field("longitude") double longitude
    );

    @POST("combo/view/all")
    @FormUrlEncoded
    Call<ResponseBody> fetchCombos( @Field("latitude") double latitude,
                                    @Field("longitude") double longitude
    );

    @POST("deals/view/all")
    @FormUrlEncoded
    Call<ResponseBody> fetchDeals( @Field("latitude") double latitude,
                                           @Field("longitude") double longitude
    );

    @GET("customer/wish/list/add/{client_id}")
    Call<ResponseBody> addToWishlist( @Header("Authorization") String authorization,
                                      @Path("client_id") int clientId
    );

    @GET("customer/wish/list/remove/{wish_list_id}")
    Call<ResponseBody> removeWishlist( @Header("Authorization") String authorization,
                                       @Path("wish_list_id") int wishListId
    );

    @GET("customer/wish/list/getData")
    Call<ResponseBody> fetchWishlist(@Header("Authorization") String authorization);

    @GET("customer/order/vendor/cancel/accept/reject/{order_id}/{status}")
    Call<ResponseBody> changeVendor(@Header("Authorization") String authorization,
                                    @Path("order_id") int orderId,
                                    @Path("status") int status
                                    );
    @GET("customer/message/list")
    Call<ResponseBody> fetchMessagelist(@Header("Authorization") String authorization,
                                        @Query("page") int page
    );

    @POST("contact/mail/send")
    @FormUrlEncoded
    Call<ResponseBody> sendFeedback(  @Header("Authorization") String authorization,
                                    @Field("category_id") int categoryId,
                                      @Field("category_id") int sub_category_id,
                                      @Field("category_id") int third_category_id,
                                    @Field("name") String name,
                                    @Field("mobile") String mobile,
                                    @Field("message") String message,
                                    @Field("booking_date") String booking_date
    );
}
