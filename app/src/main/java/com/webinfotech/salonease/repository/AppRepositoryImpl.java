package com.webinfotech.salonease.repository;

import android.util.Log;

import com.google.gson.Gson;
import com.webinfotech.salonease.domain.models.AddressDetailsWrapper;
import com.webinfotech.salonease.domain.models.AppLoadDataWrapper;
import com.webinfotech.salonease.domain.models.BankListWrapper;
import com.webinfotech.salonease.domain.models.CategoryWrapper;
import com.webinfotech.salonease.domain.models.CityWrapper;
import com.webinfotech.salonease.domain.models.CombosWrapper;
import com.webinfotech.salonease.domain.models.CommonResponse;
import com.webinfotech.salonease.domain.models.MessageData;
import com.webinfotech.salonease.domain.models.MessageWrapper;
import com.webinfotech.salonease.domain.models.OffersDataWrapper;
import com.webinfotech.salonease.domain.models.OrderPlaceResponse;
import com.webinfotech.salonease.domain.models.OrdersWrapper;
import com.webinfotech.salonease.domain.models.OtpResponse;
import com.webinfotech.salonease.domain.models.SalonDataWrapper;
import com.webinfotech.salonease.domain.models.SalonDetailsWrapper;
import com.webinfotech.salonease.domain.models.SalonListWrapper;
import com.webinfotech.salonease.domain.models.ShippingAddressWrapper;
import com.webinfotech.salonease.domain.models.SubcategoryWrapper;
import com.webinfotech.salonease.domain.models.ThirdCategoryWrapper;
import com.webinfotech.salonease.domain.models.UserInfoWrapper;
import com.webinfotech.salonease.domain.models.WalletResponse;
import com.webinfotech.salonease.domain.models.WishlistWrapper;

import java.util.ArrayList;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

public class AppRepositoryImpl {

    AppRepository mRepository;

    public AppRepositoryImpl() {
        mRepository = ApiClient.createService(AppRepository.class);
    }

    public OtpResponse sendOtp(String mobile) {
        OtpResponse otpResponse;
        String responseBody = "";
        Gson gson = new Gson();
        try {
            Call<ResponseBody> send = mRepository.sendOtp(mobile);
            Response<ResponseBody> response = send.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (!responseBody.isEmpty()) {
                otpResponse = gson.fromJson(responseBody, OtpResponse.class);
            } else {
                otpResponse = null;
            }
        }catch (Exception e){
            otpResponse = null;
        }
        return otpResponse;
    }

    public UserInfoWrapper registerUser(    int userId,
                                            String name,
                                            String email,
                                            String gender,
                                            String referalId
                                       ) {
        UserInfoWrapper userInfoWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> register = mRepository.registerUser(userId, name, gender, email, referalId);
            Response<ResponseBody> response = register.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    userInfoWrapper = null;
                }else{
                    userInfoWrapper = gson.fromJson(responseBody, UserInfoWrapper.class);
                }
            } else {
                userInfoWrapper = null;
            }
        }catch (Exception e){
            userInfoWrapper = null;
        }
        return userInfoWrapper;
    }

    public UserInfoWrapper checkLogin(String phone, String password){
        UserInfoWrapper userInfoWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> check = mRepository.checkLogin(phone, password);

            Response<ResponseBody> response = check.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    userInfoWrapper = null;
                }else{
                    userInfoWrapper = gson.fromJson(responseBody, UserInfoWrapper.class);
                }
            } else {
                userInfoWrapper = null;
            }
        }catch (Exception e){
            userInfoWrapper = null;
        }
        return userInfoWrapper;
    }

    public CommonResponse changePassword(String apiToken, int userId, String currentPassword, String newPassword){
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> change = mRepository.changePassword("Bearer " + apiToken, userId, currentPassword, newPassword, newPassword);
            Response<ResponseBody> response = change.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public OtpResponse sendForgetPasswordOtp(String mobile) {
        OtpResponse otpResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> send = mRepository.sendForgetPasswordOtp(mobile);
            Response<ResponseBody> response = send.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    otpResponse = null;
                }else{
                    otpResponse = gson.fromJson(responseBody, OtpResponse.class);
                }
            } else {
                otpResponse = null;
            }
        }catch (Exception e){
            otpResponse = null;
        }
        return otpResponse;
    }

    public CommonResponse resetPassword(String mobile, String otp, String password){
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> reset = mRepository.resetPassword(mobile, otp, password, password);
            Response<ResponseBody> response = reset.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        } catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public AppLoadDataWrapper fetchAppLoadData(double latitude, double longitude) {
        AppLoadDataWrapper appLoadDataWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchAppLoadData(latitude, longitude);
            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    appLoadDataWrapper = null;
                }else{
                    appLoadDataWrapper = gson.fromJson(responseBody, AppLoadDataWrapper.class);
                }
            } else {
                appLoadDataWrapper = null;
            }
        } catch (Exception e){
            appLoadDataWrapper = null;
        }
        return appLoadDataWrapper;
    }

    public SalonDataWrapper fetchSalonData(int serviceCityId,
                                           int categoryId,
                                           int page,
                                           int type,
                                           double latitude,
                                           double longitude,
                                           int clientType,
                                           String priceFrom,
                                           String priceTo,
                                           int ac,
                                           int parking,
                                           int wifi,
                                           int music,
                                           int sortBy
                                           ) {

        SalonDataWrapper salonDataWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = null;
            fetch = mRepository.fetchSalons(serviceCityId, categoryId, type, latitude, longitude, clientType, page, priceFrom, priceTo, ac, parking, wifi, music, sortBy);
            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    salonDataWrapper = null;
                }else{
                    salonDataWrapper = gson.fromJson(responseBody, SalonDataWrapper.class);
                }
            } else {
                salonDataWrapper = null;
            }
        } catch (Exception e){
            salonDataWrapper = null;
        }
        return salonDataWrapper;
    }

    public CityWrapper fetchCityList() {
        CityWrapper cityWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchCityList();
            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    cityWrapper = null;
                }else{
                    cityWrapper = gson.fromJson(responseBody, CityWrapper.class);
                }
            } else {
                cityWrapper = null;
            }
        }catch (Exception e){
            cityWrapper = null;
        }
        return cityWrapper;
    }

    public SalonDetailsWrapper fetchSalonDetails(int salonId) {
        SalonDetailsWrapper salonDetailsWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchSalonDetails(salonId);
            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    salonDetailsWrapper = null;
                }else{
                    salonDetailsWrapper = gson.fromJson(responseBody, SalonDetailsWrapper.class);
                }
            } else {
                salonDetailsWrapper = null;
            }
        }catch (Exception e){
            salonDetailsWrapper = null;
        }
        return salonDetailsWrapper;
    }

    public ShippingAddressWrapper fetchShippingAddress(String apiToken, int userId){
        ShippingAddressWrapper shippingAddressWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchShippingAddressList("Bearer " + apiToken);
            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    shippingAddressWrapper = null;
                }else{
                    shippingAddressWrapper = gson.fromJson(responseBody, ShippingAddressWrapper.class);
                }
            } else {
                shippingAddressWrapper = null;
            }
        }catch (Exception e){
            shippingAddressWrapper = null;
        }
        return shippingAddressWrapper;
    }

    public CommonResponse addAddress(  String apiToken,
                                       int userId,
                                       String name,
                                       String email,
                                       String mobile,
                                       String state,
                                       String city,
                                       String pin,
                                       String address,
                                       double latitude,
                                       double longitude
    ) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> add = mRepository.addAddress("Bearer " + apiToken, userId, name, email, mobile, state, city, pin, address, latitude, longitude);
            Response<ResponseBody> response = add.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public AddressDetailsWrapper fetchAddressDetails(String apiToken, int addressId){
        AddressDetailsWrapper addressDetailsWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchAddressDetails("Bearer " + apiToken, addressId);
            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    addressDetailsWrapper = null;
                }else{
                    addressDetailsWrapper = gson.fromJson(responseBody, AddressDetailsWrapper.class);
                }
            } else {
                addressDetailsWrapper = null;
            }
        }catch (Exception e){
            addressDetailsWrapper = null;
        }
        return addressDetailsWrapper;
    }

    public CommonResponse updateAddress(  String apiToken,
                                          int addressId,
                                          String name,
                                          String email,
                                          String mobile,
                                          String state,
                                          String city,
                                          String pin,
                                          String address,
                                          double latitude,
                                          double longitude
    ) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> update = mRepository.updateAddress("Bearer " + apiToken, addressId, name, email, mobile, state, city, pin, address, latitude, longitude);
            Response<ResponseBody> response = update.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public CategoryWrapper fetchCategories() {
        CategoryWrapper categoryWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchCategories();
            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    categoryWrapper = null;
                }else{
                    categoryWrapper = gson.fromJson(responseBody, CategoryWrapper.class);
                }
            } else {
                categoryWrapper = null;
            }
        }catch (Exception e){
            categoryWrapper = null;
        }
        return categoryWrapper;
    }

    public OrderPlaceResponse placeOrder(String authorization,
                                         int vendorId,
                                         ArrayList<Integer> serviceIds,
                                         ArrayList<Integer> quantities,
                                         String serviceTime,
                                         String addressId,
                                         String offerId,
                                         String offerJobId,
                                         String couponId,
                                         int isWallet) {

        OrderPlaceResponse orderPlaceResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> place = mRepository.placeOrder("Bearer " + authorization, vendorId, serviceIds, quantities, serviceTime, addressId, offerId, offerJobId, couponId, isWallet);
            Response<ResponseBody> response = place.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    orderPlaceResponse = null;
                }else{
                    orderPlaceResponse = gson.fromJson(responseBody, OrderPlaceResponse.class);
                }
            } else {
                orderPlaceResponse = null;
            }
        }catch (Exception e){
            orderPlaceResponse = null;
        }
        return orderPlaceResponse;
    }

    public CommonResponse verifyPayment( String authorization,
                                         int userId,
                                         String razorpayOrderId,
                                         String razorpayPaymentId,
                                         String razorpaySignature,
                                         int orderId) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> request = mRepository.verifyPayment("Bearer " + authorization, userId, razorpayOrderId, razorpayPaymentId, razorpaySignature, orderId);
            Response<ResponseBody> response = request.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public OrdersWrapper fetchOrderHistory(String apiToken, int userId) {
        OrdersWrapper ordersWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        try {
            Call<ResponseBody> fetch = mRepository.fetchOrderHistory("Bearer " + apiToken);
            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (!responseBody.isEmpty()) {
                ordersWrapper = gson.fromJson(responseBody, OrdersWrapper.class);
            } else {
                ordersWrapper = null;
            }
        }catch (Exception e){
            ordersWrapper = null;
        }
        return ordersWrapper;
    }

    public CommonResponse addBankAccount( String apiToken,
                                          int userId,
                                          String name,
                                          String bankName,
                                          String accountNo,
                                          String ifsc,
                                          String branchName
    ) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> add = mRepository.addBankAccount("Bearer " + apiToken, userId, bankName, accountNo, ifsc, branchName);
            Response<ResponseBody> response = add.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (!responseBody.isEmpty()) {
                commonResponse = gson.fromJson(responseBody, CommonResponse.class);
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public BankListWrapper fetchBankList (String apiToken, int userId) {
        BankListWrapper bankListWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchBankList("Bearer " + apiToken, userId);
            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    bankListWrapper = null;
                }else{
                    bankListWrapper = gson.fromJson(responseBody, BankListWrapper.class);
                }
            } else {
                bankListWrapper = null;
            }
        }catch (Exception e){
            bankListWrapper = null;
        }
        return bankListWrapper;
    }

    public CommonResponse updateBankAccount( String apiToken,
                                             int bankAccountId,
                                             String name,
                                             String bankName,
                                             String accountNo,
                                             String ifsc,
                                             String branchName
    ) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> add = mRepository.updateBankAccount("Bearer " + apiToken, bankAccountId, bankName, accountNo, ifsc, branchName);
            Response<ResponseBody> response = add.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public CommonResponse cancelOrder( String apiToken,
                                             int orderId,
                                             int isRefund,
                                             int accountId
    ) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> cancel = mRepository.cancelOrder("Bearer " + apiToken, orderId, isRefund, accountId);
            Response<ResponseBody> response = cancel.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public CommonResponse submitReview( String authorization,
                                        int userId,
                                        int clientId,
                                        String comment,
                                        int rating
    ) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> submit = mRepository.submitReview("Bearer " + authorization, userId, clientId, comment, rating);
            Response<ResponseBody> response = submit.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public CommonResponse updateProfile(String authorization,
                                        int userId,
                                        String name,
                                        String mobile,
                                        String state,
                                        String city,
                                        String gender,
                                        String address,
                                        String pin,
                                        String email
    ) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> update = mRepository.updateProfile("Bearer " + authorization, userId, name, email, mobile, state, address, city, pin, gender, "2020-11-11");
            Response<ResponseBody> response = update.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public UserInfoWrapper fetchUserProfile(String authorization, int userId){
        UserInfoWrapper userInfoWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchUserProfile("Bearer " + authorization, userId);

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    userInfoWrapper = null;
                }else{
                    userInfoWrapper = gson.fromJson(responseBody, UserInfoWrapper.class);
                }
            } else {
                userInfoWrapper = null;
            }
        }catch (Exception e){
            userInfoWrapper = null;
        }
        return userInfoWrapper;
    }

    public SalonListWrapper fetchSearchList(String searchKey,
                                            double latitude,
                                            double longitude,
                                            int page){
        SalonListWrapper salonListWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchSalonList(searchKey, latitude, longitude, page);

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    salonListWrapper = null;
                }else{
                    salonListWrapper = gson.fromJson(responseBody, SalonListWrapper.class);
                }
            } else {
                salonListWrapper = null;
            }
        }catch (Exception e){
            salonListWrapper = null;
        }
        return salonListWrapper;
    }

    public CommonResponse updateFirebaseToken( String authorization,
                                        int userId,
                                        String token
    ) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> update = mRepository.updateFirebaseToken("Bearer " + authorization, userId, token);
            Response<ResponseBody> response = update.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public CommonResponse requestPasswordChange( String mobileNumber) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> request = mRepository.requestPassword(mobileNumber, 1);
            Response<ResponseBody> response = request.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public SubcategoryWrapper fetchSubcategory(int categoryId) {
        SubcategoryWrapper subcategoryWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchSecondCategory(categoryId);
            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    subcategoryWrapper = null;
                }else{
                    subcategoryWrapper = gson.fromJson(responseBody, SubcategoryWrapper.class);
                }
            } else {
                subcategoryWrapper = null;
            }
        }catch (Exception e){
            subcategoryWrapper = null;
        }
        return subcategoryWrapper;
    }

    public ThirdCategoryWrapper fetchThirdCategoryy(int subcategoryId) {
        ThirdCategoryWrapper thirdCategoryWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchThirdCategory(subcategoryId);
            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    thirdCategoryWrapper = null;
                }else{
                    thirdCategoryWrapper = gson.fromJson(responseBody, ThirdCategoryWrapper.class);
                }
            } else {
                thirdCategoryWrapper = null;
            }
        }catch (Exception e){
            thirdCategoryWrapper = null;
        }
        return thirdCategoryWrapper;
    }

    public OffersDataWrapper fetchOffers(String apiToken) {
        OffersDataWrapper offersDataWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchOfferList("Bearer " + apiToken);
            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    offersDataWrapper = null;
                }else{
                    offersDataWrapper = gson.fromJson(responseBody, OffersDataWrapper.class);
                }
            } else {
                offersDataWrapper = null;
            }
        }catch (Exception e){
            offersDataWrapper = null;
        }
        return offersDataWrapper;
    }

    public CommonResponse checkOffer(String apiToken, int offerId, int jobId, int vendorId) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> request = mRepository.checkOffer("Bearer " + apiToken, offerId, jobId, vendorId);
            Response<ResponseBody> response = request.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public UserInfoWrapper verifyOtp(String mobileNumber,
                                     String otp) {
        UserInfoWrapper userInfoWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> check = mRepository.verifyOtp(mobileNumber, otp);

            Response<ResponseBody> response = check.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    userInfoWrapper = null;
                }else{
                    userInfoWrapper = gson.fromJson(responseBody, UserInfoWrapper.class);
                }
            } else {
                userInfoWrapper = null;
            }
        }catch (Exception e){
            userInfoWrapper = null;
        }
        return userInfoWrapper;
    }

    public CommonResponse checkCoupon(String apiToken, int couponId) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> request = mRepository.checkCoupon("Bearer " + apiToken, couponId);
            Response<ResponseBody> response = request.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public WalletResponse fetchWalletHistory(String apiToken, int page) {
        WalletResponse walletResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchWalletHistory("Bearer " + apiToken, page);
            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    walletResponse = null;
                }else{
                    walletResponse = gson.fromJson(responseBody, WalletResponse.class);
                }
            } else {
                walletResponse = null;
            }
        }catch (Exception e){
            walletResponse = null;
        }
        return walletResponse;
    }

    public SalonListWrapper fetchTopSalons( double latitude,
                                            double longitude){
        SalonListWrapper salonListWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchTopSalons(latitude, longitude);

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    salonListWrapper = null;
                }else{
                    salonListWrapper = gson.fromJson(responseBody, SalonListWrapper.class);
                }
            } else {
                salonListWrapper = null;
            }
        }catch (Exception e){
            salonListWrapper = null;
        }
        return salonListWrapper;
    }

    public SalonListWrapper fetchTopFreeLancers( double latitude,
                                                  double longitude){
        SalonListWrapper salonListWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchTopFreelancer(latitude, longitude);

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    salonListWrapper = null;
                }else{
                    salonListWrapper = gson.fromJson(responseBody, SalonListWrapper.class);
                }
            } else {
                salonListWrapper = null;
            }
        }catch (Exception e){
            salonListWrapper = null;
        }
        return salonListWrapper;
    }

    public CombosWrapper fetchCombos(double latitude,
                                             double longitude){
        CombosWrapper combosWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchCombos(latitude, longitude);

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    combosWrapper = null;
                }else{
                    combosWrapper = gson.fromJson(responseBody, CombosWrapper.class);
                }
            } else {
                combosWrapper = null;
            }
        }catch (Exception e){
            combosWrapper = null;
        }
        return combosWrapper;
    }

    public SalonListWrapper fetchDeals( double latitude,
                                                 double longitude){
        SalonListWrapper salonListWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchDeals(latitude, longitude);

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    salonListWrapper = null;
                }else{
                    salonListWrapper = gson.fromJson(responseBody, SalonListWrapper.class);
                }
            } else {
                salonListWrapper = null;
            }
        }catch (Exception e){
            salonListWrapper = null;
        }
        return salonListWrapper;
    }

    public CommonResponse addToWishlist(String apiToken, int clientId) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> request = mRepository.addToWishlist("Bearer " + apiToken, clientId);
            Response<ResponseBody> response = request.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public WishlistWrapper fetchWishlist(String apiToken) {
        WishlistWrapper wishlistWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        try {
            Call<ResponseBody> fetch = mRepository.fetchWishlist("Bearer " + apiToken);
            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (!responseBody.isEmpty()) {
                wishlistWrapper = gson.fromJson(responseBody, WishlistWrapper.class);
            } else {
                wishlistWrapper = null;
            }
        }catch (Exception e){
            wishlistWrapper = null;
        }
        return wishlistWrapper;
    }

    public CommonResponse removeFromWishlist(String apiToken, int wishListId) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> request = mRepository.removeWishlist("Bearer " + apiToken, wishListId);
            Response<ResponseBody> response = request.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public CommonResponse changeVendor(String apiToken, int orderId, int status) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> change = mRepository.changeVendor("Bearer " + apiToken, orderId, status);
            Response<ResponseBody> response = change.execute();

            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public MessageWrapper fetchMessagelist(String apiToken, int page) {
        MessageWrapper messageWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> request = mRepository.fetchMessagelist("Bearer " + apiToken, page);
            Response<ResponseBody> response = request.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if (isErrorResponse){
                    messageWrapper = null;
                } else {
                    messageWrapper = gson.fromJson(responseBody, MessageWrapper.class);
                }
            } else {
                messageWrapper = null;
            }
        }catch (Exception e){
            messageWrapper = null;
        }
        return messageWrapper;
    }

    public CommonResponse sendFeedback( String apiToken,
                                          int categoryId,
                                          int subcategoryId,
                                          int thirdcategoryId,
                                          String name,
                                          String mobile,
                                          String message,
                                          String booking_date
    ) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> add = mRepository.sendFeedback("Bearer " + apiToken, categoryId, subcategoryId, thirdcategoryId,  name, mobile, message, booking_date);
            Response<ResponseBody> response = add.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (!responseBody.isEmpty()) {
                commonResponse = gson.fromJson(responseBody, CommonResponse.class);
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }
}
