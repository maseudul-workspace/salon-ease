package com.webinfotech.salonease.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;

import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.android.material.card.MaterialCardView;
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton;
import com.google.android.material.switchmaterial.SwitchMaterial;
import com.google.gson.Gson;
import com.tbuonomo.viewpagerdotsindicator.DotsIndicator;
import com.webinfotech.salonease.AndroidApplication;
import com.webinfotech.salonease.R;
import com.webinfotech.salonease.domain.executors.impl.ThreadExecutor;
import com.webinfotech.salonease.domain.models.SalonData;
import com.webinfotech.salonease.domain.models.SalonImages;
import com.webinfotech.salonease.domain.models.SalonUser;
import com.webinfotech.salonease.domain.models.Service;
import com.webinfotech.salonease.domain.models.UserInfo;
import com.webinfotech.salonease.domain.models.testing.BookingServices;
import com.webinfotech.salonease.presentation.presenters.SalonDetailsPresenter;
import com.webinfotech.salonease.presentation.presenters.impl.SalonDetailsPresenterImpl;
import com.webinfotech.salonease.presentation.ui.adapters.BookingAddressAdapter;
import com.webinfotech.salonease.presentation.ui.adapters.ReviewsAdapter;
import com.webinfotech.salonease.presentation.ui.adapters.SalonDetailsViewpagerAdapter;
import com.webinfotech.salonease.presentation.ui.adapters.ServicesAdapter;
import com.webinfotech.salonease.threading.MainThreadImpl;
import com.webinfotech.salonease.util.GlideHelper;
import com.webinfotech.salonease.util.Helper;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class SalonDetailsActivity extends AppCompatActivity implements SalonDetailsPresenter.View, OnMapReadyCallback, ServicesAdapter.Callback {

    @BindView(R.id.img_view_header)
    ImageView imgViewHeader;
    @BindView(R.id.view_pager_main)
    ViewPager viewPagerMain;
    @BindView(R.id.dots_indicator)
    DotsIndicator dotsIndicator;
    @BindView(R.id.rating_bar_salon)
    RatingBar ratingBarSalon;
    @BindView(R.id.txt_view_review_count)
    TextView txtViewReviewCount;
    @BindView(R.id.txt_view_city)
    TextView txtViewCity;
    @BindView(R.id.txt_view_state)
    TextView txtViewState;
    @BindView(R.id.txt_view_address)
    TextView txtViewAddress;
    @BindView(R.id.txt_view_timing)
    TextView txtViewTiming;
    @BindView(R.id.txt_view_description)
    TextView txtViewDescription;
    @BindView(R.id.recycler_view_services)
    RecyclerView recyclerViewServices;
    @BindView(R.id.recycler_view_reviews)
    RecyclerView recyclerViewReviews;
    @BindView(R.id.btn_booking)
    ExtendedFloatingActionButton btnBooking;
    @BindView(R.id.scroll_view)
    NestedScrollView scrollView;
    ProgressDialog progressDialog;
    double latitude;
    double longitude;
    SalonDetailsPresenterImpl mPresenter;
    Service[] services;
    ServicesAdapter servicesAdapter;
    @BindView(R.id.txt_view_salon_name)
    TextView txtViewSalonName;
    RecyclerView recyclerViewShippingAddress;
    BottomSheetDialog addressBottomSheetDialog;
    TextView txtViewAddAddress;
    ImageView imgViewAddAddress;
    String bookingTime = null;
    MaterialCardView cardViewFirstDate;
    MaterialCardView cardViewSecondDate;
    MaterialCardView cardViewThirdDate;
    TextView txtViewFirstDate;
    TextView txtViewSecondDate;
    TextView txtViewThirdDate;
    int serviceFor = 0;
    RadioGroup radioGroupServiceFor;
    View viewAddAddress;
    int price = 0;
    int serviceId = 0;
    int addressId = 0;
    int vendorId;
    @BindView(R.id.switch_air_condition)
    SwitchMaterial switchAirCondition;
    @BindView(R.id.switch_music)
    SwitchMaterial switchMusic;
    @BindView(R.id.switch_parking)
    SwitchMaterial switchParking;
    @BindView(R.id.switch_wifi)
    SwitchMaterial switchWifi;
    @BindView(R.id.layout_features)
    View featuresLayout;
    @BindView(R.id.txt_view_parking)
    TextView txtViewParking;
    @BindView(R.id.txt_view_ac)
    TextView txtViewAc;
    @BindView(R.id.txt_view_wifi)
    TextView txtViewWifi;
    @BindView(R.id.txt_view_music)
    TextView txtViewMusic;
    ArrayList<BookingServices> bookingServices = new ArrayList<>();
    SalonUser salonUser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_salon_details);
        ButterKnife.bind(this);
        serviceId = getIntent().getIntExtra("serviceId", 0);
        initialisePresenter();
        setUpProgressDialog();
        setAddressBottomSheetDialog();
        mPresenter.fetchProductDetails(serviceId);
    }

    private void initialisePresenter() {
        mPresenter = new SalonDetailsPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    private void setAddressBottomSheetDialog() {
        if (addressBottomSheetDialog == null) {
            View view = LayoutInflater.from(this).inflate(R.layout.layout_booking_bottom_sheet, null);
            addressBottomSheetDialog = new BottomSheetDialog(this);
            txtViewAddAddress = (TextView) view.findViewById(R.id.txt_view_add_address);
            imgViewAddAddress = (ImageView) view.findViewById(R.id.img_view_add_address);
            recyclerViewShippingAddress = (RecyclerView) view.findViewById(R.id.recycler_view_address);
            cardViewFirstDate = (MaterialCardView) view.findViewById(R.id.card_view_first_date);
            cardViewSecondDate = (MaterialCardView) view.findViewById(R.id.card_view_second_date);
            cardViewThirdDate = (MaterialCardView) view.findViewById(R.id.card_view_third_date);
            txtViewFirstDate = (TextView) view.findViewById(R.id.txt_view_first_date);
            txtViewSecondDate = (TextView) view.findViewById(R.id.txt_view_second_date);
            txtViewThirdDate = (TextView) view.findViewById(R.id.txt_view_third_date);
            viewAddAddress = (View) view.findViewById(R.id.view_add_adddress);
            txtViewFirstDate.setText(getDateAhead(1));
            txtViewSecondDate.setText(getDateAhead(2));
            txtViewThirdDate.setText(getDateAhead(3));
            cardViewFirstDate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    cardViewFirstDate.setCardBackgroundColor(getResources().getColor(R.color.colorAccent));
                    cardViewSecondDate.setCardBackgroundColor(getResources().getColor(R.color.white));
                    cardViewThirdDate.setCardBackgroundColor(getResources().getColor(R.color.white));
                    txtViewFirstDate.setTextColor(getResources().getColor(R.color.white));
                    txtViewSecondDate.setTextColor(getResources().getColor(R.color.md_black_1000));
                    txtViewThirdDate.setTextColor(getResources().getColor(R.color.md_black_1000));
                    bookingTime = getDateAheadFormat(1);
                }
            });
            cardViewSecondDate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    cardViewSecondDate.setCardBackgroundColor(getResources().getColor(R.color.colorAccent));
                    cardViewFirstDate.setCardBackgroundColor(getResources().getColor(R.color.white));
                    cardViewThirdDate.setCardBackgroundColor(getResources().getColor(R.color.white));
                    txtViewFirstDate.setTextColor(getResources().getColor(R.color.md_black_1000));
                    txtViewSecondDate.setTextColor(getResources().getColor(R.color.white));
                    txtViewThirdDate.setTextColor(getResources().getColor(R.color.md_black_1000));
                    bookingTime = getDateAheadFormat(2);
                }
            });
            cardViewThirdDate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    cardViewThirdDate.setCardBackgroundColor(getResources().getColor(R.color.colorAccent));
                    cardViewSecondDate.setCardBackgroundColor(getResources().getColor(R.color.white));
                    cardViewFirstDate.setCardBackgroundColor(getResources().getColor(R.color.white));
                    txtViewFirstDate.setTextColor(getResources().getColor(R.color.md_black_1000));
                    txtViewSecondDate.setTextColor(getResources().getColor(R.color.md_black_1000));
                    txtViewThirdDate.setTextColor(getResources().getColor(R.color.white));
                    bookingTime = getDateAheadFormat(3);
                }
            });
            txtViewAddAddress.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    Intent intent = new Intent(getApplicationContext(), AddAddressActivity.class);
//                    startActivity(intent);
                }
            });

            viewAddAddress.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getApplicationContext(), AddAddressActivity.class);
                    startActivity(intent);
                }
            });

            addressBottomSheetDialog.setContentView(view);
        }
    }

    @Override
    public void loadSalonData(SalonData salonData) {

        vendorId =salonData.salonUser.id;

        salonUser = salonData.salonUser;
        SalonImages[] images = salonUser.images;
        GlideHelper.setImageView(this, imgViewHeader, getResources().getString(R.string.base_url) + "images/client/" + salonUser.image);
        txtViewCity.setText(salonUser.city);
        txtViewState.setText(salonUser.state);
        txtViewAddress.setText(salonUser.address);
        txtViewSalonName.setText(salonUser.name);
        if (salonUser.reviews.length == 0) {
            txtViewReviewCount.setText("No");
        } else {
            txtViewReviewCount.setText(Integer.toString(salonUser.reviews.length));
        }
        if (Build.VERSION.SDK_INT < 23) {
            txtViewSalonName.setTextAppearance(getApplicationContext(), R.style.CursiveStyle);
        } else {
            txtViewSalonName.setTextAppearance(R.style.CursiveStyle);
        }

        SalonDetailsViewpagerAdapter viewpagerAdapter = new SalonDetailsViewpagerAdapter(this, images);
        viewPagerMain.setAdapter(viewpagerAdapter);
        dotsIndicator.setViewPager(viewPagerMain);

        String closingTime = changeTimeFormat(salonUser.closing_time);
        String openingTime = changeTimeFormat(salonUser.openingTime);
        txtViewTiming.setText(closingTime + " - " + openingTime);

        latitude = salonUser.latitude;
        longitude = salonUser.longitude;
        initialiseMap();

        services = salonUser.services;

        servicesAdapter = new ServicesAdapter(this, services, this);
        recyclerViewServices.setAdapter(servicesAdapter);
        recyclerViewServices.setLayoutManager(new LinearLayoutManager(this));

        ratingBarSalon.setRating(salonUser.rating);

        txtViewDescription.setText(salonUser.description);

        ReviewsAdapter reviewsAdapter = new ReviewsAdapter(this, salonUser.reviews);
        recyclerViewReviews.setAdapter(reviewsAdapter);
        recyclerViewReviews.setLayoutManager(new LinearLayoutManager(this));

        if (salonUser.clientType == 2) {
            featuresLayout.setVisibility(View.VISIBLE);
            if (salonUser.ac == 2) {
                switchAirCondition.setChecked(true);
            } else {
                txtViewAc.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
                txtViewAc.setTextColor(getResources().getColor(R.color.md_grey_400));
            }
            if (salonUser.parking == 2) {
                switchParking.setChecked(true);
            } else {
                txtViewParking.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
                txtViewParking.setTextColor(getResources().getColor(R.color.md_grey_400));
            }
            if (salonUser.wifi == 2) {
                switchWifi.setChecked(true);
            } else {
                txtViewWifi.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
                txtViewWifi.setTextColor(getResources().getColor(R.color.md_grey_400));
            }
            if (salonUser.music == 2) {
                switchMusic.setChecked(true);
            } else {
                txtViewMusic.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
                txtViewMusic.setTextColor(getResources().getColor(R.color.md_grey_400));
            }
        }

    }

    private void initialiseMap() {
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    private void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    public String changeTimeFormat(String time) {
        String inputPattern = "HH:mm:ss";
        String outputPattern = "h:mm a";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(time);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public void loadAddress(BookingAddressAdapter bookingAddressAdapter) {
        recyclerViewShippingAddress.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewShippingAddress.setAdapter(bookingAddressAdapter);
        recyclerViewShippingAddress.addItemDecoration(new DividerItemDecoration(this, LinearLayout.VERTICAL));
    }

    @Override
    public void onBookNowClicked(int addressId) {
        this.addressId = addressId;
        if (bookingTime == null) {
            Toast.makeText(this, "Please Choose A Day", Toast.LENGTH_SHORT).show();
        } else {
            showTimePicker();
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        LatLng latLng = new LatLng(latitude, longitude);
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.title("Current Position");
        markerOptions.draggable(true);
        googleMap.addMarker(markerOptions);
        //move map camera
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        googleMap.animateCamera(CameraUpdateFactory.zoomTo(18));
    }

    @Override
    public void selectService(int id, int serviceFor, int price, String serviceName) {
        bookingServices.add(new BookingServices(id, serviceFor, price, serviceName));
    }

    @Override
    public void removeService(int id, int serviceFor) {
        for (int i = 0; i < bookingServices.size(); i++) {
            if (bookingServices.get(i).serviceId == id && bookingServices.get(i).serviceFor == serviceFor) {
                bookingServices.remove(i);
                break;
            }
        }
    }

    @OnClick(R.id.btn_booking) void onBookingBtnClicked() {
        if (bookingServices.size() == 0) {
            Toasty.warning(this, "Please select at least one service", Toasty.LENGTH_SHORT).show();
        } else {
            price = 0;
            for (int i = 0; i < bookingServices.size(); i++) {
                price = price + bookingServices.get(i).price;
            }
            if (price < 400 && salonUser.clientType == 1) {
                Toasty.warning(this, "Please select services of minimum Rs 400", Toasty.LENGTH_SHORT).show();
            } else {
                if (isLoggedIn()) {
                    AndroidApplication androidApplication = (AndroidApplication) getApplicationContext();
                    UserInfo userInfo = androidApplication.getUserInfo(this);
                    if (userInfo.email == null) {
                        Toasty.warning(this, "Your email id is required to initiate payment !! Please add to your profile").show();
                    } else if (userInfo.mobile == null) {
                        Toasty.warning(this, "Your mobile no is required to initiate payment !! Please add to your profile").show();
                    } else {
                        showAddressBottomSheet();
                    }
                } else {
                    Helper.showLoginBottomSheet(getSupportFragmentManager());
                }
            }
        }
    }

    private void showTimePicker() {
        final Calendar c = Calendar.getInstance();
        int mHour = c.get(Calendar.HOUR_OF_DAY);
        int mMinute = c.get(Calendar.MINUTE);

        // Launch Time Picker Dialog
        final TimePickerDialog timePickerDialog = new TimePickerDialog(this,
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {

                        Calendar calendar = Calendar.getInstance();
                        calendar.set(Calendar.HOUR_OF_DAY,hourOfDay);
                        calendar.set(Calendar.MINUTE,minute);
                        String finalTime = bookingTime + " " + new SimpleDateFormat("HH:mm:ss").format(calendar.getTime());
                        double payableAmount = price * 0.2;
                        Intent intent = new Intent(getApplicationContext(), PaymentActivity.class);
                        Bundle args = new Bundle();
                        intent.putExtra("serviceObject", new Gson().toJson(bookingServices) );
                        intent.putExtra("serviceTime", finalTime);
                        intent.putExtra("addressId", addressId);
                        intent.putExtra("vendorId", vendorId);
                        intent.putExtra("payableAmount", payableAmount);
                        intent.putExtra("BUNDLE",args);
                        startActivity(intent);
                    }
                }, mHour, mMinute, false);
        timePickerDialog.show();
    }

    private void showAddressBottomSheet() {
        addressBottomSheetDialog.show();
    }

    private String getDateAhead(int days) {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DATE, days);
        return new SimpleDateFormat("EEEE").format(c.getTime());
    }

    private String getDateAheadFormat(int days) {
        Calendar c = Calendar.getInstance();
        c.add(Calendar.DATE, days);
        return new SimpleDateFormat("yyyy-MM-dd").format(c.getTime());
    }

    private boolean isLoggedIn() {
        AndroidApplication androidApplication = (AndroidApplication) getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(this);
        if (userInfo != null) {
//            Log.e("LogMsg", "User Id: " + userInfo.userId);
//            Log.e("LogMsg", "Api token: " + userInfo.apiToken);
            return true;
        } else {
            return false;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.fetchShippingAddressList();
    }

    @OnClick(R.id.img_view_back) void onBackClicked() {
        onBackPressed();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }
}